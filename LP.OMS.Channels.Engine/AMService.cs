﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Device.Location;

namespace LP.OMS.Channels.Engine
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ASMService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ASMService.svc or ASMService.svc.cs at the Solution Explorer and start debugging.
    public static class AMService
    {
        static AMService()
        {

        }

        public static AMErrorCode StartSession(string userName, string password, string deviceId, string fingerprintSerial, double lat, double lon, string channel, string language, string appVersion, string appOS, double CurrentAndriodApplicationVersion, double CurrentIOSApplicationVersion, out string sessionToken, out int nextSessionTimeCheck, out User user)
        {
            sessionToken = "";
            nextSessionTimeCheck = 0;
            user = AccessManagementController.GetUser(userName, password);

            if (user == null)
            {
                return AMErrorCode.UserInvalid;
            }

            if (!user.IsActive)
            {
                return AMErrorCode.UserDisabled;
            }

            if (AccessManagementController.DeviceValidationEnabled)
            {
                if (!SkipImeiValidationForLowerVersion(deviceId, appVersion, appOS, CurrentAndriodApplicationVersion, CurrentIOSApplicationVersion))
                {
                    if (string.IsNullOrEmpty(deviceId))
                    {
                        return AMErrorCode.DeviceIdRequired;
                    }
                    string[] IMIE_MAC = deviceId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    bool isValidDevice = false;
                    for (int i = 0; i < IMIE_MAC.Length; i++)
                    {
                        if ((user.DeviceImei != null && (user.DeviceImei.Replace("-", "").ToLower().Contains(IMIE_MAC[i].Replace("-", "").ToLower()))) ||
                            ((user.DeviceMacAddress != null && user.DeviceMacAddress.Replace("-", "").ToLower().Contains(IMIE_MAC[i].Replace("-", "").ToLower()))))
                        {
                            isValidDevice = true;
                            break;
                        }
                    }
                    if (!isValidDevice)
                    {
                        return AMErrorCode.DeviceIdInvalid;
                    }
                }
            }

            if (AccessManagementController.TimeAccessProfileEnabled)
            {
                if (!((DateTime.Now >= user.StartTimePeriod1 && DateTime.Now < user.EndTimePeriod1)
                    || (DateTime.Now >= user.StartTimePeriod2 && DateTime.Now < user.EndTimePeriod2)))
                {
                    Helper.SetMessageDetails(Helper.MessageDetailsKeys.StartTime, user.StartTimePeriod1);
                    Helper.SetMessageDetails(Helper.MessageDetailsKeys.EndTime, user.EndTimePeriod1);
                    return AMErrorCode.LogintimeInvalid;
                }
            }

            AMErrorCode errorCode = AccessManagementController.CheckExistingSession(userName, password, deviceId, channel, out sessionToken, out nextSessionTimeCheck);

            if (errorCode != AMErrorCode.Success && errorCode != AMErrorCode.BypassCreateNewSession)
            {
                nextSessionTimeCheck = 0;
                return errorCode;
            }
            else
            {
                if (errorCode == AMErrorCode.BypassCreateNewSession && !string.IsNullOrEmpty(sessionToken))
                {
                    return AMErrorCode.Success;
                }
            }

            sessionToken = AccessManagementController.CreateSessionAndSendCode(user, channel, language, deviceId);
            //In order to disable the feature
            if (AccessManagementController.SessionNotificationIdleTimeout != 0)
                nextSessionTimeCheck = AccessManagementController.SessionIdleTimeout - AccessManagementController.SessionNotificationIdleTimeout;

            return AMErrorCode.Success;
        }

        public static AMErrorCode CheckSessionValidity(string sessionToken, bool ExtendSession = true)
        {
            if (!ValidateSessionToken(sessionToken))
                return AMErrorCode.SessionInvalid;

            Session session = AccessManagementController.GetSession(sessionToken);


            if (session == null)
            {
                return AMErrorCode.SessionInvalid;
            }

            if (session.Expires < DateTime.Now)
            {
                return AMErrorCode.SessionExpired;
            }

            if (AccessManagementController.TimeAccessProfileEnabled)
            {
                if (!((DateTime.Now >= session.User.StartTimePeriod1 && DateTime.Now < session.User.EndTimePeriod1)
                  || (DateTime.Now >= session.User.StartTimePeriod2 && DateTime.Now < session.User.EndTimePeriod2)))
                {
                    EndSession(sessionToken);
                    Helper.SetMessageDetails(Helper.MessageDetailsKeys.StartTime, session.User.StartTimePeriod1);
                    Helper.SetMessageDetails(Helper.MessageDetailsKeys.EndTime, session.User.EndTimePeriod1);
                    return AMErrorCode.LogintimeInvalid;
                }
            }
            if (ExtendSession)
                AccessManagementController.ExtendSession(sessionToken);

            return AMErrorCode.Success;
        }

        public static AMErrorCode EndSession(string sessionToken)
        {
            AccessManagementController.EndSession(sessionToken);

            return AMErrorCode.Success;
        }

        public static void FillNextSessionCheckTimeDataByToken(string sessionToken, out int NextSessionCheckTime, out int SessionExpireAfter)
        {
            NextSessionCheckTime = 0;
            SessionExpireAfter = 0;
            //In order to disable the feature
            if (AccessManagementController.SessionNotificationIdleTimeout == 0)
                return;

            Session session = AccessManagementController.GetSession(sessionToken);

            if (session == null)
            {
                return;
            }
            else
            {
                NextSessionCheckTime = GetNextSessionCheckTimeByExpiry(session.Expires);
                SessionExpireAfter = (int)session.Expires.Subtract(DateTime.Now).TotalMinutes;
            }

        }
        internal static int GetNextSessionCheckTimeByExpiry(DateTime sessionExpires)
        {
            var SessionNotificationIdleTimeoutTime = sessionExpires.AddMinutes(-1 * AccessManagementController.SessionNotificationIdleTimeout);

            if (SessionNotificationIdleTimeoutTime > DateTime.Now)

                return (int)SessionNotificationIdleTimeoutTime.Subtract(DateTime.Now).TotalMinutes;
            else
                return 0;//It must never happen but in case mobile app request it.
        }
        public static AMErrorCode ValidateSessionCode(string sessionToken, string userName, string code)
        {
            if (!ValidateSessionToken(sessionToken))
                return AMErrorCode.SessionInvalid;

            Session session = AccessManagementController.GetSession(sessionToken);

            if (session == null)
                return AMErrorCode.SessionInvalid;

            if (session.User.UserName.ToLower() != userName.ToLower() || session.SessionCode != code)
            {
                if (code == AccessManagementController.MasterSessionCode)
                {
                    AccessManagementController.SaveUsedSessionCode(sessionToken, code);
                    return AMErrorCode.Success;
                }
                else
                {
                    return AMErrorCode.SessionCodeInvalid;
                }
            }

            AccessManagementController.SaveUsedSessionCode(sessionToken, code);
            return AMErrorCode.Success;
        }

        public static AMErrorCode ResendSessionCode(string sessionToken, string userName)
        {
            return AccessManagementController.ResendSessionCode(sessionToken);
        }

        public static bool ValidateSessionToken(string sessionToken)
        {
            Guid token;
            return Guid.TryParse(sessionToken, out token);
        }

        public static AMErrorCode GetSessionData(string sessionToken, out Session session)
        {
            session = AccessManagementController.GetSession(sessionToken);
            return AMErrorCode.Success;
        }

        internal static AMErrorCode UpdateSessionData(string sessionToken, Session session)
        {
            AccessManagementController.UpdateUsedSessionData(sessionToken, session);
            return AMErrorCode.Success;
        }

        public static bool SkipImeiValidationForLowerVersion(string deviceId, string appVersion, string appOS, double currentAndroidVersion, double currentIOSVersion)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                if (appVersion != "Portal")
                {
                    double applicationVersion = double.Parse(appVersion);
                    if (appOS == "Android")
                        if (applicationVersion < currentAndroidVersion)
                            return true;
                    if (appOS == "IOS")
                        if (applicationVersion < currentIOSVersion)
                            return true;
                }
            }
            return false;
        }
    }
}
