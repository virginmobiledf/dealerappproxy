//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.Engine
{
    using System;
    using System.Collections.Generic;
    
    public partial class MessageConfiguration
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string MessageType { get; set; }
        public string TitleTemplate { get; set; }
        public string BodyTemplate { get; set; }
        public string TitleTemplateAr { get; set; }
        public string BodyTemplateAr { get; set; }
    }
}
