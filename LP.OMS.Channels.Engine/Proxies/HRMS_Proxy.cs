﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine.Utilities;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace LP.OMS.Channels.Engine.Proxies
{
    public class HRMS_Proxy
    {
        private static string baseAddress = ConfigurationManager.AppSettings["HRMS_BaseAddress"];
        private static string username = ConfigurationManager.AppSettings["HRMS_Username"];
        private static string password = ConfigurationManager.AppSettings["HRMS_Password"];
        private static string logHRMS = ConfigurationManager.AppSettings["HRMS_Enable_Request_Response_Logging"];
        private static string disableHRMSIntegration = ConfigurationManager.AppSettings["Disable_HRMS_Integration"];

        public static AddAttendanceResponse AddAttendanceEntry(AddAttendanceRequest request)
        {
            if (!string.IsNullOrEmpty(disableHRMSIntegration))
                if (bool.Parse(disableHRMSIntegration))
                    return new AddAttendanceResponse() { Status = -1, Message = "HRMS integration disabled" };

            AddAttendanceResponse addAttendanceResponse = new AddAttendanceResponse();
            string json = string.Empty;

            try
            {
                HttpClient client = new HttpClient()
                {
                    BaseAddress = new Uri(baseAddress)
                };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var authToken = Encoding.ASCII.GetBytes($"{username}:{password}");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(authToken));

                request.AttendanceDetails.ClientKey = ConfigurationManager.AppSettings["HRMS_ClientKey"];
                if(string.IsNullOrEmpty(request.AttendanceDetails.EntryTime))
                    request.AttendanceDetails.EntryTime = DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss");
                request.AttendanceDetails.ReaderType = "1";
               
                json = JsonConvert.SerializeObject(request);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("api/AddAttendanceEntry", content).Result;
                string result = response.Content.ReadAsStringAsync().Result;
                addAttendanceResponse = JsonConvert.DeserializeObject<AddAttendanceResponse>(result);
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AddAttendanceEntry",
                    request.AttendanceDetails.UserCode, ex.ToString());
            }

            if(!string.IsNullOrEmpty(logHRMS))
            {
                if(bool.Parse(logHRMS))
                {
                    string jsonResponse = JsonConvert.SerializeObject(addAttendanceResponse);
                    string description = addAttendanceResponse?.Message?.ToLower() == "success"
                           ? "AddAttendanceEntry successed" : "AddAttendanceEntry failed";
                    ErrorHandling.AddToErrorLog(ErrorTypes.Information, description,
                        "AddAttendanceEntry", request.AttendanceDetails.UserCode,
                        $"Request: {json} Response: {jsonResponse}");
                }
            }

            return addAttendanceResponse;
        }
    }
}
