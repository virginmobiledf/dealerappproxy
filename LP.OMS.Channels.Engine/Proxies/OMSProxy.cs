﻿
using LP.OMS.Channels.Engine.AdministrationServices;
using LP.OMS.Channels.Engine.OMSCommonServices;

namespace LP.OMS.Channels.Engine
{
    public class OMSProxy
    {
        public static OMSCommonServices.Requester GetRequesterByName(string requesterName)
        {
            OMSCommonServices.Requester requester = null;
            using (CommonServicesClient client = new CommonServicesClient())
            {
                using (UserContextScope userScope = new UserContextScope(client.InnerChannel, ""))
                {
                    requester = client.GetRequesterByName(requesterName);
                }
            }

            return requester;
        }

        public static ExtendedService.Requester GetRequesterByNameWithoutCache(string requesterName)
        {
            ExtendedService.Requester requester = null;
            using (ExtendedService.ExtendedServiceClient client = new ExtendedService.ExtendedServiceClient())
            {
                using (UserContextScope userScope = new UserContextScope(client.InnerChannel, ""))
                {
                    requester = client.GetRequesterByNameWithoutCache(requesterName);
                }
            }

            return requester;
        }

        public static LP.OMS.Channels.Engine.AdministrationServices.Requester GetRequesterID(int requesterID)
        {
            LP.OMS.Channels.Engine.AdministrationServices.Requester dealer;
            using (AdministrationServicesClient client = new AdministrationServicesClient("WSHttpBinding_IAdministrationServices"))
            {
                dealer = client.GetRequester(requesterID, AdministrationServices.OMSLanguages.Local);
            }
            return dealer;
        }

        public static string GetDealerContactMSISDN(string dealerCode)
        {
            return GetRequesterID(GetRequesterByName(dealerCode).ID).ContactMSISDN;
        }
    }
}


