﻿using LP.Core.Utilities.Exceptions;
using LP.OMS.Channels.Engine.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

namespace LP.OMS.Channels.Engine
{
    public static class LocationController
    {

        public static string VirginGeoLocationBaseAddress;

        static LocationController()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["VirginGeoLocationBaseAddress"]))
                VirginGeoLocationBaseAddress = ConfigurationManager.AppSettings["VirginGeoLocationBaseAddress"];
        }

   

        //
        // Summary:
        //     AddLocation From Geo Location API.
        public static Contracts.AddLocationResponse AddLocation(Contracts.AddLocationRequest request)
        {
            Contracts.AddLocationResponse response = new Contracts.AddLocationResponse();
       
            try
            {
                Contracts.CallResponse oCallResponse = new Contracts.CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = Controller.CheckRequester(request.Login, out oRequester, out userName, false);

                if (oCallResponse.IsPassed)
                {
                    string API_URL = "Geo/AddLocation?"+
                    "&location_name_ar={0}" +
                    "&location_name_en={1}" +
                    "&dealercode={2}" +
                    "&location_type={3}" +
                    "&friendi_sales={4}" +
                    "&virgin_sales={5}" +
                    "&Latitude={6}" +
                    "&Longitude={7}" +
                    "&stdev_km={8}";

                    string API_URL_DATA = string.Format(API_URL,
                             request.LocationNameAr, request.LocationNameEn, request.DealerCode,
                            request.LocationType, request.FriendiSale, request.VirginSale, request.Latitude,
                            request.Longitude, request.stdev_km);

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(VirginGeoLocationBaseAddress);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpRequestMessage HttpRequestMessage = new HttpRequestMessage(HttpMethod.Get, API_URL_DATA);

                        var httpResponse  = client.GetAsync(API_URL_DATA).Result;

                        if (httpResponse.IsSuccessStatusCode)
                        {
                            var httpResponseContent = httpResponse.Content.ReadAsStringAsync().Result;
                            dynamic result  = JsonConvert.DeserializeObject<dynamic>(httpResponseContent);

                            if(result != null)
                            {
                                if(result.Code == 0)
                                {
                                    response.Message = "Success";
                                    response.Status = true;
                                    response.LocationID = result.Id;
                                }
                                else
                                {
                                    response.Message = result.Message;
                                    response.Status = false;
                                } 
                            }
                        }
                    }
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.Status = false;
                    response.Message = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.Login.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AddLocation", ex, request.Login.Username);
                response.Status = false;
                throw;
            }

            return response;
        }


        //
        // Summary:
        //     GetLocationByID.
        public static Contracts.DHLocation GetLocationByID(string locationID, int language, string dealerCode)
        {
            Contracts.DHLocation location = new Contracts.DHLocation();

            try
            {
                string API_URL = "Geo/GetLocationById?location_id={0}";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(VirginGeoLocationBaseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage ApiResponse = client.GetAsync(string.Format(API_URL, locationID)).Result;

                    if (ApiResponse.IsSuccessStatusCode)
                    {
                        var apiResponseContent = ApiResponse.Content.ReadAsStringAsync().Result;
                        dynamic objResult = JsonConvert.DeserializeObject<dynamic>(apiResponseContent);

                        if (objResult != null)
                        {
                            location.FriendiSale = objResult.FriendiSale == "Yes" ? true : false;
                            location.VirginSale = objResult.VirginSale == "Yes" ? true : false;
                            location.Latitude = objResult.Latitude;
                            location.Longitude = objResult.Longitude;
                            location.LocationName = objResult.LocationName_en;
                            location.LocationId = objResult.Location_id;
                            location.CityId = objResult.CityId;
                            location.RegionId = objResult.regionId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "GetLocationByID location ID = " + locationID, dealerCode);
            }

            return location;
        }
        private static double GetNextLocationID()
        {
            double nextLocationId = 0;

            string API_URL = "Geo/GetNextLocationId";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(VirginGeoLocationBaseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpRequestMessage HttpRequestMessage = new HttpRequestMessage(HttpMethod.Get, API_URL);

                HttpResponseMessage httpResponse = client.GetAsync(API_URL).Result;
                var result = httpResponse.Content.ReadAsStringAsync().Result;
                if (httpResponse.IsSuccessStatusCode)
                {
                    if (!string.IsNullOrEmpty(result))
                    {
                        nextLocationId = Convert.ToDouble(result);
                    }
                }
            }

            return nextLocationId;
        }
    }
}
