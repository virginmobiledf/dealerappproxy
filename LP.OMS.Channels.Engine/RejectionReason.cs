//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.Engine
{
    using System;
    using System.Collections.Generic;
    
    public partial class RejectionReason
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ForginDescription { get; set; }
        public int DocumentTypeId { get; set; }
    
        public virtual DocumentType DocumentType { get; set; }
    }
}
