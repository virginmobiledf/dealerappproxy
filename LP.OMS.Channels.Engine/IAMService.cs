﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LP.OMS.Channels.Engine
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IASMService" in both code and config file together.
    [ServiceContract]
    public interface IAMService
    {
        [OperationContract]
        AMErrorCode StartSession(string userName, string password, string deviceId, string fingerprintSerial, double lat, double lon, out string sessionToken);

        [OperationContract]
        AMErrorCode ExtendSession(string sessionToken);

        [OperationContract]
        AMErrorCode EndSession(string sessionToken);

        [OperationContract]
        AMErrorCode ValidateSessionTransaction(string sessionToken, string userName, string deviceId, string fingerprintSerial, double lat, double lon);

        [OperationContract]
        AMErrorCode ValidateSessionCode(string sessionToken, string userName, string code);

        [OperationContract]
        AMErrorCode ResendSessionCode(string sessionToken, string userName);
    }

    [DataContract]
    public enum AMErrorCode
    {
        [EnumMember]
        Success = 0,

        [EnumMember]
        UserInvalid = 1,

        [EnumMember]
        UserLocked = 2,

        [EnumMember]
        UserDisabled = 3,

        [EnumMember]
        DeviceIdRequired = 4,

        [EnumMember]
        FingerprintDeviceIdRequired = 5,

        [EnumMember]
        LocationRequired = 6,

        [EnumMember]
        DeviceIdInvalid = 7,

        [EnumMember]
        FingerprintDeviceIdInvalid = 8,

        [EnumMember]
        LocationInvalid = 9,

        [EnumMember]
        UserAlreadyLoggedIn = 10,

        [EnumMember]
        SessionInvalid = 11,

        [EnumMember]
        SessionExpired = 12,

        [EnumMember]
        LogintimeInvalid = 13,

        [EnumMember]
        SessionCodeInvalid = 14,

        [EnumMember]
        BypassCreateNewSession = 15
    }
}
