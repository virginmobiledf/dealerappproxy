﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.Engine
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class OMSCoreEntities : DbContext
    {
        public OMSCoreEntities()
            : base("name=OMSCoreEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Action> Actions { get; set; }
        public DbSet<Channel> Channels { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<RejectionReason> RejectionReasons { get; set; }
        public DbSet<Requester> Requesters { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<WFActivity> WFActivities { get; set; }
        public DbSet<WFRequest> WFRequests { get; set; }
        public DbSet<Bundle> Bundles { get; set; }
        public DbSet<BundleRequester> BundleRequesters { get; set; }
        public DbSet<StockItem> StockItems { get; set; }
        public DbSet<StockRequest> StockRequests { get; set; }
        public DbSet<Domination> Dominations { get; set; }
        public DbSet<RequesterRight> RequesterRights { get; set; }
        public DbSet<DBRight> DBRights { get; set; }
    
        public virtual int GetDealerInteractions(string requesterId, Nullable<int> statusID, Nullable<int> documentTypeId, string channel, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> startIndex, Nullable<int> pageSize, string sortExpr, string sortDir)
        {
            var requesterIdParameter = requesterId != null ?
                new ObjectParameter("RequesterId", requesterId) :
                new ObjectParameter("RequesterId", typeof(string));
    
            var statusIDParameter = statusID.HasValue ?
                new ObjectParameter("StatusID", statusID) :
                new ObjectParameter("StatusID", typeof(int));
    
            var documentTypeIdParameter = documentTypeId.HasValue ?
                new ObjectParameter("DocumentTypeId", documentTypeId) :
                new ObjectParameter("DocumentTypeId", typeof(int));
    
            var channelParameter = channel != null ?
                new ObjectParameter("Channel", channel) :
                new ObjectParameter("Channel", typeof(string));
    
            var dateFromParameter = dateFrom.HasValue ?
                new ObjectParameter("DateFrom", dateFrom) :
                new ObjectParameter("DateFrom", typeof(System.DateTime));
    
            var dateToParameter = dateTo.HasValue ?
                new ObjectParameter("DateTo", dateTo) :
                new ObjectParameter("DateTo", typeof(System.DateTime));
    
            var startIndexParameter = startIndex.HasValue ?
                new ObjectParameter("StartIndex", startIndex) :
                new ObjectParameter("StartIndex", typeof(int));
    
            var pageSizeParameter = pageSize.HasValue ?
                new ObjectParameter("PageSize", pageSize) :
                new ObjectParameter("PageSize", typeof(int));
    
            var sortExprParameter = sortExpr != null ?
                new ObjectParameter("SortExpr", sortExpr) :
                new ObjectParameter("SortExpr", typeof(string));
    
            var sortDirParameter = sortDir != null ?
                new ObjectParameter("SortDir", sortDir) :
                new ObjectParameter("SortDir", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetDealerInteractions", requesterIdParameter, statusIDParameter, documentTypeIdParameter, channelParameter, dateFromParameter, dateToParameter, startIndexParameter, pageSizeParameter, sortExprParameter, sortDirParameter);
        }
    
        public virtual ObjectResult<GetDealerInteractions__Result> GetDealerInteractions_(string requesterId, Nullable<int> statusID, Nullable<int> documentTypeId, string channel, Nullable<System.DateTime> dateFrom, Nullable<System.DateTime> dateTo, Nullable<int> startIndex, Nullable<int> pageSize, string sortExpr, string sortDir)
        {
            var requesterIdParameter = requesterId != null ?
                new ObjectParameter("RequesterId", requesterId) :
                new ObjectParameter("RequesterId", typeof(string));
    
            var statusIDParameter = statusID.HasValue ?
                new ObjectParameter("StatusID", statusID) :
                new ObjectParameter("StatusID", typeof(int));
    
            var documentTypeIdParameter = documentTypeId.HasValue ?
                new ObjectParameter("DocumentTypeId", documentTypeId) :
                new ObjectParameter("DocumentTypeId", typeof(int));
    
            var channelParameter = channel != null ?
                new ObjectParameter("Channel", channel) :
                new ObjectParameter("Channel", typeof(string));
    
            var dateFromParameter = dateFrom.HasValue ?
                new ObjectParameter("DateFrom", dateFrom) :
                new ObjectParameter("DateFrom", typeof(System.DateTime));
    
            var dateToParameter = dateTo.HasValue ?
                new ObjectParameter("DateTo", dateTo) :
                new ObjectParameter("DateTo", typeof(System.DateTime));
    
            var startIndexParameter = startIndex.HasValue ?
                new ObjectParameter("StartIndex", startIndex) :
                new ObjectParameter("StartIndex", typeof(int));
    
            var pageSizeParameter = pageSize.HasValue ?
                new ObjectParameter("PageSize", pageSize) :
                new ObjectParameter("PageSize", typeof(int));
    
            var sortExprParameter = sortExpr != null ?
                new ObjectParameter("SortExpr", sortExpr) :
                new ObjectParameter("SortExpr", typeof(string));
    
            var sortDirParameter = sortDir != null ?
                new ObjectParameter("SortDir", sortDir) :
                new ObjectParameter("SortDir", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetDealerInteractions__Result>("GetDealerInteractions_", requesterIdParameter, statusIDParameter, documentTypeIdParameter, channelParameter, dateFromParameter, dateToParameter, startIndexParameter, pageSizeParameter, sortExprParameter, sortDirParameter);
        }
    }
}
