﻿using LP.Core.Utilities.Exceptions;
using LP.Core.Utilities.Logging;
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine.SCExternalApisService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace LP.OMS.Channels.Engine
{
    public enum SCFlowStatus
    {
        [EnumMember]
        StatusNotSet = -1,
        [EnumMember]
        NoNumberSelected = 1,
        [EnumMember]
        NoPlanSelected = 2,
        [EnumMember]
        EmailNotVerified = 3,
        [EnumMember]
        PasswordNotSet = 4,
        [EnumMember]
        NoCCRegistered = 5,
        [EnumMember]
        IDDetailsNotUploaded = 6,
        [EnumMember]
        SIMNotDelivered = 7,
        [EnumMember]
        DeliveryInProgress = 8,
        [EnumMember]
        DeliveryArrived = 9,
        [EnumMember]
        IDVerified = 10,
        [EnumMember]
        SIMActivated = 11,
        [EnumMember]
        SIMTerminated = 12,
        [EnumMember]
        CustomerBlacklisted = 13,
        [EnumMember]
        VerificationSuspicious = 14,
        [EnumMember]
        AccountSuspended = 15,
        [EnumMember]
        IsMnp = 16,
        [EnumMember]
        IsVip = 17,
        [EnumMember]
        IsLoggedIn = 18,
        [EnumMember]
        HasSocialPlan = 19,
        [EnumMember]
        NoProductSelected = 20
    }

    public static class SCWrapper
    {
        private static string _userName = ConfigurationManager.AppSettings["LP.Integration.CAS.SCWrapper.UserName"];
        private static string _password = ConfigurationManager.AppSettings["LP.Integration.CAS.SCWrapper.Password"];

        public static SCExternalApisService.SavedPlanResponse GetSavedPlan(string msisdn, bool includePAYG, bool throwForFailure = true)
        {
            SCExternalApisService.SavedPlanResponse response = new SCExternalApisService.SavedPlanResponse();
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.GetSavesdPlan(msisdn);
                    if (!includePAYG)
                    {
                        if (response.Plan != null && !string.IsNullOrWhiteSpace(response.Plan.Ids))
                        {
                            var plansList = response.Plan.Ids.Split(',');

                            response.Plan.Ids = string.Join(",", plansList.Where(x => x != "-1" && x != "0").ToArray());
                        }
                    }

                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => msisdn, () => includePAYG);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static SCExternalApisService.CustomerDetailsResponse GetCustomerDetails(string email, bool throwForFailure = true)
        {
            SCExternalApisService.CustomerDetailsResponse response = null;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.GetCustomerDetails(email);

                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static bool IsExistingCustomer(string email, bool throwForFailure = true)
        {
            bool response = false;
            SCExternalApisService.booleanResult result = new SCExternalApisService.booleanResult();//TODO check  the boolean  result
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    result = client.CheckExistingEmail(email);
                    if (result != null)
                    {
                        response = result.Response.IsSuccess;
                    }
                    else
                    {
                        response = false;
                    }



                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static BookedNumberResponseV2 GetUserBookedNumbers(string email, bool isFamilyOnboarding, bool throwForFailure = true)
        {
            BookedNumberResponseV2 bookedNumbers = new BookedNumberResponseV2();
            try
            {
                using (SCExternalApisClient client = new SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    var userBookedNumbers = client.GetUserBookedNumbersV2(email);
                    IEnumerable<BookedNumberV2> numbers = null;
                    if (userBookedNumbers != null)
                    {
                        numbers = userBookedNumbers.ListResult.Where(x => x.StatusID == ReservationType.commit);

                        if ((numbers == null || numbers.Count() == 0) && !isFamilyOnboarding) 
                        {
                            // In family onboarding we just get commited numbers
                            var initialNumbers = userBookedNumbers.ListResult.Where(x => x.StatusID == ReservationType.initial);
                            bookedNumbers.ListResult = initialNumbers.OrderBy(p => p.ReservationDate).ToList(); // take the very first reservation becuase any number after would be wrong senario
                        }
                        else
                        {
                            bookedNumbers.ListResult = numbers.OrderBy(p => p.ReservationDate).ToList();// take the very first reservation becuase any number after would be wrong senario
                        }
                    }

                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                //if (throwForFailure)
                //    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return bookedNumbers;
        }

        public static booleanResult TerminateNumber(string MSISDN, bool throwForFailure = true)
        {
            booleanResult response = null;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.Terminatenumber(MSISDN);
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                //if (throwForFailure)
                //    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => MSISDN);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        //public static void RedeemNumberAsync(string MSISDN)
        //{
        //    Task.Run((Func<Task>)(async () =>
        //    {
        //        try
        //        {
        //            using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
        //            {
        //                client.ClientCredentials.UserName.UserName = _userName;
        //                client.ClientCredentials.UserName.Password = _password;

        //                await client.RedeemnumberAsync(MSISDN);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //ExceptionManager.Add(ex);
        //            //AlarmNotifier.Initialize((IntegrationChannel)IntegrationChannel.SC).RiseAlarmAsync((Exception)ex, (AlarmNotificationPriority)AlarmNotificationPriority.PRTG, (AlarmNotifierType)AlarmNotifierType.Email, (System.Linq.Expressions.Expression<Func<object>>)(() => (object)MSISDN));
        //        }
        //    }));
        //}
        //public static string RedeemNumber(string MSISDN, bool throwForFailure = true)
        //{
        //    string responseMSG = string.Empty;
        //    SCExternalApisService.booleanResult response = new booleanResult();
        //    try
        //    {
        //        using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
        //        {
        //            client.ClientCredentials.UserName.UserName = _userName;
        //            client.ClientCredentials.UserName.Password = _password;

        //            response = client.Redeemnumber(MSISDN);
        //            responseMSG = response.Response.ErrorMessage;
        //        }
        //    }
        //    catch (LPBizException ex)
        //    {
        //        responseMSG = ex.Message;
        //        //ExceptionManager.Add(ex);
        //        if (throwForFailure)
        //            throw ex;
        //    }

        //    catch (Exception ex)
        //    {
        //        responseMSG = ex.Message;
        //        //ExceptionManager.Add(ex);
        //        //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => MSISDN);

        //        if (throwForFailure)
        //        {
        //            //throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
        //        }
        //    }
        //    return responseMSG;

        //}

        public static string UpdateProduct(string email, string productCode, bool throwForFailure = true)
        {
            string responseMSG = string.Empty;
            SCExternalApisService.booleanResult response = new SCExternalApisService.booleanResult();
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.UpdateProduct(productCode, email);
                    //responseMSG = response.Response.ErrorMessage;//TODO
                }
            }
            catch (LPBizException ex)
            {
                //responseMSG = ex.Message;
                //ExceptionManager.Add(ex);
                //if (throwForFailure)
                //    throw ex;
            }

            catch (Exception ex)
            {
                //responseMSG = ex.Message;
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email, () => productCode);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return responseMSG;

        }
        public static string VVIPSignup(string email, string msisdn, bool throwForFailure = true)
        {
            string responseMSG = string.Empty;
            SCExternalApisService.booleanResult response = new SCExternalApisService.booleanResult();
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.VVIPSignUp(msisdn, email);
                    //responseMSG = response.Response.ErrorMessage;//TODO
                    //if (!response.Response.IsSuccess)
                    //{
                    //    throw new LPBizException(ResponseCode.SC_Error.Code(), response.Response.ErrorMessage);

                    //}
                }
            }
            catch (LPBizException ex)
            {
                //responseMSG = ex.Message;
                //ExceptionManager.Add(ex);
                //if (throwForFailure)
                //    throw ex;
            }

            catch (Exception ex)
            {
                //responseMSG = ex.Message;
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email, () => msisdn);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return responseMSG;

        }
        public static SCExternalApisService.ReserveNumberResponse ReserveNumber(string email, string msisdn, int vanityLevel, bool throwForFailure = true)
        {
            SCExternalApisService.ReserveNumberResponse numberResponse = new SCExternalApisService.ReserveNumberResponse();
            //string responseMSG = string.Empty;

            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    numberResponse = client.ReserveNumber(email, msisdn, vanityLevel);
                    //responseMSG = response.Response.ErrorMessage;
                }
            }
            catch (LPBizException ex)
            {
                //responseMSG = ex.Message;
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //responseMSG = ex.Message;
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email, () => msisdn, () => vanityLevel);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return numberResponse;

        }
        public static bool IsAccountInRecoveryMode(string email, bool throwForFailure = true)
        {
            bool responseMSG = false;
            SCExternalApisService.booleanResult response = new booleanResult();
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.IsAccountRecovery(email);
                }
            }
            catch (LPBizException ex)
            {

                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                if (throwForFailure)
                {
                    //throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                }
            }
            return responseMSG;

        }
        public static string GetRandomNumbers(string email, bool throwForFailure = true)
        {
            string response = string.Empty;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    var randomNumber = client.GetRandomNumber(email);
                    if (randomNumber.Response.IsSuccess)
                    {
                        response = randomNumber.RandomNumber;
                    }
                    else
                    {
                        response = string.Empty;
                    }

                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static string Register(string email, bool throwForFailure = true)
        {
            string response = string.Empty;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    var result = client.Register(email, string.Empty);//register with the default product.
                    if (result.Response.IsSuccess)
                        response = result.CustomerId;
                    else
                        response = string.Empty;
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static GetSavedRequestMNPResponse GetMNPSavedRequest(string email, bool throwForFailure = true)
        {
            GetSavedRequestMNPResponse response = null;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    client.Open();
                    response = client.GetSavedRequestMNP(email);
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static bool UpdateSavedMNPStatus(string email, int status, bool throwForFailure = true)
        {
            bool response = false;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    response = client.UpdateSavedMNP(email, status).Response.IsSuccess;
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email, () => status);

                if (throwForFailure)
                {
                    //throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                }
            }
            return response;
        }
        //public static void SendVerificationCodeAsync(string email, bool throwForFailure = true)
        //{
        //    Task.Run(() =>
        //    {

        //        bool response = false;
        //        try
        //        {
        //            using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
        //            {
        //                client.ClientCredentials.UserName.UserName = _userName;
        //                client.ClientCredentials.UserName.Password = _password;
        //                response = client.SendVerificationCode(email).Response.IsSuccess;
        //            }
        //        }
        //        catch (LPBizException ex)
        //        {
        //            //ExceptionManager.Add(ex);
        //            //if (throwForFailure)
        //            //    throw ex;
        //        }

        //        catch (Exception ex)
        //        {
        //            //ExceptionManager.Add(ex);
        //            //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

        //            //if (throwForFailure)
        //            //{
        //            //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
        //            //}
        //        }
        //    });
        //}
        public static GetActiveCreditCardIDsByMSISDNsResponse GetActiveCreditCardIDsByMSISDNs(List<string> MSISDNs, bool throwForFailure = true)
        {
            GetActiveCreditCardIDsByMSISDNsResponse response = null;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    response = client.GetActiveCreditCardIDsByMSISDNs(MSISDNs.ToList());
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => MSISDNs);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static bool NotifySIMActivated(string msisdn, string imsi, string dealerCode, bool throwForFailure = true)
        {
            bool response = false;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    response = client.SIMActivated(msisdn, imsi, dealerCode).Response.IsSuccess;
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => msisdn, () => imsi, () => dealerCode);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        //public static void NotifySIMActivatedAsync(string msisdn, string imsi, string dealerCode)
        //{
        //    Task.Run((Func<Task>)(async () =>
        //    {
        //        try
        //        {
        //            using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
        //            {
        //                client.ClientCredentials.UserName.UserName = _userName;
        //                client.ClientCredentials.UserName.Password = _password;

        //                var response = await client.SIMActivatedAsync(msisdn, imsi, dealerCode);
        //                if (!response.Response.IsSuccess)
        //                {
        //                    //TODO
        //                    //throw new LPBizException(ResponseCode.SC_Error.Code(), string.Format(ResponseCode.SC_Error.Message(), response.Response.Code, response.Response.ErrorMessage, "SIMActivated"));
        //                }
        //            }
        //        }
        //        catch (LPBizException ex)
        //        {
        //            //ExceptionManager.Add(ex);
        //        }
        //        catch (Exception ex)
        //        {
        //            //ExceptionManager.Add(ex);
        //            //AlarmNotifier.Initialize((IntegrationChannel)IntegrationChannel.SC).RiseAlarmAsync((Exception)ex, (AlarmNotificationPriority)AlarmNotificationPriority.PRTG, (AlarmNotifierType)AlarmNotifierType.Email, (System.Linq.Expressions.Expression<Func<object>>)(() => (object)msisdn), (System.Linq.Expressions.Expression<Func<object>>)(() => (object)imsi), (System.Linq.Expressions.Expression<Func<object>>)(() => (object)dealerCode));
        //        }
        //    }));
        //}
        public static bool ExtendNumberReservation(string email, bool throwForFailure = true)
        {
            bool response = false;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    response = client.ExtendInitialNumberToChange(email).Response.IsSuccess;
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }

        public static ResponseResult AddHomeScreenNotification(int notificationId, string email, bool throwForFailure = true)
        {
            ResponseResult response = null;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.AddHomeScreenNotification(notificationId, email, DateTime.Now, null).Response;
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => email, () => notificationId);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }
        public static GetEmailByMsisdinResponse GetEmailByMsisdin(string msisdn, out string email, bool throwForFailure = true)
        {
            GetEmailByMsisdinResponse response = null;
            email = string.Empty;
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    response = client.GetEmailByMsisdin(msisdn);
                    email = response.email;
                }
            }
            catch (LPBizException ex)
            {
                //ExceptionManager.Add(ex);
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                //ExceptionManager.Add(ex);
                //AlarmNotifier.Initialize(IntegrationChannel.SC).RiseAlarmAsync(ex, AlarmNotificationPriority.PRTG, AlarmNotifierType.Email, () => msisdn);

                //if (throwForFailure)
                //{
                //    throw new LPBizException(ResponseCode.General_Error.Code(), ResponseCode.General_Error.Message());
                //}
            }
            return response;
        }

        public static ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request, bool throwForFailure = true)
        {
            ValidateAndRegisterFamilyResponse response = new ValidateAndRegisterFamilyResponse();
            try
            {
                using (SCExternalApisService.SCExternalApisClient client = new SCExternalApisService.SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;
                    ValidateAndRegisterFMRequest validateAndRegisterFMRequest = new ValidateAndRegisterFMRequest() {
                        AccountType = request.AccountType,
                        AllowanceAmount = request.AllowanceAmount,
                        IsMonthlyAllowance = request.IsMonthlyAllowance,
                        MainAccountEmail = request.MainAccountEmail,
                        SubAccountEmail = request.SubAccountEmail,
                        TagName = request.TagName
                    };

                    var result = client.ValidateAndRegisterFamily(validateAndRegisterFMRequest);
                    if(result.Response.IsSuccess)
                    {
                        response.ResponseMessage = result.Response.SuccessMessage;
                    }
                    else
                    {
                        response.ResponseMessage = result.Response.ErrorMessage;
                    }
                    
                    response.IsPassed = result.Response.IsSuccess;
                    return response;
                }
            }
            catch (LPBizException ex)
            {
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {
                
            }
            return response;
        }

        public static CancelFamilyRegistrationResponse CancelFamilyRegistration(string MSISDN, bool throwForFailure = true)
        {
            CancelFamilyRegistrationResponse response = new CancelFamilyRegistrationResponse();
            try
            {
                using (SCExternalApisClient client = new SCExternalApisClient())
                {
                    client.ClientCredentials.UserName.UserName = _userName;
                    client.ClientCredentials.UserName.Password = _password;

                    var result = client.CancelDelarRegistertion(MSISDN);
                    if (result.Response.IsSuccess)
                    {
                        response.ResponseMessage = result.Response.SuccessMessage;
                    }
                    else
                    {
                        response.ResponseMessage = result.Response.ErrorMessage;
                    }

                    response.IsPassed = result.Response.IsSuccess;
                    return response;
                }
            }
            catch (LPBizException ex)
            {
                if (throwForFailure)
                    throw ex;
            }

            catch (Exception ex)
            {

            }
            return response;
        }
    }
}


