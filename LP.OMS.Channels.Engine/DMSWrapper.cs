﻿using LP.Core.Utilities.Exceptions;
using LP.OMS.Channels.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using LP.OMS.Channels.Engine.Utilities;
using System.Threading;
using System.Threading.Tasks;

namespace LP.OMS.Channels.Engine
{
    public static class DMSWrapper
    {
        private static string _baseAddress = ConfigurationManager.AppSettings["DMSBaseAddress"];

        public static DMSDocument GetDocumentById(Guid id)
        {
            DMSDocument oDMSDocument = new DMSDocument();
            try
            {
                HttpClient client = new HttpClient(new DMSHandler());
                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(string.Format("api/GetDocument/{0}", id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    oDMSDocument = JsonConvert.DeserializeObject<DMSDocument>(result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return oDMSDocument;
        }
        public static DMSDocument GetDocumentById(string id)
        {
            DMSDocument oDMSDocument = new DMSDocument();
            try
            {
                HttpClient client = new HttpClient(new DMSHandler());
                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(string.Format("api/GetDocument/{0}", id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    oDMSDocument = JsonConvert.DeserializeObject<DMSDocument>(result);
                }
            }
            catch (LPBizException ex)
            {
                throw ex;
            }
            
            return oDMSDocument;
        }
        public static bool DeleteDocument(Guid id)
        {
            try
            {
                HttpClient client = new HttpClient(new DMSHandler());

                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT HEADER
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, string.Format("api/DeleteDocument/{0}", id));

                HttpResponseMessage response = client.SendAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (LPBizException ex)
            {
                throw ex;
            }
        }

        public static DocumentDetails PostDocument(DMSDocument oDMSDocument)
        {
            DocumentDetails oDocumentDetails = new DocumentDetails();
            try
            {
                HttpClient client = new HttpClient(new DMSHandler());

                client.BaseAddress = new Uri(_baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT HEADER
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "api/PostDocument64");

                var JsonObjDMSDocument = JsonConvert.SerializeObject(oDMSDocument);
                request.Content = new StringContent(JsonObjDMSDocument, Encoding.UTF8, "application/json");//CONTENT-TYPE HEADER

                HttpResponseMessage response = client.SendAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    oDocumentDetails = JsonConvert.DeserializeObject<DocumentDetails>(result);
                    oDocumentDetails.IsSuccess = true;
                    oDocumentDetails.Message = "Process Successfully Completed";
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    Dictionary<string, string> badRequestDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                    string message = badRequestDictionary["Message"];
                    oDocumentDetails.IsSuccess = false;
                    oDocumentDetails.Message = message;
                }
            }
            catch (LPBizException ex)
            {
                throw ex;
            }
            
            return oDocumentDetails;
        }

        public class DMSHandler : HttpClientHandler
        {
            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                string UserNameAndPassword = ConfigurationManager.AppSettings["DMSUserNameAndPassword"];
                request.Headers.Add("authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(UserNameAndPassword)));
                return base.SendAsync(request, cancellationToken);
            }
        }
    }
}
