﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LP.Core.Utilities.ExtensionMethods;
using LP.OMS.Channels.Engine.NobillService;
using LP.Core.Utilities.Exceptions;
using LP.OMS.Channels.Engine.Utilities;

namespace LP.OMS.Channels.Engine
{
    public enum Brand
    {
        Friendi = 1,
        Virgin = 2
    }
    public static class NobillWrapper
    {
        private static string _userName = string.Empty;
        private static string _password = string.Empty;
        private static bool _integraitonEnabled = false;

        static NobillWrapper()
        {
            _userName = ConfigurationManager.AppSettings["LP.OMS.Common.FRiENDi.My.APIProxyWrapper.UserName"];
            _password = ConfigurationManager.AppSettings["LP.OMS.Common.FRiENDi.My.APIProxyWrapper.Password"];
            bool.TryParse(ConfigurationManager.AppSettings["FRiENDi.ActivationEngine.WcfServices.IntegrationProxy.IntegrationEnabled"].ToString(), out _integraitonEnabled);
        }

        #region Methods


        public static float GetAccountBalance(string msisdn, out int responseCode)
        {
            responseCode = 0;
            float balance = 0.0F;
            NobillService.AccountData accountData;

            using (NobillCalls oNobillCalls = new NobillCalls())
            {
                oNobillCalls.Credentials = new System.Net.NetworkCredential(_userName, _password);
                responseCode = oNobillCalls.GetAccountData(msisdn, out accountData);
                if (responseCode == 0)
                {
                    float.TryParse(accountData.Balance, out balance);
                }

                return balance;
            }
        }
        public static string GetStatusByIMSI(string imsi, out int result, bool throwExceptionNotFound = true)
        {
            NobillService.NobillCalls client = null;
            SubscriptionItem objSubscriptionItem = SubscriptionItem.status;
            //SIMItem objSIMItem = SIMItem.iccid;
            string DataResult = "";
            //string SIMItemValue = "";
            result = 0;
            try
            {
                client = new NobillService.NobillCalls();
                client.Credentials = new System.Net.NetworkCredential(_userName, _password);//new System.Net.NetworkCredential("activation", "P#ssw-rd098");//


                //result = client.GetSimDataByIMSI(imsi, objSIMItem, out SIMItemValue);
                if (result != 0)
                {

                }
                //else { }
                //if (SIMItemValue != "" && SIMItemValue != string.Empty)
                //{

                result = client.GetSubscriptionValueByIMSIEx(ref imsi, objSubscriptionItem, out DataResult);

                if (result != 0 && throwExceptionNotFound)
                {
                    throw new Exception("APIProxy.GetSubscriptionValueByIMSI failed, reason: " + GetErrorMessage(APIName.GetSubscriptionValueByIMSIEx, result));
                }
                //}
                return DataResult;
            }
            finally
            {
                if (client != null)
                {
                    client.Dispose();
                }
                //Profiler.OperationEnded(Guid.NewGuid(), OperationType.BusinessAdapter_ValidateOrderInfo);
            }
        }


        public static string GetSIMDateByIMSI(string imsi, out int result, bool throwExceptionNotFound = true)
        {
            NobillService.NobillCalls client = null;
            SIMItem objSIMItem = SIMItem.iccid;
            string SIMItemValue = "";
            result = 0;
            try
            {
                client = new NobillService.NobillCalls();
                client.Credentials = new System.Net.NetworkCredential(_userName, _password);// new System.Net.NetworkCredential("activation", "P#ssw-rd098");//


                result = client.GetSimDataByIMSI(imsi, objSIMItem, out SIMItemValue);


                if (result != 0 && throwExceptionNotFound)
                {
                    throw new Exception("APIProxy.GetSubscriptionValueByIMSI failed, reason: " + GetErrorMessage(APIName.GetSubscriptionValueByIMSIEx, result));
                }

                return SIMItemValue;
            }
            finally
            {
                if (client != null)
                {
                    client.Dispose();
                }
                //Profiler.OperationEnded(Guid.NewGuid(), OperationType.BusinessAdapter_ValidateOrderInfo);
            }
        }


        public static int GetCreditTransferPasswordAndMaxAmount(string MSISDN, out CustomerServiceData customerServiceDataDetails)
        {

            int result = -1;

            using (NobillCalls serviceInstance = new NobillCalls())
            {
                serviceInstance.Credentials = new System.Net.NetworkCredential(_userName, _password);
                result = serviceInstance.GetCustomerServiceData(MSISDN, out customerServiceDataDetails);
            }
            return result;
        }


        public static int GetSubscribtionData(string msisdn , out SubscribtionData details)
        {
            int responseCode = -1;
            details  = null;
            using (NobillCalls oNobillCalls = new NobillCalls())
            {
                oNobillCalls.Credentials = new System.Net.NetworkCredential(_userName, _password);
                responseCode = oNobillCalls.GetSubscribtionData(msisdn, out details);
            }

            return responseCode;
        }
        /// <summary>
        /// Get validate IMSI error message (description) by error code
        /// </summary>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public static string GetValidateImsiErrorMessage(int errorCode)
        {
            return GetErrorMessage(APIName.GetSubscriptionValueByIMSIEx, errorCode);
        }

        //        /// <summary>
        //        /// SIM replacement
        //        /// </summary>
        //        /// <param name="oldIMSI"></param>
        //        /// <param name="newICCID"></param>
        //        /// <param name="userName"></param>
        //        public static void ReplaceSIM(string oldIMSI, string newICCID, string userName)
        //        {
        //#if INTEGRATION_DISABLED


        //            return ;
        //#endif
        //            using (NobillService.NobillCalls nobillCalls = new NobillService.NobillCalls())
        //            {
        //                nobillCalls.Credentials = new NetworkCredential(_userName, _password);
        //                int result = nobillCalls.ReplaceSIM(oldIMSI, newICCID, userName);
        //                if (result != 0)
        //                {
        //                    throw new Exception("APIProxy.ReplaceSIM failed, reason: " + GetErrorMessage(APIName.ReplaceSIM, result));
        //                }
        //            }
        //        }


        /// <summary>
        /// Get error error message (description) by error code
        /// </summary>
        /// <param name="apiName"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        private static string GetErrorMessage(APIName apiName, int errorCode)
        {
            switch (apiName)
            {
                case APIName.GetCustomer:
                    switch (errorCode)
                    {
                        case 0:
                            return "Success";
                        case 1:
                            return "Phone number not found";
                        case 2:
                            return "Caller is not Authorized";
                        default:
                            return "Unknown Error";
                    }

                case APIName.UpdateCustomer:
                    switch (errorCode)
                    {
                        case 0:
                            return "Success";
                        case 2:
                            return "Caller is not Authorized";
                        case 3:
                            return "Unhandled Error";
                        case 4:
                            return "Unknown Command";
                        default:
                            return "Unknown Error!";
                    }

                case APIName.GetSubscriptionValueByIMSIEx:
                    switch (errorCode)
                    {
                        case 0:
                            return "Success";
                        case 1:
                            return "Destination is Unknown";
                        case 2:
                            return "Caller is not Authorized";
                        case 3:
                            return "Internal Error, the request should be resubmitted again";
                        case 9:
                            return "Property is not Supported";
                        case 12:
                            return "Unknown Command";
                        case 13:
                            return "Unhandled Error";
                        case 15:
                            return "This is gonna throw imsi swap";
                        default:
                            return "Unknown Error!";
                    }

                case APIName.ReplaceSIM:
                    switch (errorCode)
                    {
                        case 0:
                            return "Success";
                        case 1:
                            return "Subscription not found";
                        case 2:
                            return "Caller is not Authorized";
                        case 3:
                            return "Unknown Error";
                        case 4:
                            return "Unhandled Error";
                        default:
                            return "Unknown Error!";
                    }

                case APIName.GetSubscribtionData:
                    switch (errorCode)
                    {
                        case 0:
                            return "Success";
                        case 1:
                            return "Subscription not found";
                        case 2:
                            return "Caller is not Authorized";
                        default:
                            return "Unknown Error";
                    }

                default:
                    return "Unknown Error!";
            }
        }

        /// <summary>
        /// Enum API names
        /// </summary>
        private enum APIName
        {
            GetCustomer,
            UpdateCustomer,
            GetSubscriptionValueByIMSIEx,
            ReplaceSIM,
            GetSubscribtionData,
            ResetPassword
        }

        public static bool SendSMS(string sender, string msisdn, string message, bool isArabic, bool thowExceptionOnFailure = true)
        {
            int result = -1;
            try
            {
                using (NobillService.NobillCalls client = new NobillService.NobillCalls())
                {
                    client.Credentials = new System.Net.NetworkCredential(_userName, _password);
                    result = client.SendHTTPSMS(sender, msisdn.Trim(), message.Trim(), isArabic);
                    if (result == 0)
                    {
                        return true;
                    }
                    if (thowExceptionOnFailure)
                    {
                        throw new Exception(APIName.ResetPassword + "-SendingMessage failed, reason: " + result + " :" + GetErrorMessage(APIName.ResetPassword, result));
                    }
                    else
                    {
                        LP.Core.Utilities.Logging.LPLogger.Error(APIName.ResetPassword + "-SendingMessage failed, reason: " + result + " :" + GetErrorMessage(APIName.ResetPassword, result));
                    }
                }
            }
            catch (Exception ex)
            {
                LP.Core.Utilities.Logging.LPLogger.Error(ex);
                if (thowExceptionOnFailure)
                    throw;
            }
            return false;
        }

        public static Voucher GetVoucherDetails(string pin, string dealerCode, out int response)
        {
            Voucher objVoucher = null;
            using (NobillCalls proxyClient = new NobillCalls())
            {
                proxyClient.Credentials = new System.Net.NetworkCredential(_userName, _password);
                response = proxyClient.GetVoucherDetails(pin, out objVoucher);

                if (response != 0)
                {
                    ErrorHandling.AddToErrorLog(ErrorTypes.NobillError, string.Format("Error occured on getting Voucher pin:{0} result code: {1}", pin, response), "GetVoucherDetails", dealerCode);
                    return null;
                }
            }

            return objVoucher;
        }

        public static void VoucherRefill(string refillMsisdn, string voucherPin, string dealerCode, out VoucherRefill voucherDetails,
           out int response)
        {
            using (var client = new NobillCalls())
            {
                client.Credentials = new System.Net.NetworkCredential(_userName, _password);
                response = client.Voucherrefill(refillMsisdn, voucherPin, out voucherDetails);
            }

            if (response != 0)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.NobillError, string.Format("Error occured on Voucher Refill pin:{0} result code: {1}", voucherPin, response), "VoucherRefill", dealerCode);
            }
        }

        #endregion
    }
}
