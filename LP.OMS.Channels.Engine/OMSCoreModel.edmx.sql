
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/07/2016 13:19:55
-- Generated from EDMX file: D:\Workspace\OMS-FRiENDi-KSA 2.0\3.Development\Source\Channels\LP.OMS.Channels.Engine\OMSCoreModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [OMSCore];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_DocumentTypeId_RejectionReason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RejectionReason] DROP CONSTRAINT [FK_DocumentTypeId_RejectionReason];
GO
IF OBJECT_ID(N'[dbo].[FK_WFActivity_Action]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFActivity] DROP CONSTRAINT [FK_WFActivity_Action];
GO
IF OBJECT_ID(N'[dbo].[FK_WFActivity_Status]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFActivity] DROP CONSTRAINT [FK_WFActivity_Status];
GO
IF OBJECT_ID(N'[dbo].[FK_WFActivity_WFRequest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFActivity] DROP CONSTRAINT [FK_WFActivity_WFRequest];
GO
IF OBJECT_ID(N'[dbo].[FK_WFRequest_Channel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFRequest] DROP CONSTRAINT [FK_WFRequest_Channel];
GO
IF OBJECT_ID(N'[dbo].[FK_WFRequest_DocumentType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFRequest] DROP CONSTRAINT [FK_WFRequest_DocumentType];
GO
IF OBJECT_ID(N'[dbo].[FK_WFRequest_Requester]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFRequest] DROP CONSTRAINT [FK_WFRequest_Requester];
GO
IF OBJECT_ID(N'[dbo].[FK_WFRequest_WFRequest]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WFRequest] DROP CONSTRAINT [FK_WFRequest_WFRequest];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Action]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Action];
GO
IF OBJECT_ID(N'[dbo].[Channel]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Channel];
GO
IF OBJECT_ID(N'[dbo].[Country]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Country];
GO
IF OBJECT_ID(N'[dbo].[DocumentType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocumentType];
GO
IF OBJECT_ID(N'[dbo].[RejectionReason]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RejectionReason];
GO
IF OBJECT_ID(N'[dbo].[Requester]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Requester];
GO
IF OBJECT_ID(N'[dbo].[Status]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Status];
GO
IF OBJECT_ID(N'[dbo].[WFActivity]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WFActivity];
GO
IF OBJECT_ID(N'[dbo].[WFRequest]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WFRequest];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Actions'
CREATE TABLE [dbo].[Actions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [ForeignName] nvarchar(100)  NULL
);
GO

-- Creating table 'Channels'
CREATE TABLE [dbo].[Channels] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [ForeignName] nvarchar(100)  NULL,
    [Description] nvarchar(1000)  NULL
);
GO

-- Creating table 'Countries'
CREATE TABLE [dbo].[Countries] (
    [ID] int  NOT NULL,
    [Name] varchar(200)  NOT NULL,
    [ForeignName] nvarchar(200)  NOT NULL,
    [Prefix] varchar(3)  NOT NULL
);
GO

-- Creating table 'DocumentTypes'
CREATE TABLE [dbo].[DocumentTypes] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [ForeignName] nvarchar(100)  NULL,
    [SourceID] int  NOT NULL
);
GO

-- Creating table 'RejectionReasons'
CREATE TABLE [dbo].[RejectionReasons] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(50)  NULL,
    [Description] varchar(250)  NOT NULL,
    [ForginDescription] nvarchar(250)  NOT NULL,
    [DocumentTypeId] int  NOT NULL
);
GO

-- Creating table 'Requesters'
CREATE TABLE [dbo].[Requesters] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [FaxNumber] varchar(50)  NOT NULL,
    [RegionID] int  NULL,
    [Code] varchar(50)  NULL,
    [IsActive] bit  NOT NULL,
    [Address] nvarchar(max)  NULL,
    [ContactPerson] nvarchar(100)  NULL,
    [RequesterClassId] int  NULL,
    [RefillMSISDN] varchar(50)  NOT NULL,
    [RefillMSISDN2] varchar(50)  NULL,
    [LanguagePreferenceIsEnglish] bit  NULL,
    [ContactNos] varchar(150)  NULL,
    [SalesPersons] nvarchar(max)  NULL,
    [UserName] varchar(100)  NULL,
    [IsKeyAccount] bit  NULL,
    [PoSTypeID] int  NULL,
    [Password] nvarchar(100)  NOT NULL,
    [CreationDate] datetime  NULL,
    [CreatedBy] nvarchar(100)  NULL,
    [LastActionDate] datetime  NULL,
    [LastActionBy] nvarchar(100)  NULL
);
GO

-- Creating table 'Status'
CREATE TABLE [dbo].[Status] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NOT NULL,
    [ForeignName] nvarchar(100)  NULL
);
GO

-- Creating table 'WFActivities'
CREATE TABLE [dbo].[WFActivities] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [WFRequestID] int  NOT NULL,
    [UITemplate] int  NULL,
    [WFActivityDate] datetime  NOT NULL,
    [WFActivityData] varbinary(max)  NULL,
    [Role1Id] nvarchar(100)  NULL,
    [Role2Id] nvarchar(100)  NULL,
    [Role3Id] nvarchar(100)  NULL,
    [UserID] nvarchar(100)  NULL,
    [ActionID] int  NULL,
    [StatusID] int  NULL,
    [IndexedField1] nvarchar(100)  NULL,
    [IndexedField2] nvarchar(100)  NULL,
    [IndexedField3] nvarchar(100)  NULL,
    [IndexedField4] nvarchar(100)  NULL,
    [IndexedField5] nvarchar(100)  NULL,
    [IndexedField6] nvarchar(100)  NULL,
    [IndexedField7] nvarchar(100)  NULL,
    [IndexedField8] nvarchar(100)  NULL,
    [IndexedField9] nvarchar(100)  NULL,
    [IndexedField10] nvarchar(100)  NULL,
    [ParentActivityId] int  NULL,
    [IsLocked] bit  NOT NULL,
    [LastAccessDate] datetime  NULL,
    [IsLastActivity] bit  NOT NULL
);
GO

-- Creating table 'WFRequests'
CREATE TABLE [dbo].[WFRequests] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [WFInstanceID] uniqueidentifier  NOT NULL,
    [WFStartDate] datetime  NOT NULL,
    [DocumentTypeID] int  NULL,
    [RequestTypeId] int  NOT NULL,
    [IsMovedFromInvalid] bit  NOT NULL,
    [IsMovedFromPending] bit  NOT NULL,
    [ConsolidationStatusId] int  NOT NULL,
    [SLALevel] smallint  NULL,
    [WFEndDate] datetime  NULL,
    [MoveReasonId] int  NULL,
    [MoveDesc] nvarchar(max)  NULL,
    [RequesterID] int  NULL,
    [SenderAcknowledged] bit  NULL,
    [failureReason] nvarchar(500)  NULL,
    [ChannelId] int  NOT NULL,
    [RejectionReasonId] int  NULL,
    [RejectionReasonDescription] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Actions'
ALTER TABLE [dbo].[Actions]
ADD CONSTRAINT [PK_Actions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ID] in table 'Channels'
ALTER TABLE [dbo].[Channels]
ADD CONSTRAINT [PK_Channels]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Countries'
ALTER TABLE [dbo].[Countries]
ADD CONSTRAINT [PK_Countries]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'DocumentTypes'
ALTER TABLE [dbo].[DocumentTypes]
ADD CONSTRAINT [PK_DocumentTypes]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'RejectionReasons'
ALTER TABLE [dbo].[RejectionReasons]
ADD CONSTRAINT [PK_RejectionReasons]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Id] in table 'Requesters'
ALTER TABLE [dbo].[Requesters]
ADD CONSTRAINT [PK_Requesters]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Status'
ALTER TABLE [dbo].[Status]
ADD CONSTRAINT [PK_Status]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ID] in table 'WFActivities'
ALTER TABLE [dbo].[WFActivities]
ADD CONSTRAINT [PK_WFActivities]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'WFRequests'
ALTER TABLE [dbo].[WFRequests]
ADD CONSTRAINT [PK_WFRequests]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ActionID] in table 'WFActivities'
ALTER TABLE [dbo].[WFActivities]
ADD CONSTRAINT [FK_WFActivity_Action]
    FOREIGN KEY ([ActionID])
    REFERENCES [dbo].[Actions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WFActivity_Action'
CREATE INDEX [IX_FK_WFActivity_Action]
ON [dbo].[WFActivities]
    ([ActionID]);
GO

-- Creating foreign key on [ChannelId] in table 'WFRequests'
ALTER TABLE [dbo].[WFRequests]
ADD CONSTRAINT [FK_WFRequest_Channel]
    FOREIGN KEY ([ChannelId])
    REFERENCES [dbo].[Channels]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WFRequest_Channel'
CREATE INDEX [IX_FK_WFRequest_Channel]
ON [dbo].[WFRequests]
    ([ChannelId]);
GO

-- Creating foreign key on [DocumentTypeId] in table 'RejectionReasons'
ALTER TABLE [dbo].[RejectionReasons]
ADD CONSTRAINT [FK_DocumentTypeId_RejectionReason]
    FOREIGN KEY ([DocumentTypeId])
    REFERENCES [dbo].[DocumentTypes]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DocumentTypeId_RejectionReason'
CREATE INDEX [IX_FK_DocumentTypeId_RejectionReason]
ON [dbo].[RejectionReasons]
    ([DocumentTypeId]);
GO

-- Creating foreign key on [DocumentTypeID] in table 'WFRequests'
ALTER TABLE [dbo].[WFRequests]
ADD CONSTRAINT [FK_WFRequest_DocumentType]
    FOREIGN KEY ([DocumentTypeID])
    REFERENCES [dbo].[DocumentTypes]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WFRequest_DocumentType'
CREATE INDEX [IX_FK_WFRequest_DocumentType]
ON [dbo].[WFRequests]
    ([DocumentTypeID]);
GO

-- Creating foreign key on [RequesterID] in table 'WFRequests'
ALTER TABLE [dbo].[WFRequests]
ADD CONSTRAINT [FK_WFRequest_Requester]
    FOREIGN KEY ([RequesterID])
    REFERENCES [dbo].[Requesters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WFRequest_Requester'
CREATE INDEX [IX_FK_WFRequest_Requester]
ON [dbo].[WFRequests]
    ([RequesterID]);
GO

-- Creating foreign key on [StatusID] in table 'WFActivities'
ALTER TABLE [dbo].[WFActivities]
ADD CONSTRAINT [FK_WFActivity_Status]
    FOREIGN KEY ([StatusID])
    REFERENCES [dbo].[Status]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WFActivity_Status'
CREATE INDEX [IX_FK_WFActivity_Status]
ON [dbo].[WFActivities]
    ([StatusID]);
GO

-- Creating foreign key on [WFRequestID] in table 'WFActivities'
ALTER TABLE [dbo].[WFActivities]
ADD CONSTRAINT [FK_WFActivity_WFRequest]
    FOREIGN KEY ([WFRequestID])
    REFERENCES [dbo].[WFRequests]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WFActivity_WFRequest'
CREATE INDEX [IX_FK_WFActivity_WFRequest]
ON [dbo].[WFActivities]
    ([WFRequestID]);
GO

-- Creating foreign key on [ID] in table 'WFRequests'
ALTER TABLE [dbo].[WFRequests]
ADD CONSTRAINT [FK_WFRequest_WFRequest]
    FOREIGN KEY ([ID])
    REFERENCES [dbo].[WFRequests]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------