﻿using LP.OMS.Channels.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Engine
{
    public static class POWrapper
    {
        public static List<PlanServiceReference.Service> GetServices(List<int> serviceIDs)
        {
            List<PlanServiceReference.Service> response = new List<PlanServiceReference.Service>();

            using (PlanServiceReference.PlanServiceClient client = new PlanServiceReference.PlanServiceClient())
            {
                using (new POContextScope(client.InnerChannel))
                {
                    PlanServiceReference.ServiceRequest req = new PlanServiceReference.ServiceRequest()
                    {
                        ServiceIds = serviceIDs.ToArray()
                    };
                    response = client.GetServices(req).ToList();
                }
            }

            return response;
        }

        public static List<PlanResponse> GetServicesByIDs(int[] serviceIds)
        {
            using (PlanServiceReference.PlanServiceClient client = new PlanServiceReference.PlanServiceClient())
            {
                using (new POContextScope(client.InnerChannel))
                {
                    List<PlanResponse> ListplanResponse = new List<PlanResponse>();
                    foreach (var iteam in client.GetServices(new PlanServiceReference.ServiceRequest()
                    {
                        ServiceIds = serviceIds
                    }).Where(z => z.IsActive))
                    {
                        PlanResponse planResponse = new PlanResponse();
                        planResponse.ID = iteam.Id.ToString();
                        planResponse.Name = iteam.Name;
                        planResponse.Type = iteam.ServiceType.ToString();
                        planResponse.Unit = iteam.Unit.ToString();
                        planResponse.Value = double.Parse(iteam.Cost.ToString());
                        planResponse.ServiceType = (int)iteam.ServiceType;
                        Controller.FillPlanUnitAndValue(planResponse, iteam.InitialValue, iteam.Unit);
                        ListplanResponse.Add(planResponse);

                    }

                    return ListplanResponse;
                }
            }
        }

        public static PlanServiceReference.PlanPrice GetPackagePrice(int productCode, List<int> serviceIds,
            string msisdn, List<PlanServiceReference.PromotionRequest> promotions)
        {
            PlanServiceReference.PlanPrice response = new PlanServiceReference.PlanPrice();

            using (PlanServiceReference.PlanServiceClient client = new PlanServiceReference.PlanServiceClient())
            {
                using (new POContextScope(client.InnerChannel))
                {
                    PlanServiceReference.SubscribeRequest req = new PlanServiceReference.SubscribeRequest()
                    {
                        ServiceIds = serviceIds.ToArray(),
                        CustomerInfo = new PlanServiceReference.SRECustomerInfo()
                        {
                            ProductCode = productCode,
                            MSISDN = msisdn
                        },
                        Promotions = (promotions != null && promotions.Any()) ? promotions.ToArray() : null
                    };
                    response = client.GetPackagePrice(req, true);
                    return response;
                }
            }
        }
    }
}
