﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Engine
{
    public static class Enums
    {
        public enum NotificationStatus
        {
            Pending = 1,
            Sent = 2,
            FirstReminderSent = 3,
            SecondReminderSent = 4,
            Closed = 5
        }

        public enum VerficationStatus
        {
            Pending = 1,
            UserCheckedOut = 2,
            Verified = 3,
            NotVerified = 4
        }
    }
}
