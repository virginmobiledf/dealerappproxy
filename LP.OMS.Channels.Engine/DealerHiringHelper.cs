﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.Enums;
using LP.OMS.Channels.Engine.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Data.Entity;
using LP.OMS.Channels.Engine.AdministrationServices;
using LP.OMS.VirginKSA.Common.Utilities.Messaging;

namespace LP.OMS.Channels.Engine
{
    public class DealerHiringHelper
    {
        public static string GetParentCode(DealerCreationContractRequest request, AdministrationServices.Requester requester)
        {
            try
            {
                if (!string.IsNullOrEmpty(request.ParentCode))
                {
                    return request.ParentCode;
                }
                else
                {
                    return requester.Code;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog("GetParentCode", ex.Message, "GetParentCode", requester.Code, "");
                throw ex;
            }
        }

        public static string GetLookupValue(int mainReferrenceID, int subReferrenceID)
        {
            string value = string.Empty;
            try
            {
                using (OMSEXEntities context = new OMSEXEntities())
                {
                    var allMainLookups = DealerHiringController.GetLookups(new Contracts.LookupsRequest()
                    {
                        lookupId = mainReferrenceID
                    }, false, false);
                    var subLookup = allMainLookups.Lookups.FirstOrDefault()
                        .LookupItems.FirstOrDefault(x => x.LookupItemId == subReferrenceID);
                    if (subLookup != null)
                        value = subLookup.LookupItemName;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog("GetLookupValue", ex.Message, "GetLookupValue", "", "");
            }

            return value;
        }

        public static string GetLocalizedLookupValue(int mainReferrenceID, int subReferrenceID, int language)
        {
            string value = string.Empty;
            try
            {
                using (OMSEXEntities context = new OMSEXEntities())
                {
                    var allMainLookups = DealerHiringController.GetLookups(new Contracts.LookupsRequest()
                    {
                        lookupId = mainReferrenceID
                    }, false, false);
                    var subLookup = allMainLookups.Lookups.FirstOrDefault()
                        .LookupItems.FirstOrDefault(x => x.LookupItemId == subReferrenceID);
                    if (subLookup != null)
                    {
                        if (language == 1) // English
                        {
                            value = subLookup.LookupItemName;
                        }
                        else if (language == 2) // Arabic
                            value = subLookup.LookupItemName2;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog("GetLocalizedLookupValue", ex.Message, "GetLookupValue", "", "");
            }

            return value;
        }


        public static int GetLookupID(int mainReferrenceID, string LookupItemName)
        {
            int value = 0;
            try
            {
                using (OMSEXEntities context = new OMSEXEntities())
                {
                    var allMainLookups = DealerHiringController.GetLookups(new Contracts.LookupsRequest()
                    {
                        lookupId = mainReferrenceID
                    }, false, false);
                    var subLookup = allMainLookups.Lookups.FirstOrDefault()
                        .LookupItems.FirstOrDefault(x => x.LookupItemName == LookupItemName);
                    if (subLookup != null)
                        value = subLookup.LookupItemId;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog("GetLookupID", ex.Message, "GetLookupID", "", "");
            }

            return value;
        }

        public static void DeleteDocuments(List<string> ids, string caller)
        {
            string value = string.Empty;
            try
            {
                foreach (string id in ids)
                {
                    DMSWrapper.DeleteDocument(Guid.Parse(id));
                }
            }
            catch (Exception ex)
            {
                string strIds = string.Empty;
                string stackTrace = string.Empty;
                try
                {
                    strIds = string.Join(", ", ids.ToArray());
                    stackTrace = ex.StackTrace;
                }
                catch (Exception ex1)
                {
                    ErrorHandling.AddToErrorLog($"Log DeleteDocuments exception failed", $"Main exception {ex.Message}, Secondary exception {ex1.Message}", "DeleteDocuments", caller);
                }
                ErrorHandling.AddToErrorLog($"DeleteDocuments: ids {strIds}", ex.Message, "DeleteDocuments", caller, stackTrace);
            }
        }

        public static string GetLandMark(string mark, int markPrefix)
        {
            string res = mark;

            try
            {
                if (!string.IsNullOrEmpty(mark))
                {
                    var firstSpaceIndex = mark.IndexOf(" ");
                    var firstString = mark.Substring(0, firstSpaceIndex);
                    var secondString = mark.Substring(firstSpaceIndex + 1);

                    int prefixIndex = 0;
                    bool result = int.TryParse(firstString.Trim(), out prefixIndex);

                    if (prefixIndex != 0)
                    {
                        string prefixText = GetLookupValue(markPrefix, prefixIndex);
                        if (!string.IsNullOrEmpty(prefixText))
                        {
                            res = prefixText + " " + secondString;
                        }
                    }
                }
            }
            catch (Exception ex)
            { }

            return res;
        }

        public static string GetDealerHiringMessage(DealerHiringErrorCode responseCode, int language)
        {
            return GetResourceManager(language).GetString("DealerHiring_" + responseCode.ToString(), Thread.CurrentThread.CurrentCulture);
        }

        public static ResourceManager GetResourceManager(int language)
        {
            if (Thread.CurrentThread.CurrentCulture == new CultureInfo("ar-JO") || language == 2)
            {
                return Resources.BLMessages_ar_JO.ResourceManager;
            }
            else
            {
                return Resources.BLMessages.ResourceManager;
            }
        }

        public static List<DealerDocumentDto> GetDealerDocumentDtos(List<Documents> requestDocuments,
          ObjectParameter documentTypesParameter, ObjectParameter documentFilesParameter, string fileName, string dealerUsername,
          int documentTypesLookupReferrence, out string documentFiles, out string documentTypeIds, out string documentTypeNames)
        {
            List<DealerDocumentDto> dtos = new List<DealerDocumentDto>();

            if (requestDocuments != null)
            {
                foreach (var document in requestDocuments)
                {
                    var dto = new DealerDocumentDto();
                    string documentType =
                        DealerHiringHelper.GetLookupValue(documentTypesLookupReferrence, document.DocumentType);
                    DocumentDetails documentDetails = new DocumentDetails();
                    try
                    {
                        DMSDocument oDMSDocument = new DMSDocument()
                        {
                            Base64 = document.DocumentContent,
                            Description = documentType,
                            Tags = new List<string>() { documentType },
                            Extention = ".png",
                            FileName = fileName
                        };

                        documentDetails = DMSWrapper.PostDocument(oDMSDocument);
                        dto.FileTypeId = GetLookupID(documentTypesLookupReferrence, documentType).ToString();
                        dto.FileTypeName = documentType;
                        dto.FileId = documentDetails.DocumentId;
                        dtos.Add(dto);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog("PostDocument", ex.Message, "PostDocument", dealerUsername, "");
                        throw ex;
                    }
                }
            }

            List<string> existinfDocumentFiles = new List<string>();

            if (documentFilesParameter.Value != null)
            {
                existinfDocumentFiles = documentFilesParameter.Value.ToString().Split(',').ToList();
            }

            if (documentTypesParameter.Value != null)
            {
                var existingDocumentTypeIDs = documentTypesParameter.Value.ToString().Split(',').ToList();
                foreach (var existingDocumentTypeID in existingDocumentTypeIDs)
                {
                    if (!dtos.Any(e => e.FileTypeId == existingDocumentTypeID.Trim()))
                    {
                        var dto = new DealerDocumentDto
                        {
                            FileId = existinfDocumentFiles.ElementAt(existingDocumentTypeIDs.IndexOf(existingDocumentTypeID)),
                            FileTypeId = existingDocumentTypeID,
                            FileTypeName = DealerHiringHelper.GetLookupValue(documentTypesLookupReferrence, int.Parse(existingDocumentTypeID))
                        };
                        dtos.Add(dto);
                    }
                }
            }

            documentFiles = string.Join(",", dtos.Select(e => e.FileId));
            documentTypeIds = string.Join(",", dtos.Select(e => e.FileTypeId));
            documentTypeNames = string.Join(",", dtos.Select(e => e.FileTypeName));

            return dtos;
        }

        public static string GetDealerHiringMessageByActionName(string actionName, string MessageType ,int language)
        {
            string Message = string.Empty;
            using (OMSEXEntities context = new OMSEXEntities())
            {
                var record = context.MessageConfigurations.Where(o => o.Name == actionName && o.MessageType == MessageType).FirstOrDefault();

                if (record != null)
                {
                    if (language == 1)
                        Message = record.BodyTemplate;
                    else if (language == 2)
                        Message = record.BodyTemplateAr;
                }
            }

            return Message;
        }
    }
}
