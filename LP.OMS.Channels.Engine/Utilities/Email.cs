﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LP.OMS.Channels.Engine.Utilities
{
    class Email
    {
        private static readonly string EmailHost;
        private static readonly int EmailPort;
        private static readonly int EmailTimeout;
        private static readonly string EmailUsername;
        private static readonly string EmailPassword;
        private static readonly string EmailDefaultSender;
        private static readonly bool EnableSSL;

        static Email()
        {
            EmailHost = ConfigurationManager.AppSettings["EmailHost"];
            EmailPort = int.Parse(ConfigurationManager.AppSettings["EmailPort"]);
            EmailTimeout = int.Parse(ConfigurationManager.AppSettings["EmailTimeOut"]);
            EmailUsername = ConfigurationManager.AppSettings["EmailUsername"];
            EmailPassword = ConfigurationManager.AppSettings["EmailPassword"];
            EmailDefaultSender = ConfigurationManager.AppSettings["EmailDefaultSender"];
            EnableSSL = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"]);
        }

        public static void SendEmail(List<string> toEmail, string subject, string body,  string dealerCode, string callerMethod)
        {
            try
            {
                SmtpClient client = new SmtpClient(EmailHost, EmailPort)
                {
                    Port = Convert.ToInt32(EmailPort),
                    Host = EmailHost,
                    Timeout = EmailTimeout,
                    EnableSsl = EnableSSL,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(EmailUsername, EmailPassword)
                };

                MailMessage mailMessage = new MailMessage()
                {
                    BodyEncoding = Encoding.UTF8,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    IsBodyHtml = true
                };

                mailMessage.From = new MailAddress(EmailDefaultSender, EmailDefaultSender);
                mailMessage.Body = body;

                mailMessage.Subject = subject;
                toEmail.ForEach(item => mailMessage.To.Add(item));

                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendEmail-" + callerMethod, ex,  dealerCode, false);
            }
        }
    }
}
