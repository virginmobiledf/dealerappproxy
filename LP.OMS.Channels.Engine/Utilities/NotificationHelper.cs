﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LP.Core.Utilities.Exceptions;
using System.Net.Mail;
using System.Collections.Generic;
using System.Text;
using LP.OMS.Channels.Contracts;

namespace LP.OMS.Channels.Engine.Utilities
{
    public static class NotificationHelper
    {
        private static readonly string EmailHost;
        private static readonly int EmailPort;
        private static readonly int EmailTimeout;
        private static readonly string EmailUsername;
        private static readonly string EmailPassword;
        private static readonly string EmailDefaultSender;
        private static readonly string EmailSubject;
        private static readonly string EmailBody;
        private static readonly string EmailRedirectEmail;
        private static readonly bool EnableSslFlag;

        static NotificationHelper()
        {
            EmailHost = ConfigurationManager.AppSettings["EmailHost"];
            EmailPort = int.Parse(ConfigurationManager.AppSettings["EmailPort"]);
            EmailTimeout = int.Parse(ConfigurationManager.AppSettings["EmailTimeOut"]);
            EmailUsername = ConfigurationManager.AppSettings["EmailUsername"];
            EmailPassword = ConfigurationManager.AppSettings["EmailPassword"];
            EmailDefaultSender = ConfigurationManager.AppSettings["EmailDefaultSender"];
            EmailSubject = ConfigurationManager.AppSettings["EmailSubject"];
            EmailBody = ConfigurationManager.AppSettings["EmailBody"];
            EmailRedirectEmail = ConfigurationManager.AppSettings["EmailRedirectEmail"];
            EnableSslFlag = ConfigurationManager.AppSettings.Get("EnableSsl") == "1" ? true : false;
        }
        public static void SendEmail(LoginRequest loginRequest,List<string> toEmail, string message, string sender, string subject, string senderDisplayName)
        {
            try
            {
                if (ConfigurationManager.AppSettings["DisableSendEmail"] != null)
                {
                    return;
                }
                SmtpClient client = new SmtpClient(EmailHost, EmailPort)
                {
                    Port = Convert.ToInt32(EmailPort),
                    Host = EmailHost,
                    Timeout = EmailTimeout,
                    EnableSsl = EnableSslFlag,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(EmailUsername, EmailPassword)
                };

                MailMessage mailMessage = new MailMessage()
                {
                    BodyEncoding = Encoding.UTF8,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    IsBodyHtml = true
                };


                mailMessage.From = new MailAddress(sender, senderDisplayName);
                mailMessage.Body = message;

                mailMessage.Subject = subject;
                toEmail.ForEach(item => mailMessage.To.Add(item));

                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendEmail", ex, loginRequest.Username, true);
            }
        }
    }
}
