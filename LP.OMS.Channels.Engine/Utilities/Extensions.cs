﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.VirginKSA.Common.Utilities
{
    public static class Extensions
    {
        public static string ReplaceIgnorCase(this string inputString, string oldValue, string newValue)
        {
            newValue = newValue ?? "";
            if (string.IsNullOrEmpty(inputString) || string.IsNullOrEmpty(oldValue) || oldValue.Equals(newValue, StringComparison.OrdinalIgnoreCase))
                return inputString;
            int foundAt = 0;
            while ((foundAt = inputString.IndexOf(oldValue, foundAt, StringComparison.OrdinalIgnoreCase)) != -1)
            {
                inputString = inputString.Remove(foundAt, oldValue.Length).Insert(foundAt, newValue);
                foundAt += newValue.Length;
            }
            return inputString;
        }

        public static object GetParameterValue<T>(this Dictionary<string, string> dictionary, string keyName) where T : IConvertible
        {
            string type = typeof(T).Name;
            TypeCode typecode = (TypeCode)Enum.Parse(typeof(TypeCode), type);

            if (!dictionary.ContainsKey(keyName))
            {
                return DBNull.Value;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(dictionary[keyName]))
                {
                    return DBNull.Value;
                }
                else
                {
                    if (typecode == TypeCode.DateTime)
                    {
                        return ((DateTime)Convert.ChangeType(dictionary[keyName], typecode)).ToString("yyyy-MM-dd HHmmss");
                    }
                    else
                    {
                        return ((T)Convert.ChangeType(dictionary[keyName], typecode)).ToString();
                    }
                }
            }
        }
    }
}
