﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LP.Core.Utilities.Exceptions;
using System.Net.Mail;
using System.Collections.Generic;
using System.Text;

namespace LP.OMS.Channels.Engine.Utilities
{
    public class ExceptionHandling
    {
        public static string HandleBackendException(Exception exp, int languageId, string tokens = "")
        {
            Exception expLoop = exp;
            do
            {
                if (expLoop is LPBizException)
                {
                    LPBizException bizExcp = (LPBizException)expLoop;
                    if (string.IsNullOrEmpty(bizExcp.Code) || bizExcp.Code == LPBizException.GenericCode)
                    {
                        return bizExcp.Message;
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(tokens))
                        {
                            if (languageId == 2)
                            {
                                return Resources.BLMessages_ar_JO.ResourceManager.GetString("_" + bizExcp.Code) ?? bizExcp.Message;
                            }
                            else
                            {
                                return Resources.BLMessages.ResourceManager.GetString("_" + bizExcp.Code) ?? bizExcp.Message;
                            }
                        }
                        else
                        {
                            if (languageId == 2)
                            {
                                return string.Format(Resources.BLMessages_ar_JO.ResourceManager.GetString("_" + bizExcp.Code) ?? bizExcp.Message, tokens.Split(','));
                            }
                            else
                            {
                                return string.Format(Resources.BLMessages.ResourceManager.GetString("_" + bizExcp.Code) ?? bizExcp.Message, tokens.Split(','));
                            }
                        }
                    }
                }
                else if (expLoop is LPUIException)
                {
                    LPUIException uiExcp = (LPUIException)expLoop;

                    if (string.IsNullOrEmpty(uiExcp.Code) || uiExcp.Code == LPUIException.GenericCode)
                    {
                        return uiExcp.Message;
                    }
                    else
                    {
                        return "General Error";
                    }
                }
                else
                {
                    expLoop = expLoop.InnerException;
                }

            } while (expLoop != null);

            //If we reach here, then LPBizException or LPUIException not found!
            return exp.Message;
        }

        public static string GetErrorCode(Exception exp)
        {
            Exception expLoop = exp;
            do
            {
                if (expLoop is LPBizException)
                {
                    LPBizException bizExcp = (LPBizException)expLoop;
                    if (!string.IsNullOrEmpty(bizExcp.Code))
                    {
                        return bizExcp.Code;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    expLoop = expLoop.InnerException;
                }

            } while (expLoop != null);

            //If we reach here, then LPBizException or LPUIException not found!
            return "-1";
        }
    }
}
