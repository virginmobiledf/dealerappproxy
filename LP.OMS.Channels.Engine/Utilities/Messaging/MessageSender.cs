﻿using LP.Core.Utilities.Logging;
using LP.OMS.Channels.Engine.Utilities.Messaging.Adapters;
using LP.OMS.VirginKSA.Common.Utilities.Messaging.Adapters;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LP.OMS.VirginKSA.Common.Utilities.Messaging
{
    public class MessageSender
    {
        static List<MessageConfiguration> MessageConfigurations = null;

        private List<Message> messages = null;

        static MessageSender()
        {
            using (Database db = AdapterHelper.GetExtendedNoLockSqlServerDatabase())
            {
                MessageConfigurations = db.Query<MessageConfiguration>("SELECT * FROM [MessageConfiguration]").ToList();
            }
        }

        public MessageSender(string name, MessageTypes messageType = MessageTypes.Any, Languages language = Languages.Any)
        {
            CreateMessages(name, messageType, language);
        }

        private void CreateMessages(string name, MessageTypes messageType = MessageTypes.Any, Languages language = Languages.Any)
        {
            messages = MessageConfigurations.Where(m => m.Name == name && (m.MessageType == messageType.ToString() || messageType == MessageTypes.Any) && (m.Language == language.ToString() || language == Languages.Any)).Select(messageConfig => new Message(messageConfig)).ToList();
        }

        public MessageSender Bind(string key, string value)
        {
            foreach (Message message in messages)
            {
                message.Bind(key, value);
            }

            return this;
        }

        public MessageSender BindAll(Dictionary<string, object> bindData)
        {
            foreach (KeyValuePair<string, object> pair in bindData)
            {
                foreach (Message message in messages)
                {
                    message.Bind(pair.Key, pair.Value.ToString());
                }
            }

            return this;
        }

        public MessageSender SetSender(string sender)
        {
            foreach (Message message in messages)
            {
                message.SetSender(sender);
            }

            return this;
        }

        public MessageSender SetReceiver(string receiver)
        {
            foreach (Message message in messages)
            {
                message.SetReceiver(receiver);
            }

            return this;
        }
        public MessageSender SetReceivers(List<string> receivers)
        {
            foreach (Message message in messages)
            {
                message.SetReceivers(receivers);
            }

            return this;
        }
        public void Send()
        {
            SMSMessageAdapter smsAdapter = null;
            EmailMessageAdapter emailAdapter = null;

            foreach (Message message in messages)
            {
                try
                {
                    if (message.MessageType == MessageTypes.SMS.ToString())
                    {
                        if (smsAdapter == null)
                        {
                            smsAdapter = new SMSMessageAdapter();
                        }

                        smsAdapter.Send(message);
                    }
                    else if (message.MessageType == MessageTypes.Email.ToString())
                    {
                        if (emailAdapter == null)
                        {
                            emailAdapter = new EmailMessageAdapter();
                        }

                        emailAdapter.Send(message);
                    }

                    Log(message, true, "");
                }
                catch (Exception ex)
                {
                    Log(message, false, ex.Message);
                }
            }
        }

        private void Log(Message message, bool status, string error)
        {
            try
            {
                using (Database db = AdapterHelper.GetExtendedNoLockSqlServerDatabase())
                {
                    db.Insert("MessageLog", "ID", new
                    {
                        Sender = message.GetSender(),
                        Receiver = message.GetReceiver(),
                        MessageType = message.MessageType,
                        Title = message.GetTitle(),
                        Body = message.GetBody(),
                        IsSuccess = status,
                        Error = error
                    });
                }
            }
            catch(Exception ex)
            {
                LPLogger.Information("Message Log>> " +
                    "Sender: " + message.GetSender() +
                    "Receiver: " + message.GetReceiver() +
                    "MessageType: " + message.MessageType +
                    "Title: " + message.GetTitle() +
                    "Body: " + message.GetBody() +
                    "IsSuccess: " + status +
                    "Error: " + error);
            }
        }
    }

    public class Message : MessageConfiguration
    {
        public Message(MessageConfiguration messageConfiguration)
        {
            Name = messageConfiguration.Name;
            Language = messageConfiguration.Language;
            MessageType = messageConfiguration.MessageType;
            TitleTemplate = messageConfiguration.TitleTemplate;
            BodyTemplate = messageConfiguration.BodyTemplate;
            Title = TitleTemplate;
            Body = BodyTemplate;
        }

        public string GetTitle()
        {
            return Title.ReplaceIgnorCase("{{Sender}}", Sender).ReplaceIgnorCase("{{Receiver}}", Receiver);
        }

        public string GetBody()
        {
            return Body.ReplaceIgnorCase("{{Sender}}", Sender).ReplaceIgnorCase("{{Receiver}}", Receiver);
        }

        public Message Bind(string key, string value)
        {
            Title = Title.ReplaceIgnorCase("{{" + key + "}}", value);
            Body = Body.ReplaceIgnorCase("{{" + key + "}}", value);
            return this;
        }

        public Message SetSender(string sender)
        {
            this.Sender = sender;
            return this;
        }

        public Message SetReceivers(List<string> receivers)
        {
            this.Receivers = receivers;
            return this;
        }

        public Message SetReceiver(string receiver)
        {
            this.Receiver = receiver;
            return this;
        }

        public string GetSender()
        {
            return Sender;
        }

        public string GetReceiver()
        {
            return Receiver;
        }

        public List<string> GetReceivers()
        {
            return Receivers;
        }

        private string Sender { get; set; }
        private string Receiver { get; set; }
        private List<string> Receivers { get; set; }
        private string Title { get; set; }
        private string Body { get; set; }
    }

    public class MessageConfiguration
    {
        public string Name { get; set; }
        public string Language { get; set; }
        public string MessageType { get; set; }
        public string TitleTemplate { get; set; }
        public string BodyTemplate { get; set; }
    }

    public enum MessageTypes
    {
        Any = 0,
        Email = 1,
        SMS = 2,
        WebNotification = 3,
        AppNotification = 4
    }

    public enum Languages
    {
        Any = 0,
        English = 1,
        Arabic = 2
    }
}
