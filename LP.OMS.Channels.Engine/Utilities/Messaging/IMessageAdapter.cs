﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.VirginKSA.Common.Utilities.Messaging
{
    public interface IMessageAdapter
    {
        MessageTypes AdapterType { get;}
        void Send(Message message);
    }
}
