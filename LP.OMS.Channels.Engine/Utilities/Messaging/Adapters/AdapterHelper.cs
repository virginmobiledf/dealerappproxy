﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Engine.Utilities.Messaging.Adapters
{
    class AdapterHelper
    {
        public static PetaPoco.Database GetExtendedNoLockSqlServerDatabase()
        {
            return GetNoLockSqlServerDatabase("OMSEXEntities");
        }
        private static PetaPoco.Database GetNoLockSqlServerDatabase(string connectionStringKey)
        {

            System.Data.EntityClient.EntityConnectionStringBuilder entityConnectionStringBuilder = new System.Data.EntityClient.EntityConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringKey].ConnectionString);

            var providerExists = System.Data.Common.DbProviderFactories
                                            .GetFactoryClasses()
                                            .Rows.Cast<System.Data.DataRow>()
                                            .Any(r => r[2].Equals(entityConnectionStringBuilder.Provider));
            if (providerExists)
            {
                PetaPoco.Database db = new PetaPoco.NoLockSqlServerDatabase(entityConnectionStringBuilder.ProviderConnectionString, entityConnectionStringBuilder.Provider);
                return db;
            }
            else
                return null;

        }
    }
}
