﻿using LP.OMS.Channels.Engine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.VirginKSA.Common.Utilities.Messaging.Adapters
{
    class EmailMessageAdapter : IMessageAdapter
    {
        public MessageTypes AdapterType
        {
            get
            {
                return MessageTypes.Email;
            }
        }

        public void Send(Message message)
        {
            //NobillApiClient.SendSMS(message.GetSender(), message.GetReceiver(), message.GetBody(), message.Language.Trim().ToLower().Substring(0, 2) == Languages.English.ToString().ToLower().Substring(0, 2));
            Email.SendEmail(message.GetReceivers(), message.GetTitle(), message.GetBody(), message.GetReceiver(), message.Name);
        }
    }
}
