﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using LP.Core.Utilities.Exceptions;
using System.Data;
using System.ServiceModel;
using System.IO;

namespace LP.OMS.Channels.Engine.Utilities
{
    public static class ErrorTypes
    {
        public const string GenericError = "Generic Error";
        public const string GenericEntityError = "Generic Entity Error";
        public const string BusinessError = "Business Error";
        public const string NobillError = "Nobill Error";
        public const string ZainIntegrationError = "ZainIntegrationError";
        public const string IntegrationError = "IntegrationError";
        public const string Information = "Information";
    }

    public static class ErrorCodes
    {
        public const string Success = "0";
        public const string GenericError = "1";
        public const string GenericEntityError = "2";

        public const string DealerDoesnotExist = "3";
        public const string BarcodeReaderFailedToRecognizeImage = "4";
        public const string BarcodeReaderNoDataWasRecognizedPageOne = "5";
        public const string BarcodeReaderNoDataWasRecognizedPageTwo = "6";
        public const string BarcodeReaderOnlyOnePageFound = "7";
        public const string BarcodeReaderMoreThanOneBarcodeFoundPageOne = "8";
        public const string BarcodeReaderMoreThanOneBarcodeFoundPageTwo = "9";
        public const string BarcodeReaderPageBarcodesDontMatch = "10";
        public const string BarcodeReaderInvalidIMSICheckDigit = "11";
        public const string InvalidIMSINewIMSIofSIMReplacement = "12";
        public const string IMSINotValidInNobill = "13";
        public const string MSISDNNotValidInNobill = "14";
        public const string InvalidCustomerId = "15";
        public const string ActivationRequestIsRedundant = "16";
        public const string IMSINotActivated = "17";
        public const string IMSIAlreadyChangedOwnerOnce = "18";

        public const string LINE_Already_reserved = "19";
        public const string CANNOT_ACTIVATE_NEW_LINE_FOR_THE_PROVIDED_ID = "20";


    }

    public static class ZainErrorCodes
    {
        public const string Success = "0";
        public const string SUBNO_IS_ALREADY_EXIST = "18";
        public const string Maximum_number_of_subsribers_per_ID_reached = "106";
        public const string SUCCESS_WITHOUT_ALELM_CHECK = "10000";

        public const string OPERATOR_CODE_IS_NOT_PROVIDED = "80001";
        public const string TRANS_CODE_IS_NOT_PROVIDED = "80004";
        public const string NO_ACCOUNT_FOR_PROVIDED_OPERATOR_CODE_AND_USERNAME = "80005";
        public const string Inactive_Account = "8006";
        public const string INVALID_USERNAME_OR_PASSWORD = "80007";

        public const string SUBNO_IS_MANDATORY = "80008";

        public const string ID_TYPE_IS_MANDATORY = "80009";
        public const string ID_NO_IS_MANDATORY = "80010";
        public const string ID_EXPIRY_DATE_IS_MANDATORY = "80011";

        public const string COUNTRY_IS_MANDATORY = "80012";

        public const string AR_NAME_IS_MANDATORY = "80013";

        public const string INCORRECT_SUBNO_FORMAT_IT_SHOULD_START_BY_DIGIT_5 = "80014";
        public const string INVALID_ID_TYPE_VALUE_SHOULD_BE_ONE_OF_C_Q_P_G_L_V_H = "80015";
        public const string INVALID_ID_NO = "80016";
        public const string INVALID_ID_EXPIRY_DATE_FORMAT_SHOULD_BE_GREGORIAN_YYYYMMDD_AND_VALUE_SHOULD_BE_LATER_THAN_NOW_DATE = "80017";
        public const string INVALID_DATE_OF_BIRTH_FORMAT_SHOULD_BE_GREGORIAN_YYYYMMDD_AND_VALUE_SHOULD_BE_EARLIER_THAN_NOW_DATE = "80018";
        public const string INVALID_GENDER_VALUE_SHOULD_BE_ONE_OF_C_F_M_X = "80019";
        public const string IMSI_NUMBER_IS_MANDATORY = "80020";


        public const string PUK_CODE_IS_MANDATORY = "80021";
        public const string Invalid_IMSI_NUMBER_length_it_should_be_15_digits = "80023";
        public const string INVALID_PUK_CODE_LENGTH_IT_SHOULD_BE_8_DIGITS = "80024";
        public const string OLD_SUBNO_IS_MANDATORY = "80025";

        public const string INCORRECT_OLD_SUBNO_FORMAT_IT_SHOULD_START_BY_DIGIT_5 = "80026";



        public const string CONTRNO_IS_MANDATORY = "80028";
    }


    public static class ErrorHandling
    {
        private static Dictionary<string, string> _mapping = null;
        private static object lockObj = new object();

        static ErrorHandling()
        {
            _mapping = new Dictionary<string, string>();

            _mapping.Add(ErrorCodes.Success, "Success");
            _mapping.Add(ErrorCodes.GenericError, "Generic Error");
            _mapping.Add(ErrorCodes.GenericEntityError, "Generic Entity Error");

            _mapping.Add(ErrorCodes.DealerDoesnotExist, "Distributer/Dealer not found!");

            _mapping.Add(ErrorCodes.BarcodeReaderFailedToRecognizeImage, "Barcode reader failed to recognize image");
            _mapping.Add(ErrorCodes.BarcodeReaderNoDataWasRecognizedPageOne, "Barcode reader No data was recognized in page one!");
            _mapping.Add(ErrorCodes.BarcodeReaderNoDataWasRecognizedPageTwo, "Barcode reader No data was recognized in page two!");
            _mapping.Add(ErrorCodes.BarcodeReaderOnlyOnePageFound, "Barcode reader only one page found!");
            _mapping.Add(ErrorCodes.BarcodeReaderMoreThanOneBarcodeFoundPageOne, "Barcode reader more than one barcode found in page one!");
            _mapping.Add(ErrorCodes.BarcodeReaderMoreThanOneBarcodeFoundPageTwo, "Barcode reader more than one barcode found in page two!");
            _mapping.Add(ErrorCodes.BarcodeReaderPageBarcodesDontMatch, "Barcode reader page barcodes don't match!");
            _mapping.Add(ErrorCodes.BarcodeReaderInvalidIMSICheckDigit, "Barcode reader invalid IMSI check digit!");
            _mapping.Add(ErrorCodes.InvalidIMSINewIMSIofSIMReplacement, "Invalid IMSI - a new IMSI of a SIM replacement!");
            _mapping.Add(ErrorCodes.IMSINotValidInNobill, "IMSI not valid in Nobill!");
            _mapping.Add(ErrorCodes.MSISDNNotValidInNobill, "MSISDN not valid in Nobill!");
            _mapping.Add(ErrorCodes.InvalidCustomerId, "Invalid customer Id!");
            _mapping.Add(ErrorCodes.ActivationRequestIsRedundant, "The activation request is redundant for the provided IMSI!");
            _mapping.Add(ErrorCodes.IMSINotActivated, "The IMSI is not active!");
            _mapping.Add(ErrorCodes.IMSIAlreadyChangedOwnerOnce, "The IMSI had a change owner before!");

            _mapping.Add(ErrorCodes.LINE_Already_reserved, "line already reserved!");
        }

        public static void HandleException(string methodName, Exception ex, string username, bool rethrow = false)
        {
            string errorCode = string.Empty;
            string errorType = string.Empty;
            string errorMessage = string.Empty;
            try
            {
                if (ex is LPBizException)
                {
                    LPBizException lpBizEx = (LPBizException)ex;
                    errorCode = lpBizEx.Code;
                    errorType = ErrorTypes.BusinessError;
                    if (string.IsNullOrWhiteSpace(lpBizEx.Message))
                    {
                        errorMessage = _mapping[errorCode];
                    }
                    else
                    {
                        errorMessage = lpBizEx.Message;
                    }
                }
                else if (ex is EntityException)
                {
                    errorCode = ErrorCodes.GenericEntityError;
                    errorType = ErrorTypes.GenericEntityError;
                    errorMessage = ex.Message;
                }
                else // Exception
                {
                    errorCode = ErrorCodes.GenericError;
                    errorType = ErrorTypes.GenericError;
                    errorMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        errorMessage += "\n InnerEx: " + ex.InnerException.Message + "\n InnerStack: " + ex.InnerException.StackTrace;
                    }
                }


                using (Entities context = new Entities())
                {
                    ErrorLog log = new ErrorLog();

                    log.DateTime = System.DateTime.Now;
                    log.ErrorDescription = errorMessage;
                    log.ErrorType = errorType;
                    log.ExtraDetails = ex.ToString();
                    log.Source = methodName;
                    log.Username = username;

                    context.ErrorLogs.Add(log);
                    context.SaveChanges();
                }
            }
            catch (Exception severeEx)
            {
                //In case we couldn't log to database we log to Event Viewer
                //System.Diagnostics.EventLog.WriteEntry("Activation Engine", severeEx.ToString() + "\nPrevious Exception:\n" + ex.ToString());
                lock (lockObj)
                {
                    string errorContent = String.Concat("Orginal Error : ", errorMessage, Environment.NewLine, "Orginal Stack Trace: ", ex.StackTrace, Environment.NewLine, "Logging Exception:", severeEx.ToString());
                    File.AppendAllText(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/"), "ErrorLogs.txt"), String.Concat(Environment.NewLine, DateTime.Now.ToString(), ": ", errorContent, errorMessage, Environment.NewLine, "-------------------------------------------------------------------"));
                }     
            }

            if (rethrow)
            {
                throw new FaultException(errorMessage, new FaultCode(errorCode));
            }
        }

        public static void HandleDealerHiringException(string methodName, Exception ex, string username, bool rethrow = false)
        {
            string errorCode = string.Empty;
            string errorType = string.Empty;
            string errorMessage = string.Empty;
            try
            {
                if (ex is LPBizException)
                {
                    LPBizException lpBizEx = (LPBizException)ex;
                    errorCode = lpBizEx.Code;
                    errorType = ErrorTypes.BusinessError;
                    if (string.IsNullOrWhiteSpace(lpBizEx.Message))
                    {
                        errorMessage = _mapping[errorCode];
                    }
                    else
                    {
                        errorMessage = lpBizEx.Message;
                    }
                }
                else if (ex is EntityException)
                {
                    errorCode = ErrorCodes.GenericEntityError;
                    errorType = ErrorTypes.GenericEntityError;
                    errorMessage = ex.Message;
                }
                else // Exception
                {
                    errorCode = ErrorCodes.GenericError;
                    errorType = ErrorTypes.GenericError;
                    errorMessage = ex.Message;
                    if (ex.InnerException != null)
                    {
                        errorMessage += "\n InnerEx: " + ex.InnerException.Message + "\n InnerStack: " + ex.InnerException.StackTrace;
                    }
                }


                using (Entities context = new Entities())
                {
                    ErrorLog log = new ErrorLog();

                    log.DateTime = System.DateTime.Now;
                    log.ErrorDescription = errorMessage;
                    log.ErrorType = errorType;
                    log.ExtraDetails = ex.ToString();
                    log.Source = methodName;
                    log.Username = username;

                    context.ErrorLogs.Add(log);
                    context.SaveChanges();
                }
            }
            catch (Exception severeEx)
            {
                //In case we couldn't log to database we log to Event Viewer
                //System.Diagnostics.EventLog.WriteEntry("Activation Engine", severeEx.ToString() + "\nPrevious Exception:\n" + ex.ToString());
                lock (lockObj)
                {
                    string errorContent = String.Concat("Orginal Error : ", errorMessage, Environment.NewLine, "Orginal Stack Trace: ", ex.StackTrace, Environment.NewLine, "Logging Exception:", severeEx.ToString());
                    File.AppendAllText(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/"), "ErrorLogs.txt"), String.Concat(Environment.NewLine, DateTime.Now.ToString(), ": ", errorContent, errorMessage, Environment.NewLine, "-------------------------------------------------------------------"));
                }
            }

            if (rethrow)
            {
                throw new FaultException(errorMessage, new FaultCode(errorCode));
            }
        }

        public static void AddToErrorLog(string errorType, string description, string source, string username, string extraDetails = "")
        {
            try
            {
                ErrorLog oErrorLog = null;

                using (Entities context = new Entities())
                {
                    oErrorLog = new ErrorLog();

                    oErrorLog.DateTime = System.DateTime.Now;
                    oErrorLog.ErrorDescription = description;
                    oErrorLog.ErrorType = errorType;
                    oErrorLog.ExtraDetails = extraDetails;
                    oErrorLog.Source = source;
                    oErrorLog.Username = username;

                    context.ErrorLogs.Add(oErrorLog);
                    context.SaveChanges();
                }
            }
            catch (Exception severeEx)
            {
                //In case we couldn't log to database we log to Event Viewer
                //System.Diagnostics.EventLog.WriteEntry("Activation Engine", severeEx.ToString() + "\nPrevious Log:\n" +
                //    "ErrorType: " + errorType + "Description: " + description + "Source: " + source + "Extra Details: " + extraDetails);
                lock (lockObj)
                {
                    string errorContent = String.Concat("Orginal Error : ", description, Environment.NewLine, "Orginal Stack Trace: ", extraDetails, Environment.NewLine, "Logging Exception:", severeEx.ToString());
                    var currentDirectory = System.Web.Hosting.HostingEnvironment.MapPath("~/") == null ? System.AppDomain.CurrentDomain.BaseDirectory : System.Web.Hosting.HostingEnvironment.MapPath("~/");
                    File.AppendAllText(Path.Combine(currentDirectory, "ErrorLogs.txt"), String.Concat(Environment.NewLine, DateTime.Now.ToString(), ": ", errorContent, Environment.NewLine, "-------------------------------------------------------------------"));
                }
            }
        }
    }
}