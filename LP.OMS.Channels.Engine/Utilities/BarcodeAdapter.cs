﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using BarcodeLib.BarcodeReader;
using LP.Core.Utilities.Exceptions;
using System.Drawing.Imaging;
using System.Configuration;

namespace LP.OMS.Channels.Engine.Utilities
{
    public class BarcodeAdapter
    {
        public static List<ImagePages> Read(List<byte[]> imageBuffers)
        {
            List<ImagePages> imagePagesList = new List<ImagePages>();

            if (imageBuffers == null || imageBuffers.Count == 0)
            {
                return imagePagesList;
            }

            foreach (byte[] imageBuffer in imageBuffers)
            {
                imagePagesList.AddRange(Read(imageBuffer));
            }

            return imagePagesList;
        }

        public static List<ImagePages> Read(byte[] image1Buffer, byte[] image2Buffer)
        {
            List<ImagePages> imagePagesList = Read(image1Buffer);
            imagePagesList.AddRange(Read(image2Buffer));
            return imagePagesList;
        }

        public static List<ImagePages> Read(byte[] imageBuffer)
        {
            List<ImagePages> imagePagesList = new List<ImagePages>();

            if (imageBuffer == null || imageBuffer.Length == 0)
            {
                return imagePagesList;
            }

            //Convert to image...
            Image image = null;
            using (MemoryStream imageMemoryStream = new MemoryStream(imageBuffer))
            {
                image = Image.FromStream(imageMemoryStream);

                //Split the image should it have more than one page
                FrameDimension frameDimension = new FrameDimension(image.FrameDimensionsList[0]);
                int numberOfFrames = image.GetFrameCount(frameDimension);

                for (int frameIndex = 0; frameIndex < numberOfFrames; frameIndex++)
                {
                    image.SelectActiveFrame(frameDimension, frameIndex);
                    Bitmap pageBitmap = new Bitmap(image);

                    string[] pageBarcodesArray = null;
                    List<string> newPageBarcodesArray = new List<string>();
                    try
                    {
                        pageBarcodesArray = BarcodeReader.read(pageBitmap, BarcodeReader.CODE128);
                        if (pageBarcodesArray != null)
                        {
                            foreach (string item in pageBarcodesArray)
                            {
                                if (item.Length == 15 || item.Length == 16)
                                {
                                    newPageBarcodesArray.Add(item);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new LPBizException(ErrorCodes.BarcodeReaderFailedToRecognizeImage,
                            "Barcode reader failed to recognize image page: " + frameIndex + 1 + "\nDetails: " + ex.ToString());
                    }

                    ImagePages imagePages = new ImagePages();
                    imagePages.PageIndex = frameIndex;
                    imagePages.PageBitmap = pageBitmap;
                    if (newPageBarcodesArray != null && newPageBarcodesArray.Count != 0)
                    {
                        foreach(String item in newPageBarcodesArray)
                        {
                            imagePages.Barcodes.Add(FixBarcodeFirstDigit(item));
                        }
                    }

                    imagePagesList.Add(imagePages);
                }
            }

            return imagePagesList;
        }

        public static void ValidateBarcodesList(List<ImagePages> pageBarcodesList)
        {
            string errorCode = ErrorCodes.Success;

            if (pageBarcodesList.Count == 0)
            {
                errorCode = ErrorCodes.BarcodeReaderFailedToRecognizeImage;
            }

            //TODO: what to do when we have more than 2 pages? now we're taking the first 2 anyways..
            //We also reject the page if it has more than one barcode..

            if (pageBarcodesList.Count == 1)
            {
                errorCode = ErrorCodes.BarcodeReaderOnlyOnePageFound;
            }

            else if (pageBarcodesList[0].Barcodes.Count == 0)
            {
                errorCode = ErrorCodes.BarcodeReaderNoDataWasRecognizedPageOne;
            }

            else if (pageBarcodesList[1].Barcodes.Count == 0)
            {
                errorCode = ErrorCodes.BarcodeReaderNoDataWasRecognizedPageTwo;
            }

            else if (pageBarcodesList[0].Barcodes.Count > 1)
            {
                errorCode = ErrorCodes.BarcodeReaderMoreThanOneBarcodeFoundPageOne;
            }

            else if (pageBarcodesList[1].Barcodes.Count > 1)
            {
                errorCode = ErrorCodes.BarcodeReaderMoreThanOneBarcodeFoundPageTwo;
            }

            else if (pageBarcodesList[0].Barcodes[0] != pageBarcodesList[1].Barcodes[0])
            {
                errorCode = ErrorCodes.BarcodeReaderPageBarcodesDontMatch;
            }

            else if (!CheckDigit(pageBarcodesList[0].Barcodes[0]))
            {
                errorCode = ErrorCodes.BarcodeReaderInvalidIMSICheckDigit;
            }
            else
            {
                if (pageBarcodesList[0].Barcodes[0].Length > 15)
                {
                    pageBarcodesList[0].Barcodes[0] = pageBarcodesList[1].Barcodes[0] = pageBarcodesList[0].Barcodes[0].Substring(0, pageBarcodesList[0].Barcodes[0].Length - 1);
                }
            }

            if (errorCode != ErrorCodes.Success)
            {
                throw new LPBizException(errorCode);
            }
        }

        private static string FixBarcodeFirstDigit(string barcode)
        {
            return "4" + barcode.Substring(1);
        }

        private static bool CheckDigit(string imsi)
        {
            try
            {
                //as a big number of imsi stickers comes without check digit so we skip the chack digit validation 
                if (imsi.Length == 15 || imsi.Length == 16)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                String idWithoutCheckdigit = imsi.Substring(0, imsi.Length - 1);
                int chkDegitVlaue = int.Parse(imsi.Substring(imsi.Length - 1));

                // allowable characters within identifier
                string validChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVYWXZ_";

                // remove leading or trailing whitespace, convert to uppercase
                idWithoutCheckdigit = idWithoutCheckdigit.Trim().ToUpper();

                // this will be a running total
                int sum = 0;

                // loop through digits from right to left
                for (int i = 0; i < idWithoutCheckdigit.Length; i++)
                {

                    //set ch to "current" character to be processed
                    char ch = idWithoutCheckdigit.ToCharArray()[idWithoutCheckdigit.Length - i - 1];

                    // throw exception for invalid characters
                    if (validChars.IndexOf(ch) == -1)
                        return false;

                    // our "digit" is calculated using ASCII value - 48
                    int digit = (int)ch - 48;

                    // weight will be the current digit's contribution to
                    // the running total
                    int weight;
                    if (i % 2 == 0)
                    {
                        // for alternating digits starting with the rightmost, we
                        // use our formula this is the same as multiplying x 2 and
                        // adding digits together for values 0 to 9.  Using the 
                        // following formula allows us to gracefully calculate a
                        // weight for non-numeric "digits" as well (from their 
                        // ASCII value - 48).
                        weight = (2 * digit) - (int)(digit / 5) * 9;

                    }
                    else
                    {

                        // even-positioned digits just contribute their ascii
                        // value minus 48
                        weight = digit;

                    }

                    // keep a running total of weights
                    sum += weight;

                }

                // avoid sum less than 10 (if characters below "0" allowed,
                // this could happen)
                sum = Math.Abs(sum) + 10;

                // check digit is amount needed to reach next number
                // divisible by ten
                return chkDegitVlaue == ((10 - (sum % 10)) % 10);
            }
            catch
            {
                return false;
            }
        }
    }

    public class ImagePages
    {
        public int PageIndex = -1;
        public List<string> Barcodes = new List<string>();
        public Bitmap PageBitmap = null;
    }
}