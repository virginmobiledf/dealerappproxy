﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.Channels.Engine
{
    public class User
    {
        public string UserName { get; set; }
        public bool IsActive { get; set; }
        public string FingerprintSerial { get; set; }
        public string DeviceMacAddress { get; set; }
        public string DeviceImei { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public DateTime StartTimePeriod1 { get; set; }
        public DateTime EndTimePeriod1 { get; set; }
        public DateTime StartTimePeriod2 { get; set; }
        public DateTime EndTimePeriod2 { get; set; }
        public string ContactMSISDN { get; set; }
        public int GeofenceRadius { get; set; }
    }
}
