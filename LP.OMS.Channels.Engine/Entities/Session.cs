﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.Channels.Engine
{
    public class Session
    {
        public string SessionToken { get; set; }
        public string SessionCode { get; set; }
        public DateTime Expires { get; set; }
        public string Language { get; set; }
        public string Channel { get; set; }
        public User User { get; set; }
        public string UsedSematiToken { get; set; }
        public string UsedFingerprintSerial { get; set; }
        public string UsedLat { get; set; }
        public string UsedLon { get; set; }
        public bool Is24HourBranch { get; set; }
        public string LoginAttemps { get; set; }
        public int MinimumFRiENDiAllowedDenomination { get; set; }
        public string AttendanceId { get; set; }
        public int MinimumVirginAllowedDenomination { get; set; }
    }

}
