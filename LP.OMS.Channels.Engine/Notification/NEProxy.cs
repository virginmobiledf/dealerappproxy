﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace LP.OMS.Channels.Engine.Notification
{
    [DataContract]
    public class NEPostEventData
    {
        [DataMember]
        public List<EventParam> EventParam { get; set; }
    }

    public class EventParam
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Value { get; set; }

    }
    public class NEProxy
    {
        private static readonly string NotificationURL = ConfigurationManager.AppSettings["NEServicePath"];

        public static bool CallNotification(string ApiName, string responseStatusCode, string MSISDN, NEPostEventData Param, string SysCallerId = "21")
        {
            if (Param.EventParam == null)
                Param.EventParam = new List<EventParam>();

            string postJoson = JsonConvert.SerializeObject(Param);
            string callNotificationURL = ConfigurationManager.AppSettings["NE.CallNotificationURL"];
            string apiUri = string.Format(callNotificationURL, ApiName, SysCallerId, responseStatusCode, MSISDN);
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(NotificationURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    HttpResponseMessage response = PostAsJson(Param, apiUri, client);

                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        LogResponse(response, ApiName, Param, apiUri);
                        return true;
                    }
                    else
                    {
                        LogResponse(response, ApiName, Param, apiUri);
                    }
                }
            }
            catch (Exception ex)
            {
                string exceptionDetails = $"ApiName={ApiName} ,responseStatusCode={responseStatusCode} ,MSISDN={MSISDN}, Param={postJoson}";
                using (AttendenceEntities context = new AttendenceEntities())
                {
                    context.AddNotificationLog(DateTime.Now, exceptionDetails,
                        JsonConvert.SerializeObject(ex),
                       false, ApiName, apiUri);
                }
            }

            return false;
        }

        private static HttpResponseMessage PostAsJson(object Param, string apiUri, HttpClient client)
        {
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var myContent = JsonConvert.SerializeObject(Param);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = client.PostAsync(apiUri, byteContent).Result;
            return response;
        }

        private static void LogResponse(HttpResponseMessage response, string api, NEPostEventData data, string apiUri)
        {
            using (AttendenceEntities context = new AttendenceEntities())
            {
                context.AddNotificationLog(DateTime.Now, JsonConvert.SerializeObject(data),
                    JsonConvert.SerializeObject(response.Content.ReadAsStringAsync()),
                    response.IsSuccessStatusCode, api, apiUri);
            }
        }

        private static void AddErrorLog(string description, string stackTrace, DateTime datetime, int attendanceID)
        {
            using (AttendenceEntities context = new AttendenceEntities())
            {
                context.AddErrorLog(description, stackTrace, datetime, attendanceID);
            }
        }

        public static bool UpdateCustomerPreferences(UpdateCustomerPreferencesRequest objUpdateCustomerPreferencesRequest, int attendanceID)
        {
            var Request = new JavaScriptSerializer().Serialize(objUpdateCustomerPreferencesRequest);
            string updateCustomerPreferencesURL = ConfigurationManager.AppSettings["NE.UpdateCustomerPreferencesURL"];
            string MSISDN = objUpdateCustomerPreferencesRequest.MSISDN.FirstOrDefault();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(NotificationURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    HttpResponseMessage response = PostAsJson(objUpdateCustomerPreferencesRequest, updateCustomerPreferencesURL, client);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync();
                        result.Wait();
                        UpdateCustomerPreferencesResponse Result = JsonConvert.DeserializeObject<UpdateCustomerPreferencesResponse>(result.Result);
                        var Response = new JavaScriptSerializer().Serialize(Result);
                        if (Result.Code == "0")
                        {
                            AddErrorLog("Notification-UpdateCustomerPreferencesRequest-Success", $"MSISDN{MSISDN}, Caller:UpdateCustomerPreferences, ResultCode={ Result.Code } ,MSISDN={MSISDN}, Param={Request}", DateTime.Now, attendanceID);
                            return true;
                        }
                        else
                        {
                            AddErrorLog($"Notification-UpdateCustomerPreferencesRequest", $"MSISDN{MSISDN}, Caller:UpdateCustomerPreferences,  message={Result.Message} ,responseStatusCode={ Result.Code } ,MSISDN={MSISDN}, Param={Request}", DateTime.Now, attendanceID);
                            return false;
                        }
                    }
                    else
                    {
                        AddErrorLog($"Notification-UpdateCustomerPreferencesRequest", $"MSISDN{MSISDN}, Caller:UpdateCustomerPreferences,  Response={ response.StatusCode.ToString()}, Param={Request}", DateTime.Now, attendanceID);
                        return false;
                    }

                }
            }
            catch (HttpRequestException e)
            {
                AddErrorLog($"Notification-UpdateCustomerPreferencesRequest", $"MSISDN{MSISDN},  Caller: UpdateCustomerPreferencesMSISDN={MSISDN}, Exception={ e.Message}, Param={Request}", DateTime.Now, attendanceID);
                return false;
            }
            catch (Exception ex)
            {
                AddErrorLog($"Notification-UpdateCustomerPreferencesRequest", $"MSISDN{MSISDN},  Caller: UpdateCustomerPreferencesMSISDN={MSISDN}, Exception={ ex.Message}, Param={Request}", DateTime.Now, attendanceID);
                return false;
            }
        }
    }
}
