﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace LP.OMS.Channels.Engine
{
    public class POContextScope : IDisposable
    {
        private string sourceHeaderName = ConfigurationManager.AppSettings["SourceHeaderName"];
        private string sourceHeaderNS = ConfigurationManager.AppSettings["SourceHeaderNS"];
        private string sourceHeaderValue = ConfigurationManager.AppSettings["SourceHeaderValue"];
        private OperationContextScope _operationContextScope;

        public POContextScope(IClientChannel innerChannel)
        {
            _operationContextScope = new OperationContextScope(innerChannel);

            MessageHeader header = MessageHeader.CreateHeader(sourceHeaderName, sourceHeaderNS, sourceHeaderValue);
            OperationContext.Current.OutgoingMessageHeaders.Add(header);
        }

        public void Dispose()
        {
            _operationContextScope.Dispose();
        }
    }
}
