﻿using LP.Core.Utilities.Exceptions;
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.Enums;
using LP.OMS.Channels.Engine.AdministrationServices;
using LP.OMS.Channels.Engine.Utilities;
using LP.OMS.VirginKSA.Common.Utilities.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Resources;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace LP.OMS.Channels.Engine
{
    public static class DealerHiringController
    {
        private static readonly double CurrentAndriodApplicationMinimalVersion = 0;
        private static string _oMSDropFolderPath = string.Empty;
        private static readonly int _documentTypesLookupReferrence = 21;
        private static readonly int _reasonLookupReferrence = 19;
        private static readonly int _resetPasswordMessageTemplate = 23;
        private static readonly int _resetPasswordMessageTemplateSub = 135;
        private static readonly int _landMarkPrefix = 24;
        private static readonly double CurrentIOSApplicationVersion = 0;
        private static readonly double CurrentIOSApplicationMinimalVersion = 0;
        private static readonly string IOSApplicationUrl = "";
        private static readonly double CurrentAndriodApplicationVersion = 0;
        private static readonly string AndriodApplicationUrl = "";
        private static readonly List<string> OnSuccessRequestEmails = new List<string>();

        private static ObjectParameter _existingDocumentTypes = new ObjectParameter("documentTypes", typeof(string));
        private static ObjectParameter _existingDocumentFiles = new ObjectParameter("documentFiles", typeof(string));
        private static ObjectParameter _responseCode = new ObjectParameter("responseCode", typeof(string));

        private static void RebindObjectParamters()
        {
            _existingDocumentTypes = new ObjectParameter("documentTypes", typeof(string));
            _existingDocumentFiles = new ObjectParameter("documentFiles", typeof(string));
            _responseCode = new ObjectParameter("responseCode", typeof(string));
        }

        static DealerHiringController()
        {
            CurrentAndriodApplicationMinimalVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationMinimalVersion"]);
            _oMSDropFolderPath = ConfigurationManager.AppSettings["FRiENDi.ActivationEngine.WcfServices.IntegrationProxy.OATDropFolderPath"];
            CurrentIOSApplicationVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.IOSApplicationVersion"]);
            CurrentIOSApplicationMinimalVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.IOSApplicationMinimalVersion"]);
            IOSApplicationUrl = ConfigurationManager.AppSettings["LP.OMS.Channels.IOSApplicationUrl"];
            CurrentAndriodApplicationVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationVersion"]);
            AndriodApplicationUrl = ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationUrl"];

            string emails = ConfigurationManager.AppSettings["OnSuccessRequestEmails"];

            if (!string.IsNullOrWhiteSpace(emails))
            {
                OnSuccessRequestEmails = emails.Split(',').ToList();
            }
        }

        public static DealerHiringLoginResponse Login(DHLoginRequest request)
        {
            DealerHiringLoginResponse oLoginResponse = new DealerHiringLoginResponse()
            {
                UserPrivilageList = new List<int>()
            };

            AdministrationServices.Requester requester = null;
            string authenticationResponseMessage = string.Empty;

            try
            {
                if (!IsAuthenticRequest(request, out requester, out authenticationResponseMessage))
                {
                    return new DealerHiringLoginResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    var result = DbContext.GetDealer(requester.Code, null, null, null).FirstOrDefault();
                    if (result != null)
                    {
                        oLoginResponse.DefaultSalesPersonCode = result.DefaultSalesPersonCode;
                        oLoginResponse.UserTypeID = result.UserTypeId;
                        oLoginResponse.ChannelTypeID = result.ChannelTypeID.HasValue ? result.ChannelTypeID.Value : 0;
                        oLoginResponse.LanguageIsEnglish = result.LanguagePreferenceIsEnglish.HasValue ? result.LanguagePreferenceIsEnglish.Value : false;
                        oLoginResponse.RegionID = result.RegionID.HasValue ? result.RegionID.Value : 0;
                        oLoginResponse.CityID = result.CityId.HasValue ? result.CityId.Value : 0; 
                        oLoginResponse.Code = result.Code;
                        oLoginResponse.UserConfiguration = File.ReadAllText(ConfigurationManager.AppSettings["UserConfigurationFile"]);
                        oLoginResponse.ParentCode = result.ParentCode;
                        oLoginResponse.UserName = result.UserName;

                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["VirginCRNumber"]))
                            oLoginResponse.CRNumber = ConfigurationManager.AppSettings["VirginCRNumber"];


                        if (requester.Rights != null && requester.Rights.Any())
                        {
                            foreach (var right in requester.Rights)
                            {
                                oLoginResponse.UserPrivilageList.Add(int.Parse(right.Code));
                            }
                        }
                        oLoginResponse.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                        oLoginResponse.Status = true;
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                oLoginResponse.Status = false;
                oLoginResponse.Message = ExceptionHandling.HandleBackendException(LPBizEx, request.language);
            }
            catch (FaultException ex)
            {
                LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                oLoginResponse.Status = false;
                oLoginResponse.Message = ExceptionHandling.HandleBackendException(LPBizEx, request.language);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Login", ex, request.Username);
                oLoginResponse.Status = false;
            }

            return oLoginResponse;
        }

        public static bool IsAuthenticRequest(IAuthenticatableRequest AuthenticatableRequest,
            out AdministrationServices.Requester oRequester, out string authenticationResponseMessage)
        {
            string name = "";

            double appVersion = double.Parse(AuthenticatableRequest.oLoginRequest.ApplicationVersion);

            CallResponse callResponse = Controller.CheckRequester(AuthenticatableRequest.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

            authenticationResponseMessage = callResponse.ResponseMessage;

            return callResponse.IsPassed;
        }

        #region "Requests"

        public static DealerCreationResponse AddDealerCreationRequest(DealerCreationContractRequest request)
        {
            RebindObjectParamters();
            RequestType emailTemplateBasedOnRequestType = RequestType.DealerCreateRequest;
            string documentFiles = string.Empty, documentTypeIds = string.Empty, documentTypeNames = string.Empty, authenticationResponseMessage = string.Empty;
            AdministrationServices.Requester requester = null;
            bool EnableIDNumberValidation = true;

            try
            {
                if (!IsAuthenticRequest(request, out requester, out authenticationResponseMessage))
                {
                    return new DealerCreationResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SkipIDNumberValidationForDealersCode"]))
                {
                    string SkipDealers = ConfigurationManager.AppSettings["SkipIDNumberValidationForDealersCode"];

                    List<string> dealersList = SkipDealers.Split(',').ToList();

                    if (dealersList != null && dealersList.Count > 0)
                    {
                        if (dealersList.Contains(requester.Code))
                            EnableIDNumberValidation = false;
                    }
                }

                if (EnableIDNumberValidation)
                {
                    if (!IsValidIDNumber(request.SaudiID))
                    {
                        return new DealerCreationResponse
                        {
                            Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Not_Valid_SaudiID, request.oLoginRequest.language),
                            Status = false
                        };
                    }
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    if (request.IsResubmitted)
                    {
                        try
                        {
                            DbContext.DiscardCreateDealerRequest(request.Id, _existingDocumentFiles, _existingDocumentTypes, _responseCode);
                            emailTemplateBasedOnRequestType = RequestType.DealerCreationRequestResubmission;
                        }
                        catch (Exception ex)
                        {
                            ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AddDealerCreationRequest-DiscardCreateDealerRequest " + request.Id, ex.Message, ex.Source);
                            throw ex;
                        }

                    }

                    List<string> dtosIds = new List<string>();
                    try
                    {
                        if (request.DocumentsList != null && request.DocumentsList.Count > 0)
                        {
                            var dtos = DealerHiringHelper.GetDealerDocumentDtos(request.DocumentsList,
                           _existingDocumentTypes, _existingDocumentFiles,
                           $"AddDealerCreationRequest {request.SaudiID}",
                           request.oLoginRequest.Username, _documentTypesLookupReferrence,
                            out documentFiles, out documentTypeIds, out documentTypeNames);

                            if (dtos != null)
                            {
                                dtosIds = dtos.Select(e => e.FileId).ToList();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AddDealerCreationRequest-GetDealerDocumentDtos", request.oLoginRequest.Username, ex.Source);
                    }
                    try
                    {

                        if (request.UserTypeId == 65 && !string.IsNullOrEmpty(request.LocationID)) // Indirect Sale >> we will get location details from geo master
                        {
                            DHLocation locationDetails = LocationController.GetLocationByID(request.LocationID, request.oLoginRequest.language, request.oLoginRequest.Username);

                            if (locationDetails != null && !string.IsNullOrEmpty(locationDetails.LocationId))
                            {
                                request.CityId = locationDetails.CityId;
                                request.RegionID = locationDetails.RegionId;
                                request.Location = locationDetails.Latitude + "," + locationDetails.Longitude;
                            }
                        }

                        // request.CityId , request.RegionID , request.Address , request.AddressCode

                        DbContext.CreateDealerRequest(requester.Code,
                            DealerHiringHelper.GetParentCode(request, requester),
                            request.UserTypeId,
                            request.RequesterClassId,
                            request.SubChannelTypeID,
                            request.Name,
                            request.Email,
                            request.FaxNumber,
                            request.RegionID,
                            request.SaudiID,
                            request.Address,
                            request.ContactPerson,
                            request.RequesterClassId,
                            request.LanguagePreferenceIsEnglish,
                            request.ContactNos,
                            request.IsKeyAccount,
                            request.PoSTypeID,
                            request.DistributerID,
                            request.HasCommission,
                            request.CityId,
                            request.Location,
                            request.GroupId,
                            request.ContactMSISDN,
                            request.DeviceIMEI,
                            request.DeviceMAC,
                            request.FPDeviceSerial,
                            request.GeofenceRadius,
                            true, false, 0, "COD",
                            request.DefaultSalesPersonCode,
                            request.GrCuClassClass,
                            request.PartnerName,
                            request.MunicipalityLicenseCode,
                            request.StoreOwnerCode,
                            request.RefillMSISDN,
                            request.PreviousCode,
                            DealerHiringHelper.GetLandMark(request.LandMarkClass, _landMarkPrefix), //request.LandMarkClass,  
                            request.AddressCode.ToString(),
                            request.OwnerSaudiIdNumber,
                            request.OwnerContactNumber,
                            request.SalesPerson,
                            request.CRNumber,
                            DateTime.Now,
                            string.Empty,
                            string.Empty,
                            (int)RequestStatus.Pending,
                            documentFiles,
                            documentTypeIds,
                            request.EmployeeID,
                            request.LocationID,
                            _responseCode);

                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AddDealerCreationRequest-CreateDealerRequest", request.oLoginRequest.Username, ex.Source);
                    }
                    try
                    {
                        int castedResponseCode = (int)_responseCode.Value;

                        switch (castedResponseCode)
                        {
                            case 1:
                            case 2:
                                DealerHiringErrorCode errorCode = castedResponseCode == 1 ?
                                    DealerHiringErrorCode.SaudiId_Already_Exist :
                                    DealerHiringErrorCode.Dealer_Already_Exist;

                                if (dtosIds != null && dtosIds.Count > 0)
                                {
                                    DealerHiringHelper.DeleteDocuments(dtosIds, "AddDealerCreationRequest");
                                }

                                return new DealerCreationResponse
                                {
                                    Message = DealerHiringHelper.GetDealerHiringMessage(errorCode, request.oLoginRequest.language)
                                };
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AddDealerCreationRequest-DeleteDocuments", request.oLoginRequest.Username, ex.Source);
                    }

                    try
                    {
                        DropFilesToOMS_AddUserCase(request.Name, request.Email,
                            request.FaxNumber,
                            request.RegionID, request.SaudiID, request.CRNumber, request.MunicipalityLicenseCode,
                            request.Address, request.ContactPerson,
                            request.RequesterClassId, request.RefillMSISDN, request.LanguagePreferenceIsEnglish,
                            request.ContactNos, request.IsKeyAccount, request.PoSTypeID,
                            request.DistributerID, request.RequesterClassId, request.HasCommission,
                            request.CityId, request.Location, request.GroupId, request.ContactMSISDN,
                            request.DeviceIMEI, request.DeviceMAC, request.FPDeviceSerial, request.GeofenceRadius,
                            0, false, true, "COD", request.DefaultSalesPersonCode,
                            request.OwnerSaudiIdNumber, request.UserTypeId.Value,
                            request.ParentCode, request.Code,
                            documentFiles, documentTypeNames, "Tablet", requester.Code, "Add User", "",
                            request.OwnerContactNumber,
                            request.SubChannelTypeID, request.StoreOwnerCode, request.PreviousCode,
                            DealerHiringHelper.GetLandMark(request.LandMarkClass, _landMarkPrefix),
                            request.AddressCode, request.PartnerName, request.GrCuClassClass, request.RequestStatus,
                            request.RequestType, request.SalesPerson, request.RejectionReason, request.EmployeeID, request.LocationID);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AddDealerCreationRequest-DropFilesToOMS", request.oLoginRequest.Username, ex.Source);
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
            emailTemplateData.Add("DealerCode", request.ParentCode);
            emailTemplateData.Add("SaudiId", request.SaudiID);
            OnSuccessRequestSubmission(emailTemplateBasedOnRequestType,
                RequestUpdateType.NotSet, request.oLoginRequest.Username,
                request.oLoginRequest.Username, emailTemplateData);

            return new DealerCreationResponse
            {
                Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language),
                Status = true
            };
        }

        public static DeActivateRequestResponse DeActivateRequest(DeActivateRequest request)
        {
            AdministrationServices.Requester requester = null;
            string authenticationResponseMessage = null;
            RequestUpdateType emailTemplateBasedOnRequestType = RequestUpdateType.Deactivate;

            RebindObjectParamters();

            if (!IsAuthenticRequest(request, out requester, out authenticationResponseMessage))
            {
                return new DeActivateRequestResponse
                {
                    Message = authenticationResponseMessage
                };
            }

            using (OMSEXEntities DbContext = new OMSEXEntities())
            {
                if (request.IsResubmitted)
                {
                    DbContext.DiscardUpdateDealerRequest(request.Id, _existingDocumentFiles, _existingDocumentTypes, _responseCode);
                    emailTemplateBasedOnRequestType = RequestUpdateType.DeactivateResubmission;
                }

                DbContext.DeactivateDealerRequest(requester.Code, request.Code, request.Reason, request.Note, "", "", _responseCode);

                int castedResponseCode = (int)_responseCode.Value;

                switch (castedResponseCode)
                {
                    case 1:
                    case 2:
                        DealerHiringErrorCode errorCode = castedResponseCode == 1 ?
                            DealerHiringErrorCode.De_Activate_Pending_Request_Already_exist :
                            DealerHiringErrorCode.Dealer_Already_Deactivated;

                        return new DeActivateRequestResponse
                        {
                            Message = DealerHiringHelper.GetDealerHiringMessage(errorCode, request.oLoginRequest.language)
                        };
                }

                try
                {
                    string reasonText = request.Note; // We agreed (Murad, Anas and Osama) on 2019-12-19 to have the reasin as free text
                    DropFilesToOMS_UpdateUserCase(request.Code, reasonText, "", "", requester.Code,
                        "Update User", "Deactivate User", "", "", "", "", "Tablet", request.Note);
                }
                catch (Exception ex)
                {
                    ErrorHandling.AddToErrorLog("DropFilesToOMS", ex.Message, "DeActivateRequest", request.oLoginRequest.Username, "");
                    throw ex;
                }

                Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
                emailTemplateData.Add("DealerCode", requester.Code);
                OnSuccessRequestSubmission(RequestType.DealerUpdateRequest,
                    emailTemplateBasedOnRequestType, request.oLoginRequest.Username,
                    request.oLoginRequest.Username, emailTemplateData);

                return new DeActivateRequestResponse
                {
                    Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language),
                    Status = true
                };
            }
        }

        public static ReActivateResponse ReActivateRequest(ReActivateRequest request)
        {
            string documentFiles, documentTypeIds, documentTypeNames, authenticationResponseMessage;
            AdministrationServices.Requester requester;
            RequestUpdateType emailTemplateBasedOnRequestType = RequestUpdateType.Reactivate;

            RebindObjectParamters();

            if (!IsAuthenticRequest(request, out requester, out authenticationResponseMessage))
            {
                return new ReActivateResponse
                {
                    Message = authenticationResponseMessage
                };
            }

            using (OMSEXEntities DbContext = new OMSEXEntities())
            {
                if (request.IsResubmitted)
                {
                    DbContext.DiscardUpdateDealerRequest(request.id, _existingDocumentFiles, _existingDocumentTypes, _responseCode);
                    emailTemplateBasedOnRequestType = RequestUpdateType.ReactivateResubmission;
                }

                var dtos = DealerHiringHelper.GetDealerDocumentDtos(request.documents,
                       _existingDocumentTypes, _existingDocumentFiles,
                       $"ReActivateRequest {request.code}",
                       request.oLoginRequest.Username, _documentTypesLookupReferrence,
                       out documentFiles, out documentTypeIds, out documentTypeNames);

                DbContext.ReactivateDealerRequest(requester.Code, request.code, request.reason, request.note,
                    documentFiles, documentTypeIds, _responseCode);

                int castedResponseCode = (int)_responseCode.Value;

                switch (castedResponseCode)
                {
                    case 1:
                    case 2:
                        DealerHiringErrorCode errorCode = castedResponseCode == 1 ?
                        DealerHiringErrorCode.Re_Activate_Pending_Request_Already_exist :
                        DealerHiringErrorCode.Dealer_Already_Activated;

                        DealerHiringHelper.DeleteDocuments(dtos.Select(e => e.FileId).ToList(), "ReActivateRequest");

                        return new ReActivateResponse
                        {
                            Message = DealerHiringHelper.GetDealerHiringMessage(errorCode, request.oLoginRequest.language)
                        };
                }

                try
                {
                    string reason = request.note; // We agreed (Murad, Anas and Osama) on 2019-12-19 to have the reasin as free text
                    DropFilesToOMS_UpdateUserCase(request.code, reason, documentFiles, documentTypeNames,
                        requester.Code, "Update User", "Reactivate User", "", "", "", "", "Tablet", request.note);
                }
                catch (Exception ex)
                {
                    ErrorHandling.AddToErrorLog("DropFilesToOMS", ex.Message, "ReActivateRequest", request.oLoginRequest.Username, "");
                    throw ex;
                }
            }

            Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
            emailTemplateData.Add("DealerCode", requester.Code);

            OnSuccessRequestSubmission(RequestType.DealerUpdateRequest,
                emailTemplateBasedOnRequestType, request.oLoginRequest.Username,
                request.oLoginRequest.Username, emailTemplateData);

            return new ReActivateResponse
            {
                Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success,
                request.oLoginRequest.language),
                Status = true
            };
        }

        public static DiscardResponse DiscardRequest(DiscardRequest request)
        {
            RebindObjectParamters();
            DiscardResponse response = new DiscardResponse();
            AdministrationServices.Requester oRequester = null;
            string authenticationResponseMessage = string.Empty;

            try
            {
                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new DiscardResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    if (request.RequestTypeID == RequestType.DealerCreateRequest)
                    {
                        DbContext.DiscardCreateDealerRequest(request.Id, _existingDocumentFiles, _existingDocumentTypes, _responseCode);
                    }
                    else if (request.RequestTypeID == RequestType.DealerUpdateRequest)
                    {
                        DbContext.DiscardUpdateDealerRequest(request.Id, _existingDocumentFiles, _existingDocumentTypes, _responseCode);
                    }

                    int castedResponseCode = (int)_responseCode.Value;

                    switch (castedResponseCode)
                    {
                        case 1:
                        case 2:
                            DealerHiringErrorCode errorCode = castedResponseCode == 1 ?
                            DealerHiringErrorCode.Discard_Pending_Request_Already_Exist :
                            DealerHiringErrorCode.Process_Already_Approved;
                            response.Message = DealerHiringHelper.GetDealerHiringMessage(errorCode,
                                request.oLoginRequest.language);
                            response.Status = false;
                            return response;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
            response.Status = true;

            Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
            emailTemplateData.Add("DealerCode", oRequester.Code);
            emailTemplateData.Add("RequestType", request.RequestTypeID.ToString());

            OnSuccessRequestSubmission(RequestType.DealerUpdateRequest, RequestUpdateType.Discard,
                request.oLoginRequest.Username, request.oLoginRequest.Username, emailTemplateData);

            return response;
        }

        public static DHBaseResponse UpdateSaudiID(UpdateSaudiIDRequest request)
        {
            RebindObjectParamters();
            DHBaseResponse response = new DHBaseResponse();
            AdministrationServices.Requester oRequester = null;
            RequestUpdateType emailTemplateBasedOnRequestType = RequestUpdateType.SaudiID; ;
            string documentFiles, documentTypeIds, documentTypeNames, authenticationResponseMessage;
            bool EnableIDNumberValidation = true;
            try
            {
                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new DHBaseResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SkipIDNumberValidationForDealersCode"]))
                {
                    string SkipDealers = ConfigurationManager.AppSettings["SkipIDNumberValidationForDealersCode"];

                    List<string> dealersList = SkipDealers.Split(',').ToList();

                    if (dealersList != null && dealersList.Count > 0)
                    {
                        if (dealersList.Contains(oRequester.Code))
                            EnableIDNumberValidation = false;
                    }

                }

                if (EnableIDNumberValidation)
                {
                    if (!IsValidIDNumber(request.saudiID))
                    {
                        return new DHBaseResponse
                        {
                            Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Not_Valid_SaudiID, request.oLoginRequest.language),
                            Status = false
                        };
                    }
                }


                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    if (request.IsResubmitted)
                    {
                        ObjectParameter discardCreateDealerResponseCode = new ObjectParameter("responseCode", typeof(string));
                        DbContext.DiscardUpdateDealerRequest(request.id, _existingDocumentFiles, _existingDocumentTypes, discardCreateDealerResponseCode);

                        emailTemplateBasedOnRequestType = RequestUpdateType.SaudiIDResubmission;
                    }

                    var dtos = DealerHiringHelper.GetDealerDocumentDtos(request.DocumentsList,
                       _existingDocumentTypes, _existingDocumentFiles,
                       $"UpdateSaudiID {request.saudiID}",
                       request.oLoginRequest.Username, _documentTypesLookupReferrence,
                       out documentFiles, out documentTypeIds, out documentTypeNames);

                    DbContext.UpdateSaudiIDRequest
                        (oRequester.Code, request.code, request.saudiID, request.name, request.contactMSISDN,
                        request.contactNos, request.reason, request.note, documentFiles, documentTypeIds, _responseCode);

                    int castedResponseCode = (int)_responseCode.Value;

                    switch (castedResponseCode)
                    {
                        case 1:
                        case 2:
                            DealerHiringErrorCode errorCode = castedResponseCode == 1 ?
                                DealerHiringErrorCode.Update_SaudiID_Pending_Request_Already_exist :
                                DealerHiringErrorCode.Update_OTP_MSISDN_Pending_Request_Already_exist;

                            response.Message = DealerHiringHelper.GetDealerHiringMessage(errorCode,
                                request.oLoginRequest.language);
                            response.Status = false;
                            DealerHiringHelper.DeleteDocuments(dtos.Select(e => e.FileId).ToList(), "UpdateSaudiID");
                            return response;
                    }

                    string reasonText = DealerHiringHelper.GetLookupValue(_reasonLookupReferrence, request.reason);

                    try
                    {
                        DropFilesToOMS_UpdateUserCase(request.code, reasonText, documentFiles,
                            documentTypeNames, oRequester.Code, "Update User", "Update Saudi ID",
                            request.saudiID, request.name, request.contactMSISDN, request.contactNos, "Tablet", request.note);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog("DropFilesToOMS", ex.Message, "UpdateSaudiID",
                            request.oLoginRequest.Username, "");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
            response.Status = true;

            Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
            emailTemplateData.Add("DealerCode", oRequester.Code);
            emailTemplateData.Add("NewID", request.saudiID);

            OnSuccessRequestSubmission(RequestType.DealerUpdateRequest, emailTemplateBasedOnRequestType,
                request.oLoginRequest.Username, request.oLoginRequest.Username, emailTemplateData);

            return response;
        }

        public static DHBaseResponse UpdateOTPMSISDN(UpdateOTPMSISDNRequest request)
        {
            RebindObjectParamters();
            string documentFiles, documentTypeIds, documentTypeNames, authenticationResponseMessage = null;
            AdministrationServices.Requester requester = null;

            try
            {
                if (!IsAuthenticRequest(request, out requester, out authenticationResponseMessage))
                {
                    return new DHBaseResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    if (request.IsResubmitted)
                    {
                        DbContext.DiscardUpdateDealerRequest(request.id, _existingDocumentFiles, _existingDocumentTypes, _responseCode);
                    }

                    var dtos = DealerHiringHelper.GetDealerDocumentDtos(request.documents,
                        _existingDocumentTypes, _existingDocumentFiles,
                        $"{nameof(UpdateOTPMSISDN)} {request.code} {request.id}",
                        request.oLoginRequest.Username, _documentTypesLookupReferrence,
                        out documentFiles, out documentTypeIds, out documentTypeNames);

                    DbContext.UpdateOTPMSISDNRequest
                        (requester.Code, request.code, request.contactMSISDN, request.reason, request.note,
                        documentFiles, documentTypeIds, _responseCode);

                    int castedResponseCode = (int)_responseCode.Value;

                    switch (castedResponseCode)
                    {
                        case 1:
                        case 2:
                            DealerHiringErrorCode errorCode = castedResponseCode == 1 ?
                              DealerHiringErrorCode.Update_OTP_MSISDN_Pending_Request_Already_exist :
                              DealerHiringErrorCode.Update_SaudiID_Pending_Request_Already_exist;

                            DealerHiringHelper.DeleteDocuments(dtos.Select(e => e.FileId).ToList(), "UpdateOTPMSISDN");
                            return new DHBaseResponse
                            {
                                Message = DealerHiringHelper.GetDealerHiringMessage(errorCode, request.oLoginRequest.language)
                            };
                    }

                    try
                    {
                        using (ExtendedServiceDH.ExtendedServiceClient client = new ExtendedServiceDH.ExtendedServiceClient())
                        {
                            client.UpdateOtpMssdn(request.code, request.contactMSISDN);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog("Call UpdateOtpMssdn", ex.Message, "UpdateOTPMSISDN", request.oLoginRequest.Username, "");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
            emailTemplateData.Add("DealerCode", requester.Code);
            emailTemplateData.Add("NEWMSISDN", request.contactMSISDN);

            OnSuccessRequestSubmission(RequestType.DealerUpdateRequest, RequestUpdateType.MSISDN_OTP,
                request.oLoginRequest.Username, request.oLoginRequest.Username, emailTemplateData);

            return new DHBaseResponse
            {
                Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language),
                Status = true
            }; ;
        }

        private static void OnSuccessRequestSubmission(RequestType requestType,
            RequestUpdateType requestUpdateType, string parentCode, string dealerCode,
            Dictionary<string, object> data = null)
        {
            if (OnSuccessRequestEmails.Any())
            {
                List<string> toEmai = new List<string>();
                SendRequestEmail(OnSuccessRequestEmails, requestType, requestUpdateType,
                    dealerCode, data);
            }
        }

        public static void SendRequestEmail(List<string> toEmail,
            RequestType requestType, RequestUpdateType requestUpdateType, string dealerCode,
            Dictionary<string, object> data = null)
        {
            MessageSender messageSender = null;
            switch (requestType)
            {
                case RequestType.DealerCreateRequest:
                    messageSender = new MessageSender(requestType.ToString(), MessageTypes.Email).BindAll(data);
                    break;
                case RequestType.DealerUpdateRequest:
                    messageSender = new MessageSender(requestUpdateType.ToString(), MessageTypes.Email).BindAll(data);
                    break;
            }
            if (messageSender != null)
                messageSender
                    .SetSender("Virgin")
                    .SetReceiver(dealerCode)
                    .SetReceivers(toEmail)
                    .Send();

        }
        #endregion
        public static LookupsList GetLookups(LookupsRequest request, bool checkRequester = true, bool getOMSLookups = true)
        {
            LookupsList lookupsList = new LookupsList();
            try
            {
                if (checkRequester)
                {
                    AdministrationServices.Requester oRequester = null;
                    string authenticationResponseMessage = string.Empty;

                    if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                    {
                        return new LookupsList
                        {
                            Lookups = new List<DealerHiringLookup>()
                        };
                    }
                }

                // we will return account manager and line manager
                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    List<Lookup> result = new List<Lookup>();
                    if (request.lookupId > 0)
                    {
                        result = DbContext.Lookups.Where(a => a.Id == request.lookupId).ToList();
                    }
                    else
                    {
                        result = DbContext.Lookups.ToList();
                    }

                    if (result != null && result.Any())
                    {
                        lookupsList.Lookups = new List<DealerHiringLookup>();

                        foreach (Lookup item in result)
                        {
                            DealerHiringLookup lookupsListItem = new DealerHiringLookup();
                            lookupsListItem.LookupId = item.Id;
                            lookupsListItem.LookupName = item.Name;
                            lookupsListItem.LookupName2 = item.Name2;
                            lookupsListItem.LookupType = item.Type;
                            lookupsListItem.LookupItems = new List<LookupItems>();
                            foreach (var LookupItem in item.LookupItems)
                            {
                                lookupsListItem.LookupItems.Add(new LookupItems
                                {
                                    Code = LookupItem.Code,
                                    LookupItemId = LookupItem.Id,
                                    LookupItemName = LookupItem.Name,
                                    LookupItemName2 = LookupItem.Name2,
                                    ReferenceIDs = LookupItem.ReferenceIDs
                                });
                            }

                            lookupsList.Lookups.Add(lookupsListItem);
                        }

                        try
                        {
                            // Remove account manager and distributor from lookups list, we have to maintain them in db cause OMS use them.
                            var am_d = lookupsList.Lookups.FirstOrDefault(x => x.LookupId == 3).LookupItems.Where(y => y.LookupItemId == 63).ToList();
                            am_d.ForEach(lookup => lookupsList.Lookups.FirstOrDefault(x => x.LookupId == 3).LookupItems.Remove(lookup));
                            //var dis = lookupsList.Lookups.FirstOrDefault(x => x.LookupId == 3).LookupItems.FirstOrDefault(y => y.LookupItemId == 63);
                            //lookupsList.Lookups.FirstOrDefault(x => x.LookupId == 3).LookupItems.Remove(dis);
                        }
                        catch (Exception ex) { }
                    }

                    if (getOMSLookups)
                    {
                        OMS3_AdministrationServices.OMSLanguages omsLanguage = request.oLoginRequest.language == 1 ? OMS3_AdministrationServices.OMSLanguages.Foreign : OMS3_AdministrationServices.OMSLanguages.Local;
                        List<OMS3_AdministrationServices.City> cityList = null;
                        List<OMS3_AdministrationServices.Region> regionList = null;
                        List<OMS3_AdministrationServices.PoSType> poSTypeList = null;
                        List<OMS3_AdministrationServices.RequesterGroup> groupList = null;
                        using (OMS3_AdministrationServices.AdministrationServicesClient client =
                            new OMS3_AdministrationServices.AdministrationServicesClient())
                        {
                            cityList = client.GetCityList().ToList();
                            regionList = client.GetRegionList().ToList();
                            poSTypeList = client.GetPoSTypeList(omsLanguage).ToList();
                            groupList = client.GetRequesterGroupList().ToList();
                        }

                        DealerHiringLookup lookupsListItemCity = new DealerHiringLookup();
                        lookupsListItemCity.LookupItems = new List<LookupItems>();
                        lookupsListItemCity.LookupId = 8;
                        lookupsListItemCity.LookupType = 1;
                        lookupsListItemCity.LookupName = "Semati City";
                        lookupsListItemCity.LookupName2 = "Semati City";
                        int cityCounter = 1;
                        foreach (var item in cityList)
                        {
                            if (item.Id == 0)
                                continue;// Remove Automated 
                            lookupsListItemCity.LookupItems.Add(new LookupItems
                            {
                                LookupItemId = item.Id,
                                LookupItemName = item.Name,
                                LookupItemName2 = item.ForeignName,
                                Code = cityCounter.ToString()
                            });
                            cityCounter++;
                        }
                        lookupsList.Lookups.Add(lookupsListItemCity);

                        DealerHiringLookup lookupsListItemRegion = new DealerHiringLookup();
                        lookupsListItemRegion.LookupItems = new List<LookupItems>();
                        lookupsListItemRegion.LookupId = 7;
                        lookupsListItemRegion.LookupType = 1;
                        lookupsListItemRegion.LookupName = "Region";
                        lookupsListItemRegion.LookupName2 = "Region";
                        int regionCounter = 1;
                        foreach (var item in regionList)
                        {
                            lookupsListItemRegion.LookupItems.Add(new LookupItems
                            {
                                LookupItemId = item.Id,
                                LookupItemName = item.Name,
                                LookupItemName2 = item.ForeignName,
                                Code = regionCounter.ToString()
                            });
                            regionCounter++;
                        }
                        lookupsList.Lookups.Add(lookupsListItemRegion);

                        DealerHiringLookup lookupsListItemPoSTypeList = new DealerHiringLookup();
                        lookupsListItemPoSTypeList.LookupItems = new List<LookupItems>();
                        lookupsListItemPoSTypeList.LookupId = 15;
                        lookupsListItemPoSTypeList.LookupType = 1;
                        lookupsListItemPoSTypeList.LookupName = "POSType";
                        lookupsListItemPoSTypeList.LookupName2 = "POSType";
                        int poSTypeCounter = 1;
                        foreach (var item in poSTypeList)
                        {
                            lookupsListItemPoSTypeList.LookupItems.Add(new LookupItems
                            {
                                LookupItemId = item.Id,
                                LookupItemName = item.Name,
                                LookupItemName2 = item.ForeignName,
                                Code = poSTypeCounter.ToString()
                            });
                            poSTypeCounter++;
                        }
                        lookupsList.Lookups.Add(lookupsListItemPoSTypeList);

                        DealerHiringLookup lookupsListItemGroup = new DealerHiringLookup();
                        lookupsListItemGroup.LookupItems = new List<LookupItems>();
                        lookupsListItemGroup.LookupId = 16;
                        lookupsListItemGroup.LookupType = 1;
                        lookupsListItemGroup.LookupName = "Group";
                        lookupsListItemGroup.LookupName2 = "Group";
                        int groupCounter = 1;
                        foreach (var item in groupList)
                        {
                            lookupsListItemGroup.LookupItems.Add(new LookupItems
                            {
                                LookupItemId = item.Id,
                                LookupItemName = item.Name,
                                LookupItemName2 = item.ForeignName,
                                Code = groupCounter.ToString()
                            });
                            groupCounter++;
                        }
                        lookupsList.Lookups.Add(lookupsListItemGroup);
                    }

                    return lookupsList;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #region "Get Request Methods"
        public static MyRequestsResponse GetMyRequests(MyRequestsRequest request)
        {
            MyRequestsResponse response = new MyRequestsResponse() { Result = new List<MyRequestsResult>() };

            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new MyRequestsResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    int? status = null;
                    if (request.Status != RequestStatus.NotSet)
                        status = (int)request.Status;
                    var dealerCreateUpdateRequests = DbContext.GetDealerCreateAndUpdateRequests(request.CreatedBy, null, null, null, null, null, null, status, null, null, null, null).ToList();
                    if (!dealerCreateUpdateRequests.Any())
                    {
                        response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.No_Available_Data, request.oLoginRequest.language);
                        response.Status = false;
                        return response;
                    }
                    foreach (var item in dealerCreateUpdateRequests)
                    {
                        MyRequestsResult result = new MyRequestsResult();
                        result.Id = item.Id;
                        result.CreatedBy = item.CreatedBy;
                        result.ParentCode = item.ParentCode;
                        result.UserTypeId = item.UserTypeId;
                        result.ChannelTypeID = item.ChannelTypeID;
                        result.SubChannelTypeID = item.SubChannelTypeID;
                        result.SaudiID = item.SaudiID;
                        result.Code = item.Code;
                        result.RequestStatus = (RequestStatus)item.RequestStatus;
                        result.CreationDate = item.CreationDate.ToString("dd/MM/yyyy");
                        result.RequestType = (RequestType)Enum.Parse(typeof(RequestType), item.RequestType);
                        result.RequestSubType = item.RequestSubType;

                        response.Result.Add(result);
                    }

                    response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                    response.Status = true;

                    return response;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static CreationDetailsResponse GetCreationRequestDetails(CreationDetailsRequest request)
        {
            CreationDetailsResponse response = new CreationDetailsResponse()
            {
                DocumentsList = new List<Documents>()
            };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new CreationDetailsResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    if (request.IsApprovedDealer)
                    {
                        var result = DbContext.GetDealer(request.Code, null, null, null).FirstOrDefault();

                        if (result != null)
                        {
                            response.UserTypeId = result.UserTypeId;
                            response.Address = result.Address;
                            response.MunicipalityLicenseCode = result.MunicipalityLicenseCode;
                            response.StoreOwnerCode = result.StoreOwnerCode;
                            response.CityId = result.CityId.HasValue ? result.CityId.Value : 0;
                            response.Code = result.Code;
                            response.ContactMSISDN = result.ContactMSISDN;
                            response.ContactNos = result.ContactNos;
                            response.ContactPerson = result.ContactPerson;
                            response.CreatedBy = result.CreatedBy;
                            response.CreationDate = result.CreationDate.HasValue ? result.CreationDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                            response.CreditLimit = result.CreditLimit;
                            response.DefaultSalesPersonCode = result.DefaultSalesPersonCode;
                            response.DeviceIMEI = result.DeviceIMEI;
                            response.DeviceMAC = result.DeviceMAC;
                            response.Email = result.Email;
                            response.FaxNumber = result.FaxNumber;
                            response.FPDeviceSerial = result.FPDeviceSerial;
                            response.GeofenceRadius = result.GeofenceRadius.HasValue ? result.GeofenceRadius.Value : 0;
                            response.GroupId = result.GroupId.HasValue ? result.GroupId.Value : 0;
                            response.GrCuClassClass = result.GrCuClassClass.HasValue ? result.GrCuClassClass.Value : 0;
                            response.PartnerName = result.PartnerName.HasValue ? result.PartnerName.Value : 0;
                            response.HasCommission = result.HasCommission.HasValue ? result.HasCommission.Value : false;
                            response.IsKeyAccount = result.IsKeyAccount.HasValue ? result.IsKeyAccount.Value : false;
                            response.LanguagePreferenceIsEnglish = result.LanguagePreferenceIsEnglish.HasValue ? result.LanguagePreferenceIsEnglish.Value : false;
                            response.Location = result.Location;
                            response.MandatoryCreditLimit = result.MandatoryCreditLimit;
                            response.Name = result.Name;
                            response.OverRideOverDuePayment = result.OverrideOverduePayment;
                            response.PaymentTermsCode = result.PaymentTermsCode;
                            response.PoSTypeID = result.PoSTypeID.HasValue ? result.PoSTypeID.Value : 0;
                            response.RegionID = result.RegionID.HasValue ? result.RegionID.Value : 0;
                            response.RequesterClassId = result.RequesterClassId.HasValue ? result.RequesterClassId.Value : 0;
                            response.SaudiID = result.SaudiID;
                            // response.RequestStatus = (RequestStatus)result.RequestStatus;
                            response.ParentCode = result.ParentCode;
                            response.AddressCode = string.IsNullOrEmpty(result.AddressCode) ? 0 : int.Parse(result.AddressCode);
                            response.ChannelTypeID = result.ChannelTypeID.HasValue ? result.ChannelTypeID.Value : 0;
                            response.LandMarkClass = result.LandmarkClass;
                            response.PreviousCode = result.PreviousCode;
                            response.RefillMSISDN = result.RefillMSISDN;
                            response.OwnerSaudiIdNumber = result.OwnerSaudiIdNumber;
                            response.SubChannelTypeID = result.SubChannelTypeID.HasValue ? result.SubChannelTypeID.Value : 0;
                            response.OwnerContactNumber = result.OwnerContactNumber;
                            response.SalesPerson = result.SalesPerson;
                            response.Id = request.Id;
                            // response.RejectionReason = result.RejectionReason;
                            response.CRNumber = result.CRNumber;

                            response.LocationDetails = new DHLocation();
                            response.LocationDetails = LocationController.GetLocationByID(result.LocationID, request.oLoginRequest.language, request.oLoginRequest.Username);
                            response.EmployeeID = result.EmployeeID;

                            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                            response.Status = true;
                        }
                    }
                    else
                    {
                        var result = DbContext.GetDealerCreateRequestById(request.Id).FirstOrDefault();
                        if (result != null)
                        {
                            response.UserTypeId = result.UserTypeId;
                            response.Address = result.Address;
                            response.MunicipalityLicenseCode = result.MunicipalityLicenseCode;
                            response.StoreOwnerCode = result.StoreOwnerCode;
                            response.CityId = result.CityId;
                            response.Code = result.Code;
                            response.ContactMSISDN = result.ContactMSISDN;
                            response.ContactNos = result.ContactNos;
                            response.ContactPerson = result.ContactPerson;
                            response.CreatedBy = result.CreatedBy;
                            response.CreationDate = result.CreationDate.ToString("dd/MM/yyyy");
                            response.CreditLimit = result.CreditLimit;
                            response.DefaultSalesPersonCode = result.DefaultSalesPersonCode;
                            response.DeviceIMEI = result.DeviceIMEI;
                            response.DeviceMAC = result.DeviceMAC;
                            response.Email = result.Email;
                            response.FaxNumber = result.FaxNumber;
                            response.FPDeviceSerial = result.FPDeviceSerial;
                            response.GeofenceRadius = result.GeofenceRadius.Value;
                            response.GroupId = result.GroupId;
                            response.GrCuClassClass = result.GrCuClassClass.Value;
                            response.PartnerName = result.PartnerName.Value;
                            response.HasCommission = result.HasCommission;
                            response.IsKeyAccount = result.IsKeyAccount;
                            response.LanguagePreferenceIsEnglish = result.LanguagePreferenceIsEnglish;
                            response.Location = result.Location;
                            response.MandatoryCreditLimit = result.MandatoryCreditLimit;
                            response.Name = result.Name;
                            response.OverRideOverDuePayment = result.OverrideOverduePayment;
                            response.PaymentTermsCode = result.PaymentTermsCode;
                            response.PoSTypeID = result.PoSTypeID;
                            response.RegionID = result.RegionID;
                            response.RequesterClassId = result.RequesterClassId;
                            response.SaudiID = result.SaudiID;
                            response.RequestStatus = (RequestStatus)result.RequestStatus;
                            response.ParentCode = result.ParentCode;
                            response.AddressCode = string.IsNullOrEmpty(result.AddressCode) ? 0 : int.Parse(result.AddressCode);
                            response.ChannelTypeID = result.ChannelTypeID.Value;
                            response.LandMarkClass = result.LandmarkClass;
                            response.PreviousCode = result.PreviousCode;
                            response.RefillMSISDN = result.RefillMSISDN;
                            response.OwnerSaudiIdNumber = result.OwnerSaudiIdNumber;
                            response.SubChannelTypeID = result.SubChannelTypeID.Value;
                            response.OwnerContactNumber = result.OwnerContactNumber;
                            response.SalesPerson = result.SalesPerson;
                            response.Id = result.Id;
                            response.RejectionReason = result.RejectionReason;
                            response.CRNumber = result.CRNumber;


                            if (!string.IsNullOrEmpty(result.DocumentFiles))
                            {
                                List<string> files = result.DocumentFiles.Split(',').ToList();
                                List<int> types = result.DocumentTypes.Split(',').Select(t => int.Parse(t)).ToList();

                                for (int i = 0; i <= files.Count - 1; i++)
                                {
                                    response.DocumentsList.Add(new Documents()
                                    {
                                        DocumentType = types[i],
                                        DocumentUrl = string.Format("https://apwat.friendisaudi.com/DealerHiringDocumentHandler/DocumentHandler.ashx?id={0}", files[i])
                                    });
                                }
                            }

                            response.LocationDetails = new DHLocation();
                            response.LocationDetails = LocationController.GetLocationByID(result.LocationId, request.oLoginRequest.language, request.oLoginRequest.Username);
                            response.EmployeeID = result.EmployeeId;

                            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                            response.Status = true;
                        }
                    }

                    return response;
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static UpdatingDetailsResponse GetUpdatingRequestDetails(UpdatingDetailsRequest request)
        {
            UpdatingDetailsResponse response = new UpdatingDetailsResponse()
            {
                DocumentsList = new List<Documents>()
            };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new UpdatingDetailsResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    var result = DbContext.GetDealerUpdateRequestById(request.Id).FirstOrDefault();
                    if (result != null)
                    {
                        //response.UserTypeId = result.UserTypeId.Value;
                        //response.Address = result.Address;
                        //response.MunicipalityLicenseCode = result.MunicipalityLicenseCode.Value;
                        //response.StoreOwnerCode = result.StoreOwnerCode.Value;
                        //response.CityId = result.CityId.Value;
                        response.Code = result.Code;
                        response.ContactMSISDN = result.ContactMSISDN;
                        response.ContactNos = result.ContactNos;
                        //response.ContactPerson = result.ContactPerson;
                        response.CreatedBy = result.CreatedBy;
                        response.CreationDate = result.CreationDate.ToString("dd/MM/yyyy");
                        //response.CreditLimit = result.CreditLimit.Value;
                        //response.DefaultSalesPersonCode = result.DefaultSalesPersonCode;
                        //response.DeviceIMEI = result.DeviceIMEI;
                        //response.DeviceMAC = result.DeviceMAC;
                        //response.DistributerID = result.DistributerID.Value;
                        //response.Email = result.Email;
                        //response.FaxNumber = result.FaxNumber;
                        //response.FPDeviceSerial = result.FPDeviceSerial;
                        //response.GeofenceRadius = result.GeofenceRadius.Value;
                        //response.GroupId = result.GroupId.Value;
                        //response.GrCuClassClass = result.GrCuClassClass.Value;
                        //response.PartnerName = result.PartnerName;
                        //response.HasCommission = result.HasCommission.Value;
                        //response.IsKeyAccount = result.IsKeyAccount.Value;
                        //response.LanguagePreferenceIsEnglish = result.LanguagePreferenceIsEnglish.Value;
                        //response.Location = result.Location;
                        //response.MandatoryCreditLimit = result.MandatoryCreditLimit.Value;
                        response.Name = result.Name;
                        //response.OverRideOverDuePayment = result.OverrideOverduePayment.Value;
                        //response.PaymentTermsCode = result.PaymentTermsCode;
                        //response.PoSTypeID = result.PoSTypeID.Value;
                        //response.RegionID = result.RegionID.Value;
                        //response.RequesterClassId = result.RequesterClassId.Value;
                        response.SaudiID = result.SaudiID;
                        response.RequestStatus = (RequestStatus)result.RequestStatus;
                        response.RequestDetails = result.RequestDetails;
                        response.RequestUpdateType = result.RequestUpdateType;
                        response.RequestReason = result.RequestReason.Value;
                        //response.AddressCode = result.AddressCode;
                        //response.ChannelTypeID = result.ChannelTypeID;
                        //response.LandMarkClass = result.LandmarkClass;
                        //response.PreviousCode = result.PreviousCode;
                        //response.RefillMSISDN = result.RefillMSISDN;
                        //response.OwnerSaudiIdNumber = result.OwnerSaudiIdNumber;
                        //response.SubChannel = result.SubChannelTypeID;
                        //response.OwnerContactNumber = result.OwnerContactNumber;
                        //response.SalesPerson = result.SalesPerson;
                        response.RejectionReason = result.RejectionReason;
                        response.Id = result.Id;

                        if (!string.IsNullOrEmpty(result.DocumentFiles))
                        {
                            List<string> files = result.DocumentFiles.Split(',').ToList();
                            List<int> types = result.DocumentTypes.Split(',').Select(t => int.Parse(t)).ToList();

                            for (int i = 0; i <= files.Count - 1; i++)
                            {
                                response.DocumentsList.Add(new Documents()
                                {
                                    DocumentType = types[i],
                                    DocumentUrl = string.Format("https://apwat.friendisaudi.com/DealerHiringDocumentHandler/DocumentHandler.ashx?id={0}", files[i])
                                });
                            }
                        }

                        response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                        response.Status = true;
                    }

                    return response;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        public static ResetPasswordResponse ResetPassword(DHResetPasswordRequest request)
        {
            ResetPasswordResponse response = new ResetPasswordResponse();
            try
            {
                RebindObjectParamters();
                string documentFiles, documentTypeIds, documentTypeNames, authenticationResponseMessage = null;

                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new ResetPasswordResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    var dtos = DealerHiringHelper.GetDealerDocumentDtos(request.documents,
                        _existingDocumentTypes, _existingDocumentFiles,
                        $"{nameof(ResetPassword)} {request.code}",
                        request.oLoginRequest.Username, _documentTypesLookupReferrence,
                        out documentFiles, out documentTypeIds, out documentTypeNames);

                    ObjectParameter password = new ObjectParameter("NewPassword", typeof(string));
                    ObjectParameter mobileNumber = new ObjectParameter("MobileNumber", typeof(string));
                    DbContext.ResetDealerPasswordRequest(oRequester.Code, request.code,
                        request.reason, request.note, documentFiles, documentTypeNames, password, mobileNumber);

                    try
                    {
                        string actionName = "DealerResetPassword";
                        string messageType = "SMS";
                        string Message = DealerHiringHelper.GetDealerHiringMessageByActionName(actionName, messageType, request.oLoginRequest.language);
                        bool isArabic = request.oLoginRequest.language == 2 ? true : false;
                        NobillWrapper.SendSMS(
                        ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"],
                        mobileNumber.Value.ToString(), string.Format(Message.Replace("{{Password}}", "{0}"),
                        password.Value.ToString()), isArabic);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog("SendSMS", ex.Message, "SendSMS",
                          request.oLoginRequest.Username, "");
                    }

                    Dictionary<string, object> emailTemplateData = new Dictionary<string, object>();
                    emailTemplateData.Add("PDealerCode", oRequester.Code);
                    emailTemplateData.Add("SDealerCode", request.code);

                    OnSuccessRequestSubmission(RequestType.DealerUpdateRequest, RequestUpdateType.ResetPassword,
                        request.oLoginRequest.Username, request.oLoginRequest.Username, emailTemplateData);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
            response.Status = true;

            return response;
        }

        public static SearchDealerResponse SearchDealerRequestNew(SearchDealerRequest request)
        {
            SearchDealerResponse response = new SearchDealerResponse() { Result = new List<SearchDealerResult>() };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new SearchDealerResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    string username = string.IsNullOrEmpty(request.Code) && string.IsNullOrEmpty(request.SaudiID) ? request.oLoginRequest.Username : null;
                    var result = DbContext.GetDescendantCodes(username).ToList();

                    if (!result.Any())
                    {
                        response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.No_Available_Data, request.oLoginRequest.language);
                        response.Status = false;
                        return response;
                    }

                    foreach (var dealer in result)
                    {
                        if (dealer != null)
                        {
                            SearchDealerResult dealerRequest = new SearchDealerResult()
                            {
                                IsActive = dealer.IsActive,
                                CreationDate = dealer.CreationDate.ToString("dd/MM/yyyy"),
                                UserTypeId = dealer.UserTypeID,
                                Code = dealer.Code,
                                SaudiID = dealer.SaudiId,
                                Id = dealer.DealerCreationRequestID,
                                ParentCode = dealer.ParentCode,
                                Hierarchy = dealer.Hierarchy
                            };
                            response.Result.Add(dealerRequest);
                        }
                    }

                    response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static SearchDealerResponse SearchDealerRequest(SearchDealerRequest request)
        {
            SearchDealerResponse response = new SearchDealerResponse() { Result = new List<SearchDealerResult>() };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new SearchDealerResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    string username = string.IsNullOrEmpty(request.Code) && string.IsNullOrEmpty(request.SaudiID) ? request.oLoginRequest.Username : null;
                    var result = DbContext.GetDescendantCodes(username).ToList();

                    if (!result.Any())
                    {
                        response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.No_Available_Data, request.oLoginRequest.language);
                        response.Status = false;
                        return response;
                    }

                    foreach (var Dealer in result)
                    {
                        var dealer = DbContext.GetDealer(Dealer.Code, null, null, null).FirstOrDefault();

                        if (dealer != null)
                        {
                            response.Result.Add(new SearchDealerResult()
                            {
                                Name = dealer.Name,
                                Email = dealer.Email,
                                FaxNumber = dealer.FaxNumber,
                                RegionID = dealer.RegionID.Value,
                                IsActive = dealer.IsActive,
                                Address = dealer.Address,
                                ContactPerson = dealer.ContactPerson,
                                RequesterClassId = dealer.RequesterClassId.HasValue ? dealer.RequesterClassId.Value : 0,
                                RefillMSISDN = dealer.RefillMSISDN,
                                LanguagePreferenceIsEnglish = dealer.LanguagePreferenceIsEnglish.Value,
                                ContactNos = dealer.ContactNos,
                                SalesPerson = dealer.SalesPerson,
                                UserName = dealer.UserName,
                                IsKeyAccount = dealer.IsKeyAccount.HasValue ? dealer.IsKeyAccount.Value : false,
                                PoSTypeID = dealer.PoSTypeID.Value,
                                CreationDate = dealer.CreationDate.HasValue ? dealer.CreationDate.Value.ToString("dd/MM/yyyy") : "",
                                CreatedBy = dealer.CreatedBy,
                                LastActionDate = dealer.LastActionDate.HasValue ? dealer.LastActionDate.Value.ToString("dd/MM/yyyy") : "",
                                LastActionBy = dealer.LastActionBy,
                                DistributerID = dealer.DistributerId.HasValue ? dealer.DistributerId.Value : 0,
                                PreviousCode = dealer.PreviousCode,
                                ChannelTypeID = dealer.ChannelTypeID.Value,
                                HasCommission = dealer.HasCommission.Value,
                                UpdatedInCAS = dealer.UpdatedInCAS,
                                CleanData = dealer.CleanData,
                                CityId = dealer.CityId.Value,
                                Location = dealer.Location,
                                GroupId = dealer.GroupId.Value,
                                ContactMSISDN = dealer.ContactMSISDN,
                                DeviceIMEI = dealer.DeviceIMEI,
                                DeviceMAC = dealer.DeviceMAC,
                                FPDeviceSerial = dealer.FPDeviceSerial,
                                GeofenceRadius = dealer.GeofenceRadius.Value,
                                BlockReason = dealer.BlockReason,
                                BlockDate = dealer.BlockDate.HasValue ? dealer.BlockDate.Value.ToString("dd/MM/yyyy") : "",
                                ParentCode = dealer.ParentCode,
                                UserTypeId = dealer.UserTypeId,
                                Code = dealer.Code,
                                SubChannelTypeID = dealer.SubChannelTypeID.Value,
                                SaudiID = dealer.SaudiID,
                                MandatoryCreditLimit = dealer.MandatoryCreditLimit,
                                OverRideOverDuePayment = dealer.OverrideOverduePayment,
                                CreditLimit = dealer.CreditLimit,
                                PaymentTermsCode = dealer.PaymentTermsCode,
                                DefaultSalesPersonCode = dealer.DefaultSalesPersonCode,
                                GrCuClassClass = dealer.GrCuClassClass.HasValue ? dealer.GrCuClassClass.Value : 0,
                                PartnerName = dealer.PartnerName.Value,
                                MunicipalityLicenseCode = dealer.MunicipalityLicenseCode,
                                StoreOwnerCode = dealer.StoreOwnerCode,
                                LandMarkClass = dealer.LandmarkClass,
                                AddressCode = string.IsNullOrEmpty(dealer.AddressCode) ? 0 : int.Parse(dealer.AddressCode),
                                OwnerSaudiIdNumber = dealer.OwnerSaudiIdNumber,
                                OwnerContactNumber = dealer.OwnerContactNumber,
                                CRNumber = dealer.CRNumber,
                                Hierarchy = Dealer.Hierarchy
                            });
                        }
                    }

                    response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static FullDealerRequestDetails GetFullDealerRequestDetails(SearchDealerRequest request)
        {
            FullDealerRequestDetails response = null;
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest(request, out oRequester, out authenticationResponseMessage))
                {
                    return new FullDealerRequestDetails
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    string username = string.IsNullOrEmpty(request.Code) && string.IsNullOrEmpty(request.SaudiID) ? request.oLoginRequest.Username : null;

                    var dealer = DbContext.GetDealer(username, null, null, null).FirstOrDefault();

                    if (dealer == null)
                    {
                        return new FullDealerRequestDetails
                        {
                            Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.No_Available_Data, request.oLoginRequest.language),
                        };
                    };

                    response = new FullDealerRequestDetails()
                    {
                        Name = dealer.Name,
                        Email = dealer.Email,
                        FaxNumber = dealer.FaxNumber,
                        RegionID = dealer.RegionID.Value,
                        IsActive = dealer.IsActive,
                        Address = dealer.Address,
                        ContactPerson = dealer.ContactPerson,
                        RequesterClassId = dealer.RequesterClassId.HasValue ? dealer.RequesterClassId.Value : 0,
                        RefillMSISDN = dealer.RefillMSISDN,
                        LanguagePreferenceIsEnglish = dealer.LanguagePreferenceIsEnglish.Value,
                        ContactNos = dealer.ContactNos,
                        SalesPerson = dealer.SalesPerson,
                        UserName = dealer.UserName,
                        IsKeyAccount = dealer.IsKeyAccount.HasValue ? dealer.IsKeyAccount.Value : false,
                        PoSTypeID = dealer.PoSTypeID.Value,
                        CreationDate = dealer.CreationDate.HasValue ? dealer.CreationDate.Value.ToString("dd/MM/yyyy") : "",
                        CreatedBy = dealer.CreatedBy,
                        LastActionDate = dealer.LastActionDate.HasValue ? dealer.LastActionDate.Value.ToString("dd/MM/yyyy") : "",
                        LastActionBy = dealer.LastActionBy,
                        DistributerID = dealer.DistributerId.HasValue ? dealer.DistributerId.Value : 0,
                        PreviousCode = dealer.PreviousCode,
                        ChannelTypeID = dealer.ChannelTypeID.Value,
                        HasCommission = dealer.HasCommission.Value,
                        UpdatedInCAS = dealer.UpdatedInCAS,
                        CleanData = dealer.CleanData,
                        CityId = dealer.CityId.Value,
                        Location = dealer.Location,
                        GroupId = dealer.GroupId.Value,
                        ContactMSISDN = dealer.ContactMSISDN,
                        DeviceIMEI = dealer.DeviceIMEI,
                        DeviceMAC = dealer.DeviceMAC,
                        FPDeviceSerial = dealer.FPDeviceSerial,
                        GeofenceRadius = dealer.GeofenceRadius.Value,
                        BlockReason = dealer.BlockReason,
                        BlockDate = dealer.BlockDate.HasValue ? dealer.BlockDate.Value.ToString("dd/MM/yyyy") : "",
                        ParentCode = dealer.ParentCode,
                        UserTypeId = dealer.UserTypeId,
                        Code = dealer.Code,
                        SubChannelTypeID = dealer.SubChannelTypeID.Value,
                        SaudiID = dealer.SaudiID,
                        MandatoryCreditLimit = dealer.MandatoryCreditLimit,
                        OverRideOverDuePayment = dealer.OverrideOverduePayment,
                        CreditLimit = dealer.CreditLimit,
                        PaymentTermsCode = dealer.PaymentTermsCode,
                        DefaultSalesPersonCode = dealer.DefaultSalesPersonCode,
                        GrCuClassClass = dealer.GrCuClassClass.HasValue ? dealer.GrCuClassClass.Value : 0,
                        PartnerName = dealer.PartnerName.Value,
                        MunicipalityLicenseCode = dealer.MunicipalityLicenseCode,
                        StoreOwnerCode = dealer.StoreOwnerCode,
                        LandMarkClass = dealer.LandmarkClass,
                        AddressCode = string.IsNullOrEmpty(dealer.AddressCode) ? 0 : int.Parse(dealer.AddressCode),
                        OwnerSaudiIdNumber = dealer.OwnerSaudiIdNumber,
                        OwnerContactNumber = dealer.OwnerContactNumber,
                        CRNumber = dealer.CRNumber,
                        EmployeeID = dealer.EmployeeID,
                    };

                    response.LocationDetails = new DHLocation();
                    response.LocationDetails = LocationController.GetLocationByID(dealer.LocationID, request.oLoginRequest.language, request.oLoginRequest.Username);

                    response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.oLoginRequest.language);
                    response.Status = true;
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void GetStaticalReportPeriod(out DateTime LastMonthDateFrom, out DateTime LastMonthDateTo,
                                                                            out DateTime yesterdayDateFrom, out DateTime yesterdayDateTo,
                                                                            out DateTime TodayDateFrom, out DateTime TodayDateTo)
        {
            LastMonthDateFrom = new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);
            LastMonthDateTo = LastMonthDateFrom.AddMonths(1).AddMilliseconds(-1);

            yesterdayDateFrom = DateTime.Today.AddDays(-1);
            yesterdayDateFrom = new DateTime(yesterdayDateFrom.Year, yesterdayDateFrom.Month, yesterdayDateFrom.Day);
            yesterdayDateTo = yesterdayDateFrom.AddDays(1).AddMilliseconds(-1);

            TodayDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            TodayDateTo = TodayDateFrom.AddDays(1).AddMilliseconds(-1);
        }

        public static DescendantActivationsResponse GetDescendantActivations(LoginRequest request)
        {
            DescendantActivationsResponse response = new DescendantActivationsResponse()
            {
                Result = new List<DescendantActivations>()
            };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest((IAuthenticatableRequest)request, out oRequester, out authenticationResponseMessage))
                {
                    return new DescendantActivationsResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    var result = DbContext.GetDescendantCodes(request.Username).ToList();

                    if (result == null || !result.Any() || result.FirstOrDefault() == null
                      /*  || result.FirstOrDefault().Codes == null || !result.FirstOrDefault().Codes.Split(',').Any()*/)
                    {
                        response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.No_Descendant_To_Retrieve_Activation_Report, request.language);
                        response.Status = false;
                        return response;
                    }
                    else
                    {
                        List<string> dealerCodes = result.Select(o => o.Code).ToList();
                        DateTime LastMonthDateFrom;
                        DateTime LastMonthDateTo;

                        DateTime yesterdayDateFrom;
                        DateTime yesterdayDateTo;

                        DateTime TodayDateFrom;
                        DateTime TodayDateTo;

                        GetStaticalReportPeriod(out LastMonthDateFrom, out LastMonthDateTo, out yesterdayDateFrom, out yesterdayDateTo, out TodayDateFrom, out TodayDateTo);
                        response.Message = String.Empty;
                        response.Status = true;

                        using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                        {
                            dealerCodes.ForEach(dealer => { response.Result.Add(new DescendantActivations() { DescendantCode = dealer }); });
                            var lstActivations = client.GetActivationsByDealerList(dealerCodes, LastMonthDateFrom, TodayDateTo).ToList();
                            lstActivations.ForEach(Activation =>
                            {
                                DescendantActivations descendantActivations = response.Result.First(x => x.DescendantCode == Activation.DealerCode);
                                if (Activation.CreationDate >= LastMonthDateFrom && Activation.CreationDate <= LastMonthDateTo)
                                {
                                    descendantActivations.TotalLastMonthActivations += 1;
                                }
                                if (Activation.CreationDate >= yesterdayDateFrom && Activation.CreationDate <= yesterdayDateTo)
                                {
                                    descendantActivations.TotalYesterdayActivations += 1;
                                }
                                if (Activation.CreationDate >= TodayDateFrom && Activation.CreationDate <= TodayDateTo)
                                {
                                    descendantActivations.TotalTodayActivations += 1;
                                }
                            });
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.language);
            response.Status = true;

            return response;
        }

        public static DescendantCommissionsResponse GetDescendantCommissions(LoginRequest request)
        {
            DescendantCommissionsResponse response = new DescendantCommissionsResponse()
            {
                Result = new List<DescendantCommissions>()
            };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest((IAuthenticatableRequest)request, out oRequester, out authenticationResponseMessage))
                {
                    return new DescendantCommissionsResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    var result = DbContext.GetDescendantCodes(request.Username).ToList();

                    if (result == null || !result.Any() || result.FirstOrDefault() == null
                       /*|| result.FirstOrDefault().Codes == null || !result.FirstOrDefault().Codes.Split(',').Any()*/)
                    {
                        response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.No_Descendant_To_Retrieve_Commission_Report, request.language);
                        response.Status = false;
                        return response;
                    }
                    else
                    {
                        List<string> dealerCodes = result.Select(o => o.Code).ToList();

                        using (ReportingService.ReportingServiceClient client = new Engine.ReportingService.ReportingServiceClient())
                        {
                            //List<ReportingService.CommissionsSummaryReport> lstCommissionReport = client.GetCommissionsSummaryReport(dealerCodes).ToList();
                            //lstCommissionReport.ForEach(item =>
                            //{
                            //    response.Result.Add(new DescendantCommissions() { DescendantCode = item.DescendantCode, TotalLastMonthCommissions = item.TotalLastMonthCommissions, TotalYesterdayCommissions = item.TotalYesterdayCommissions, TotalTodayCommissions = item.TotalTodayCommissions });
                            //});
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.Message = DealerHiringHelper.GetDealerHiringMessage(DealerHiringErrorCode.Success, request.language);
            response.Status = true;

            return response;
        }

        public static ActivationSummaryReportResponse GetActivationSummaryReport(LoginRequest request, string dealerCode, DateSearchCriteria dateSearchCriteria)
        {
            ActivationSummaryReportResponse response = new ActivationSummaryReportResponse()
            {
                Result = new ActivationSummaryReport()
            };

            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MaxValue;
            ReportingService.ActivationSummaryReport report = null;

            if (dateSearchCriteria == DateSearchCriteria.Today)
            {
                fromDate = DateTime.Now;
                toDate = DateTime.Now;
            }
            else if (dateSearchCriteria == DateSearchCriteria.Yesterday)
            {
                fromDate = DateTime.Now.AddDays(-1);
                toDate = DateTime.Now.AddDays(-1);

            }
            else if (dateSearchCriteria == DateSearchCriteria.LastMonth)
            {
                //fromDate = DateTime.Now.AddDays(-30);
                //toDate = DateTime.Now;

                fromDate = DateTime.Now.AddDays(-90);
                toDate = DateTime.Now;

            }


            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest((IAuthenticatableRequest)request, out oRequester, out authenticationResponseMessage))
                {
                    return new ActivationSummaryReportResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                //using (OMSEXEntities DbContext = new OMSEXEntities())
                //{
                //    List<string> dealerCodes = new List<string>();
                //    var result = DbContext.GetDescendantCodes(request.Username).ToList();
                //    if (result == null /*|| result.Codes == null*/)
                //    {
                //        response.Message = "No data found";
                //        response.Status = false;
                //        return response;
                //    }
                //    else
                //    {
                //        dealerCodes = result.Select(o => o.Code).ToList();
                //    }

                //    if (dealerCodes.Count() == 0)
                //    {
                //        response.Message = "No data found";
                //        response.Status = false;
                //        return response;
                //    }
                //    else
                //    {
                //        using (ReportingService.ReportingServiceClient client = new Engine.ReportingService.ReportingServiceClient())
                //        {
                //            var report = client.GetActivationSummaryReport(dealerCode, (ReportingService.DateSearchCriteria)(int)dateSearchCriteria);
                //            response.Result.TotalActivations = report.TotalActivations;
                //            response.Result.TotalFirstPayableEvents = report.TotalFirstPayableEvents;
                //        }
                //    }
                //}

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    using (ReportingService.ReportingServiceClient client = new Engine.ReportingService.ReportingServiceClient())
                    {
                        List<string> dealerCodes = new List<string>();

                        if (dealerCode.ToLower() == request.Username.ToLower()) // Inserted Dealer Code == Logged Dealer
                        {
                            report = client.GetActivationSummaryReport(dealerCode, ReportingService.Brand.Both, fromDate, toDate);
                            response.Result.TotalActivations = report.TotalActivations;
                            response.Result.TotalFirstPayableEvents = report.TotalFirstPayableEvents;
                            response.Message = "Success";
                            response.Status = true;
                            return response;
                        }
                        else
                        {
                            var result = DbContext.GetDescendantCodes(request.Username).ToList();

                            if (result != null && result.Count > 0)
                            {
                                if (!result.Select(o => o.Code.ToLower() == dealerCode.ToLower()).Any())
                                {
                                    response.Message = "Invalid Dealer Code ";
                                    response.Status = false;
                                    return response;
                                }
                                else
                                {
                                    report = client.GetActivationSummaryReport(dealerCode, ReportingService.Brand.Both, fromDate, toDate);
                                    response.Result.TotalActivations = report.TotalActivations;
                                    response.Result.TotalFirstPayableEvents = report.TotalFirstPayableEvents;
                                    response.Message = "Success";
                                    response.Status = true;
                                }
                            }
                            else
                            {
                                response.Message = "No data found";
                                response.Status = false;
                                return response;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.Message = "Success";
            response.Status = true;

            return response;
        }

        public static RechargeSummaryReportResponse GetRechargeSummaryReport(LoginRequest request, string dealerCode, DateSearchCriteria dateSearchCriteria)
        {

            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MaxValue;

            if (dateSearchCriteria == DateSearchCriteria.Today)
            {
                fromDate = DateTime.Now;
                toDate = DateTime.Now;
            }
            else if (dateSearchCriteria == DateSearchCriteria.Yesterday)
            {
                fromDate = DateTime.Now.AddDays(-1);
                toDate = DateTime.Now.AddDays(-1);

            }
            else if (dateSearchCriteria == DateSearchCriteria.LastMonth)
            {
                fromDate = DateTime.Now.AddDays(-90);
                toDate = DateTime.Now;

                //int daysInMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month - 1);
                //fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 1);
                //toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, daysInMonth);
            }

            RechargeSummaryReportResponse response = new RechargeSummaryReportResponse()
            {
                Result = new RechargeSummaryReport()
            };
            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest((IAuthenticatableRequest)request, out oRequester, out authenticationResponseMessage))
                {
                    return new RechargeSummaryReportResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    List<string> dealerCodes = new List<string>();
                    var result = DbContext.GetDescendantCodes(request.Username).ToList();
                    if (result == null /*|| result.Codes == null*/)
                    {
                        response.Message = "No data found";
                        response.Status = false;
                        return response;
                    }
                    else
                    {
                        dealerCodes = result.Select(o => o.Code).ToList();
                    }

                    if (dealerCodes.Count() == 0)
                    {
                        response.Message = "Failed, No descendant for you";
                        response.Status = false;
                        return response;
                    }
                    else
                    {
                        using (ReportingService.ReportingServiceClient client = new Engine.ReportingService.ReportingServiceClient())
                        {
                            var report = client.GetRechargeSummaryReport(dealerCode, ReportingService.Brand.Both, fromDate, toDate);
                            response.Result.TotalActivations = report.TotalActivations;
                            response.Result.TotalNoRecharges = report.TotalNoRecharges;
                            response.Result.TotalFirstRecharges = report.TotalFirstRecharges;
                            response.Result.TotalSecondRecharges = report.TotalSecondRecharges;
                            response.Result.TotalThirdRecharges = report.TotalThirdRecharges;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            response.Message = "Success";
            response.Status = true;

            return response;
        }

        public static CommissionsDetailsReportResponse GetCommissionsDetailsReport(LoginRequest request, string dealerCode, DHGeneralSearchCriteria generalSearchCriteria, DateSearchCriteria dateSearchCriteria)
        {
            CommissionsDetailsReportResponse response = new CommissionsDetailsReportResponse()
            {
                Result = new CommissionsDetailsReport()
                {
                    CommissionsDetailsDataItems = new List<CommissionsDetailsDataItem>(),
                    Headers = new List<string>()
                }
            };

            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MaxValue;

            if (dateSearchCriteria == DateSearchCriteria.Today)
            {
                fromDate = DateTime.Now;
                toDate = DateTime.Now;
            }
            else if (dateSearchCriteria == DateSearchCriteria.Yesterday)
            {
                fromDate = DateTime.Now.AddDays(-1);
                toDate = DateTime.Now.AddDays(-1);

            }
            else if (dateSearchCriteria == DateSearchCriteria.LastMonth)
            {
                fromDate = DateTime.Now.AddDays(-90);
                toDate = DateTime.Now;
            }


            try
            {
                string authenticationResponseMessage = string.Empty;
                AdministrationServices.Requester oRequester = null;

                if (!IsAuthenticRequest((IAuthenticatableRequest)request, out oRequester, out authenticationResponseMessage))
                {
                    return new CommissionsDetailsReportResponse
                    {
                        Message = authenticationResponseMessage
                    };
                }

                using (OMSEXEntities DbContext = new OMSEXEntities())
                {
                    if (request.Username.ToLower() != dealerCode.ToLower())
                    {
                        var result = DbContext.GetDescendantCodes(request.Username).ToList();

                        if (result != null && result.Count > 0)
                        {
                            List<string> dealerCodes = result.Select(o => o.Code).ToList();

                            if (dealerCodes != null && dealerCodes.Count > 0)
                            {
                                if (!dealerCodes.Contains(dealerCode))
                                {
                                    response.Message = "Invalid Dealer Code";
                                    response.Status = false;
                                    return response;
                                }
                            }
                        }
                    }

                    //if (result == null || result.Count() == 0)
                    //{
                    //    response.Message = "Failed no descendant for you";
                    //    response.Status = false;
                    //    return response;
                    //}
                    //else
                    //{           
                    //    if(request.Username.ToLower() != dealerCode.ToLower())
                    //    {
                    //        List<string> dealerCodes = result.Select(o => o.Code).ToList();

                    //        if (dealerCodes != null && dealerCodes.Count > 0)
                    //        {
                    //            if (!dealerCodes.Contains(dealerCode))
                    //            {
                    //                response.Message = "Invalid Dealer Code";
                    //                response.Status = false;
                    //                return response;
                    //            }
                    //        }
                    //    }
                    //}


                    using (ReportingService.ReportingServiceClient client = new Engine.ReportingService.ReportingServiceClient())
                    {
                        var lstCommissionsDetails = client.GetCommissionsDetailsReports(dealerCode, (ReportingService.GeneralSearchCriteria)(int)generalSearchCriteria, ReportingService.Brand.Both, fromDate, toDate);
                        lstCommissionsDetails.Headers.ForEach(item =>
                        {
                            response.Result.Headers.Add(item);
                        });
                        lstCommissionsDetails.CommissionsDetailsDataItems.ForEach(item =>
                        {
                            var row = new CommissionsDetailsDataItem()
                            {
                                ID = item.ID,
                                IMSI = item.IMSI,
                                MSISDN = item.MSISDN,
                                TotalCommissions = item.TotalCommissions,
                                elementCommissions = new List<ElementCommission>()
                            };

                            item.elementCommissions.ForEach(itemDetails =>
                            {
                                row.elementCommissions.Add(new ElementCommission() { Name = itemDetails.Name, Value = itemDetails.Value });
                            });

                            response.Result.CommissionsDetailsDataItems.Add(row);

                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            if (response.Result.CommissionsDetailsDataItems != null && response.Result.CommissionsDetailsDataItems.Count > 0)
            {
                response.Message = "Success";
            }
            else
            {
                response.Message = "No Data Found";
            }

            response.Status = true;

            return response;
        }

        #region OMS UTL

        public static void DropFilesToOMS_AddUserCase(string Name, string Email, string FaxNumber, int RegionID,
            string SaudiID, string CRN, string MunicipalityLicense, string Address, string ContactPerson,
            int RequesterClassId, string RefillMSISDN, bool LanguagePreferenceIsEnglish, string ContactNos,
            bool IsKeyAccount, int PoSTypeID, int DistributerCode, int ChannelTypeID, bool HasCommission,
            int CityId, string Location, int GroupId, string ContactMSISDN, string DeviceIMEI,
            string DeviceMAC, string FPDeviceSerial, int? GeofenceRadius, int CreditLimit,
            bool OverrideOverduePayment, bool MandatoryCreditLimit, string PaymentTermsCode,
            string DefaultSalesPersonCode, string OwnerSaudiID,
            int UserType, string ParentCode, string DealerCode,
            string DOCUMENTS, string DOCUMENT_TYPES, string CHANNEL, string REQUESTER_CODE, string PROCESS,
            string SUB_PROCESS, string ownerSaudiContactNum, int subChanneltypeID, string StoreOwnerCode
            , string PreviousCode, string landMark, int AddressCode, int partnerName, int grCuClassClass,
            RequestStatus requestStatus, RequestType RequestType, string salesPerson, string rejectionReason, string EmployeeId, string LocationId)
        {
            try
            {
                string fileName = Guid.NewGuid().ToString();
                string detailsFilePath = _oMSDropFolderPath + fileName + ".txt";

                StringBuilder details = new StringBuilder(
                string.Format(DropFileTemplates.AddUser_Template,
                Name,//0
                Email,//1
                FaxNumber,//2
                RegionID,//3 
                SaudiID,//4
                CRN,//5
                MunicipalityLicense,//6
                Address,//7
                ContactPerson,//8
                RequesterClassId,//9
                RefillMSISDN,//10
                LanguagePreferenceIsEnglish,//11
                ContactNos,//12
                IsKeyAccount,//13
                PoSTypeID,//14
                DistributerCode,//15
                ChannelTypeID,//16
                HasCommission,//17
                CityId,//18
                Location,//19
                GroupId,//20
                ContactMSISDN,//21
                DeviceIMEI,//22
                DeviceMAC,//23
                FPDeviceSerial,//24
                GeofenceRadius,//25
                CreditLimit,//26
                OverrideOverduePayment,//27
                MandatoryCreditLimit,//28
                PaymentTermsCode,//29
                DefaultSalesPersonCode,//30
                OwnerSaudiID,//31
                UserType,//32
                ParentCode,//33
                DealerCode,//34
                DOCUMENTS,//35
                DOCUMENT_TYPES,//36
                CHANNEL,//37
                REQUESTER_CODE,//38
                PROCESS,//39
                SUB_PROCESS,//40
                ownerSaudiContactNum,//41
                subChanneltypeID,//42
                StoreOwnerCode,//43
                PreviousCode,//44
                landMark,//45
                AddressCode,//46
                partnerName,//47
                grCuClassClass,//48
                EmployeeId,//49
                LocationId,//50
                requestStatus,//51 
                RequestType,//52
                salesPerson,//53
                rejectionReason//54
                ));

                File.WriteAllText(detailsFilePath, details.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DropFilesToOMS_UpdateUserCase(string code, string reason, string documents
            , string documentTypes, string REQUESTER_CODE, string PROCESS, string SUB_PROCESS, string saudiID,
            string name, string contactMSISDN, string contactNos, string CHANNEL, string Note)
        {
            try
            {
                string fileName = Guid.NewGuid().ToString();
                string detailsFilePath = Path.Combine(_oMSDropFolderPath, $"{ fileName }.txt");

                StringBuilder details = new StringBuilder(
                string.Format(DropFileTemplates.UpdateUser_Template, code, reason, documents, documentTypes,
                REQUESTER_CODE, PROCESS, SUB_PROCESS, saudiID, name, contactMSISDN, contactNos, CHANNEL, Note));

                File.WriteAllText(detailsFilePath, details.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class DropFileTemplates
        {
            public static readonly string AddUser_Template = "Name={0}" + Environment.NewLine + "Email={1}" +
                Environment.NewLine + "FaxNumber={2}" + Environment.NewLine + "RegionID={3}" +
                Environment.NewLine + "SaudiId={4}" + Environment.NewLine + "CRNumber={5}" +
                Environment.NewLine + "MunicipalityLicense={6}" + Environment.NewLine + "Address={7}" +
                Environment.NewLine + "ContactPerson={8}" + Environment.NewLine + "RequesterClassID={9}" +
                Environment.NewLine + "RefillMSISDN={10}" + Environment.NewLine + "LanguagePreferenceIsEnglish={11}"
                + Environment.NewLine + "ContactNos={12}" + Environment.NewLine + "IsKeyAccount={13}" +
                Environment.NewLine + "PoSTypeID={14}" + Environment.NewLine + "DistributerCode={15}" +
                Environment.NewLine + "ChannelTypeID={16}" + Environment.NewLine + "HasCommission={17}" +
                Environment.NewLine + "CityID={18}" + Environment.NewLine + "Location={19}" + Environment.NewLine +
                "GroupID={20}" + Environment.NewLine + "ContactMSISDN={21}" + Environment.NewLine + "DeviceIMEI={22}" +
                Environment.NewLine + "DeviceMAC={23}" + Environment.NewLine + "FPDeviceSerial={24}" + Environment.NewLine +
                "GeofenceRadius={25}" + Environment.NewLine + "CreditLimit={26}" + Environment.NewLine
                + "OverrideOverduePayment={27}" + Environment.NewLine + "MandatoryCreditLimit={28}" + Environment.NewLine
                + "PaymentTermsCode={29}" + Environment.NewLine + "DefaultSalesPersonCode={30}" + Environment.NewLine +
                "OwnerSaudiId={31}" +
                Environment.NewLine + "UserTypeID={32}" + Environment.NewLine +
                "ParentCode={33}" + Environment.NewLine + "DealerCode={34}" + Environment.NewLine +
                "DOCUMENTS={35}" + Environment.NewLine + "DOCUMENT_TYPES={36}" + Environment.NewLine
                + "CHANNEL={37}" + Environment.NewLine + "REQUESTER_CODE={38}"
                + Environment.NewLine + "PROCESS={39}" + Environment.NewLine + "SUB_PROCESS={40}"
                + Environment.NewLine + "OwnerSaudiContactNumber={41}"
                + Environment.NewLine + "SubChannelTypeID={42}"
                + Environment.NewLine + "StoreOwnerCode={43}"
                + Environment.NewLine + "PreviousCode={44}"
                + Environment.NewLine + "LandMarkClass={45}"
                + Environment.NewLine + "AddressCode={46}"
                + Environment.NewLine + "PartnerName={47}"
                + Environment.NewLine + "GrCuClassClass={48}"
                + Environment.NewLine + "EmployeeId={49}"
                + Environment.NewLine + "LocationId={50}";

            public static readonly string UpdateUser_Template = "DealerCode={0}" + Environment.NewLine
                + "Reason={1}" + Environment.NewLine +
                "DOCUMENTS={2}" + Environment.NewLine + "DOCUMENT_TYPES={3}" + Environment.NewLine + "REQUESTER_CODE={4}"
                + Environment.NewLine + "PROCESS={5}" + Environment.NewLine + "SUB_PROCESS={6}"
                + Environment.NewLine + "SaudiId={7}" + Environment.NewLine + "Name={8}"
                + Environment.NewLine + "ContactMSISDN={9}" + Environment.NewLine + "ContactNos={10}"
                + Environment.NewLine + "CHANNEL={11}"
                + Environment.NewLine + "Note={12}";
        }
        #endregion

        public static bool IsValidIDNumber(String nicNumber)
        {
            string testingIDNumbersConfig = ConfigurationManager.AppSettings["TestingIDNumbers"];
            if (!string.IsNullOrWhiteSpace(testingIDNumbersConfig))
            {
                var TestingIDNumbers = testingIDNumbersConfig.Split(',').ToList();
                if (TestingIDNumbers.Contains(nicNumber))
                {
                    return true;
                }
            }

            string prefix = "12";
            long outNumber = 0;
            long.TryParse(nicNumber, out outNumber);

            if (nicNumber.Length != 10 || outNumber == 0 || prefix.IndexOf(nicNumber[0]) < 0)
                return false;

            char[] charArray = nicNumber.ToCharArray();
            int[] numArray = new int[10];
            for (int i = 0; i < charArray.Length; i++)
            {
                numArray[i] = int.Parse(charArray[i].ToString());
            }
            int sum = 0;
            for (int i = 0; i < numArray.Length - 1; i++)
            {
                if (i % 2 != 0)
                {
                    sum += numArray[i];
                }
                else
                {
                    int oddByTwo = numArray[i] * 2;
                    String oddByTwoString = oddByTwo.ToString();
                    int[] oddByTwoArray = new int[oddByTwoString.Length];
                    int oddByTwoSum = 0;
                    for (int j = 0; j < oddByTwoArray.Length; j++)
                    {
                        oddByTwoArray[j] = int.Parse(oddByTwoString[j].ToString());
                        oddByTwoSum += oddByTwoArray[j];
                    }
                    sum += oddByTwoSum;
                }
            }

            String sumString = sum.ToString();
            int unit = int.Parse(sumString[sumString.Length - 1].ToString());

            if ((unit == 0 && numArray[9] == 0) || ((10 - unit) == numArray[9]))
            {
                return true;
                //return VerifyIdNumberWS(nicNumber);
            }

            //throw new CustomException(string.Format(Resources.Resources.Messages.IdNumberError, nicNumber));
            return false;
        }
    }
}
