//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.Engine
{
    using System;
    using System.Collections.Generic;
    
    public partial class ErrorLog
    {
        public int ID { get; set; }
        public string ErrorType { get; set; }
        public string ErrorDescription { get; set; }
        public string Source { get; set; }
        public System.DateTime DateTime { get; set; }
        public string ExtraDetails { get; set; }
        public string Username { get; set; }
    }
}
