﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;

namespace LP.OMS.Channels.Engine
{
    public class UserContextScope : IDisposable
    {
        private const string UserHeaderName = "LP.Core.Utilities.ServiceModel.UserNameHeader";
        private const string UserHeaderDealerCode = "LP.Core.Utilities.ServiceModel.DealerCodeHeader";
        private const string UserHeaderChannel = "LP.Core.Utilities.ServiceModel.ChannelHeader";
        private const string UserHeaderNS = "http://www.Leading-Point.com/Utilities/ServiceModel";
        private const string UserHeaderSessionToken = "LP.Core.Utilities.ServiceModel.SessionTokenHeader";



        private OperationContextScope _operationContextScope = null;

        public UserContextScope(IClientChannel innerChannel, string userId)
        {
            _operationContextScope = new OperationContextScope(innerChannel);

            MessageHeader header = MessageHeader.CreateHeader(UserHeaderName, UserHeaderNS, userId);

            OperationContext.Current.OutgoingMessageHeaders.Add(header);
        }

        public UserContextScope(IClientChannel innerChannel, string userId, string dealerCode)
        {
            _operationContextScope = new OperationContextScope(innerChannel);

            MessageHeader header = MessageHeader.CreateHeader(UserHeaderName, UserHeaderNS, userId);
            OperationContext.Current.OutgoingMessageHeaders.Add(header);

            if (!string.IsNullOrWhiteSpace(dealerCode))
            {
                header = MessageHeader.CreateHeader(UserHeaderDealerCode, UserHeaderNS, dealerCode);
                OperationContext.Current.OutgoingMessageHeaders.Add(header);
            }
        }

        public UserContextScope(IClientChannel innerChannel, string userId, string dealerCode, string channel)
        {
            MessageHeaders headers = OperationContext.Current.IncomingMessageHeaders;
            
            _operationContextScope = new OperationContextScope(innerChannel);

            MessageHeader header = MessageHeader.CreateHeader(UserHeaderName, UserHeaderNS, userId);
            OperationContext.Current.OutgoingMessageHeaders.Add(header);

            if (!string.IsNullOrWhiteSpace(dealerCode))
            {
                header = MessageHeader.CreateHeader(UserHeaderDealerCode, UserHeaderNS, dealerCode);
                OperationContext.Current.OutgoingMessageHeaders.Add(header);
            }

            if (!string.IsNullOrWhiteSpace(channel))
            {
                header = MessageHeader.CreateHeader(UserHeaderChannel, UserHeaderNS, channel);
                OperationContext.Current.OutgoingMessageHeaders.Add(header);
            }
            if (headers != null)
            {
                string headerVal = headers.GetHeader<string>("SessionToken", "http://www.Leading-Point.com/Utilities/ServiceModel");
                if (!string.IsNullOrWhiteSpace(channel))
                {
                    MessageHeader header2 = MessageHeader.CreateHeader(UserHeaderSessionToken, UserHeaderNS, headerVal);
                    OperationContext.Current.OutgoingMessageHeaders.Add(header2);
                }
            }
        }

        public static string GetChannel()
        {
            return OperationContext.Current == null ? null : OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(UserHeaderChannel, UserHeaderNS);
        }


        #region IDisposable Members

        public void Dispose()
        {
            _operationContextScope.Dispose();
        }

        #endregion

        public static string GetUserId()
        {
            if (OperationContext.Current == null)
            {
                return null;
            }

            return OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(UserHeaderName, UserHeaderNS);
        }

        public static string GetdealerCode()
        {
            return OperationContext.Current == null ? null : OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(UserHeaderDealerCode, UserHeaderNS);
        }

    }
}