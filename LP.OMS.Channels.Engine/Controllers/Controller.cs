﻿using LP.Core.Utilities.Logging;
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LP.Core.Utilities.Serialization;
using System.Threading;
using System.Globalization;
using System.Resources;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using LP.Core.Utilities.Exceptions;
using System.ServiceModel;
using System.Xml.Serialization;
using LP.OMS.Channels.Engine.ActivationService;
using System.Data.Entity;
using LP.OMS.Channels.Contracts.DealerSImAssociation;
using LP.OMS.Channels.Engine.SCExternalApisService;
using LP.OMS.Channels.Engine.PlanServiceReference;
using System.Web;
using LP.OMS.Channels.Engine.Models;
using Newtonsoft.Json;

namespace LP.OMS.Channels.Engine
{
    static public class Controller
    {
        #region Static variables

        private static ResourceManager objResourceManager;

        private static string _targetImageExtension = string.Empty;

        private static int MaxAllowedSubscriptionsPerCustomer = 10;
        private static double _maxSignitureDocumentSizeKB = 0.0;
        private static double _maxIDCopyDocumentSizeKB = 0.0;
        // HusseiniI
        private static readonly Double CurrentAndriodApplicationVersion = 0;
        private static readonly Double CurrentAndriodApplicationMinimalVersion = 0;
        private static readonly string AndriodApplicationUrl = "";

        private static readonly Double CurrentIOSApplicationVersion = 0;
        private static readonly Double CurrentIOSApplicationMinimalVersion = 0;
        private static readonly string IOSApplicationUrl = "";

        private static string _userName = string.Empty;
        private static string _password = string.Empty;
        private static string COUNTRY_CODE = string.Empty;
        private static string Gulf_COUNTRIES = string.Empty;
        private static DateTime MinimDateTime = DateTime.MinValue;
        private static bool _storeApplicationFormImage = false;
        private static List<string> _fboRequesterClassIds = new List<string>();
        private static string _defaultWelcomeMessage = string.Empty;
        private static string _arabicWelcomeMessage = string.Empty;

        private static List<int> _lstWelcomeMessageRequesterClasses = new List<int>();
        private static string familyActivationRightCode = string.Empty;
        private static string deviceActivationRightCode = string.Empty;
        //private static readonly string OMSDropFileTemplate = "CUSTOMERID={0}" + Environment.NewLine + "FIRSTNAME={1}" + Environment.NewLine +
        //    "SECONDNAME={2}" + Environment.NewLine + "THIRDNAME={3}" + Environment.NewLine + "LASTNAME={4}" + Environment.NewLine +
        //    "DOB={5}" + Environment.NewLine + "NATIONALITYCODE={6}" + Environment.NewLine + "IDNO={7}" + Environment.NewLine +
        //    "IMSI={8}" + Environment.NewLine + "MSISDN={9}" + Environment.NewLine + "IDTYPECODE={10}" + Environment.NewLine +
        //    "IDEXPIRYDATE={11}" + Environment.NewLine + "DEALERCODE={12}" + Environment.NewLine + "GENDER_CODE={13}" + Environment.NewLine + "TITLE_ID={14}" + Environment.NewLine + "LANGUAGEID={15}" + Environment.NewLine + "CHANNEL={16}";


        private static readonly string OMSDropFileTemplate = "CUSTOMERID={0}" + Environment.NewLine + "FIRSTNAME={1}" + Environment.NewLine +
          "SECONDNAME={2}" + Environment.NewLine + "THIRDNAME={3}" + Environment.NewLine + "LASTNAME={4}" + Environment.NewLine +
          "DOB={5}" + Environment.NewLine + "NATIONALITYCODE={6}" + Environment.NewLine + "IDNO={7}" + Environment.NewLine +
          "ICCID={8}" + Environment.NewLine + "MSISDN={9}" + Environment.NewLine + "IDTYPECODE={10}" + Environment.NewLine +
          "IDEXPIRYDATE={11}" + Environment.NewLine + "DEALERCODE={12}" + Environment.NewLine + "GENDER_CODE={13}" + Environment.NewLine +
          "TITLE_ID={14}" + Environment.NewLine + "LANGUAGEID={15}" + Environment.NewLine + "CHANNEL={16}" + Environment.NewLine + "PROCESS_ID={17}" + Environment.NewLine +
          "WFACTIVITYID={18}" + Environment.NewLine + "OLDICCID={19}" + Environment.NewLine + "REPLACEMENTREASON={20}" + Environment.NewLine + "REPLACEMENTREASONID={21}" + Environment.NewLine;

        private static string DSTChannelValue = "DST";
        private static List<Country> oCountries = null;
        #endregion

        #region Constructor

        static Controller()
        {
            try
            {
                Assembly objAssembly = Assembly.GetExecutingAssembly();
                objResourceManager = new ResourceManager("LP.OMS.Channels.Engine.Resources.OMSResources", objAssembly);

                MaxAllowedSubscriptionsPerCustomer = Convert.ToInt32(ConfigurationManager.AppSettings["MaxAllowedSubscriptionsPerCustomer"]);

                _maxIDCopyDocumentSizeKB = int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.MaxIDCopyDocumentSizeKB"].ToString());
                _maxSignitureDocumentSizeKB = int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.MaxSignitureDocumentSizeKB"].ToString());


                //HusseiniI
                CurrentAndriodApplicationVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationVersion"]);
                CurrentAndriodApplicationMinimalVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationMinimalVersion"]);
                AndriodApplicationUrl = ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationUrl"];


                CurrentIOSApplicationVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.IOSApplicationVersion"]);
                CurrentIOSApplicationMinimalVersion = Double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.IOSApplicationMinimalVersion"]);
                IOSApplicationUrl = ConfigurationManager.AppSettings["LP.OMS.Channels.IOSApplicationUrl"];
                _storeApplicationFormImage = bool.Parse(ConfigurationManager.AppSettings["StoreApplicationFormImage"].ToString());
                //_applicationVersion = ConfigurationManager.AppSettings["LP.OMS.Channels.ApplicationVersion"];
                _fboRequesterClassIds = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Controller.FBORequesterClassId"].ToString().Split(',').ToList();

                _userName = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.CreditTransferService.UserName"];
                _password = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.CreditTransferService.Password"];
                COUNTRY_CODE = ConfigurationManager.AppSettings["COUNTRY_CODE"];
                Gulf_COUNTRIES = ConfigurationManager.AppSettings["Gulf_COUNTRIES"];

                string welcomeMessage = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.WelcomeMessage.Text"];
                familyActivationRightCode = ConfigurationManager.AppSettings["VirginFamilyActivationRightCode"];
                deviceActivationRightCode = ConfigurationManager.AppSettings["VirginIOTActivationRightCode"];

                if (!string.IsNullOrWhiteSpace(welcomeMessage))
                {
                    string[] messages = welcomeMessage.Split(';');
                    _defaultWelcomeMessage = messages[0];
                    if (messages.Length > 1)
                    {
                        _arabicWelcomeMessage = messages[1];
                    }
                }

                string welcomeMessagesRequesterClasses = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.WelcomeMessage.RequesterClasses"];
                if (!string.IsNullOrWhiteSpace(welcomeMessagesRequesterClasses))
                {
                    welcomeMessagesRequesterClasses.Split(',').ToList().ForEach(p =>
                    {
                        int requesterClass = 0;
                        int.TryParse(p, out requesterClass);
                        if (requesterClass != 0)
                        { _lstWelcomeMessageRequesterClasses.Add(requesterClass); }
                    }
                    );
                }

                if (!DateTime.TryParseExact(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Controller.MiniDate"].ToString(), "dd/MM/yyyy", null, DateTimeStyles.None, out MinimDateTime))
                {
                    MinimDateTime = new DateTime(1900, 1, 1);
                }

                _targetImageExtension = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.TargetImageExtension"];
                if (string.IsNullOrEmpty(_targetImageExtension))
                {
                    throw new Exception("Target Image Extension is not provided!");
                }

                //An initial check to eliminate the need for App Pool recycling after depoyment to load resources (Fix Resources Issue)
                try
                {
                    string resourceTester = Resources.BLMessages.ResourceManager.GetString("AuthenticationManagement_Success");
                }
                catch (Exception)
                {
                    try
                    {
                        ErrorHandling.AddToErrorLog("AppInit", "Application Context Will Be Auto Recycled To Load English Resources Correctly.", "Controller Constructor", "System");
                        System.Web.HttpRuntime.UnloadAppDomain();
                    }
                    catch
                    {
                        throw new Exception("Error in loading the resources!");
                    }
                }

                try
                {
                    string resourceTesterAr = Resources.BLMessages_ar_JO.ResourceManager.GetString("AuthenticationManagement_Success");
                }
                catch (Exception)
                {
                    try
                    {
                        ErrorHandling.AddToErrorLog("AppInit", "Application Context Will Be Auto Recycled To Load Arabic Resources Correctly.", "Controller Constructor", "System");
                        System.Web.HttpRuntime.UnloadAppDomain();
                    }
                    catch
                    {
                        throw new Exception("Error in loading the resources!");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Controller Constructor", ex, "");
            }
        }

        private static ResourceManager GetResourceManager()
        {
            if (Thread.CurrentThread.CurrentCulture.Name.ToLower() == "ar-jo")
            {
                return Resources.BLMessages_ar_JO.ResourceManager;
            }
            else
            {
                return Resources.BLMessages.ResourceManager;
            }
        }

        #endregion

        #region public methods


        public static CallResponse SubmitVirginActivation(VirginActivationRequest request)
        {
            string name = "";
            CallResponse oCallResponse = new CallResponse() { ESIMData = new Contracts.ESIMData() };
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
            oCallResponse = CheckRequester(request.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return oCallResponse;
            }

            AMErrorCode responseCode = AMErrorCode.Success;

            if (responseCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(responseCode);
                return oCallResponse;
            }

            if (_storeApplicationFormImage)
            {
                string rootFolder = System.Web.Hosting.HostingEnvironment.MapPath("~/");
                string fileName = Guid.NewGuid().ToString();
                File.WriteAllBytes(Path.Combine(rootFolder, "ApplicationForms", fileName + "_Virgin.jpeg"), Convert.FromBase64String(request.IdCopyDocument.DocumentContent));
            }

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    //HijriCalendar hijriCal = new HijriCalendar();
                    ActivationService.CustomerInfo customerInfo = new ActivationService.CustomerInfo();
                    customerInfo.CustomerDocuments = new List<ActivationService.Document>();
                    customerInfo.IdNumber = request.IdNo;
                    customerInfo.IsDealerActivation = false;
                    customerInfo.ActivationDealerCode = request.DealerCode;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                    if (request.oLoginRequest.language == 2)
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                    }
                    else
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                    }
                    customerInfo.IdTypeCode = request.IDTypeCode;
                    customerInfo.Email = request.Email;
                    ActivationService.FingerPrint objFingerprint = null;

                    if (request.FingerPrint != null)
                    {
                        if (!request.FingerPrint.IsSematiOTPUsed)
                        {
                            if (!string.IsNullOrWhiteSpace(request.FingerPrint.FingerPrintText))
                            {
                                objFingerprint = GetActivationServiceFingerprint(request.FingerPrint);
                            }
                        }
                        else
                        {
                            objFingerprint = GetActivationServiceFingerprint(request.FingerPrint);
                        }

                        customerInfo.FingerPrint = objFingerprint;
                    }

                    if (request.IdCopyDocument != null && !string.IsNullOrEmpty(request.IdCopyDocument.DocumentContent))
                    {
                        customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                        {
                            FileContent = Convert.FromBase64String(request.IdCopyDocument.DocumentContent),
                            Type = ActivationService.DocumentType.IDCopy
                        });
                    }

                    switch (request.IDTypeCode.ToUpper())
                    {
                        case "V":
                        case "PB":
                        case "VV":
                        case "UV":
                        case "HV":
                            {
                                if (request.VisaCopyDocument != null && !string.IsNullOrEmpty(request.VisaCopyDocument.DocumentContent))
                                {
                                    customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                                    {
                                        FileContent = Convert.FromBase64String(request.VisaCopyDocument.DocumentContent),
                                        Type = ActivationService.DocumentType.Visa
                                    });
                                }
                                break;
                            }
                        case "GP":
                        case "PP":
                        case "UP":
                            {
                                if (request.PassportCopyDocument != null && !string.IsNullOrEmpty(request.PassportCopyDocument.DocumentContent))
                                {
                                    customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                                    {
                                        FileContent = Convert.FromBase64String(request.PassportCopyDocument.DocumentContent),
                                        Type = ActivationService.DocumentType.GulfPassport
                                    });
                                }
                                break;
                            }
                    }
                    customerInfo.CountryCode = request.NationalityCode;
                    ActivationService.ActivationRequest objActivationRequest = new ActivationService.ActivationRequest();
                    objActivationRequest.customerInfo = customerInfo;
                    objActivationRequest.Latitude = request.Latitude;
                    objActivationRequest.Longitude = request.Longitude;
                    objActivationRequest.IsRequesterFBO = IsFBORequester(oRequester.DistributerId);
                    objActivationRequest.ActivationSource = ActivationService.ActivationSource.Commercial;
                    objActivationRequest.RatePlan = (LP.OMS.Channels.Engine.ActivationService.VirginSubscriptionType)Enum.Parse(typeof(LP.OMS.Channels.Engine.ActivationService.VirginSubscriptionType), request.VirginSubscriptionType.ToString());
                    objActivationRequest.VoucherPinCode = request.VoucherPinCode;
                    //ErrorHandling.AddToErrorLog("Information", "ActivateVirgin Request", "SubmitVirginActivation", request.oLoginRequest.Username, XMLSerialize(objActivationRequest));
                    objActivationRequest.ActivationPlanCode = request.ActivationPlanCode;
                    objActivationRequest.VoucherPinCode = request.VoucherPinCode;
                    objActivationRequest.RechargeAmount = request.RechargeDenominationId;
                    objActivationRequest.BundleVoucherCode = request.BundleVoucherCode;
                    objActivationRequest.ReferralCode = request.ReferralCode;
                    objActivationRequest.SubscriptionTypeId = request.SubscriptionTypeId;
                    objActivationRequest.ProductId = request.ProductId;
                    objActivationRequest.Language = request.oLoginRequest.language == 1 ? ActivationService.EngineLanguage.Local : ActivationService.EngineLanguage.Foreign;
                    objActivationRequest.KitID = request.KitCode;
                    objActivationRequest.IsDigitalActivation = request.IsDigitalActivation;
                    objActivationRequest.IsFamilyMember = request.IsFamilyOnboarded;
                    objActivationRequest.FamilyMemberMSISDN = request.FamilyMemberMSISDN;
                    objActivationRequest.MNPPortIn = FillMNPPortInRequest(request.MNPPortInRequest);

                    if (request.SignatureDocument != null && !string.IsNullOrEmpty(request.SignatureDocument.DocumentContent))
                        objActivationRequest.customerInfo.Signature = Convert.FromBase64String(request.SignatureDocument.DocumentContent);


                    if (request.DealerSignatureDocument != null && !string.IsNullOrEmpty(request.DealerSignatureDocument.DocumentContent))
                        objActivationRequest.customerInfo.DealerSignature = Convert.FromBase64String(request.DealerSignatureDocument.DocumentContent);

                    objActivationRequest.customerInfo.Address = request.Address;
                    objActivationRequest.IsESIMActivation = request.IsEsimActivation;
                    objActivationRequest.ReferenceNo = request.ReferenceNo;

                    if (request.ActivationPackageDetails != null && request.ActivationPackageDetails.ServiceDetails.Any())
                    {
                        objActivationRequest.ActivationPackageServiceDetails = new List<string>();
                        foreach (string service in request.ActivationPackageDetails.ServiceDetails)
                        {
                            objActivationRequest.ActivationPackageServiceDetails.Add(service);
                        }
                    }

                    objActivationRequest.LocationID = request.oLoginRequest.LocationId;

                    objActivationRequest.DealerAppOnboardedDetailsDTO =
                           new ActivationService.DealerAppOnboardedDetailsDTO()
                           { VanityTypeDetails = new ActivationService.VanityTypeDetails() };

                    if (request.DealerAppOnboarded != null)
                    {
                        objActivationRequest.DealerAppOnboardedDetailsDTO.IsDealerAppOnboarded = request.DealerAppOnboarded.IsDealerAppOnboarded;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.MSISDN = request.DealerAppOnboarded.MSISDN;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.ID = request.DealerAppOnboarded.VanityTypeDetails.ID;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.NameEN = request.DealerAppOnboarded.VanityTypeDetails.NameEN;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.NameAR = request.DealerAppOnboarded.VanityTypeDetails.NameAR;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.Price = request.DealerAppOnboarded.VanityTypeDetails.Price;
                    }

                    objActivationRequest.IsWalletDeductionEnabled = IsWalletDeductionEnabled(oRequester,
                        request.IsDigitalActivation, request.IsDigitalOnboardingPaid);

                    ActivationService.ActivationPlanDetails objActivationPlanDetails = null;

                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        var activationServiceResponse = client.ActivateVirgin(out objActivationPlanDetails, objActivationRequest);
                        oCallResponse.MSISDN = activationServiceResponse.MSISDN;
                        if (request.IsEsimActivation)
                        {
                            if (activationServiceResponse != null && activationServiceResponse.ESIMData != null
                                && !string.IsNullOrEmpty(activationServiceResponse.ESIMData.SMDP_URL))
                            {
                                oCallResponse.ESIMData.SMDP_URL = activationServiceResponse.ESIMData.SMDP_URL;
                            }
                            else
                            {
                                throw new LPBizException(ErrorCodes.GenericError, "ESIM SMDP_URL is empty");
                            }
                        }
                        oCallResponse.RechargeMessage = string.Empty;

                        if (!string.IsNullOrWhiteSpace(oCallResponse.MSISDN) && oCallResponse.MSISDN.Contains("_"))
                        {
                            string[] parts = oCallResponse.MSISDN.Split('_');
                            oCallResponse.MSISDN = parts[0].Trim();
                            oCallResponse.RechargeMessage = string.Format(", {0}",
                               string.Format(GetBusinessMessage(string.Format("_{0}", parts[1]),
                               request.oLoginRequest.language), objActivationRequest.RechargeAmount));
                        }
                        else
                        {
                            if (objActivationRequest.RechargeAmount > 0)
                            {
                                oCallResponse.RechargeMessage =
                                    string.Format(GetResourceMessage("MSG_CreditTransfer_Success",
                                    request.oLoginRequest.language), objActivationRequest.RechargeAmount);
                            }
                        }
                    }
                    if (objActivationPlanDetails != null)
                    {
                        oCallResponse.ResponseMessage = objActivationPlanDetails.Name;
                    }
                    oCallResponse.IsPassed = true;
                }
                catch (FaultException ex)
                {
                    Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "SubmitVirginActivation", request.oLoginRequest.Username, ex.ToString());
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        oCallResponse.IsPassed = false;

                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language, request.RechargeDenominationId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("SubmitVirginActivation", ex, request.oLoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return oCallResponse;
        }
        public static CallResponse SendRequest(SubmitRequest request)
        {
            string name = "";
            CallResponse oCallResponse = new CallResponse();
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
            oCallResponse = CheckRequester(request.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return oCallResponse;
            }


            if (_storeApplicationFormImage)
            {
                string rootFolder = System.Web.Hosting.HostingEnvironment.MapPath("~/");
                string fileName = Guid.NewGuid().ToString();
                File.WriteAllBytes(Path.Combine(rootFolder, "ApplicationForms", fileName + "_FRiENDi.jpeg"), Convert.FromBase64String(request.IdCopyDocument.DocumentContent));


            }
            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {

                try
                {
                    //HijriCalendar hijriCal = new HijriCalendar();
                    ActivationService.CustomerInfo customerInfo = new ActivationService.CustomerInfo();
                    customerInfo.CustomerDocuments = new List<ActivationService.Document>();
                    customerInfo.IdNumber = request.IdNo;
                    customerInfo.IsDealerActivation = request.IsDealerActivation;
                    customerInfo.ActivationDealerCode = request.DealerCode;
                    System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                    customerInfo.IdTypeCode = request.IDTypeCode;

                    if (request.IdCopyDocument != null && !string.IsNullOrEmpty(request.IdCopyDocument.DocumentContent))
                    {
                        customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                        {
                            FileContent = Convert.FromBase64String(request.IdCopyDocument.DocumentContent),
                            Type = ActivationService.DocumentType.IDCopy
                        });
                    }

                    ActivationService.FingerPrint objFingerprint = null;
                    if (request.FingerPrint != null)
                    {
                        if (!request.FingerPrint.IsSematiOTPUsed)
                        {
                            if (!string.IsNullOrWhiteSpace(request.FingerPrint.FingerPrintText))
                            {
                                objFingerprint = GetActivationServiceFingerprint(request.FingerPrint);
                            }
                        }
                        else
                        {
                            objFingerprint = GetActivationServiceFingerprint(request.FingerPrint);
                        }

                    }
                    customerInfo.FingerPrint = objFingerprint;
                    switch (request.IDTypeCode.ToUpper())
                    {
                        case "V":
                        case "PB":
                        case "VV":
                        case "UV":
                        case "HV":
                            {
                                if (request.VisaCopyDocument != null && !string.IsNullOrEmpty(request.VisaCopyDocument.DocumentContent))
                                {
                                    customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                                    {
                                        FileContent = Convert.FromBase64String(request.VisaCopyDocument.DocumentContent),
                                        Type = ActivationService.DocumentType.Visa
                                    });
                                }
                                break;
                            }
                        case "GP":
                        case "PP":
                        case "UP":
                            {
                                if (request.PassportCopyDocument != null && !string.IsNullOrEmpty(request.PassportCopyDocument.DocumentContent))
                                {
                                    customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                                    {
                                        FileContent = Convert.FromBase64String(request.PassportCopyDocument.DocumentContent),
                                        Type = ActivationService.DocumentType.GulfPassport
                                    });
                                }
                                break;
                            }
                    }

                    customerInfo.CountryCode = request.NationalityCode;
                    customerInfo.Email = request.Email;

                    ActivationService.ActivationRequest objActivationRequest = new ActivationService.ActivationRequest();
                    objActivationRequest.customerInfo = customerInfo;
                    objActivationRequest.Latitude = request.latitude;
                    objActivationRequest.Longitude = request.longitude;
                    objActivationRequest.MSISDN = request.VanityMSISDN;
                    objActivationRequest.VanityBookingCode = request.VanityBookingCode;
                    objActivationRequest.ActivationSource = ActivationService.ActivationSource.Commercial;
                    objActivationRequest.RechargeAmount = request.RechargeDenominationId;
                    objActivationRequest.RatePlan = (request.SubscriptionType == FRiENDiSubscriptionType.PrePaid) ? ActivationService.VirginSubscriptionType.PrePaid : ActivationService.VirginSubscriptionType.Basic;
                    objActivationRequest.BundleVoucherCode = request.BundleVoucherCode;
                    objActivationRequest.ReferralCode = request.ReferralCode;
                    objActivationRequest.SubscriptionTypeId = request.SubscriptionTypeId;
                    objActivationRequest.ProductId = request.ProductId;
                    objActivationRequest.Language = request.oLoginRequest.language == 1 ? ActivationService.EngineLanguage.Local : ActivationService.EngineLanguage.Foreign;
                    objActivationRequest.IMSI = request.IMSI;
                    //objActivationRequest.KitID = request.IMSI;
                    objActivationRequest.MNPPortIn = FillMNPPortInRequest(request.MNPPortInRequest);
                    objActivationRequest.ReferenceNo = request.ReferenceNo;
                    objActivationRequest.IsESIMActivation = request.IsEsimActivation;
                    if (request.SignatureDocument != null && !string.IsNullOrEmpty(request.SignatureDocument.DocumentContent))
                        objActivationRequest.customerInfo.Signature = Convert.FromBase64String(request.SignatureDocument.DocumentContent);
                    if (request.DealerSignatureDocument != null && !string.IsNullOrEmpty(request.DealerSignatureDocument.DocumentContent))
                        objActivationRequest.customerInfo.DealerSignature = Convert.FromBase64String(request.DealerSignatureDocument.DocumentContent);
                    objActivationRequest.customerInfo.Address = request.Address;
                    if (request.ActivationPackageDetails != null && request.ActivationPackageDetails.ServiceDetails.Any())
                    {
                        objActivationRequest.ActivationPackageServiceDetails = new List<string>();
                        foreach (string service in request.ActivationPackageDetails.ServiceDetails)
                        {
                            objActivationRequest.ActivationPackageServiceDetails.Add(service);
                        }
                    }

                    objActivationRequest.LocationID = request.oLoginRequest.LocationId;

                    objActivationRequest.DealerAppOnboardedDetailsDTO =
                        new ActivationService.DealerAppOnboardedDetailsDTO()
                        { VanityTypeDetails = new ActivationService.VanityTypeDetails() };

                    if (request.DealerAppOnboarded != null)
                    {
                        objActivationRequest.DealerAppOnboardedDetailsDTO.IsDealerAppOnboarded = request.DealerAppOnboarded.IsDealerAppOnboarded;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.MSISDN = request.DealerAppOnboarded.MSISDN;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.ID = request.DealerAppOnboarded.VanityTypeDetails.ID;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.NameEN = request.DealerAppOnboarded.VanityTypeDetails.NameEN;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.NameAR = request.DealerAppOnboarded.VanityTypeDetails.NameAR;
                        objActivationRequest.DealerAppOnboardedDetailsDTO.VanityTypeDetails.Price = request.DealerAppOnboarded.VanityTypeDetails.Price;
                    }

                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        ActivationService.ActivationResponse activationResponse = new ActivationService.ActivationResponse();
                        activationResponse = client.Activate(objActivationRequest);
                        oCallResponse.MSISDN = activationResponse.MSISDN;

                        if (request.IsEsimActivation)
                        {
                            if (activationResponse != null && activationResponse.ESIMData != null
                                && !string.IsNullOrEmpty(activationResponse.ESIMData.SMDP_URL))
                            {
                                oCallResponse.ESIMData = new Contracts.ESIMData();
                                oCallResponse.ESIMData.SMDP_URL = activationResponse.ESIMData.SMDP_URL;
                            }
                        }

                        oCallResponse.RechargeMessage = string.Empty;
                        if (!string.IsNullOrWhiteSpace(oCallResponse.MSISDN) && oCallResponse.MSISDN.Contains("_"))
                        {
                            string[] parts = oCallResponse.MSISDN.Split('_');
                            oCallResponse.MSISDN = parts[0].Trim();
                            oCallResponse.RechargeMessage = string.Format(", {0}",
                               string.Format(GetBusinessMessage(string.Format("_{0}", parts[1]),
                               request.oLoginRequest.language), objActivationRequest.RechargeAmount));
                        }
                        else
                        {
                            if (objActivationRequest.RechargeAmount > 0)
                            {
                                oCallResponse.RechargeMessage =
                                    string.Format(GetResourceMessage("MSG_CreditTransfer_Success",
                                    request.oLoginRequest.language), objActivationRequest.RechargeAmount);
                            }
                        }
                    }
                    oCallResponse.IsPassed = true;
                }
                catch (LPBizException LPBizEx)
                {
                    oCallResponse.IsPassed = false;

                    oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language, request.RechargeDenominationId.ToString());
                }
                catch (FaultException ex)
                {
                    LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language, request.RechargeDenominationId.ToString());
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("SendRequest", ex, request.oLoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return oCallResponse;
        }
        public static string XMLSerialize(object objSerilizableObject)
        {
            string XMLSerialization = "Object Of Type \"" + objSerilizableObject.GetType().ToString() + "\" is not serilizable!!!";
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(objSerilizableObject.GetType());
                StringWriter objStringWriter = new StringWriter();
                XmlWriter writer = XmlWriter.Create(objStringWriter);
                xmlSerializer.Serialize(writer, objSerilizableObject);
                XMLSerialization = objStringWriter.ToString();
            }
            catch
            { }

            return XMLSerialization;
        }

        public static LoginResponse Login(LoginRequest request)
        {
            if (ConfigurationManager.AppSettings["AllowedLoginDealerCode"] != null)
            {
                List<string> allowedDealers = ConfigurationManager.AppSettings["AllowedLoginDealerCode"].Split(',').Select(x => x.ToLower()).ToList();

                if (!allowedDealers.Contains(request.Username.ToLower()))
                {
                    ErrorHandling.AddToErrorLog("Information", $"Dealer {request.Username} has not an access for using pre prod apk", "CheckAccessPreProd", request.Username);
                    return new LoginResponse()
                    {
                        ResponseMessage = "Login is restricted, Please contact administrator"
                    };
                }
            }

            LoginResponse oLoginResponse = new LoginResponse();
            try
            {

                COUNTRY_CODE = ConfigurationManager.AppSettings["COUNTRY_CODE"];
                Gulf_COUNTRIES = ConfigurationManager.AppSettings["Gulf_COUNTRIES"];

                if (request.language == 2)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                }

                string userName = request.Username;
                string refillMSISDN = "";
                int distributerId = -1;
                AdministrationServices.Requester oRequester = null;

                if (request.ApplicationVersion != "Portal")
                {
                    CallResponse oCallResponse = null;

                    oCallResponse = CheckRequester(request, out oRequester, out userName, false);

                    if (!oCallResponse.IsPassed)
                    {
                        oLoginResponse.IsPassed = false;
                        oLoginResponse.ResponseMessage = oCallResponse.ResponseMessage;
                        return oLoginResponse;
                    }
                    refillMSISDN = oRequester.RefillMSISDN;
                    distributerId = oRequester.DistributerId;
                }

                string sessionToken = "";
                int nextSessionTimeCheck = 0;

                User user = null;
                AMErrorCode responseCode = AMService.StartSession(request.Username, request.Password, request.MobileDeviceId,
                    request.FigerPrintSerial, request.Latitude,
                    request.Longitude, request.ApplicationVersion == "Portal" ? "Portal" : request.ApplicationVersion,
                    Thread.CurrentThread.CurrentCulture.Name == "en-GB" ? "English" : "Arabic", request.ApplicationVersion,
                    request.DSTAppOS.ToString(), CurrentAndriodApplicationVersion, CurrentIOSApplicationVersion,
                    out sessionToken, out nextSessionTimeCheck, out user);

                oLoginResponse.NextSessionCheckTime = nextSessionTimeCheck;

                if (responseCode != AMErrorCode.Success)
                {
                    oLoginResponse.IsPassed = false;
                    oLoginResponse.ResponseMessage = GetAuthenticationManagementMessage(responseCode);
                    return oLoginResponse;
                }
                else
                {
                    oLoginResponse.IsPassed = true;
                    oLoginResponse.Token = sessionToken;
                }

                if (request.ApplicationVersion != "Portal")
                {
                    //HusseiniI
                    double appVersion = double.Parse(request.ApplicationVersion);

                    if (request.DSTAppOS == DSTAppOS.IOS)
                    {
                        if (appVersion < CurrentIOSApplicationVersion)
                        {
                            oLoginResponse.NewApplicationVersionExists = true;
                            oLoginResponse.ApplicationApkUrl = IOSApplicationUrl;

                        }
                        oLoginResponse.hasMinimalAppVersion = (appVersion >= CurrentIOSApplicationMinimalVersion) ? true : false;

                    }
                    else
                    {
                        if (appVersion < CurrentAndriodApplicationVersion)
                        {
                            oLoginResponse.NewApplicationVersionExists = true;
                            oLoginResponse.ApplicationApkUrl = AndriodApplicationUrl;

                        }
                        oLoginResponse.hasMinimalAppVersion = (appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false;

                    }
                }

                oLoginResponse.IsPassed = oLoginResponse.IsPassed;
                oLoginResponse.ResponseMessage = oLoginResponse.ResponseMessage;
                oLoginResponse.UserName = userName;
                oLoginResponse.RefillMSISDN = refillMSISDN;
                //oLoginResponse.Token = Guid.NewGuid();

                oLoginResponse.msisdnLength = Convert.ToInt32(ConfigurationManager.AppSettings["msisdnLength"]); ;
                oLoginResponse.msisdnStartWith = ConfigurationManager.AppSettings["msisdnStartWith"].ToString();
                oLoginResponse.PortInMSISDNStartWith = ConfigurationManager.AppSettings["PortInMSISDNStartWith"];
                oLoginResponse.iccidLength = int.Parse(ConfigurationManager.AppSettings["iccidLength"].ToString()); ;
                oLoginResponse.iccidStartWith = ConfigurationManager.AppSettings["iccidStartWith"].ToString();
                oLoginResponse.imsiLength = int.Parse(ConfigurationManager.AppSettings["imsiLength"].ToString()); ;
                oLoginResponse.imsiStartWith = ConfigurationManager.AppSettings["imsiStartWith"].ToString();
                oLoginResponse.vaildAge = int.Parse(ConfigurationManager.AppSettings["vaildAge"].ToString());
                oLoginResponse.OpActivation = int.Parse(ConfigurationManager.AppSettings["OpActivation"].ToString());
                oLoginResponse.OpELoad = int.Parse(ConfigurationManager.AppSettings["OpELoad"].ToString());
                oLoginResponse.OpOnlyResubmission = int.Parse(ConfigurationManager.AppSettings["OpOnlyResubmission"].ToString());
                oLoginResponse.OpDealerActivation = int.Parse(ConfigurationManager.AppSettings["OpDealerActivation"].ToString());
                oLoginResponse.OpActivationReport = int.Parse(ConfigurationManager.AppSettings["OpActivationReport"].ToString());

                oLoginResponse.OpVirginPrepaidActivation = int.Parse(ConfigurationManager.AppSettings["OpVirginPrepaidActivation"].ToString());
                oLoginResponse.OpVirginPostpaidActivation = int.Parse(ConfigurationManager.AppSettings["OpVirginPostpaidActivation"].ToString());
                oLoginResponse.OpFNDIResubmission = int.Parse(ConfigurationManager.AppSettings["OpFNDIResubmission"].ToString());
                oLoginResponse.OpVirginResubmission = int.Parse(ConfigurationManager.AppSettings["OpVirginResubmission"].ToString());
                oLoginResponse.OpCommissionReport = int.Parse(ConfigurationManager.AppSettings["OpCommissionReport"].ToString());

                oLoginResponse.OpVirginVanityActivation = int.Parse(ConfigurationManager.AppSettings["OpVirginVanityActivation"].ToString());

                oLoginResponse.ConvertActivationDocsToGrayScale = bool.Parse(ConfigurationManager.AppSettings["ConvertActivationDocsToGrayScale"].ToString());
                oLoginResponse.NotifyApplicationLogs = bool.Parse(ConfigurationManager.AppSettings["NotifyApplicationLogs"].ToString());
                oLoginResponse.LogApplicationCrashes = bool.Parse(ConfigurationManager.AppSettings["LogApplicationCrashes"].ToString());
                oLoginResponse.ApplicationLogExpiryHours = int.Parse(ConfigurationManager.AppSettings["ApplicationLogExpiryHours"].ToString());

                oLoginResponse.ApplicationFormImageQuality = int.Parse(ConfigurationManager.AppSettings["ApplicationFormImageQuality"].ToString());
                oLoginResponse.IdCopyImageQuality = int.Parse(ConfigurationManager.AppSettings["IdCopyImageQuality"].ToString());

                oLoginResponse.KitIDLength = int.Parse(ConfigurationManager.AppSettings["KitIDLength"].ToString());
                oLoginResponse.KitIDStartWith = ConfigurationManager.AppSettings["KitIDStartWith"].ToString();

                oLoginResponse.OpPhysicalFormCollection = int.Parse(ConfigurationManager.AppSettings["OpPhysicalFormCollection"].ToString());
                oLoginResponse.MaxPhysicalFormCollectionBatchSize = int.Parse(ConfigurationManager.AppSettings["MaxPhysicalFormCollectionBatchSize"].ToString());

                oLoginResponse.IsRequesterFBO = IsFBORequester(distributerId);

                oLoginResponse.MaximumCustomerAge = int.Parse(ConfigurationManager.AppSettings["MaximumCustomerAge"].ToString());
                oLoginResponse.MinFriendiPrepaidActivationYears = int.Parse(ConfigurationManager.AppSettings["MinFriendiPrepaidActivationYears"].ToString());
                oLoginResponse.MinVirginPostpaidActivationYears = int.Parse(ConfigurationManager.AppSettings["MinVirginPostpaidActivationYears"].ToString());
                oLoginResponse.MinVirginPrepaidActivationYears = int.Parse(ConfigurationManager.AppSettings["MinVirginPrepaidActivationYears"].ToString());
                oLoginResponse.OpVerifyCustomerDetails = int.Parse(ConfigurationManager.AppSettings["OpVerifyCustomerDetails"].ToString());

                oLoginResponse.MinAllowedNFIQFingerprintValue = int.Parse(ConfigurationManager.AppSettings["MinAllowedNFIQFingerprintValue"].ToString());
                oLoginResponse.OpOwnershipManagement = int.Parse(ConfigurationManager.AppSettings["OpOwnershipManagement"]);
                oLoginResponse.OpFRiENDiPostpaidActivation = int.Parse(ConfigurationManager.AppSettings["OpFRiENDiPostpaidActivation"]);
                oLoginResponse.OpVirginPostpaidBasicActivation = int.Parse(ConfigurationManager.AppSettings["OpVirginPostpaidBasicActivation"]);
                oLoginResponse.OpSIMReplacement = int.Parse(ConfigurationManager.AppSettings["OpSIMReplacement"]);
                oLoginResponse.OpAddonproducts = int.Parse(ConfigurationManager.AppSettings["OpAddonproducts"].ToString());
                oLoginResponse.OpAddonproductsVirgin = int.Parse(ConfigurationManager.AppSettings["OpAddonproductsVirgin"].ToString());
                oLoginResponse.OpFRiENDiDataSimActivation = int.Parse(ConfigurationManager.AppSettings["OpFRiENDiDataSimActivation"]);
                oLoginResponse.OpVirginDataSimActivation = int.Parse(ConfigurationManager.AppSettings["OpVirginDataSimActivation"]);
                oLoginResponse.EnableFRiENDiPostpaidBasicDataSimActivation = bool.Parse(ConfigurationManager.AppSettings["EnableFRiENDiPostpaidBasicDataSimActivation"]);
                oLoginResponse.EnableVirginPostpaidBasicDataSimActivation = bool.Parse(ConfigurationManager.AppSettings["EnableVirginPostpaidBasicDataSimActivation"]);
                oLoginResponse.EnableFRiENDiReferralCodeFeature = bool.Parse(ConfigurationManager.AppSettings["EnableFRiENDiReferralCodeFeature"]);
                oLoginResponse.EnableVirginReferralCodeFeature = bool.Parse(ConfigurationManager.AppSettings["EnableVirginReferralCodeFeature"]);
                oLoginResponse.OpDigitalActivation = int.Parse(ConfigurationManager.AppSettings["OpDigitalActivation"]);
                oLoginResponse.OpELoadVirgin = int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ELoad.Virgin"]);
                oLoginResponse.OpActivationReportVirgin = int.Parse(ConfigurationManager.AppSettings["OpActivationReportVirgin"]);
                oLoginResponse.OpCommissionReportVirgin = int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpCommissionReport.Virgin"]);
                oLoginResponse.OpVerifyCustomerDetailsVirgin = int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.VerifyCustomerDetails.Virgin"]);
                oLoginResponse.OpOwnershipManagementVirgin = int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ManageOwnership.Virgin"]);
                oLoginResponse.OpSIMReplacementVirgin = int.Parse(ConfigurationManager.AppSettings["OpSIMReplacement.Virgin"]);
                oLoginResponse.OpStockFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpStockFRiENDi"]);
                oLoginResponse.OpStockVirgin = int.Parse(ConfigurationManager.AppSettings["OpStockVirgin"]);
                oLoginResponse.OpStatusCheckFRiEDNi = int.Parse(ConfigurationManager.AppSettings["OpStatusCheckFRiEDNi"]);
                oLoginResponse.OpStatusCheckVirgin = int.Parse(ConfigurationManager.AppSettings["OpStatusCheckVirgin"]);
                oLoginResponse.OpPortInFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpPortInFRiENDi"]);
                oLoginResponse.OpPortInVirgin = int.Parse(ConfigurationManager.AppSettings["OpPortInVirgin"]);
                oLoginResponse.OpCancelPortInFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpCancelPortInFRiENDi"]);
                oLoginResponse.OpCancelPortInVirgin = int.Parse(ConfigurationManager.AppSettings["OpCancelPortInVirgin"]);
                oLoginResponse.OpAssociateScannedSim = int.Parse(ConfigurationManager.AppSettings["OpAssociateScannedSim"]);
                oLoginResponse.OpAssociateScannedSimVM = int.Parse(ConfigurationManager.AppSettings["OpAssociateScannedSimVM"]);
                oLoginResponse.OpEsimActivation = int.Parse(ConfigurationManager.AppSettings["OpEsimActivation"]);
                oLoginResponse.OpRefillDealerEWalletFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpRefillDealerEWalletFRiENDi"]);
                oLoginResponse.OpRefillDealerEWalletVirgin = int.Parse(ConfigurationManager.AppSettings["OpRefillDealerEWalletVirgin"]);
                oLoginResponse.EnableFingerprintDeviceLogging = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableFingerprintDeviceLogging"]) ? bool.Parse(ConfigurationManager.AppSettings["EnableFingerprintDeviceLogging"]) : false;
                oLoginResponse.OpAcquisitionMode = int.Parse(ConfigurationManager.AppSettings["OpAcquisitionMode"]);
                oLoginResponse.EnableAcquisitionMode = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableAcquisitionMode"]) ? bool.Parse(ConfigurationManager.AppSettings["EnableAcquisitionMode"]) : false;
                oLoginResponse.EnableOTPValidationMode = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableOTPValidationMode"]) ? bool.Parse(ConfigurationManager.AppSettings["EnableOTPValidationMode"]) : false;
                oLoginResponse.OpSimTerminationVirgin = int.Parse(ConfigurationManager.AppSettings["OpSimTerminationVirgin"]);
                oLoginResponse.OpSimTerminationFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpSimTerminationFRiENDi"]);
                oLoginResponse.OpEsimActivationFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpEsimActivationFRiENDi"]);
                oLoginResponse.EnablePostPaidBasicFPAuth = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnablePostPaidBasicFPAuth"]) ? bool.Parse(ConfigurationManager.AppSettings["EnablePostPaidBasicFPAuth"]) : false;
                oLoginResponse.OpChangeSubscriptionTypeVirgin = int.Parse(ConfigurationManager.AppSettings["OpChangeSubscriptionTypeVirgin"]);
                oLoginResponse.OpChangeSubscriptionTypeFRiENDi = int.Parse(ConfigurationManager.AppSettings["OpChangeSubscriptionTypeFRiENDi"]);
                oLoginResponse.IsVPNDetectionEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsVPNDetectionEnabled"]);
                oLoginResponse.IsMockLocationDetectionEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsMockLocationDetectionEnabled"]);
                oLoginResponse.EnableFPDealerLoginToSEMATI = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableFPDealerLoginToSEMATI"]) ? bool.Parse(ConfigurationManager.AppSettings["EnableFPDealerLoginToSEMATI"]) : false;
                oLoginResponse.EnableIAMOTPDealerLoginToSEMATI = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableIAMOTPDealerLoginToSEMATI"]) ? bool.Parse(ConfigurationManager.AppSettings["EnableIAMOTPDealerLoginToSEMATI"]) : false;
                oLoginResponse.OpDealerHiring = int.Parse(ConfigurationManager.AppSettings["OpDealerHiring"]);
                oLoginResponse.AllowedAreaRadius = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["AllowedAreaRadius"]) ? Convert.ToInt32(ConfigurationManager.AppSettings["AllowedAreaRadius"]) : 0;
                oLoginResponse.EnableInDirectSaleZeroSIMActivationCR = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableInDirectSaleZeroSIMActivationCR"]);
                List<String> list = ConfigurationManager.AppSettings["MandatoryField"].ToString().Split('|').ToList();
                oLoginResponse.MandatoryField = list;
                oLoginResponse.EnableSecuGenFP = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSecuGenFP"]);
                oLoginResponse.EnableColumboFP = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableColumboFP"]);
                oLoginResponse.FRiENDiBookingRight = int.Parse(ConfigurationManager.AppSettings["FRiENDiBookingRight"]);
                oLoginResponse.VirginBookingRight = int.Parse(ConfigurationManager.AppSettings["VirginBookingRight"]);
                oLoginResponse.VirginVanityBookingRight = int.Parse(ConfigurationManager.AppSettings["VirginVanityBookingRight"]);
                if (request.ApplicationVersion != "Portal")
                    oLoginResponse.CanPOSActivateDigital = CanPOSActivateDigital(oRequester);
                oLoginResponse.FP_Bitrate = ConfigurationManager.AppSettings["FP_Bitrate"];
                //To DO Set User Privilage
                oLoginResponse.UserPrivilageList = new List<int>();
                // M.Obeidat
                //AAlzu'bi 
                //set all privilage

                if (request.ApplicationVersion != "Portal")
                {
                    if (oRequester.Rights != null)
                    {
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.Activation"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpActivation); //FRiENDi Prepaid Activation
                        }
                        // In case the refill MSISDN is not set, the dealer will not have the prividlge to use eload
                        if (!string.IsNullOrEmpty(oRequester.RefillMSISDN) && oRequester.RefillMSISDN != ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.DefaultRefillMSISDN"].ToString() &&
                            oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ELoad"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpELoad);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.DealerActivation"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpDealerActivation);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpActivationReport"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpActivationReport);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginPostpaidActivation"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginPostpaidActivation);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginPrepaidActivation"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginPrepaidActivation);
                        }

                        //if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginPrepaidActivation"]).Any())
                        //{
                        //    oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginPostpaidActivation);
                        //}
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginResubmition"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginResubmission);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpFriendiResubmition"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpFNDIResubmission);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpCommissionReport"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpCommissionReport);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpPhysicalFormCollection"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpPhysicalFormCollection);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginVanityActivation"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginVanityActivation);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.FamilyIDFriendi"]))
                        {
                            oLoginResponse.CanUseDateOfBirthFriendi = true;
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.FamilyIDVirgin"]))
                        {
                            oLoginResponse.CanUseDateOfBirthVirgin = true;
                        }
                        if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.VerifyCustomerDetails"]) || oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.VerifyCustomerDetails"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVerifyCustomerDetails);
                        }
                        if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.PlanVoucherPrepaid"]) || oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.PlanVoucherPrepaid"]))
                        {
                            oLoginResponse.CanUsePlanVoucherPrepaid = true;
                        }
                        if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.PlanActivationCode"]) || oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.PlanActivationCode"]))
                        {
                            oLoginResponse.CanUsePlanActivationCodePrepaid = true;
                        }
                        if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ManageOwnership"]) || oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ManageOwnership"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpOwnershipManagement);
                        }
                        if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.UpdateID"]) || oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.UpdateID"]))
                        {
                            oLoginResponse.CanUpdateIDForOwnership = true;
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpFRiENDiPostpaidActivation"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpFRiENDiPostpaidActivation);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpVirginPostpaidBasicActivation"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginPostpaidBasicActivation);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpSIMReplacement"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpSIMReplacement);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["OpAddonproducts"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpAddonproducts);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["OpAddonproductsVirgin"]).Any())
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpAddonproductsVirgin);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ActivateWithBundle"]).Any())
                        {
                            oLoginResponse.CanActivateWithBundle = true;
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ActivateWithNoAdditions"]).Any())
                        {
                            oLoginResponse.CanActivateWithNoAdditions = true;
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpFRiENDiDataSimActivation"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpFRiENDiDataSimActivation);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpVirginDataSimActivation"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVirginDataSimActivation);
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["FRiENDiSematiOTPRightCode"]).Any())
                        {
                            oLoginResponse.CanUseFRiENDiSematiOTP = true;
                        }
                        if (oRequester.Rights.Where(p => p.Code == ConfigurationManager.AppSettings["VirginSematiOTPRightCode"]).Any())
                        {
                            oLoginResponse.CanUseVirginSematiOTP = true;
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpDigitalActivation"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpDigitalActivation);
                        }
                        if (!string.IsNullOrEmpty(oRequester.RefillMSISDN) && oRequester.RefillMSISDN != ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.DefaultRefillMSISDN"].ToString() &&
                             oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ELoad.Virgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpELoadVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpActivationReportVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpActivationReportVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpCommissionReport.Virgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpCommissionReportVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.VerifyCustomerDetails.Virgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpVerifyCustomerDetailsVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.ManageOwnership.Virgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpOwnershipManagementVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.UpdateID.Virgin"]))
                        {
                            oLoginResponse.CanUpdateIDForOwnershipVirgin = true;
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpSIMReplacement.Virgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpSIMReplacementVirgin);
                        }

                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpStockFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpStockFRiENDi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpStockVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpStockVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpStatusCheckFRiEDNi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpStatusCheckFRiEDNi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpStatusCheckVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpStatusCheckVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpPortInFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpPortInFRiENDi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpPortInVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpPortInVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpCancelPortInFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpCancelPortInFRiENDi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpCancelPortInVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpCancelPortInVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpAssociateScannedSim"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpAssociateScannedSim);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpAssociateScannedSimVM"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpAssociateScannedSimVM);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpEsimActivation"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpEsimActivation);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpRefillDealerEWalletFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpRefillDealerEWalletFRiENDi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpRefillDealerEWalletVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpRefillDealerEWalletVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpAcquisitionMode"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpAcquisitionMode);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpSimTerminationVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpSimTerminationVirgin);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpSimTerminationFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpSimTerminationFRiENDi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpEsimActivationFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpEsimActivationFRiENDi);
                        }

                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpChangeSubscriptionTypeVirgin"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpChangeSubscriptionTypeVirgin);
                        }

                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpChangeSubscriptionTypeFRiENDi"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpChangeSubscriptionTypeFRiENDi);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["OpDealerHiring"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.OpDealerHiring);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["FRiENDiBookingRight"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.FRiENDiBookingRight);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["VirginBookingRight"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.VirginBookingRight);
                        }
                        if (oRequester.Rights.Any(p => p.Code == ConfigurationManager.AppSettings["VirginVanityBookingRight"]))
                        {
                            oLoginResponse.UserPrivilageList.Add(oLoginResponse.VirginVanityBookingRight);
                        }
                    }
                }

                oLoginResponse.HasAnyVirginOperation = oLoginResponse.UserPrivilageList.Any(p => p == oLoginResponse.OpVirginPostpaidActivation || p == oLoginResponse.OpVirginPrepaidActivation || p == oLoginResponse.OpVirginResubmission || p == oLoginResponse.OpSIMReplacementVirgin || p == oLoginResponse.OpAddonproductsVirgin || p == oLoginResponse.OpVirginDataSimActivation || p == oLoginResponse.OpELoadVirgin || p == oLoginResponse.OpActivationReportVirgin || p == oLoginResponse.OpCommissionReportVirgin || p == oLoginResponse.OpVerifyCustomerDetailsVirgin || p == oLoginResponse.OpOwnershipManagementVirgin || p == oLoginResponse.OpVirginVanityActivation || p == oLoginResponse.OpStockVirgin || p == oLoginResponse.OpStatusCheckVirgin || p == oLoginResponse.OpPortInVirgin || p == oLoginResponse.OpCancelPortInVirgin);
                oLoginResponse.HasAnyFriendiOperation = oLoginResponse.UserPrivilageList.Any(p => p == oLoginResponse.OpActivation || p == oLoginResponse.OpDealerActivation || p == oLoginResponse.OpFNDIResubmission || p == oLoginResponse.OpFRiENDiPostpaidActivation || p == oLoginResponse.OpSIMReplacement || p == oLoginResponse.OpAddonproducts || p == oLoginResponse.OpFRiENDiDataSimActivation || p == oLoginResponse.OpELoad || p == oLoginResponse.OpActivationReport || p == oLoginResponse.OpCommissionReport || p == oLoginResponse.OpVerifyCustomerDetails || p == oLoginResponse.OpOwnershipManagement || p == oLoginResponse.OpStockFRiENDi || p == oLoginResponse.OpStatusCheckFRiEDNi || p == oLoginResponse.OpPortInFRiENDi || p == oLoginResponse.OpCancelPortInFRiENDi);

                oLoginResponse.WellcomeMessage = string.Empty;
                // Show message for pos only
                if (_lstWelcomeMessageRequesterClasses.Any(p => p == distributerId))
                {
                    oLoginResponse.WellcomeMessage = _defaultWelcomeMessage;
                    if (request.language == 2)
                    {
                        oLoginResponse.WellcomeMessage = _arabicWelcomeMessage;
                    }
                }

                oLoginResponse.IsAttendanceEnabled = ModernTradeController.IS_ATTENDANCE_ENABLED;

                if (request.ApplicationVersion != "Portal")
                    oLoginResponse.IsModernTrade = ModernTradeController.IsModernTradeDealer(oRequester.UserName);

                if (oLoginResponse.IsModernTrade)
                {
                    string locationID = string.Empty;
                    oLoginResponse.IsCheckInRequired = ModernTradeController.IsDealerCheckInRequired(user, oRequester.UserName, oLoginResponse.IsModernTrade, sessionToken, out locationID);
                    using (AttendenceEntities context = new AttendenceEntities())
                    {
                        ObjectParameter result = new ObjectParameter("Result", typeof(int));
                        context.IsVerificationRequired(oRequester.UserName, result);
                        oLoginResponse.IsAttendanceVerificationRequired = (int)result.Value == 1 ? true : false;
                    }

                    if (!oLoginResponse.IsCheckInRequired && !string.IsNullOrEmpty(locationID))
                    {
                        oLoginResponse.LocationID = locationID;
                        Contracts.Location location = ModernTradeController.GetLocationByID(locationID, request.language, oRequester.UserName);
                        if (location != null)
                        {
                            oLoginResponse.CurrentLocation = location.LocationName;
                        }
                        else
                        {
                            oLoginResponse.CurrentLocation = string.Empty;
                        }
                    }
                }
                else
                {
                    Contracts.Location dealerLocation = ModernTradeController.GetDealerDefaultLocationDetails(request.Username, request.language);

                    if (dealerLocation != null)
                    {
                        oLoginResponse.LocationID = dealerLocation.LocationId;
                    }
                }

                //if ((/*!oLoginResponse.IsModernTrade ||*/ !oLoginResponse.IsCheckInRequired) && request.ApplicationVersion != "Portal")
                if (ModernTradeController.IS_ATTENDANCE_ENABLED && request.ApplicationVersion != "Portal")
                {
                    #region AttendanceTracking

                    Contracts.AttendanceTrackingRequest AttendanceTrackingRequest = new AttendanceTrackingRequest();
                    AttendanceTrackingRequest.AttendanceProcessType = (int)AttendanceProcessType.Login;
                    AttendanceTrackingRequest.Latitude = request.Latitude;
                    AttendanceTrackingRequest.Longitude = request.Longitude;
                    AttendanceTrackingRequest.LoginRequest = new LoginRequest();
                    request.Token = sessionToken;

                    if (!string.IsNullOrEmpty(oLoginResponse.LocationID))
                        request.LocationId = oLoginResponse.LocationID;


                    AttendanceTrackingRequest.LoginRequest = request;

                    if (ModernTradeController.LOCATION_TRACKING_MODE == 1) // All Dealers
                    {
                        ModernTradeController.AttendanceTracking(AttendanceTrackingRequest);

                    }
                    else if (ModernTradeController.LOCATION_TRACKING_MODE == 2) // Modern Trade Dealers
                    {
                        if (oLoginResponse.IsModernTrade)
                        {
                            ModernTradeController.AttendanceTracking(AttendanceTrackingRequest);
                        }
                    }
                    #endregion
                }

                if (request.ApplicationVersion != "Portal")
                {
                    oLoginResponse.isIndirectSalesChannel = IsIndirectSalesChannel(oRequester.DistributerId);
                }
            }
            catch (LPBizException LPBizEx)
            {
                oLoginResponse.IsPassed = false;
                oLoginResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
            }
            catch (FaultException ex)
            {
                LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                oLoginResponse.IsPassed = false;
                oLoginResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Login", ex, request.Username);
                oLoginResponse.IsPassed = false;
                oLoginResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return oLoginResponse;
        }
        static private int GetIDTypeValidityYears(string code)
        {
            switch (code)
            {
                case "I":
                    return int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.ValidityYears.Iqama"]);
                    break;
                case "S":
                    return int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.ValidityYears.SaudiId"]);
                    break;
                case "V":
                    return int.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.ValidityYears.VisitVisa"]);
                    break;
            }
            return 0;
        }
        public static Lookups GetLookups(LoginRequest oLoginRequest)
        {
            Lookups lookups = new Lookups();
            OMSCoreEntities db = new OMSCoreEntities();
            Requester Requester = db.Requesters.FirstOrDefault(r => r.UserName == oLoginRequest.Username);
            int RequesterId = Requester.Id;
            //lookups.SaudiCountryCode = ConfigurationManager.AppSettings["SaudiCountryCode"];
            lookups.EnableTCCOTPVerificationMechanism = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableTCCOTPVerificationMechanism"]);
            lookups.ResendTime = ConfigurationManager.AppSettings["ResendTime"];
            lookups.MessageCodeTime = ConfigurationManager.AppSettings["MessageCodeTime"];
            lookups.OTPVisabilty = ConfigurationManager.AppSettings["OTPVisabilty"];
            lookups.SaudiCountryCode = ConfigurationManager.AppSettings["SaudiCountryCode"];
            lookups.ActivationAndCommissionReportsDateTimeFormat = ConfigurationManager.AppSettings["ActivationAndCommissionReportsCustomDateTimeFormat"];
            lookups.VAT = ConfigurationManager.AppSettings["VAT"];
            lookups.ValidVoucherLength = 14;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ValidVoucherLength"]))
            {
                lookups.ValidVoucherLength = int.Parse(ConfigurationManager.AppSettings["ValidVoucherLength"]);
            }
            lookups.SimPriceOrSubscriptionFee = 5;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SimPriceOrSubscriptionFee"]))
            {
                lookups.SimPriceOrSubscriptionFee = decimal.Parse(ConfigurationManager.AppSettings["SimPriceOrSubscriptionFee"]);
            }

            lookups.Genders = new List<Contracts.Gender>(0);
            lookups.Titles = new List<Contracts.Title>(0);
            lookups.WFActivityStatuses = new List<Contracts.WFActivityStatus>(0);
            lookups.ReplacementReasons = new List<Contracts.ReplacementReason>(0);
            lookups.IDTypes = new List<IDType>(0);
            lookups.DealerActivationIDTypes = new List<IDType>(0);

            #region(Country)

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {

                try
                {
                    List<ActivationService.CountryContract> Contrylist = client.GetCountryList(oLoginRequest.language == 1 ? ActivationService.EngineLanguage.Local : ActivationService.EngineLanguage.Foreign);
                    String[] arrGulf_COUNTRIES = Gulf_COUNTRIES.Split(',');
                    Contrylist = (from p in Contrylist where arrGulf_COUNTRIES.Contains(p.Code) select p).ToList();
                    lookups.Countries = new List<Contracts.Country>(Contrylist.Count());
                    foreach (ActivationService.CountryContract country in Contrylist)
                    {
                        Contracts.Country countryContract = new Contracts.Country();
                        countryContract.Id = Convert.ToInt16(country.Code);
                        countryContract.Code = country.Code;
                        countryContract.Description = country.Name;


                        lookups.Countries.Add(countryContract);
                    }
                }
                catch (Exception ex)
                {
                    Utilities.ErrorHandling.HandleException("GetLookups", ex, oLoginRequest.Username);
                    return lookups;
                }

            }

            #endregion(Country)
            #region(Status)
            //TODO: to be comminted in case of deploying a build for phase 1
            //oStatuses = (from statuses in coreContext.Status select statuses).ToList();
            //lookups.WFActivityStatuses = new List<WFActivityStatus>(oStatuses.Count());
            //foreach (Status stat in oStatuses)
            //{
            //    Contracts.WFActivityStatus wFActivityStatus = new Contracts.WFActivityStatus();
            //    wFActivityStatus.Id = stat.Id;
            //    if (oLoginRequest.language == 1) wFActivityStatus.Description = stat.Name;
            //    else wFActivityStatus.Description = stat.ForeignName;

            //    lookups.WFActivityStatuses.Add(wFActivityStatus);
            //}
            //lookups.WFActivityStatuses = lookups.WFActivityStatuses.OrderBy(x => x.Description).ToList();
            #endregion(Status)

            #region(Gender)
            //genderList = (from genders in channelContext.Genders select genders).ToList();
            //lookups.Genders = new List<Contracts.Gender>(genderList.Count);

            //foreach (Gender gender in genderList)
            //{
            //    Contracts.Gender genderContract = new Contracts.Gender();
            //    genderContract.Code = gender.Code;
            //    genderContract.Id = gender.ID;

            //    if (oLoginRequest.language == 1) genderContract.Name = gender.Name;
            //    else genderContract.Name = gender.ForeignName;

            //    lookups.Genders.Add(genderContract);
            //}
            #endregion(Gender)
            #region(Title)
            //titleList = (from tittles in channelContext.Titles select tittles).ToList();
            //lookups.Titles = new List<Contracts.Title>(titleList.Count());

            //foreach (Title title in titleList)
            //{
            //    Contracts.Title titleContract = new Contracts.Title();
            //    titleContract.Code = title.Code;
            //    titleContract.Id = title.ID;
            //    if (oLoginRequest.language == 1) titleContract.Description = title.Name;
            //    else titleContract.Description = title.ForeignName;

            //    lookups.Titles.Add(titleContract);
            //}
            #endregion(Title)
            #region(IDTypes)

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {

                try
                {
                    List<ActivationService.IdTypeContract> IdTypelist = client.GetIdTypeList(oLoginRequest.language == 1 ? ActivationService.EngineLanguage.Local : ActivationService.EngineLanguage.Foreign);
                    lookups.IDTypes = new List<IDType>(IdTypelist.Count());
                    lookups.DealerActivationIDTypes = new List<IDType>(IdTypelist.Count());
                    lookups.VanityActivationIDTypes = new List<IDType>();
                    lookups.OwnershipIDTypes = new List<IDType>();
                    //Fill ID Type for CustomerActivation
                    foreach (ActivationService.IdTypeContract idType in IdTypelist)
                    {
                        Contracts.IDType idTypeContract = new Contracts.IDType();
                        idTypeContract.Id = 0;
                        idTypeContract.Code = idType.Code;
                        idTypeContract.Description = idType.Name;
                        idTypeContract.ValidityYears = GetIDTypeValidityYears(idTypeContract.Code);

                        lookups.IDTypes.Add(idTypeContract);
                    }

                    string allowedIDType = ConfigurationManager.AppSettings["AllowedIDTypesForDealerActivation"].ToString();
                    //Fill ID Types for dealer Activation
                    foreach (ActivationService.IdTypeContract idType in IdTypelist)
                    {
                        if (allowedIDType.Split(',').Where(p => p == idType.Code).Any())
                        {
                            Contracts.IDType idTypeContract = new Contracts.IDType();
                            idTypeContract.Id = 0;
                            idTypeContract.Code = idType.Code;
                            idTypeContract.Description = idType.Name;
                            idTypeContract.ValidityYears = GetIDTypeValidityYears(idTypeContract.Code);

                            lookups.DealerActivationIDTypes.Add(idTypeContract);
                        }
                    }

                    string vanityActivationIDTypes = ConfigurationManager.AppSettings["VanityActivationIDTypes"].ToString();
                    //Fill ID Types for dealer Activation
                    foreach (ActivationService.IdTypeContract idType in IdTypelist)
                    {
                        if (vanityActivationIDTypes.Split(',').Where(p => p == idType.Code).Any())
                        {
                            Contracts.IDType idTypeContract = new Contracts.IDType();
                            idTypeContract.Id = 0;
                            idTypeContract.Code = idType.Code;
                            idTypeContract.Description = idType.Name;
                            idTypeContract.ValidityYears = GetIDTypeValidityYears(idTypeContract.Code);

                            lookups.VanityActivationIDTypes.Add(idTypeContract);
                        }
                    }

                    string ownershipIDTypes = ConfigurationManager.AppSettings["OwnershipIDTypes"].ToString();
                    //Fill ID Types for dealer Activation
                    foreach (ActivationService.IdTypeContract idType in IdTypelist)
                    {
                        if (ownershipIDTypes.Split(',').Where(p => p == idType.Code).Any())
                        {
                            Contracts.IDType idTypeContract = new Contracts.IDType();
                            idTypeContract.Id = 0;
                            idTypeContract.Code = idType.Code;
                            idTypeContract.Description = idType.Name;
                            idTypeContract.ValidityYears = GetIDTypeValidityYears(idTypeContract.Code);

                            lookups.OwnershipIDTypes.Add(idTypeContract);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LPLogger.Information("error get IDTypes: " + ex.ToString());
                    return lookups;
                }

            }
            #endregion(IDTypes)
            #region(SIMReplacementReason)
            //AAlzu'bi :: Load Repalcement reasons.
            //List<SIMReplacementReason> lstReplacementReason = channelContext.SIMReplacementReasons.ToList();
            //lookups.ReplacementReasons = new List<Contracts.ReplacementReason>(lstReplacementReason.Count);

            //foreach (var replacementReason in lstReplacementReason)
            //{
            //    Contracts.ReplacementReason objReplacementReasonContract = new Contracts.ReplacementReason();
            //    objReplacementReasonContract.Id = replacementReason.ID;
            //    objReplacementReasonContract.Description = replacementReason.Name;
            //    if (oLoginRequest.language != 1)
            //    {
            //        objReplacementReasonContract.Description = replacementReason.ForeignName;
            //    }

            //    lookups.ReplacementReasons.Add(objReplacementReasonContract);
            //}
            #endregion(SIMReplacementReason)
            #region(Get CurrentHijriDate)
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");


            System.Globalization.HijriCalendar c = new HijriCalendar();

            lookups.currentHijriDate = new CurrentHijriDate();
            lookups.currentHijriDate.Day = c.GetDayOfMonth(DateTime.Now).ToString();
            lookups.currentHijriDate.Month = c.GetMonth(DateTime.Now).ToString();
            lookups.currentHijriDate.Year = c.GetYear(DateTime.Now).ToString();

            #endregion(Get CurrentHijriDate)

            //Anas Alzube
            #region(denomination)
            List<Domination> denominations = db.Dominations.ToList();//list of all denominations 
            List<RequesterRight> requesterRights = db.RequesterRights.Where(b => b.RequesterID == RequesterId).ToList();// list of rights for requester
            List<DBRight> rights = db.DBRights.ToList();//list of all rights 
            lookups.Dominations = new List<Contracts.Domination>();
            lookups.VirginDenominations = new List<Contracts.Domination>();
            foreach (RequesterRight qr in requesterRights)
            {
                for (int i = 0; i < denominations.Count; i++)
                {
                    if (rights.Where(r => r.ID == qr.RightID).First().Name == denominations[i].Name)
                    {
                        Contracts.Domination denomination = new Contracts.Domination();//To Contract 

                        denomination.ID = denominations[i].ID;
                        denomination.Name = denominations[i].Name;
                        denomination.Value = denominations[i].Value;

                        lookups.Dominations.Add(denomination);
                    }
                    else if (rights.Where(r => r.ID == qr.RightID).First().Name.IndexOf("_Virgin") != -1)
                    {
                        if (rights.Where(r => r.ID == qr.RightID).First().Name.Replace("_Virgin", "") == denominations[i].Name)
                        {
                            Contracts.Domination denomination = new Contracts.Domination();//To Contract 

                            denomination.ID = denominations[i].ID;
                            denomination.Name = denominations[i].Name;
                            denomination.Value = denominations[i].Value;

                            lookups.VirginDenominations.Add(denomination);
                        }
                    }
                }
            }

            lookups.ELoadDenominations = new List<Contracts.Domination>();
            string eLoadDenominations = ConfigurationManager.AppSettings["ELoadDenominations"];
            List<int> eLoadDenominationlst = eLoadDenominations.Split(',').Select(int.Parse).ToList();
            foreach (int eloadValue in eLoadDenominationlst)
            {
                lookups.ELoadDenominations.Add(new Contracts.Domination()
                {
                    Name = eloadValue.ToString() + " " + (oLoginRequest.language == 1 ? "SAR" : "ر.س"),
                    Value = eloadValue
                });
            }
            #endregion

            //Anas Alzube
            #region(Items)
            List<StockItem> itemlist = db.StockItems.ToList();
            lookups.Items = new List<Contracts.StockItem>();
            foreach (StockItem item in itemlist)
            {
                Contracts.StockItem stock = new Contracts.StockItem();
                stock.ID = item.ID;
                if (oLoginRequest.language == (int)OMSLanguages.Foreign)
                {
                    stock.Name = item.ForgeinName;
                }
                else
                {
                    stock.Name = item.Name;
                }
                stock.CategoryID = item.CategoryID.Value;
                lookups.Items.Add(stock);
            }
            #endregion

            #region(Nationality)
            try
            {
                lookups.NationalityList = GetNationalityList((OMSLanguages)oLoginRequest.language);
            }
            catch (Exception ex)
            {
                Utilities.ErrorHandling.HandleException("GetLookups", ex, oLoginRequest.Username);
                return lookups;
            }
            #endregion

            #region Cities
            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                lookups.Cities = new List<Contracts.City>();
                try
                {
                    List<ActivationService.City> Cities = client.GetCityList();
                    foreach (ActivationService.City city in Cities)
                    {
                        Contracts.City contractsCity = new Contracts.City()
                        {
                            ID = city.ID,
                            Name = oLoginRequest.language == 1 ? city.NameEn : city.NameAr
                        };

                        lookups.Cities.Add(contractsCity);
                    }
                }
                catch (Exception ex)
                {
                    Utilities.ErrorHandling.HandleException("GetLookups", ex, oLoginRequest.Username);
                    return lookups;
                }

            }
            #endregion

            lookups.PoorNFIQMessage = oLoginRequest.language == 1 ? ConfigurationManager.AppSettings["PoorNFIQMessage"].Split('|')[0] : ConfigurationManager.AppSettings["PoorNFIQMessage"].Split('|')[1];

            if (lookups.Dominations != null && lookups.Dominations.Count() > 0)
            {
                int lowestFRiENDiDenomination = lookups.Dominations.Min(d => d.Value);
                int lowestVirginDenomination = lookups.VirginDenominations.Min(d => d.Value);
                AccessManagementController.SaveLowestAllowedDenomination(oLoginRequest.Token.ToString(), lowestFRiENDiDenomination, lowestVirginDenomination);
            }

            #region TerminationReasons
            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                lookups.TerminationReasons = new List<Contracts.TerminationReason>();

                List<ActivationService.TerminationReasonModel> TerminationReasons = client.GetTerminationReasonsList();

                if (TerminationReasons != null)
                {
                    foreach (ActivationService.TerminationReasonModel ReasonObj in TerminationReasons)
                    {
                        Contracts.TerminationReason contractTerminationReason = new Contracts.TerminationReason()
                        {
                            key = ReasonObj.ID.ToString(),
                            value = oLoginRequest.language == 1 ? ReasonObj.Reason : ReasonObj.ReasonAr
                        };

                        lookups.TerminationReasons.Add(contractTerminationReason);
                    }
                }
            }

            #endregion

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EsimPriceVATExcluded"]))
                lookups.EsimPriceVATExcluded = decimal.Parse(ConfigurationManager.AppSettings["EsimPriceVATExcluded"]);

            lookups.MTResendLocationPeriod = ModernTradeController.MT_RESEND_LOCATION_PERIOD;
            lookups.LocationTrackingMode = ModernTradeController.LOCATION_TRACKING_MODE;
            lookups.MTConfiguration = new MTConfiguration()
            {
                LocationsConfiguration = ModernTradeController.GetMTLocationsConfiguration(),
                AllowedAppFeatures = new List<int>()
            };

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MT_AllowedAppFeatures"]))
                lookups.MTConfiguration.AllowedAppFeatures
                        = ConfigurationManager.AppSettings["MT_AllowedAppFeatures"].Split(',')
                        .Select(int.Parse).ToList();

            return lookups;
        }
        public static DealerActivitiesResponseColelction GetDealerActivitiesList(AndroidReportRequest oAndroidReportRequest)
        {
            DealerActivitiesResponseColelction oDealerActivitiesResponseColelction = new DealerActivitiesResponseColelction();
            oDealerActivitiesResponseColelction.DealerActivitiesResponseList = new List<DealerActivitiesResponse>();

            string name = "";
            AdministrationServices.Requester dealer = null;

            double appVersion = double.Parse(oAndroidReportRequest.oLoginRequest.ApplicationVersion);
            CallResponse oCallResponse = CheckRequester(oAndroidReportRequest.oLoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                oDealerActivitiesResponseColelction.IsPassed = false;
                oDealerActivitiesResponseColelction.ResponseMessage = oCallResponse.ResponseMessage;

                return oDealerActivitiesResponseColelction;
            }

            DealerActivitiesResponse oDealerActivitiesResponse = new DealerActivitiesResponse();


            try
            {
                using (OMSCoreEntities context = new OMSCoreEntities())
                {
                    List<WFActivity> lstWFActivities = new List<WFActivity>();
                    List<int> documentTypeIds = new List<int>();
                    if (oAndroidReportRequest.lstDocumentTypeIds != String.Empty)
                    {

                        string[] arr = oAndroidReportRequest.lstDocumentTypeIds.Split(',');
                        for (int i = 0; i < arr.Length; i++)
                        {
                            documentTypeIds.Add(int.Parse(arr[i]));
                        }
                    }
                    switch (oAndroidReportRequest.Resubmission)
                    {
                        case (int)ResubmissionEnum.NoResubmission:
                            lstWFActivities = (from wfActivities in context.WFActivities
                                               where wfActivities.IsLastActivity == true &&
                                                   (oAndroidReportRequest.StatusId == 0 ? wfActivities.StatusID != 3 : wfActivities.StatusID == oAndroidReportRequest.StatusId) &&
                                                   wfActivities.WFRequest.RequesterID == (int?)dealer.ID &&
                                                   documentTypeIds.Contains((int)wfActivities.WFRequest.DocumentTypeID) &&
                                                   wfActivities.WFActivityDate >= oAndroidReportRequest.FromDate &&
                                                   wfActivities.WFActivityDate <= oAndroidReportRequest.ToDate
                                               //&& wfActivities.WFRequest.ChannelId == 3
                                               select wfActivities).ToList<WFActivity>();
                            break;
                        case (int)ResubmissionEnum.IncludeResubmission:
                            lstWFActivities = (from wfActivities in context.WFActivities
                                               where wfActivities.IsLastActivity == true &&
                                                   (oAndroidReportRequest.StatusId == 0 ? wfActivities.StatusID != 3 : wfActivities.StatusID == oAndroidReportRequest.StatusId) &&
                                                   (wfActivities.WFRequest.RequesterID == dealer.ID || wfActivities.IndexedField10 == dealer.Code) &&
                                                   documentTypeIds.Contains((int)wfActivities.WFRequest.DocumentTypeID) &&
                                                   wfActivities.WFActivityDate >= oAndroidReportRequest.FromDate &&
                                                   wfActivities.WFActivityDate <= oAndroidReportRequest.ToDate
                                               //&& wfActivities.WFRequest.ChannelId == 3
                                               select wfActivities).ToList<WFActivity>();
                            break;
                        case (int)ResubmissionEnum.OnlyResubmission:
                            lstWFActivities = (from wfActivities in context.WFActivities
                                               where wfActivities.IsLastActivity == true &&
                                                   (oAndroidReportRequest.StatusId == 0 ? wfActivities.StatusID != 3 : wfActivities.StatusID == oAndroidReportRequest.StatusId) &&
                                                   wfActivities.IndexedField9 == dealer.Code &&
                                                   wfActivities.WFActivityDate >= oAndroidReportRequest.FromDate &&
                                                   wfActivities.WFActivityDate <= oAndroidReportRequest.ToDate
                                               //&& wfActivities.WFRequest.ChannelId == 3
                                               select wfActivities).ToList<WFActivity>();
                            break;
                        default:
                            break;
                    }
                    oDealerActivitiesResponseColelction.NumberOfRecords = lstWFActivities.Count;
                    lstWFActivities = lstWFActivities.OrderBy(p => p.ID).Skip(oAndroidReportRequest.StartIndex).Take(oAndroidReportRequest.PageSize).ToList<WFActivity>();
                    if (lstWFActivities.Count != 0)
                    {
                        foreach (WFActivity objWFActivity in lstWFActivities)
                        {
                            oDealerActivitiesResponse = new DealerActivitiesResponse();
                            oDealerActivitiesResponse.StatusName = objWFActivity.Action.Name;
                            oDealerActivitiesResponse.StatusForeignName = objWFActivity.Action.ForeignName;
                            oDealerActivitiesResponse.ChannelName = objWFActivity.WFRequest.Channel.Name;
                            oDealerActivitiesResponse.RequestDate = Convert.ToDateTime(objWFActivity.WFRequest.WFStartDate).ToShortDateString();
                            oDealerActivitiesResponse.Imsi = objWFActivity.IndexedField2;
                            oDealerActivitiesResponse.MSISDN = objWFActivity.IndexedField1;
                            oDealerActivitiesResponse.DocumentTypeId = (int)objWFActivity.WFRequest.DocumentTypeID;
                            oDealerActivitiesResponse.DocumentTypeDescription = objWFActivity.WFRequest.DocumentTypeID.ToString();//Enum.Parse(typeof(DocumentType), objWFActivity.WFRequest.DocumentTypeID.ToString()).ToString();
                            if (oAndroidReportRequest.Resubmission == (int)ResubmissionEnum.OnlyResubmission)
                            {
                                oDealerActivitiesResponse.IsResubmit = true;
                            }
                            else if (oAndroidReportRequest.Resubmission == (int)ResubmissionEnum.IncludeResubmission && objWFActivity.IndexedField9 == dealer.Code)
                            {
                                oDealerActivitiesResponse.IsResubmit = true;
                            }
                            oDealerActivitiesResponseColelction.DealerActivitiesResponseList.Add(oDealerActivitiesResponse);
                        }
                    }
                    //oDealerActivitiesResponseColelction.NumberOfRecords = lstWFActivities.Count + oAndroidReportRequest.StartIndex;
                }
            }
            finally
            {
                oDealerActivitiesResponseColelction.IsPassed = true;
                oDealerActivitiesResponseColelction.ResponseMessage = "Success";
            }

            return oDealerActivitiesResponseColelction;
        }
        public static ResubmissionCallResponse GetFailedRequests(FaildActivationRequest oFaildActivationRequest)
        {
            string name = "";
            AdministrationServices.Requester dealer = null;
            WFActivity oWFActivity = null;
            RejectionReason objRejectionReason = null;
            ResubmissionCallResponse oResubmissionCallResponse = new ResubmissionCallResponse();
            oResubmissionCallResponse.CallResponseObj = new CallResponse();


            double appVersion = double.Parse(oFaildActivationRequest.oLoginRequest.ApplicationVersion);
            oResubmissionCallResponse.CallResponseObj = CheckRequester(oFaildActivationRequest.oLoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

            if (oResubmissionCallResponse.CallResponseObj.IsPassed == false)
            {
                return oResubmissionCallResponse;
            }

            using (OMSCoreEntities context = new OMSCoreEntities())
            {

                //oWFActivity = (from activities in context.WFActivities where activities.IndexedField1 == oFaildActivationRequest.Msisdn && activities.ActionID == 3 && activities.IsLastActivity == true select activities).FirstOrDefault();
                oWFActivity = context.WFActivities.Include("WFRequest").Include("WFRequest.DocumentType").Where(a => a.IndexedField1 == oFaildActivationRequest.Msisdn && a.ActionID == 3 && a.IsLastActivity == true).FirstOrDefault();
                // WFActivity query = context.WFActivities.Include("WFRequest").FirstOrDefault();
                if (oWFActivity != null)
                {
                    if (oWFActivity.WFRequest.RejectionReasonId.HasValue)
                    {

                        objRejectionReason = context.RejectionReasons.Where(p => p.ID == oWFActivity.WFRequest.RejectionReasonId.Value).FirstOrDefault();

                    }
                }
            }



            if (oWFActivity != null)
            {
                oResubmissionCallResponse.RequestedTypeId = oWFActivity.WFRequest.DocumentTypeID.Value;
                oResubmissionCallResponse.RequestedDate = oWFActivity.WFRequest.WFStartDate.ToString();
                oResubmissionCallResponse.RejectedDate = oWFActivity.WFActivityDate.ToString();
                oResubmissionCallResponse.WFActivityId = oWFActivity.ID;
                oResubmissionCallResponse.RequestedTypeId = oWFActivity.WFRequest.DocumentType.ID;// Name;
                if (objRejectionReason != null)
                {
                    oResubmissionCallResponse.RejectionReason = string.Concat(objRejectionReason.Description, Environment.NewLine, oWFActivity.WFRequest.RejectionReasonDescription);
                }
                oResubmissionCallResponse.CallResponseObj.IsPassed = true;
                oResubmissionCallResponse.CallResponseObj.ResponseMessage = objResourceManager.GetString("MSG_ReturnedRecord", Thread.CurrentThread.CurrentCulture);
                //if (oFaildActivationRequest.oLoginRequest.language == 2)
                //{
                //    //oCallResponse.ResponseMessage = ".يوجد عملية مفعلة للرقم المدخل";
                // d   oResubmissionCallResponse.CallResponseObj.ResponseMessage = "هناك سجل مسترجع";
                //}
                //else
                //{
                //    //oCallResponse.ResponseMessage = "There is an active processes for the mentioned MSISDN.";
                //    d oResubmissionCallResponse.CallResponseObj.ResponseMessage = "There is a returned record";
                //}
                Dictionary<string, string> activityDictionary = ToDictionary(oWFActivity.WFActivityData);

                //if (!string.IsNullOrWhiteSpace(oWFActivity.IndexedField4) && !string.IsNullOrWhiteSpace(oWFActivity.IndexedField5))
                //{
                //    oResubmissionCallResponse.FirstName = oWFActivity.IndexedField4;

                //    oResubmissionCallResponse.SecondName = oWFActivity.IndexedField5;
                //}
                //else
                //{
                oResubmissionCallResponse.FirstName = activityDictionary["ID_NUMBER"];
                oResubmissionCallResponse.SecondName = string.Empty;
                //}

                oResubmissionCallResponse.CallResponseObj.IsPassed = true;

            }
            else
            {
                oResubmissionCallResponse.CallResponseObj.IsPassed = false;




                oResubmissionCallResponse.CallResponseObj.ResponseMessage = objResourceManager.GetString("MSG_No_Return_Record", Thread.CurrentThread.CurrentCulture);//"لا يوجد سجل مسترجع لعمليات مرفوضة متعلقة بالرقم المدخل";

                //if (oFaildActivationRequest.oLoginRequest.language == 2)
                //    oResubmissionCallResponse.CallResponseObj.ResponseMessage = "لا يوجد سجل مسترجع لعمليات مرفوضة متعلقة بالرقم المدخل";
                //else
                //    oResubmissionCallResponse.CallResponseObj.ResponseMessage = "There is no returned record for rejected processes fot the provided number";
            }

            return oResubmissionCallResponse;
        }
        public static CallResponse ChangePassword(HashingRequest oHashingRequest)
        {
            string name = "";
            AdministrationServices.Requester dealer = null;
            oHashingRequest.LoginRequest.Password = oHashingRequest.OldPassword;
            double appVersion = double.Parse(oHashingRequest.LoginRequest.ApplicationVersion);
            CallResponse oCallResponse = CheckRequester(oHashingRequest.LoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

            if (oCallResponse.IsPassed == false)
            {

                //oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_InvalidUserOrPass", Thread.CurrentThread.CurrentCulture);
                return oCallResponse;
            }


            using (AdministrationServices.AdministrationServicesClient client = new AdministrationServices.AdministrationServicesClient("WSHttpBinding_IAdministrationServices"))
            {
                try
                {
                    client.ChangePassword(dealer.ID, oHashingRequest.NewPassword);
                    oCallResponse.IsPassed = true;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_PassSavedSuccess", Thread.CurrentThread.CurrentCulture);


                }
                catch (Exception)
                {

                    oCallResponse.IsPassed = false;

                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_InvalidUserOrPass", Thread.CurrentThread.CurrentCulture);
                }

            }
            //using (OMSCoreEntities context = new OMSCoreEntities())
            //{
            //    Requester orequester = (from requesters in context.Requesters where requesters.UserName == oHashingRequest.LoginRequest.Username && requesters.Password == oHashingRequest.OldPassword && requesters.IsActive == true select requesters).FirstOrDefault();

            //    if (orequester != null)
            //    {

            //        orequester.Password = oHashingRequest.NewPassword;
            //        context.SaveChanges();

            //        oCallResponse.IsPassed = true;
            //        oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_PassSavedSuccess", Thread.CurrentThread.CurrentCulture);
            //        //if (oHashingRequest.LoginRequest.language == 2)
            //        //  d  oCallResponse.ResponseMessage = "تم تخزين كلمة السر بنجاح";
            //        //else
            //        // d   oCallResponse.ResponseMessage = "The password is saved successfully.";
            //    }
            //    else
            //    {
            //        oCallResponse.IsPassed = false;

            //        oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_InvalidUserOrPass", Thread.CurrentThread.CurrentCulture);
            //        //if (oHashingRequest.LoginRequest.language == 2)
            //        //  d  oCallResponse.ResponseMessage = "اسم المستخدم او كلمة السر غير موجودة";
            //        //else
            //        //  d  oCallResponse.ResponseMessage = "Invalid username or password";
            //    }


            //}

            return oCallResponse;
        }
        public static CallResponse ResubmitRequest(ResubmitRequestObj request)//the return value should include the msisdn owner in addition to the CallResponse object
        {

            CallResponse CallResponseObj = new CallResponse();

            string name = "";
            AdministrationServices.Requester dealer = null;

            LoginRequest oLoginRequest = new LoginRequest();
            oLoginRequest.Username = request.LoginRequest.Username;
            oLoginRequest.Password = request.LoginRequest.Password;
            oLoginRequest.language = request.LoginRequest.language;
            oLoginRequest.ApplicationVersion = request.LoginRequest.ApplicationVersion;
            try
            {
                double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
                CallResponseObj = CheckRequester(oLoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (CallResponseObj.IsPassed == false)
                {
                    return CallResponseObj;
                }

                List<byte[]> lstDocuments = new List<byte[]>();
                lstDocuments.Add(Convert.FromBase64String(request.IdCopyDocument.DocumentContent));
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, request.LoginRequest.Username, dealer.Code, DSTChannelValue))
                    {
                        bool isValidResumissionRequest = client.ResubmitRejectedRequest(request.MSISDN, request.IDNumber, lstDocuments);

                        if (!isValidResumissionRequest)
                        {
                            CallResponseObj.IsPassed = false;
                            CallResponseObj.ResponseMessage = objResourceManager.GetString("MSG_NoRejectionForMSISDN", Thread.CurrentThread.CurrentCulture);
                            return CallResponseObj;
                        }
                    }
                }
            }
            catch (FaultException ex)
            {

                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    CallResponseObj.IsPassed = false;

                    CallResponseObj.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ResubmitRequest", ex, request.LoginRequest.Username);
                CallResponseObj.IsPassed = false;
                CallResponseObj.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return CallResponseObj;
        }
        public static StatusCheckResponse StatusCheck(StatusCheckRequest objRequest)
        {
            string name = "";
            AdministrationServices.Requester oRequester = null;
            StatusCheckResponse objResponse = new StatusCheckResponse();
            objResponse.IMSIStatus = objResponse.KitStatus = false;
            objResponse.IsPassed = true;
            double appVersion = double.Parse(objRequest.LoginRequest.ApplicationVersion);
            CallResponse oCallResponse = CheckRequester(objRequest.LoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

            if (oCallResponse.IsPassed == false)
            {
                objResponse.IsPassed = false;
                objResponse.ResponseMessage = objResourceManager.GetString("MSG_UserPassValidation", Thread.CurrentThread.CurrentCulture);// "Check Requester";

                return objResponse;
            }
            int result;
            if (!string.IsNullOrWhiteSpace(objRequest.IMSI))
            {
                string Dataresult = NobillWrapper.GetSIMDateByIMSI(objRequest.IMSI, out result, false);
                if (result != 0)
                {
                }
                if (Dataresult != String.Empty && Dataresult != "")
                {
                    Dataresult = NobillWrapper.GetStatusByIMSI(objRequest.IMSI, out result, false);
                    if (result != 0)
                    {
                        objResponse.IMSIStatus = false;
                    }

                    //if ((NobillService.status)Enum.Parse(typeof(NobillService.status), Dataresult) == NobillService.status.activeInUse)
                    //{

                    //}
                    else
                        objResponse.IMSIStatus = true;
                }
                else
                    objResponse.IMSIStatus = false;


            }
            if (!string.IsNullOrWhiteSpace(objRequest.KitID))
            {
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, objRequest.LoginRequest.Username, oRequester.Code, DSTChannelValue))
                    {
                        string imsi = string.Empty;
                        VirginKitStatus objVirginKitStatus = client.GetVirginKitStatus(out imsi, objRequest.KitID);
                        if (objVirginKitStatus == VirginKitStatus.KitIDNotValid)
                        {
                            objResponse.IsPassed = false;
                            objResponse.ResponseMessage = objResourceManager.GetString("MSG_KitIdNotValid", Thread.CurrentThread.CurrentCulture);
                        }
                        else
                        {
                            objResponse.KitStatus = (objVirginKitStatus == VirginKitStatus.KitIDIsActive);
                        }
                    }
                }
            }

            return objResponse;
        }
        public static EloadResponse Eload(EloadRequest objRequest)
        {
            EloadResponse objResponse = new EloadResponse();
            string name = "";
            AdministrationServices.Requester dealer = null;
            string messageResult = "";
            EloadService.CreditTransferServiceClient client = null;

            double appVersion = double.Parse(objRequest.LoginRequest.ApplicationVersion);
            CallResponse oCallResponse = CheckRequester(objRequest.LoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (objRequest.LoginRequest.language == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }
            if (oCallResponse.IsPassed == false)
            {
                objResponse.IsPassed = false;
                objResponse.ResponseMessage = objResourceManager.GetString("MSG_UserPassValidation", Thread.CurrentThread.CurrentCulture);// "Check Requester";

                return objResponse;
            }

            Contracts.BaseResponse BaseResponse = IsValidDealerEWallet(dealer.RefillMSISDN,
                objRequest.LoginRequest.language);

            if (!BaseResponse.IsPassed)
            {
                return new EloadResponse()
                {
                    IsPassed = false,
                    ResponseMessage = BaseResponse.ResponseMessage
                };
            }

            String[] Amount = objRequest.Amount.Split('.');
            try
            {
                client = new EloadService.CreditTransferServiceClient();
                client.ClientCredentials.Windows.ClientCredential.UserName = _userName;
                client.ClientCredentials.Windows.ClientCredential.Password = _password;
                int result = client.TransferCredit(out messageResult, dealer.RefillMSISDN, objRequest.MSISDN, int.Parse(Amount[0]), (Amount.Length > 1 ? int.Parse(Amount[1]) : 0), objRequest.PIN);
                if (result != 0)
                {
                    objResponse.IsPassed = false;
                    if (!string.IsNullOrEmpty(objResourceManager.GetString("MSG_CreditTransfer_" + result.ToString(), Thread.CurrentThread.CurrentCulture)))
                    {

                        objResponse.ResponseMessage = objResourceManager.GetString("MSG_CreditTransfer_" + result.ToString(), Thread.CurrentThread.CurrentCulture);
                    }
                    else
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.GenericError.ToString(), "Error Transfer Credit , response: " + result.ToString(), "ELoad", objRequest.LoginRequest.Username);
                        objResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                    }
                    return objResponse;
                }
                objResponse.IsPassed = true;
                objResponse.ResponseMessage = objResourceManager.GetString("MSG_EloadSuccess", Thread.CurrentThread.CurrentCulture);


            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Eload", ex, objRequest.LoginRequest.Username);
                objResponse.IsPassed = false;
                objResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                return objResponse;
            }
            finally
            {
                client.Close();
                client = null;
            }
            return objResponse;
        }
        public static IDNumberResultStatus GetIDNumber(string msisdn, string IDnumber)
        {
            try
            {
                string ussdMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><umsprot version=\"1\"><exec_req function=\"ProcessUSSD\" locale=\"en\" msisdn=\"{0}\" location=\"{0}\"> <data name=\"MSISDN\">{0}</data><data name=\"CIVILID\">{1}</data></exec_req></umsprot>";
                ussdMessage = string.Format(ussdMessage, msisdn, IDnumber);
                System.Net.WebClient client = new System.Net.WebClient();
                client.Headers.Add("Content-type", "application / xml");
                string result = client.UploadString("http://10.4.1.54/IDVerification/IDVerification.aspx", ussdMessage);

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(result);

                string xpath = "umsprot/exec_rsp";
                var nodes = xmlDoc.SelectNodes(xpath);
                string childNode = "2";
                foreach (XmlNode childrenNode in nodes)
                {
                    childNode = childrenNode.SelectSingleNode("//data").InnerXml;
                }
                IDNumberResultStatus resultStatus = (IDNumberResultStatus)Convert.ToInt32(childNode);
                return resultStatus;
                //string result = client.UploadString("http://localhost/CreditTransfer/ussd", ussdMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static ActivationReportCounters GetActivationReportCounters(LoginRequest objLogin)
        {
            string name = "";
            AdministrationServices.Requester oRequester = null;
            CallResponse oCallResponse = CheckRequester(objLogin, out oRequester, out name, false);
            LP.OMS.Channels.Contracts.ActivationReportCounters objActivationReportCounters = new ActivationReportCounters();
            List<int> lstCounters = new List<int>(4) { 0, 0, 0, 0 };
            DayOfWeek dayOfWeek = DayOfWeek.Saturday;
            Enum.TryParse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Controller.DayOfWeek"], out dayOfWeek);
            List<Tuple<DateTime, DateTime>> lstDateFromTo = new List<Tuple<DateTime, DateTime>>();
            lstDateFromTo.Add(new Tuple<DateTime, DateTime>(DateTime.Today, DateTime.Now));
            lstDateFromTo.Add(new Tuple<DateTime, DateTime>(DateTime.Today.AddDays(DateTime.Now.DayOfWeek - dayOfWeek + 1), DateTime.Now));
            lstDateFromTo.Add(new Tuple<DateTime, DateTime>(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now));
            lstDateFromTo.Add(new Tuple<DateTime, DateTime>(MinimDateTime, DateTime.Now));

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    lstCounters = client.GetActivationCounters(lstDateFromTo, oRequester.Code);

                    objActivationReportCounters.DailyActivations = lstCounters[0];
                    objActivationReportCounters.WeeklyActivations = lstCounters[1];
                    objActivationReportCounters.MonthlyActivations = lstCounters[2];
                    objActivationReportCounters.LanchToDateActivations = lstCounters[3];
                    objActivationReportCounters.IsPassed = true;
                }
                catch (Exception ex)
                {
                    objActivationReportCounters.IsPassed = false;
                    objActivationReportCounters.ResponseMessage = ex.Message;
                }
            }
            return objActivationReportCounters;
        }
        public static List<Contracts.ActivationResponse> GetDealerActivations(Contracts.CommissionDataByDealerRequest objActivationReportRequest)
        {
            List<Contracts.ActivationResponse> lstActivations = new List<Contracts.ActivationResponse>();
            List<ActivationService.ActivationContract> lstActivationsDB = new List<ActivationService.ActivationContract>();
            switch (objActivationReportRequest.reportViewMode)
            {
                case ReportViewMode.Daily:
                    objActivationReportRequest.dateFrom = DateTime.Today;
                    objActivationReportRequest.dateTo = DateTime.Now;
                    break;
                case ReportViewMode.Weekly:
                    DayOfWeek dayOfWeek = DayOfWeek.Saturday;
                    Enum.TryParse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Controller.DayOfWeek"], out dayOfWeek);
                    objActivationReportRequest.dateFrom = DateTime.Today.AddDays(DateTime.Now.DayOfWeek - dayOfWeek + 1);
                    objActivationReportRequest.dateTo = DateTime.Now;
                    break;
                case ReportViewMode.Monthly:
                    objActivationReportRequest.dateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    objActivationReportRequest.dateTo = DateTime.Now;
                    break;
                case ReportViewMode.LanchToDate:
                    objActivationReportRequest.dateFrom = MinimDateTime;
                    objActivationReportRequest.dateTo = DateTime.Now;
                    break;
                    // case ReportViewMode.CustomRequest: Dates will be send as it is recieved.
            }

            try
            {
                string name = "";
                AdministrationServices.Requester oRequester = null;
                CallResponse oCallResponse = CheckRequester(objActivationReportRequest.objLogin, out oRequester, out name, false);
                if (!oCallResponse.IsPassed)
                {
                    return null;
                }
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    lstActivationsDB = client.GetActivations(oRequester.Code, objActivationReportRequest.dateFrom, objActivationReportRequest.dateTo);
                    lstActivationsDB.ForEach(a =>
                    {
                        Contracts.ActivationResponse activity = new Contracts.ActivationResponse();
                        activity.IMSI = a.IMSI;
                        activity.ActivationDate = a.CreatedOn;
                        activity.FirstPayableEventReceived = a.IsFirstCharegableEventReceived;
                        Grade g = Grade.NotSet;
                        if (!string.IsNullOrWhiteSpace(a.QualityGrade))
                        {
                            Enum.TryParse(a.QualityGrade, out g);
                        }
                        activity.Grade = g;
                        //// Change  D to C for reporting purposes.
                        //if (activity.Grade == Grade.D)
                        //{
                        //    activity.Grade = Grade.C;
                        //}
                        activity.PhysicalFormDelivered = a.PhysicalFormStatusId == 0 ? false : true;

                        lstActivations.Add(activity);
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstActivations;
        }
        public static List<CommissionReportResponse> GetCommissionReport(CommissionDataByDealerRequest objCommissionDataByDealerRequest)
        {
            string name = "";
            AdministrationServices.Requester oRequester = null;
            CallResponse oCallResponse = CheckRequester(objCommissionDataByDealerRequest.objLogin, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                return null;
            }
            List<CommissionReportResponse> lstCommissionReportResponse = new List<CommissionReportResponse>();

            using (ReportingService.ReportingServiceClient client = new Engine.ReportingService.ReportingServiceClient())
            {
                List<ReportingService.CommissionReport> lstCommissionReport = null;
                if (objCommissionDataByDealerRequest.reportViewMode == ReportViewMode.CustomRequest)
                {
                    lstCommissionReport = client.GetCommissionDataByDealer(objCommissionDataByDealerRequest.dateFrom, objCommissionDataByDealerRequest.dateTo, oRequester.Code).ToList();
                }
                else
                {
                    lstCommissionReport = client.GetCommissionDataByDealer(GetPeriodDate(objCommissionDataByDealerRequest.reportViewMode), DateTime.Now, oRequester.Code).ToList();
                }

                lstCommissionReport.ToList().ForEach(cycle =>
                {
                    CommissionReportResponse objCommissionReport = new CommissionReportResponse();
                    objCommissionReport.CycleDate = cycle.CycleDate;
                    objCommissionReport.CycleName = cycle.CycleName;
                    if (cycle.CycleSchemas != null)
                    {
                        foreach (var schema in cycle.CycleSchemas)
                        {
                            Schema objSchema = new Schema();

                            objSchema.PaymentDate = schema.PaymentDate;
                            objSchema.PaymentStatus = schema.PaymentStatus.ToString();
                            objSchema.SchemaName = schema.SchemaName;

                            if (schema.CommissionElements != null)
                            {

                                foreach (var element in schema.CommissionElements)
                                {
                                    CommissionSchemaElement objCommissionSchemaElement = new CommissionSchemaElement();
                                    objCommissionSchemaElement.Category = element.Category;

                                    foreach (var commissionDetail in element.CommissionDetails)
                                    {
                                        CommissionPayment objCommissionPayment = new CommissionPayment();
                                        objCommissionPayment.CommissionPaidAmount = commissionDetail.Value.CommissionPaidAmount;
                                        objCommissionSchemaElement.CommissionDetails.Add(commissionDetail.Key, objCommissionPayment);
                                    }


                                    objCommissionSchemaElement.Date = element.Date; //"12/12/2014";// 
                                    objCommissionSchemaElement.UIdentifier = element.UIdentifier;
                                    objCommissionSchemaElement.Requester = element.Requester;

                                    objSchema.CommissionElements.Add(objCommissionSchemaElement);
                                }

                            }

                            objCommissionReport.CycleSchemas.Add(objSchema);
                        }
                    }
                    lstCommissionReportResponse.Add(objCommissionReport);
                });

            }
            return lstCommissionReportResponse;
        }
        public static void NotifyLocalApplicationLog(NotifyApplicationLogRequest request)
        {
            using (Entities context = new Entities())
            {
                DateTime dbMinmumDate = new DateTime(1900, 1, 1);
                if (request.AppLogTimestamp == null || request.AppLogTimestamp < dbMinmumDate)
                {
                    request.AppLogTimestamp = request.AppLogTimestamp;
                }
                context.AppErrorLogs.Add(new AppErrorLog()
                {
                    AppLogTimestamp = request.AppLogTimestamp,
                    Username = request.oLoginRequest.Username,
                    DSTAppOS = request.oLoginRequest.DSTAppOS.ToString(),
                    LogContent = request.LogContent,
                    Timestamp = DateTime.Now,
                    IMEI = request.IMEI,
                    DeviceModel = request.DeviceModel,
                    DeviceManufacturer = request.DeviceManufacturer
                });
                context.SaveChanges();
            }
        }
        public static PhysicalFormCheckResponse PhysicalFormCollectionCheck(PhysicalFormCheckRequest request)
        {
            PhysicalFormCheckResponse objPhysicalFormCheckResponse = new PhysicalFormCheckResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            CallResponse oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                objPhysicalFormCheckResponse.IsValid = false;
                objPhysicalFormCheckResponse.Message = oCallResponse.ResponseMessage;
                return objPhysicalFormCheckResponse;
            }
            using (ActivationEntities1 context = new ActivationEntities1())
            {
                List<Activation> lstActivations = context.Activations.Where(p => p.FormSerial == request.FormSerial).ToList();
                objPhysicalFormCheckResponse.IsValid = false;
                if (lstActivations == null || lstActivations.Count == 0)
                {
                    objPhysicalFormCheckResponse.PhysicalFormCheckStatus = PhysicalFormCheckStatus.NoActivation;
                }
                else if (lstActivations.All(p => p.PhysicalFormStatusId == 1))
                {
                    objPhysicalFormCheckResponse.PhysicalFormCheckStatus = PhysicalFormCheckStatus.AlreadySalesDelivered;
                }
                else if (lstActivations.All(p => p.PhysicalFormStatusId == 2))
                {
                    objPhysicalFormCheckResponse.PhysicalFormCheckStatus = PhysicalFormCheckStatus.AlreadyOfficeDelivered;
                }
                else
                {
                    objPhysicalFormCheckResponse.IsValid = true;
                }

                if (!objPhysicalFormCheckResponse.IsValid)
                {
                    objPhysicalFormCheckResponse.Message = objResourceManager.GetString("MSG_" + objPhysicalFormCheckResponse.PhysicalFormCheckStatus.ToString(), Thread.CurrentThread.CurrentCulture);
                }
            }
            return objPhysicalFormCheckResponse;
        }
        public static CallResponse PhysicalFormsCollectionSubmit(PhysicalFormCollectionRequest request)
        {
            CallResponse objCallResponse = new CallResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            objCallResponse = CheckRequester(request.LoginRequest, out oRequester, out name, false);
            if (!objCallResponse.IsPassed)
            {
                return objCallResponse;
            }
            DateTime currentDate = DateTime.Now;
            using (ActivationEntities1 context = new ActivationEntities1())
            {
                List<Activation> lstActivations = (from activation in context.Activations
                                                   join formSerial in request.FormSerials
                                                   on activation.FormSerial equals formSerial
                                                   select activation).ToList();

                lstActivations.Where(p => p.BrandId == (int)Brand.Virgin).ToList().ForEach(p =>
                {
                    if (p.IsPrimary)
                    {
                        context.PhysicalFormsCollections.Add(new PhysicalFormsCollection()
                        {
                            ArchivedFilePath = "NoDocumentAvaiable-DSTFeeding",
                            CategoryId = (int)PhysicalFormCollectionCategory.Successful,
                            ChannelId = (int)PhysicalFormCollectionChannel.DST,
                            FormSerial = p.FormSerial,
                            IMSI = p.KitID,
                            Timestamp = currentDate
                        });
                    }
                    p.PhysicalFormStatusId = 1;
                    p.PhysicalFormReceivedSalesCode = request.LoginRequest.Username;
                    p.PhysicalFormSalesReceivedDate = currentDate;
                });

                lstActivations.Where(p => p.BrandId == (int)Brand.Friendi).ToList().ForEach(p =>
                {
                    context.PhysicalFormsCollections.Add(new PhysicalFormsCollection()
                    {
                        ArchivedFilePath = "NoDocumentAvaiable-DSTFeeding",
                        CategoryId = (int)PhysicalFormCollectionCategory.Successful,
                        ChannelId = (int)PhysicalFormCollectionChannel.DST,
                        FormSerial = p.FormSerial,
                        IMSI = p.IMSI,
                        Timestamp = currentDate
                    });
                    p.PhysicalFormStatusId = 1;
                    p.PhysicalFormReceivedSalesCode = request.LoginRequest.Username;
                    p.PhysicalFormSalesReceivedDate = currentDate;
                });

                context.SaveChanges();
            }
            return objCallResponse;
        }
        private static Contracts.VirginSubscriptionType GetVirginSubscriptionTypeMapping(string expectedSubscriptionType)
        {
            switch (expectedSubscriptionType.ToLower())
            {
                case "free":
                    return Contracts.VirginSubscriptionType.NotSet;
                case "prepaid":
                    return Contracts.VirginSubscriptionType.PrePaid;
                case "standard":
                    return Contracts.VirginSubscriptionType.Standard;
                case "premium":
                    return Contracts.VirginSubscriptionType.Premium;
                case "basic":
                    return Contracts.VirginSubscriptionType.Basic;

            }
            return Contracts.VirginSubscriptionType.NotSet;
        }
        public static VirginVanityActivationCheckResponse VerifyVirginVanityNumber(VirginVanityActivationCheckRequest objVirginVanityActivationCheckRequest)
        {
            VirginVanityActivationCheckResponse objVirginVanityActivationCheckResponse = new VirginVanityActivationCheckResponse();

            string name = "";
            AdministrationServices.Requester oRequester = null;
            CallResponse oCallResponse = CheckRequester(objVirginVanityActivationCheckRequest.LoginRequest, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                objVirginVanityActivationCheckResponse.IsPassed = false;
                objVirginVanityActivationCheckResponse.Message = oCallResponse.ResponseMessage;
                return objVirginVanityActivationCheckResponse;
            }
            try
            {
                using (ActivationService.ActivationServiceClient client = new ActivationServiceClient())
                {
                    ActivationService.ReservationInfo objReservationInfo = null;
                    ActivationService.ActivationPlanDetails objActivationPlanDetails = null;

                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        ActivationService.FingerPrint objFingerprint = null;
                        if (objVirginVanityActivationCheckRequest.FingerPrint != null)
                        {
                            if (!objVirginVanityActivationCheckRequest.FingerPrint.IsSematiOTPUsed)
                            {
                                if (!string.IsNullOrWhiteSpace(objVirginVanityActivationCheckRequest.FingerPrint.FingerPrintText))
                                {
                                    objFingerprint = GetActivationServiceFingerprint(objVirginVanityActivationCheckRequest.FingerPrint);
                                }
                            }
                            else
                            {
                                objFingerprint = GetActivationServiceFingerprint(objVirginVanityActivationCheckRequest.FingerPrint);
                            }
                        }
                        objReservationInfo = client.VerifyVirginVanityNumber(out objActivationPlanDetails, new ActivationService.VerifyVirginVanityNumber()
                        {
                            BookingCode = objVirginVanityActivationCheckRequest.BookingCode,
                            IdNumber = objVirginVanityActivationCheckRequest.IDNumber,
                            IdTypeCode = objVirginVanityActivationCheckRequest.IDTypeCode,
                            Fingerprint = objFingerprint,
                            ActivationPlanCode = objVirginVanityActivationCheckRequest.ActivationPlanCode,
                            PlanVoucherNumber = objVirginVanityActivationCheckRequest.PlanVoucherNumber
                        });
                    }

                    if (objReservationInfo != null)
                    {
                        bool isFBORequester = IsFBORequester(oRequester.DistributerId);
                        bool validBookings = true;

                        //Based on Abed AlWahabs request on phone call we ignored the below 2 validations on 26 Nov 2017 as a hot fix
                        //if (!isFBORequester && !objReservationInfo.IsPaid)
                        //{
                        //    objVirginVanityActivationCheckResponse.IsPassed = validBookings = false;
                        //    objVirginVanityActivationCheckResponse.Message = objResourceManager.GetString("MSG_VanityCanBeActivatedOnlyByFOB", Thread.CurrentThread.CurrentCulture);
                        //}
                        //if (objActivationPlanDetails != null && !isFBORequester && !objActivationPlanDetails.IsPaid)
                        //{
                        //    objVirginVanityActivationCheckResponse.IsPassed = validBookings = false;
                        //    objVirginVanityActivationCheckResponse.Message += objResourceManager.GetString("MSG_UnPaidPlanCanBeActivatedOnlyByFOB", Thread.CurrentThread.CurrentCulture);
                        //}

                        if (!validBookings)
                        {
                            return objVirginVanityActivationCheckResponse;

                        }

                        objVirginVanityActivationCheckResponse.IsPassed = true;
                        objVirginVanityActivationCheckResponse.MSISDN = objReservationInfo.MSISDN;
                        objVirginVanityActivationCheckResponse.IsPaid = objReservationInfo.IsPaid;
                        objVirginVanityActivationCheckResponse.ExpectedSubscriptionType = GetVirginSubscriptionTypeMapping(objReservationInfo.ExpectedSubscriptionType);
                        objVirginVanityActivationCheckResponse.Email = objReservationInfo.Email;
                        objVirginVanityActivationCheckResponse.Price = objReservationInfo.Price;
                        if (objReservationInfo.AvailableSubscription != null)
                        {
                            objVirginVanityActivationCheckResponse.AvailableSubscriptionTypes = new List<Contracts.VirginSubscriptionType>();
                            objReservationInfo.AvailableSubscription.ForEach(p => objVirginVanityActivationCheckResponse.AvailableSubscriptionTypes.Add(GetVirginSubscriptionTypeMapping(p.SubscriptionType)));
                        }

                    }
                    else
                    {
                        objVirginVanityActivationCheckResponse.IsPassed = false;
                        objVirginVanityActivationCheckResponse.Message = objResourceManager.GetString("MSG_InvalidVirginBookingDetails", Thread.CurrentThread.CurrentCulture);
                        return objVirginVanityActivationCheckResponse;
                    }
                }

            }
            catch (FaultException ex)
            {
                try
                {
                    var LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    objVirginVanityActivationCheckResponse.IsPassed = false;

                    objVirginVanityActivationCheckResponse.Message = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objVirginVanityActivationCheckRequest.LoginRequest.language);

                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyVirginVanityNumber", ex, objVirginVanityActivationCheckRequest.LoginRequest.Username);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }
            return objVirginVanityActivationCheckResponse;
        }
        public static CallResponse ActivateVirginVanityNumber(VirginVanityActivationActivationRequest objVirginVanityActivationActivationRequest)
        {
            CallResponse oCallResponse = new CallResponse();

            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCallResponse = CheckRequester(objVirginVanityActivationActivationRequest.LoginRequest, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                return oCallResponse;
            }

            VirginActivationRequest request = objVirginVanityActivationActivationRequest.VirginActivationRequest;
            #region Premium to Basic Fix : to be removed next DST release
            //request.VirginSubscriptionType = request.VirginSubscriptionType == Contracts.VirginSubscriptionType.Premium ? Contracts.VirginSubscriptionType.Basic : request.VirginSubscriptionType;
            #endregion
            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {

                try
                {
                    ActivationService.ReservationInfo objReservationInfo = null;
                    ActivationService.ActivationPlanDetails objActivationPlanDetails = null;

                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        ActivationService.FingerPrint objFingerprint = null;
                        if (objVirginVanityActivationActivationRequest.VirginActivationRequest.FingerPrint != null)
                        {
                            if (!objVirginVanityActivationActivationRequest.VirginActivationRequest.FingerPrint.IsSematiOTPUsed)
                            {
                                if (!string.IsNullOrWhiteSpace(objVirginVanityActivationActivationRequest.VirginActivationRequest.FingerPrint.FingerPrintText))
                                {
                                    objFingerprint = GetActivationServiceFingerprint(objVirginVanityActivationActivationRequest.VirginActivationRequest.FingerPrint);
                                }
                            }
                            else
                            {
                                objFingerprint = GetActivationServiceFingerprint(objVirginVanityActivationActivationRequest.VirginActivationRequest.FingerPrint);
                            }
                        }
                        objReservationInfo = client.VerifyVirginVanityNumber(out objActivationPlanDetails, new ActivationService.VerifyVirginVanityNumber()
                        {
                            BookingCode = objVirginVanityActivationActivationRequest.BookingCode,
                            IdNumber = objVirginVanityActivationActivationRequest.IDNumber,
                            IdTypeCode = objVirginVanityActivationActivationRequest.IDTypeCode,
                            Fingerprint = objFingerprint,
                            ActivationPlanCode = objVirginVanityActivationActivationRequest.VirginActivationRequest.ActivationPlanCode,
                            PlanVoucherNumber = objVirginVanityActivationActivationRequest.VirginActivationRequest.PlanVoucherNumber
                        });
                    }
                    if (objReservationInfo != null)
                    {

                        if (!string.IsNullOrWhiteSpace(request.VirginSubscriptionType.ToString()))
                        {
                            objReservationInfo.ExpectedSubscriptionType = request.VirginSubscriptionType.ToString();
                        }
                        if (_storeApplicationFormImage)
                        {
                            string rootFolder = System.Web.Hosting.HostingEnvironment.MapPath("~/");
                            string fileName = Guid.NewGuid().ToString();
                            File.WriteAllBytes(Path.Combine(rootFolder, "ApplicationForms", fileName + "_VirginVanity.jpeg"), Convert.FromBase64String(request.IdCopyDocument.DocumentContent));


                        }
                        //DateTime datetime;
                        //HijriCalendar hijriCal = new HijriCalendar();
                        ActivationService.CustomerInfo customerInfo = new ActivationService.CustomerInfo
                        {
                            CustomerDocuments = new List<ActivationService.Document>(),
                            IdNumber = request.IdNo,
                            IsDealerActivation = false,
                            ActivationDealerCode = request.DealerCode,
                            IdTypeCode = request.IDTypeCode,
                            Email = request.Email,
                            FingerPrint = GetActivationServiceFingerprint(objVirginVanityActivationActivationRequest.VirginActivationRequest.FingerPrint),
                            Signature = Convert.FromBase64String(request.SignatureDocument.DocumentContent),
                            Address = request.Address
                        };

                        if (request.DealerSignatureDocument != null)
                            customerInfo.DealerSignature = Convert.FromBase64String(request.DealerSignatureDocument.DocumentContent);

                        //customerInfo.CustomerDocuments = new List<byte[]>();
                        //customerInfo.IdNumber = request.IdNo;
                        //customerInfo.IsDealerActivation = false;
                        //customerInfo.ActivationDealerCode = request.DealerCode;
                        //System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-SA");
                        //customerInfo.IdTypeCode = request.IDTypeCode;
                        //customerInfo.Email = request.Email;

                        if (request.IdCopyDocument != null)
                            customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                            {
                                FileContent = Convert.FromBase64String(request.IdCopyDocument.DocumentContent),
                                Type = ActivationService.DocumentType.IDCopy
                            });

                        switch (request.IDTypeCode.ToUpper())
                        {
                            case "V":
                            case "PB":
                            case "VV":
                            case "UV":
                            case "HV":
                                {
                                    if (request.VisaCopyDocument != null)
                                        customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                                        {
                                            FileContent = Convert.FromBase64String(request.VisaCopyDocument.DocumentContent),
                                            Type = ActivationService.DocumentType.Visa
                                        });
                                    break;
                                }
                            case "GP":
                            case "PP":
                            case "UP":
                                {
                                    if (request.PassportCopyDocument != null)
                                        customerInfo.CustomerDocuments.Add(new ActivationService.Document()
                                        {
                                            FileContent = Convert.FromBase64String(request.PassportCopyDocument.DocumentContent),
                                            Type = ActivationService.DocumentType.GulfPassport
                                        });
                                    break;
                                }
                        }
                        customerInfo.CountryCode = request.NationalityCode;

                        ActivationService.ActivationRequest objActivationRequest = new ActivationService.ActivationRequest();
                        objActivationRequest.customerInfo = customerInfo;
                        objActivationRequest.Latitude = request.Latitude;
                        objActivationRequest.Longitude = request.Longitude;
                        objActivationRequest.IsRequesterFBO = IsFBORequester(oRequester.DistributerId);
                        objActivationRequest.ActivationSource = ActivationService.ActivationSource.Commercial;
                        objActivationRequest.RatePlan = (LP.OMS.Channels.Engine.ActivationService.VirginSubscriptionType)Enum.Parse(typeof(LP.OMS.Channels.Engine.ActivationService.VirginSubscriptionType), request.VirginSubscriptionType.ToString());
                        objActivationRequest.VoucherPinCode = request.VoucherPinCode;
                        objActivationRequest.MSISDN = objReservationInfo.MSISDN;
                        objActivationRequest.PlanVoucherNumber = objVirginVanityActivationActivationRequest.VirginActivationRequest.PlanVoucherNumber;
                        objActivationRequest.ActivationPlanCode = objVirginVanityActivationActivationRequest.VirginActivationRequest.ActivationPlanCode;
                        objActivationRequest.BundleVoucherCode = objVirginVanityActivationActivationRequest.VirginActivationRequest.BundleVoucherCode;
                        objActivationRequest.RechargeAmount = objVirginVanityActivationActivationRequest.VirginActivationRequest.RechargeDenominationId;
                        objActivationRequest.ReferralCode = objVirginVanityActivationActivationRequest.VirginActivationRequest.ReferralCode;
                        objActivationRequest.Language = request.oLoginRequest.language == 1 ? ActivationService.EngineLanguage.Local : ActivationService.EngineLanguage.Foreign;
                        objActivationRequest.customerInfo.Signature = Convert.FromBase64String(request.SignatureDocument.DocumentContent);
                        objActivationRequest.KitID = objVirginVanityActivationActivationRequest.VirginActivationRequest.KitCode;

                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            bool isActivated = client.ActivateVirginVanityNumber(out objActivationPlanDetails, objActivationRequest, objReservationInfo);
                            if (isActivated)
                            {
                                oCallResponse.MSISDN = objReservationInfo.MSISDN;
                                oCallResponse.RechargeMessage = string.Empty;

                                // Set the plan name if the subscription activated with plan
                                if (objActivationPlanDetails != null)
                                {
                                    oCallResponse.ResponseMessage = objActivationPlanDetails.Name;
                                }

                                if (!string.IsNullOrWhiteSpace(oCallResponse.MSISDN) && oCallResponse.MSISDN.Contains("_"))
                                {
                                    string[] parts = oCallResponse.MSISDN.Split('_');
                                    oCallResponse.MSISDN = parts[0].Trim();
                                    oCallResponse.RechargeMessage = string.Format(", {0}",
                                        string.Format(GetBusinessMessage(string.Format("_{0}", parts[1]),
                                        request.oLoginRequest.language), objActivationRequest.RechargeAmount));
                                }
                                else
                                {
                                    if (objActivationRequest.RechargeAmount > 0)
                                    {
                                        oCallResponse.RechargeMessage =
                                            string.Format(GetResourceMessage("MSG_CreditTransfer_Success",
                                            request.oLoginRequest.language), objActivationRequest.RechargeAmount);
                                    }
                                }
                            }
                            else
                            {
                                oCallResponse.IsPassed = false;
                                oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_FaildToActivatedLine", Thread.CurrentThread.CurrentCulture);
                                return oCallResponse;
                            }
                        }
                        oCallResponse.IsPassed = true;
                    }
                    else
                    {
                        oCallResponse.IsPassed = false;
                        oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_InvalidVirginBookingDetails", Thread.CurrentThread.CurrentCulture);
                        return oCallResponse;
                    }
                }
                catch (FaultException ex)
                {
                    Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "SubmitVirginActivation", request.oLoginRequest.Username, ex.ToString());
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        oCallResponse.IsPassed = false;

                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("SubmitVirginActivation", ex, request.oLoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }
            return oCallResponse;

        }

        public static DealerBalanceResponse GetDealerBalance(LoginRequest objLoginRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            DealerBalanceResponse objDealerBalanceResponse = new DealerBalanceResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCallResponse = CheckRequester(objLoginRequest, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                objDealerBalanceResponse.IsPassed = false;
                objDealerBalanceResponse.ResponseMessage = oCallResponse.ResponseMessage;

            }
            else
            {
                int result = -1;
                objDealerBalanceResponse.Balance = NobillWrapper.GetAccountBalance(oRequester.RefillMSISDN, out result);
                switch (result)
                {
                    case 0:
                        objDealerBalanceResponse.IsPassed = true;
                        break;
                    case 7:
                        objDealerBalanceResponse.ResponseMessage = string.Format("Dealer refill number {0} is not valid.", oRequester.RefillMSISDN);
                        objDealerBalanceResponse.IsPassed = false;
                        break;
                    default:
                        objDealerBalanceResponse.ResponseMessage = string.Format("Error in get account balance for {0}, API response is {1}", oRequester.RefillMSISDN, result);
                        objDealerBalanceResponse.IsPassed = false;
                        break;
                }

            }
            return objDealerBalanceResponse;
        }

        public static CallResponse CheckCustomerId(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            CallResponse oCallResponse = new CallResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCallResponse = CheckRequester(objCheckCustomerIdRquest.LoginRequest, out oRequester, out name, false);
            string response = string.Empty;
            if (oCallResponse.IsPassed)
            {
                try
                {

                    using (ActivationService.ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            ActivationService.FingerPrint objFingerprint = null;
                            if (objCheckCustomerIdRquest.FingerPrint != null)
                            {
                                if (!objCheckCustomerIdRquest.FingerPrint.IsSematiOTPUsed)
                                {
                                    if (!string.IsNullOrWhiteSpace(objCheckCustomerIdRquest.FingerPrint.FingerPrintText))
                                    {
                                        objFingerprint = GetActivationServiceFingerprint(objCheckCustomerIdRquest.FingerPrint);
                                    }
                                }
                                else
                                {
                                    objFingerprint = GetActivationServiceFingerprint(objCheckCustomerIdRquest.FingerPrint);
                                }
                            }
                            oCallResponse.IsPassed = client.VerifyCustomerId(out response, objCheckCustomerIdRquest.IdNumber, objCheckCustomerIdRquest.IDTypeCode, objFingerprint, true);

                        }
                    }
                }
                catch (LPBizException LPBizEx)
                {
                    ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "CheckCustomerId", objCheckCustomerIdRquest.LoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objCheckCustomerIdRquest.LoginRequest.language);
                }
                catch (FaultException ex)
                {
                    Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "CheckCustomerId", objCheckCustomerIdRquest.LoginRequest.Username, ex.ToString());
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        oCallResponse.IsPassed = false;

                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objCheckCustomerIdRquest.LoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("CheckCustomerId", ex, objCheckCustomerIdRquest.LoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }
            return oCallResponse;
        }
        public static CheckCustomerSubscriptionsResponse CheckCustomerSubscriptions(CheckCustomerIdRquest objCheckCustomerSubscriptionRquest)
        {
            CheckCustomerSubscriptionsResponse objCheckCustomerSubscriptionsResponse = new CheckCustomerSubscriptionsResponse();
            CallResponse oCallResponse = new CallResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCallResponse = CheckRequester(objCheckCustomerSubscriptionRquest.LoginRequest, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                objCheckCustomerSubscriptionsResponse.IsPassed = false;
                objCheckCustomerSubscriptionsResponse.ResponseMessage = oCallResponse.ResponseMessage;
                return objCheckCustomerSubscriptionsResponse;
            }
            string response = string.Empty;
            CustomerInfo objCustomerInfo = null;
            List<ActivationService.CustomerSubscription> lstCustomerSubscriptions = null;

            if (oCallResponse.IsPassed)
            {
                try
                {
                    objCustomerInfo = new CustomerInfo() { IdNumber = objCheckCustomerSubscriptionRquest.IdNumber, IdTypeCode = objCheckCustomerSubscriptionRquest.IDTypeCode, CountryCode = objCheckCustomerSubscriptionRquest.CountryCode };
                    using (ActivationService.ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            ActivationService.FingerPrint objFingerprint = null;
                            if (objCheckCustomerSubscriptionRquest.FingerPrint != null)
                            {
                                if (!objCheckCustomerSubscriptionRquest.FingerPrint.IsSematiOTPUsed)
                                {
                                    if (!string.IsNullOrWhiteSpace(objCheckCustomerSubscriptionRquest.FingerPrint.FingerPrintText))
                                    {
                                        objFingerprint = GetActivationServiceFingerprint(objCheckCustomerSubscriptionRquest.FingerPrint);
                                    }
                                }
                                else
                                {
                                    objFingerprint = GetActivationServiceFingerprint(objCheckCustomerSubscriptionRquest.FingerPrint);
                                }
                            }

                            objCustomerInfo.FingerPrint = objFingerprint;

                            oCallResponse.IsPassed = client.CheckCustomerSubscriptions(out lstCustomerSubscriptions, out response, objCustomerInfo, true);


                        }
                    }

                    if (lstCustomerSubscriptions != null)
                    {
                        objCheckCustomerSubscriptionsResponse.MSISDNs = new List<Contracts.CustomerSubscription>();
                        lstCustomerSubscriptions.ForEach(p =>
                        objCheckCustomerSubscriptionsResponse.MSISDNs.Add(
                            new Contracts.CustomerSubscription()
                            {
                                ActivationId = p.ActivationId,
                                Brand = objResourceManager.GetString(p.Brand.ToString(), Thread.CurrentThread.CurrentCulture),
                                IdentityVerificationStatus = objResourceManager.GetString(p.IdentityVerificationStatus.ToString(), Thread.CurrentThread.CurrentCulture),
                                MSISDN = p.MSISDN,
                                SubscriptionType = objResourceManager.GetString(p.SubscriptionType.ToString(), Thread.CurrentThread.CurrentCulture)
                            }));
                        objCheckCustomerSubscriptionsResponse.IsPassed = true;
                    }
                }
                catch (LPBizException LPBizEx)
                {
                    objCheckCustomerSubscriptionsResponse.IsPassed = false;
                    objCheckCustomerSubscriptionsResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objCheckCustomerSubscriptionRquest.LoginRequest.language);
                }
                catch (FaultException ex)
                {
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        objCheckCustomerSubscriptionsResponse.IsPassed = false;

                        objCheckCustomerSubscriptionsResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objCheckCustomerSubscriptionRquest.LoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("objCheckCustomerSubscriptionsResponse", ex, objCheckCustomerSubscriptionRquest.LoginRequest.Username);
                    objCheckCustomerSubscriptionsResponse.IsPassed = false;
                    objCheckCustomerSubscriptionsResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return objCheckCustomerSubscriptionsResponse;
        }

        public static VerifyCustomerResponse VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest)
        {
            VerifyCustomerResponse objVerifyCustomerResponse = new VerifyCustomerResponse();
            CallResponse oCallResponse = new CallResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCallResponse = CheckRequester(objVerifyCustomerRequest.LoginRequest, out oRequester, out name, false);
            if (!oCallResponse.IsPassed)
            {
                objVerifyCustomerResponse.IsPassed = false;
                objVerifyCustomerResponse.ResponseMessage = oCallResponse.ResponseMessage;
                return objVerifyCustomerResponse;
            }
            string response = string.Empty;
            CustomerInfo objCustomerInfo = null;
            Dictionary<string, string> dicVerifyResult = null;

            if (oCallResponse.IsPassed)
            {
                try
                {
                    objCustomerInfo = new CustomerInfo() { IdNumber = objVerifyCustomerRequest.IDNumber, IdTypeCode = objVerifyCustomerRequest.IDTypeCode, CountryCode = objVerifyCustomerRequest.CountryCode, Longitude = objVerifyCustomerRequest.Longitude, Latitude = objVerifyCustomerRequest.Latitude };
                    using (ActivationService.ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            ActivationService.FingerPrint objFingerprint = null;
                            if (objVerifyCustomerRequest.FingerPrint != null)
                            {
                                if (!objVerifyCustomerRequest.FingerPrint.IsSematiOTPUsed)
                                {
                                    if (!string.IsNullOrWhiteSpace(objVerifyCustomerRequest.FingerPrint.FingerPrintText))
                                    {
                                        objFingerprint = GetActivationServiceFingerprint(objVerifyCustomerRequest.FingerPrint);
                                    }
                                }
                                else
                                {
                                    objFingerprint = GetActivationServiceFingerprint(objVerifyCustomerRequest.FingerPrint);
                                }
                            }

                            objCustomerInfo.FingerPrint = objFingerprint;
                            List<ActivationService.VerifyCustomerSubscription> lstVerifyCustomerSubscriotions = new List<ActivationService.VerifyCustomerSubscription>();
                            objVerifyCustomerRequest.VerifyCustomerSubscriptions.ForEach(p => lstVerifyCustomerSubscriotions.Add(new ActivationService.VerifyCustomerSubscription()
                            {
                                ActivationId = p.ActivationId,
                                MSISDN = p.MSISDN,
                                Priority = p.Priority

                            }));

                            dicVerifyResult = client.VerifyCustomerSubscriptions(objCustomerInfo, lstVerifyCustomerSubscriotions);


                        }
                    }

                    if (dicVerifyResult != null)
                    {
                        objVerifyCustomerResponse.VerifyCustomerSubscriptions = new List<Contracts.VerifyCustomerSubscription>();
                        foreach (var verifiedSubscription in dicVerifyResult)
                        {
                            objVerifyCustomerResponse.VerifyCustomerSubscriptions.Add(new Contracts.VerifyCustomerSubscription() { MSISDN = verifiedSubscription.Key, Status = objResourceManager.GetString(verifiedSubscription.Value, Thread.CurrentThread.CurrentCulture) });
                        }
                        objVerifyCustomerResponse.IsPassed = true;

                    }
                }
                catch (LPBizException LPBizEx)
                {
                    objVerifyCustomerResponse.IsPassed = false;
                    objVerifyCustomerResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objVerifyCustomerRequest.LoginRequest.language);
                }
                catch (FaultException ex)
                {
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        objVerifyCustomerResponse.IsPassed = false;

                        objVerifyCustomerResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objVerifyCustomerRequest.LoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("VerifyCustomerSubscriptions", ex, objVerifyCustomerRequest.LoginRequest.Username);
                    objVerifyCustomerResponse.IsPassed = false;
                    objVerifyCustomerResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return objVerifyCustomerResponse;
        }
        public static CheckVirginCustomerIdResponse CheckVirginCustomerId(CheckVirginCustomerIdRequest objCheckVirginCustomerIdRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCallResponse = CheckRequester(objCheckVirginCustomerIdRequest.LoginRequest, out oRequester, out name, false);
            string response = string.Empty;
            ActivationPlanDetails objActivationPlanDetails = null;
            ActivationService.VerifyVirginCustomerId objVerifyVirginCustomerId = null;
            CheckVirginCustomerIdResponse objCheckVirginCustomerIdResponse = null;
            if (oCallResponse.IsPassed)
            {
                try
                {

                    using (ActivationService.ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            ActivationService.FingerPrint objFingerprint = null;
                            if (objCheckVirginCustomerIdRequest.FingerPrint != null)
                            {
                                if (!objCheckVirginCustomerIdRequest.FingerPrint.IsSematiOTPUsed)
                                {
                                    if (!string.IsNullOrWhiteSpace(objCheckVirginCustomerIdRequest.FingerPrint.FingerPrintText))
                                    {
                                        objFingerprint = GetActivationServiceFingerprint(objCheckVirginCustomerIdRequest.FingerPrint);
                                    }
                                }
                                else
                                {
                                    objFingerprint = GetActivationServiceFingerprint(objCheckVirginCustomerIdRequest.FingerPrint);
                                }
                            }
                            objVerifyVirginCustomerId = new VerifyVirginCustomerId()
                            {
                                FingerPrint = objFingerprint,
                                IdNumber = objCheckVirginCustomerIdRequest.IdNumber,
                                IdTypeCode = objCheckVirginCustomerIdRequest.IDTypeCode,
                                ActivationPlanCode = objCheckVirginCustomerIdRequest.ActivationPlanCode,
                                PlanVoucherNumber = objCheckVirginCustomerIdRequest.PlanVoucherNumber
                            };
                            objCheckVirginCustomerIdResponse = new CheckVirginCustomerIdResponse();

                            objCheckVirginCustomerIdResponse.IsPassed = client.VerifyVirginCustomerId(out response, out objActivationPlanDetails, objVerifyVirginCustomerId, true);
                            if (objActivationPlanDetails != null && !objActivationPlanDetails.IsPaid)
                            {
                                objCheckVirginCustomerIdResponse.NotificationMessage = string.Format(objResourceManager.GetString("MSG_Plan_Price", Thread.CurrentThread.CurrentCulture), objActivationPlanDetails.Price.ToString());
                            }
                        }
                    }
                }
                catch (LPBizException LPBizEx)
                {
                    ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "CheckCustomerId", objCheckVirginCustomerIdRequest.LoginRequest.Username);
                    objCheckVirginCustomerIdResponse.IsPassed = false;
                    objCheckVirginCustomerIdResponse.ErrorMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objCheckVirginCustomerIdRequest.LoginRequest.language);
                }
                catch (FaultException ex)
                {
                    Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "CheckCustomerId", objCheckVirginCustomerIdRequest.LoginRequest.Username, ex.ToString());
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        objCheckVirginCustomerIdResponse.IsPassed = false;

                        objCheckVirginCustomerIdResponse.ErrorMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, objCheckVirginCustomerIdRequest.LoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("CheckCustomerId", ex, objCheckVirginCustomerIdRequest.LoginRequest.Username);
                    objCheckVirginCustomerIdResponse.IsPassed = false;
                    objCheckVirginCustomerIdResponse.ErrorMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }
            else
            {
                objCheckVirginCustomerIdResponse = new CheckVirginCustomerIdResponse() { IsPassed = false, ErrorMessage = oCallResponse.ResponseMessage };

            }
            return objCheckVirginCustomerIdResponse;
        }

        public static CallResponse LogFingerprintNFIQTrials(LogFingerprintNFIQTrialsRequest logNFIQTrialsRequest)
        {
            CallResponse response = new CallResponse();

            AdministrationServices.Requester requester;
            string username;
            var loginResponse = CheckRequester(logNFIQTrialsRequest.LoginRequest, out requester, out username, false);
            if (!loginResponse.IsPassed)
            {
                return loginResponse;
            }

            try
            {
                var serviceTrialsList = logNFIQTrialsRequest.Trials.Select(t => new FingerprintNFIQTrialInfo
                {
                    DealerCode = t.DealerCode,
                    NFIQValue = t.NFIQValue
                }).ToList();

                using (var client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, requester.UserName, requester.Code, DSTChannelValue))
                    {
                        response.IsPassed = client.LogFingerprintNFIQTrials(serviceTrialsList);
                    }
                }
            }
            catch (Exception)
            {
                response.IsPassed = false;
            }

            return response;
        }

        public static CallResponse VerifyOwnershipChange(Contracts.OwnerChangeRequest request)
        {
            var response = new CallResponse();

            AdministrationServices.Requester requester;
            string username;
            var loginResponse = CheckRequester(request.LoginRequest, out requester, out username, false);
            if (!loginResponse.IsPassed)
            {
                return loginResponse;
            }

            try
            {
                var serviceRequest = new ActivationService.OwnerChangeRequest
                {
                    Process = (OwnerShipProcess)request.ProcessID,
                    MSISDN = request.MSISDN,
                    CurrentOwnerIDType = request.CurrentOwnerIDType,
                    CurrentOwnerIDNumber = request.CurrentOwnerIDNumber,
                    CurrentCountryCode = request.CurrentCountryCode,
                    NewOwnerIDType = request.NewOwnerIDType,
                    NewOwnerIDNumber = request.NewOwnerIDNumber,
                    NewCountryCode = request.NewCountryCode,
                    SourceBrand = request.SourceBrand,
                    Fingerprint = GetActivationServiceFingerprint(request.Fingerprint)
                };
                using (var client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, requester.UserName, requester.Code, DSTChannelValue))
                    {
                        response.IsPassed = client.VerifyOwnershipChange(serviceRequest, true);
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "VerifyOwnershipChange", request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "VerifyOwnershipChange", request.LoginRequest.Username, ex.ToString());
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;

                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyOwnershipChange", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static CallResponse SubmitOwnershipChange(Contracts.OwnerChangeRequest request)
        {
            var response = new CallResponse();
            AdministrationServices.Requester requester;
            string username;
            var loginResponse = CheckRequester(request.LoginRequest, out requester, out username, false);
            if (!loginResponse.IsPassed)
            {
                return loginResponse;
            }

            try
            {
                var serviceRequest = new ActivationService.OwnerChangeRequest
                {
                    Process = (OwnerShipProcess)request.ProcessID,
                    MSISDN = request.MSISDN,
                    CurrentOwnerIDType = request.CurrentOwnerIDType,
                    CurrentOwnerIDNumber = request.CurrentOwnerIDNumber,
                    CurrentCountryCode = request.CurrentCountryCode,
                    NewOwnerIDType = request.NewOwnerIDType,
                    NewOwnerIDNumber = request.NewOwnerIDNumber,
                    NewCountryCode = request.NewCountryCode,
                    SourceBrand = request.SourceBrand,
                    Fingerprint = GetActivationServiceFingerprint(request.Fingerprint),
                    Latitude = request.Latitude,
                    Longitude = request.Longitude,
                    CurrentOwnerAddress = request.CurrentOwnerAddress,
                    NewOwnerAddress = request.NewOwnerAddress,
                    ReferenceNo = request.ReferenceNo,
                    LocationId = request.LoginRequest.LocationId
                };

                serviceRequest.Documents = new List<ActivationService.Document>();
                if (request.Documents != null && request.Documents.Any())
                {
                    serviceRequest.Documents = request.Documents.Select(d => new ActivationService.Document
                    {
                        FileContent = Convert.FromBase64String(d.DocumentContent),
                        Type = (ActivationService.DocumentType)d.DocumentTypeID,
                        Name = d.RequestDocumentStagingGuid
                    }).ToList();
                }

                if (request.CurrentOwnerSignature != null && !string.IsNullOrEmpty(request.CurrentOwnerSignature.DocumentContent))
                    serviceRequest.CurrentOwnerSignature = Convert.FromBase64String(request.CurrentOwnerSignature.DocumentContent);

                if (request.NewOwnerSignature != null && !string.IsNullOrEmpty(request.NewOwnerSignature.DocumentContent))
                    serviceRequest.NewOwnerSignature = Convert.FromBase64String(request.NewOwnerSignature.DocumentContent);

                if (request.DealerSignature != null && !string.IsNullOrEmpty(request.DealerSignature.DocumentContent))
                    serviceRequest.DealerSignature = Convert.FromBase64String(request.DealerSignature.DocumentContent);

                using (var client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, requester.UserName, requester.Code, DSTChannelValue))
                    {
                        response.IsPassed = client.SubmitOwnershipChange(serviceRequest, true);
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "SubmitOwnershipChange", request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "SubmitOwnershipChange", request.LoginRequest.Username, ex.ToString());
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;

                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitOwnershipChange", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        //Anas Alzube
        public static CallResponse ReplaceSIM(Contracts.SIMReplacementRequest request)
        {
            bool needResubmission;
            string SMDB = string.Empty;
            CallResponse response = new CallResponse();
            AdministrationServices.Requester requester;
            string userName;
            bool success = false;
            ActivationServiceClient client = null;
            try
            {
                response = CheckRequester(request.oLoginRequest, out requester, out userName, false);
                if (!response.IsPassed)
                {
                    return response;
                }

                ActivationService.SIMReplacementRequest serviceRequest = new ActivationService.SIMReplacementRequest
                {
                    IDNumber = request.IDNumber,
                    IDType = request.IDType,
                    MSISDN = request.MSISDN,
                    Fingerprint = GetActivationServiceFingerprint(request.Fingerprint),
                    SourceBrand = (BrandsEnum)Enum.Parse(typeof(BrandsEnum), request.SourceBrand.ToString(), true),
                    Latitude = request.Latitude,
                    Longitude = request.Longitude,
                    CountryCode = request.NationalityCode,
                    IsESIM = request.IsESIM,
                    SIMCode = request.SIMCode,
                    LocationId = request.oLoginRequest.LocationId,
                    ReferenceNo = request.ReferenceNo
                };

                client = new ActivationServiceClient();
                client.Open();
                using (UserContextScope userScope = new UserContextScope(client.InnerChannel, requester.UserName, requester.Code, DSTChannelValue))
                {
                    response.IsPassed = client.ReplaceSIM(out needResubmission, out SMDB, serviceRequest);
                    if (needResubmission == true)
                    {
                        response.ResponseMessage = objResourceManager.GetString("NeedResubmation", Thread.CurrentThread.CurrentCulture);
                        //"the SIM Replacement is success but the resubmation needed for this Msisdn";
                    }

                    if (!string.IsNullOrEmpty(SMDB))
                    {
                        response.ESIMData = new Contracts.ESIMData();
                        response.ESIMData.SMDP_URL = SMDB;
                    }
                }
                client.Close();
                success = true;
            }
            catch (LPBizException LPBizEx)
            {
                success = false;
                ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "ReplaceSIM", request.oLoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
            }
            catch (FaultException ex)
            {
                success = false;
                Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "ReplaceSIM", request.oLoginRequest.Username, ex.ToString());
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;

                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                success = false;
                ErrorHandling.HandleException("ReplaceSIM", ex, request.oLoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }
            finally
            {
                if (!success && client != null)
                {
                    client.Abort();
                }
            }
            return response;
        }

        //Anas Alzube
        public static CallResponse ValidateReplaceSIM(Contracts.SIMReplacementRequest request)
        {
            CallResponse response = new CallResponse();
            AdministrationServices.Requester requester;
            string userName;
            bool success = false;
            ActivationServiceClient client = null;
            try
            {
                response = CheckRequester(request.oLoginRequest, out requester, out userName, false);
                if (!response.IsPassed)
                {
                    return response;
                }
                ActivationService.SIMReplacementRequest serviceRequest = new ActivationService.SIMReplacementRequest
                {
                    IDNumber = request.IDNumber,
                    IDType = request.IDType,
                    MSISDN = request.MSISDN,
                    Fingerprint = null,
                    SourceBrand = (BrandsEnum)request.SourceBrand,
                    Latitude = request.Latitude,
                    Longitude = request.Longitude,
                    Documents = new List<ActivationService.Document>()
                };

                client = new ActivationServiceClient();
                client.Open();
                using (UserContextScope userScope = new UserContextScope(client.InnerChannel, requester.UserName, requester.Code, DSTChannelValue))
                {
                    response.IsPassed = client.ValidateReplaceSIM(serviceRequest);
                }
                client.Close();
                success = true;
            }
            catch (LPBizException LPBizEx)
            {
                success = false;
                ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "ValidateReplaceSIM", request.oLoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
            }
            catch (FaultException ex)
            {
                success = false;
                Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "ValidateReplaceSIM", request.oLoginRequest.Username, ex.ToString());
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;

                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                success = false;
                ErrorHandling.HandleException("ValidateReplaceSIM", ex, request.oLoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }
            finally
            {
                if (!success && client != null)
                {
                    client.Abort();
                }
            }
            return response;
        }

        //Anas Alzube
        public static CallResponse AddStockRequest(StockItemRequest sRequest)
        {
            string userName;
            double appVersion = double.Parse(sRequest.oLoginRequest.ApplicationVersion);
            LP.OMS.Channels.Engine.AdministrationServices.Requester requester;
            var oCallResponse = CheckRequester(sRequest.oLoginRequest, out requester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return oCallResponse;
            }

            try
            {
                using (OMSCoreEntities db = new OMSCoreEntities())
                {
                    StockRequest Request = new StockRequest();

                    Request.LastActionDate = DateTime.Now;
                    Request.RequestAmount = 50;
                    Request.RequesterDate = DateTime.Now;
                    Request.RequesterId = requester.ID;
                    Request.StockItemId = sRequest.ItemID;
                    Request.StockRequestStatus = "Pending";
                    db.StockRequests.Add(Request);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AddStockRequest", ex, "");
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public static void LogError(DSTErrorLog dstErrorLog)
        {
            LPLogger.Error(dstErrorLog.error);
            using (Entities context = new Entities())
            {
                ErrorLog log = new ErrorLog();

                log.DateTime = System.DateTime.Now;
                log.ErrorDescription = "DTS LogError";
                log.ErrorType = "DTS LogError";
                log.ExtraDetails = dstErrorLog.error;
                log.Source = "DTS LogError";

                context.ErrorLogs.Add(log);
                context.SaveChanges();
            }
        }

        //Anas Alzube
        public static BundleResponse GetBundle(BundlesRequest request)
        {
            BundleResponse oBundleResponse = new BundleResponse();
            try
            {
                oBundleResponse.Bundles = new List<Contracts.Bundle>();
                oBundleResponse.Bundles.Add(new Contracts.Bundle { ID = 1, Name = "1GB", Value = 1000 });
                oBundleResponse.Bundles.Add(new Contracts.Bundle { ID = 2, Name = "2GB", Value = 2000 });
                oBundleResponse.Bundles.Add(new Contracts.Bundle { ID = 3, Name = "3GB", Value = 3000 });
                oBundleResponse.IsPassed = true;
                oBundleResponse.ResponseMessage = "Success";
            }

            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetBundle", ex, "");
                oBundleResponse.IsPassed = false;
                oBundleResponse.ResponseMessage = ex.Message;
            }
            return oBundleResponse;
        }

        public static CallResponse SubmitBundle(SubmitBundleRequest SubmitBundleRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                oCallResponse.IsPassed = true;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitBundle", ex, "");
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public static int SelfcareResubmitRequest(string msisdn, string idNumber, int brand, int documentType, string additionalInfo, List<byte[]> lstResubmissionDocuments)
        {
            int ResponseCode = (int)ResubmissionResponseCodes.UNKNOWN_ERROR;
            bool success = false;
            bool isValidResumissionRequest = false;
            ActivationService.ActivationServiceClient client = null;
            try
            {
                string dealerUsername = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.SelfcareDealer.UserName"];
                client = new ActivationService.ActivationServiceClient();
                client.Open();
                using (var userScope = new UserContextScope(client.InnerChannel, dealerUsername, dealerUsername, "Selfcare"))
                {
                    if (brand == (int)Brand.Friendi)
                    {
                        isValidResumissionRequest = client.ResubmitRejectedRequest(msisdn, idNumber, lstResubmissionDocuments);
                    }
                    else
                    {
                        isValidResumissionRequest = client.ResubmitRejectedRequestVirgin(msisdn, idNumber, lstResubmissionDocuments);
                    }
                }
                client.Close();
                success = true;
                if (!isValidResumissionRequest)
                {
                    ResponseCode = (int)ResubmissionResponseCodes.FAILED;
                }
                else
                {
                    ResponseCode = (int)ResubmissionResponseCodes.SUCCESS;
                    // Send SMS Message to client
                    try
                    {
                        if (brand == (int)Brand.Virgin)
                        {
                            string[] messages = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Virgin.ResubmissionMessage"].Split(';');
                            if (messages != null && messages.Length > 0)
                            {
                                NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Virgin.MessageSender"], msisdn, messages[0], true);
                            }
                            if (messages.Length > 1) // for Arabic
                            {
                                NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Virgin.MessageSender"], msisdn, messages[1], true);
                            }
                        }
                        else
                        {
                            string[] messages = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.ResubmissionMessage"].Split(';');
                            if (messages != null && messages.Length > 0)
                            {
                                NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.MessageSender"], msisdn, messages[0], true);
                            }
                            if (messages.Length > 1) // for Arabic
                            {
                                NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.MessageSender"], msisdn, messages[1], true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.HandleException("SelfcareResubmitRequest - SendSMS", ex, "Selfcare");
                    }
                }

                using (Entities context = new Entities())
                {
                    SelfcareResubmittedRequest request = new SelfcareResubmittedRequest();
                    request.MSISDN = msisdn;
                    request.IdNumber = idNumber;
                    request.Brand = brand;
                    request.DocumentType = documentType;
                    request.IsSuccess = ResponseCode == (int)ResubmissionResponseCodes.SUCCESS ? true : false;
                    context.SelfcareResubmittedRequests.Add(request);
                    context.SaveChanges();
                }
                try
                {
                    string urlUserName = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.UrlShortenerAPI.UserName"];
                    string urlPassword = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.UrlShortenerAPI.Password"];

                    using (UrlShortenerAPI.UrlShortenerAPISoapClient shortURLClient = new UrlShortenerAPI.UrlShortenerAPISoapClient())
                    {
                        shortURLClient.DeleteUrl("", additionalInfo, urlUserName, urlPassword);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("SelfcareResubmitRequest", ex, "Selfcare");
                }
            }
            catch (Exception ex)
            {
                success = false;
                ResponseCode = (int)ResubmissionResponseCodes.UNKNOWN_ERROR;
                ErrorHandling.HandleException("SelfcareResubmitRequest", ex, "Selfcare");
            }
            finally
            {
                if (!success && client != null)
                {
                    client.Abort();
                }
            }

            return ResponseCode;
        }

        public static AvailableProductsResponse GetAvailableProducts(Contracts.ProductSellingRequest request)
        {
            AvailableProductsResponse oAvailableResponse = new AvailableProductsResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
            CallResponse oCallResponse = CheckRequester(request.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            oAvailableResponse.IsPassed = oCallResponse.IsPassed;
            oAvailableResponse.ResponseMessage = oCallResponse.ResponseMessage;
            oAvailableResponse.MSISDN = oCallResponse.MSISDN;
            if (oAvailableResponse.IsPassed == false)
            {
                return oAvailableResponse;
            }

            Contracts.BaseResponse BaseResponse = IsValidDealerEWallet(oRequester.RefillMSISDN,
                request.oLoginRequest.language);

            if (!BaseResponse.IsPassed)
            {
                return new AvailableProductsResponse()
                {
                    IsPassed = false,
                    ResponseMessage = BaseResponse.ResponseMessage
                };
            }

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    ActivationService.ProductSellingRequest ActRequest = new ActivationService.ProductSellingRequest
                    {
                        IDNumber = request.IDNumber,
                        MSISDN = request.MSISDN,
                        SourceBrand = (BrandsEnum)request.SourceBrand,
                        SourceLanguage = request.oLoginRequest.language,
                        IsOnActivation = request.IsOnActivation,
                        Latitude = request.Latitude,
                        Longitude = request.Longitude
                    };
                    List<ActivationService.ProductCategoryDC> Product;
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        Product = client.GetAvailableProducts(ActRequest);
                    }
                    if (Product != null)
                    {
                        oAvailableResponse.ProductCategory = Product.Select(c => new Contracts.ProductCategory
                        {
                            ID = c.ID,
                            Name = c.Name,
                            Products = c.Products.Select(p => new Contracts.Product
                            {
                                DealerCommission = p.DealerCommission,
                                Details = p.Details,
                                ID = p.ID,
                                Name = p.Name,
                                Price = p.Price,
                                Validity = p.Validity,
                                Promotions = (p.Promotions != null && p.Promotions.Count > 0) ? p.Promotions.Select(e => new Contracts.PlanInfo { Type = (int)e.Type, Unit = e.Unit, Value = e.Value, Description = e.Description }).ToList() : null,
                                NewPropositionDetails = (p.NewPropositionDetails != null && p.NewPropositionDetails.Count > 0) ? p.NewPropositionDetails.Select(e => new Contracts.PlanInfo { Type = (int)e.Type, Unit = e.Unit, Value = e.Value, Description = e.Description }).ToList() : null,
                                OldPrice = p.OldPrice,
                                PriceWithoutVAT = p.PriceWithoutVAT,
                                PromotionsDTO = (p.PromotionsDTO != null && p.PromotionsDTO.Count > 0) ? p.PromotionsDTO.Select(x => new Contracts.PromotionsDTO { Type = x.Type, Value = x.Value }).ToList() : null,
                            }).ToList(),
                            IsNewProposition = c.IsNewProposition

                        }).ToList();
                    }
                    oAvailableResponse.IsPassed = true;
                    oAvailableResponse.ResponseMessage = "Success";
                }
                catch (LPBizException LPBizEx)
                {
                    ErrorHandling.HandleException("GetAvailableProducts", LPBizEx, request.oLoginRequest.Username);
                    oAvailableResponse.IsPassed = false;
                    if (LPBizEx.Code == "-1111111")
                    {
                        oAvailableResponse.ResponseMessage = LPBizEx.Message;
                    }
                    else
                    {
                        oAvailableResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                    }
                }
                catch (FaultException ex)
                {
                    const string CustomError = "-1111111";
                    LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    ErrorHandling.HandleException("GetAvailableProducts", LPBizEx, request.oLoginRequest.Username);
                    oAvailableResponse.IsPassed = false;

                    if (ex.Code.Name == CustomError)
                    {
                        oAvailableResponse.ResponseMessage = LPBizEx.Message;
                    }
                    else
                    {
                        oAvailableResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("GetAvailableProducts", ex, request.oLoginRequest.Username);
                    oAvailableResponse.IsPassed = false;
                    oAvailableResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return oAvailableResponse;
        }

        public static AvailableProductsResponse GetAvailableProductsBySubscriptionType(Contracts.ProductSellingBySubscriptionTypeRequest request)
        {
            AvailableProductsResponse oAvailableResponse = new AvailableProductsResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
            CallResponse oCallResponse = CheckRequester(request.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            oAvailableResponse.IsPassed = oCallResponse.IsPassed;
            oAvailableResponse.ResponseMessage = oCallResponse.ResponseMessage;
            oAvailableResponse.MSISDN = oCallResponse.MSISDN;
            if (oAvailableResponse.IsPassed == false)
            {
                return oAvailableResponse;
            }

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    ActivationService.ProductSellingBySubscriptionTypeRequest ActRequest = new ActivationService.ProductSellingBySubscriptionTypeRequest
                    {
                        //IDNumber = request.IDNumber,
                        MSISDN = "", // during activation MSISDN has to be empty to return all available
                        SourceBrand = (BrandsEnum)request.SourceBrand,
                        SourceLanguage = request.oLoginRequest.language,
                        IsOnActivation = request.IsOnActivation,
                        Latitude = request.Latitude,
                        Longitude = request.Longitude,
                        SubscriptionTypeId = request.SubscriptionTypeId
                    };
                    List<ActivationService.ProductCategoryDC> Product;
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {

                        Product = client.GetAvailableProductsBySubscriptionType(ActRequest);
                    }
                    if (Product != null)
                    {
                        oAvailableResponse.ProductCategory = Product.Select(c => new Contracts.ProductCategory
                        {
                            ID = c.ID,
                            Name = c.Name,
                            Products = c.Products.Select(p => new Contracts.Product
                            {
                                DealerCommission = p.DealerCommission,
                                Details = p.Details,
                                ID = p.ID,
                                Name = p.Name,
                                Price = p.Price,
                                Validity = p.Validity,
                                Promotions = (p.Promotions != null && p.Promotions.Count > 0) ? p.Promotions.Select(e => new Contracts.PlanInfo { Type = (int)e.Type, Unit = e.Unit, Value = e.Value, Description = e.Description }).ToList() : null,
                                NewPropositionDetails = (p.NewPropositionDetails != null && p.NewPropositionDetails.Count > 0) ? p.NewPropositionDetails.Select(e => new Contracts.PlanInfo { Type = (int)e.Type, Unit = e.Unit, Value = e.Value, Description = e.Description }).ToList() : null,
                                OldPrice = p.OldPrice,
                                PriceWithoutVAT = p.PriceWithoutVAT,
                                PromotionsDTO = (p.PromotionsDTO != null && p.PromotionsDTO.Count > 0) ? p.PromotionsDTO.Select(x => new Contracts.PromotionsDTO { Type = x.Type, Value = x.Value }).ToList() : null,
                            }).ToList(),
                            IsNewProposition = c.IsNewProposition
                        }).ToList();
                    }
                    oAvailableResponse.IsPassed = true;
                    oAvailableResponse.ResponseMessage = "Success";
                }
                catch (LPBizException LPBizEx)
                {

                    ErrorHandling.HandleException("GetAvailableProductsBySubscriptionType", LPBizEx, request.oLoginRequest.Username);
                    oAvailableResponse.IsPassed = false;
                    if (LPBizEx.Code == "-1111111")
                    {
                        oAvailableResponse.ResponseMessage = LPBizEx.Message;
                    }
                    else
                    {
                        oAvailableResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                    }
                }
                catch (FaultException ex)
                {

                    const string CustomError = "-1111111";
                    LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    ErrorHandling.HandleException("GetAvailableProductsBySubscriptionType", LPBizEx, request.oLoginRequest.Username);
                    oAvailableResponse.IsPassed = false;

                    if (ex.Code.Name == CustomError)
                    {
                        oAvailableResponse.ResponseMessage = LPBizEx.Message;
                    }
                    else
                    {
                        oAvailableResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                    }
                }
                catch (Exception ex)
                {

                    ErrorHandling.HandleException("GetAvailableProductsBySubscriptionType", ex, request.oLoginRequest.Username);
                    oAvailableResponse.IsPassed = false;
                    oAvailableResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return oAvailableResponse;
        }

        public static Contracts.ProductSellingResponse SubmitProductSelling(Contracts.ProductSellingRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            Contracts.ProductSellingResponse productSellingResponse = new Contracts.ProductSellingResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
            oCallResponse = CheckRequester(request.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                productSellingResponse.ResponseMessage = oCallResponse.ResponseMessage;
                productSellingResponse.RechargeMessage = oCallResponse.RechargeMessage;
                productSellingResponse.IsPassed = oCallResponse.IsPassed;
                productSellingResponse.MSISDN = oCallResponse.MSISDN;
                return productSellingResponse;
            }

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    ActivationService.ProductSellingRequest ActRequest = new ActivationService.ProductSellingRequest
                    {
                        IDNumber = request.IDNumber,
                        MSISDN = request.MSISDN,
                        SourceBrand = (BrandsEnum)request.SourceBrand,
                        SourceLanguage = request.oLoginRequest.language,
                        ProductID = request.ProductID,
                        IsOnActivation = request.IsOnActivation,
                        Latitude = request.Latitude,
                        Longitude = request.Longitude,
                        ReferenceNo = request.ReferenceNo,
                        ProductSellingTransactionId = request.ProductSellingTransactionId,
                        ForceSubscribe = request.ForceSubscribe,
                        LocationId = request.oLoginRequest.LocationId
                    };

                    ActivationService.ProductSellingResponse clientProductSellingResponse = new ActivationService.ProductSellingResponse();
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        clientProductSellingResponse = client.SubmitProductSelling(ActRequest, IsDealerHasRight(ConfigurationManager.AppSettings["OpChangePlanRightCode"], oRequester));
                    }

                    productSellingResponse.IsPassed = clientProductSellingResponse.IsSuccess;
                    productSellingResponse.ShowChangePlanConfirmationPage = clientProductSellingResponse.ShowChangePlanConfirmationPage;
                    productSellingResponse.ProductSellingTransactionId = clientProductSellingResponse.ProductSellingTransactionId;

                    if (productSellingResponse.IsPassed)
                    {
                        productSellingResponse.ResponseMessage = "Success";
                    }

                }
                catch (LPBizException LPBizEx)
                {
                    ErrorHandling.HandleException("SubmitProductSelling", LPBizEx, request.oLoginRequest.Username);
                    productSellingResponse.IsPassed = false;

                    if (LPBizEx.Code == "-1111111")
                        productSellingResponse.ResponseMessage = LPBizEx.Message;
                    else
                        productSellingResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);

                }
                catch (FaultException ex)
                {
                    const string CustomError = "-1111111";
                    LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    ErrorHandling.HandleException("SubmitProductSelling", LPBizEx, request.oLoginRequest.Username);
                    productSellingResponse.IsPassed = false;

                    if (ex.Code.Name == CustomError)
                        productSellingResponse.ResponseMessage = LPBizEx.Message;
                    else
                        productSellingResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);

                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("SubmitProductSelling", ex, request.oLoginRequest.Username);
                    productSellingResponse.IsPassed = false;
                    productSellingResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return productSellingResponse;
        }

        public static CallResponse CheckProductSellingEligibility(CheckProductSellingEligibilityRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            string name = "";
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
            oCallResponse = CheckRequester(request.oLoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return oCallResponse;
            }

            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        if (request.MSISDN.Length > 12)
                            request.MSISDN = request.MSISDN.Substring(0, 12);
                        oCallResponse.IsPassed = client.CheckProductSellingEligibility(request.MSISDN);
                    }
                }
                catch (LPBizException LPBizEx)
                {
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                }
                catch (FaultException ex)
                {
                    LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("CheckProductSellingEligibility", ex, request.oLoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }
            return oCallResponse;
        }

        public static CallResponse VerfiyLoginCode(VerfiyLoginCode request)
        {
            string sessionToken = "";
            if (request.oLoginRequest.language == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            CallResponse oCallResponse = new CallResponse();
            AMErrorCode resultCode = new AMErrorCode();
            int nextSessionTimeCheck = 0;
            resultCode = AccessManagementController.CheckExistingSession(request.oLoginRequest.Username, request.oLoginRequest.Password, request.oLoginRequest.MobileDeviceId, request.oLoginRequest.ApplicationVersion == "Portal" ? "Portal" : request.oLoginRequest.ApplicationVersion, out sessionToken, out nextSessionTimeCheck, needToExtendExistingSession: false);


            if (resultCode == AMErrorCode.Success)
                resultCode = AMService.ValidateSessionCode(request.oLoginRequest.Token, request.oLoginRequest.Username, request.MessagesCode);

            if (resultCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
            }
            else
            {
                oCallResponse.IsPassed = true;
            }

            oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(resultCode);

            return oCallResponse;
        }

        public static CallResponse ResendLoginCode(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            AMErrorCode resultCode = new AMErrorCode();
            try
            {
                if (request.language == 2)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                }

                if (!AMService.ValidateSessionToken(request.Token))
                    resultCode = AMErrorCode.SessionInvalid;
                else
                    resultCode = AMService.ResendSessionCode(request.Token, request.Username);

                if (resultCode != AMErrorCode.Success)
                {
                    oCallResponse.IsPassed = false;
                }
                else
                {
                    oCallResponse.IsPassed = true;
                }

                oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(resultCode);
            }

            catch (LPBizException LPBizEx)
            {
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
            }
            catch (FaultException ex)
            {
                LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ResendLoginCode", ex, request.Username);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return oCallResponse;
        }

        public static CallResponse CheckSessionValidity(string sessionToken, int lang, bool ExtendSession = true)
        {
            CallResponse oCallResponse = new CallResponse();

            if (lang == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            AMErrorCode resultCode = AMService.CheckSessionValidity(sessionToken, ExtendSession);

            if (resultCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
            }
            else
            {
                oCallResponse.IsPassed = true;
            }

            oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(resultCode);

            return oCallResponse;
        }

        public static CallResponse Logout(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            string name = "";
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.ApplicationVersion);
            oCallResponse = CheckRequester(request, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return oCallResponse;
            }

            if (request.language == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            #region AttendanceTracking

            if (ModernTradeController.IS_ATTENDANCE_ENABLED && request.ApplicationVersion != "Portal")
            {
                Contracts.AttendanceTrackingRequest AttendanceTrackingRequest = new AttendanceTrackingRequest();
                AttendanceTrackingRequest.AttendanceProcessType = (int)AttendanceProcessType.Logout;
                AttendanceTrackingRequest.Latitude = request.Latitude;
                AttendanceTrackingRequest.Longitude = request.Longitude;
                AttendanceTrackingRequest.LoginRequest = new LoginRequest();
                AttendanceTrackingRequest.LoginRequest = request;

                if (ModernTradeController.LOCATION_TRACKING_MODE == 1) // All Dealers
                {
                    ModernTradeController.AttendanceTracking(AttendanceTrackingRequest);
                }
                else if (ModernTradeController.LOCATION_TRACKING_MODE == 2) // Only Modern Trade Dealers
                {
                    if (ModernTradeController.IsModernTradeDealer(oRequester.UserName))
                    {
                        ModernTradeController.AttendanceTracking(AttendanceTrackingRequest);
                    }
                }

            }
            #endregion

            AMErrorCode resultCode = AMService.EndSession(request.Token);

            if (resultCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
            }
            else
            {
                oCallResponse.IsPassed = true;
            }

            oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(resultCode);

            return oCallResponse;
        }

        public static CallResponse ValidateSessionTransaction(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            if (request.language == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            AMErrorCode resultCode = AMErrorCode.Success;

            if (resultCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
            }
            else
            {
                oCallResponse.IsPassed = true;
            }

            objResourceManager.GetString("ValidateSessionTransaction_" + resultCode.ToString(), Thread.CurrentThread.CurrentCulture);

            return oCallResponse;
        }

        public static NextSessionCheckTimeResponse GetNextSessionCheckTime(string sessionToken)
        {
            NextSessionCheckTimeResponse response = new NextSessionCheckTimeResponse();
            int nextSessionCheckTime = 0;
            int sessionExpireAfter = 0;
            AMService.FillNextSessionCheckTimeDataByToken(sessionToken, out nextSessionCheckTime, out sessionExpireAfter);
            response.NextSessionCheckTime = nextSessionCheckTime;
            response.SessionExpireAfter = sessionExpireAfter;
            return response;
        }

        public static ExtendSessionRequest ExtendSession(string sessionToken)
        {
            ExtendSessionRequest oExtendSessionRequest = new ExtendSessionRequest();
            int nextSessionCheckTime = 0;
            int sessionExpireAfter = 0;
            AMService.FillNextSessionCheckTimeDataByToken(sessionToken, out nextSessionCheckTime, out sessionExpireAfter);
            oExtendSessionRequest.NextSessionCheckTime = nextSessionCheckTime;
            return oExtendSessionRequest;
        }

        //Anas Alzube
        //public static CallResponse PasswordResetGetVerificationCode(PasswordResetGetVerificationCodeRequest request)
        //{
        //    CallResponse oCallResponse = new CallResponse();
        //    try
        //    {
        //        Requester requester = new Requester();
        //        using (OMSCoreEntities db = new OMSCoreEntities())
        //        {
        //            requester = db.Requesters.Where(r => r.UserName == request.UserName).FirstOrDefault();
        //            if (requester != null)
        //            {
        //                if (requester.Address == request.IDNumber && requester.UserName == request.UserName)
        //                {
        //                    string verificationCode = GenerateVerificationCode();
        //                    requester.VerificationCode = verificationCode;
        //                    db.Entry(requester).State = EntityState.Modified;
        //                    oCallResponse.IsPassed = true;
        //                }
        //                else
        //                {
        //                    oCallResponse.IsPassed = false;
        //                    oCallResponse.ResponseMessage = "IdNumber is InValid";
        //                    return oCallResponse;
        //                }
        //            }
        //            else
        //            {
        //                oCallResponse.IsPassed = false;
        //                oCallResponse.ResponseMessage = "User Name is InValid";
        //                return oCallResponse;
        //            }
        //        }
        //        oCallResponse.MSISDN = requester.RefillMSISDN;
        //        NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Controller.Sender"].ToString(), requester.RefillMSISDN, requester.VerificationCode, false, false);
        //    }
        //    catch (LPBizException LPBizEx)
        //    {
        //        ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "PasswordResetGetVerificationCode", request.UserName);
        //        oCallResponse.IsPassed = false;
        //        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, 1);
        //    }
        //    catch (FaultException ex)
        //    {
        //        Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "PasswordResetGetVerificationCode", request.UserName, ex.ToString());
        //        try
        //        {
        //            throw new LPBizException(ex.Code.Name, ex.Message);
        //        }
        //        catch (LPBizException LPBizEx)
        //        {
        //            oCallResponse.IsPassed = false;

        //            oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, 1 /*language*/);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("PasswordResetGetVerificationCode", ex, request.UserName);
        //        oCallResponse.IsPassed = false;
        //        oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
        //    }
        //    return oCallResponse;
        //}

        //Anas Alzube
        //public static string GenerateVerificationCode()
        //{
        //    Random rdm = new Random();
        //    string verificationCode = rdm.Next(0, 9999).ToString("0000");
        //    return verificationCode;
        //}

        //Anas Alzube
        //public static CheckVerificationResetPasswordResponse CheckVerificationResetPassword(CheckVerificationResetPasswordRequest request)
        //{
        //    CheckVerificationResetPasswordResponse response = new CheckVerificationResetPasswordResponse();
        //    try
        //    {
        //        Requester requester = new Requester();
        //        using (OMSCoreEntities db = new OMSCoreEntities())
        //        {
        //            requester = db.Requesters.Where(r => r.UserName == request.Username).FirstOrDefault();
        //        }
        //        if (requester != null)
        //        {
        //            if (requester.VerificationCode == request.VerificationCode)
        //            {
        //                response.oCallResponse.IsPassed = true;
        //                response.oCallResponse.ResponseMessage = "Verification Code is Valid";
        //                response.Password = requester.Password;
        //            }
        //            else
        //            {
        //                response.oCallResponse.IsPassed = false;
        //                response.oCallResponse.ResponseMessage ="Verification Code is InValid";
        //            }
        //        }
        //        else
        //        {
        //            response.oCallResponse.IsPassed = false;
        //            response.oCallResponse.ResponseMessage = "Msisdn is Invalid";
        //        }
        //    }
        //    catch (LPBizException LPBizEx)
        //    {
        //        ErrorHandling.AddToErrorLog("Infromation", "BizError : " + LPBizEx.Code, "CheckVerificationResetPassword", "");
        //        response.oCallResponse.IsPassed = false;
        //        response.oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, 1);
        //    }
        //    catch (FaultException ex)
        //    {
        //        Utilities.ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "CheckVerificationResetPassword", "", ex.ToString());
        //        try
        //        {
        //            throw new LPBizException(ex.Code.Name, ex.Message);
        //        }
        //        catch (LPBizException LPBizEx)
        //        {
        //            response.oCallResponse.IsPassed = false;

        //            response.oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, 1 /*language*/);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("CheckVerificationResetPassword", ex, "");
        //        response.oCallResponse.IsPassed = false;
        //        response.oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
        //    }
        //    return response;
        //}

        #endregion

        #region Old Lookups /* STILL IN USE */

        //kept the LoginRequest object to use the language and retrieve the list based on it, no validation wil lhappen thogh
        public static List<Contracts.IDType> GetIdTypeList(LoginRequest oLoginRequest)
        {


            List<Contracts.IDType> idTypeContractList = new List<IDType>();

            List<IdType> idTypeList = null;

            using (Entities context = new Entities())
            {
                idTypeList = (from idTypes in context.IdTypes select idTypes).ToList();
            }

            idTypeContractList = new List<Contracts.IDType>(idTypeList.Count);

            foreach (IdType idType in idTypeList)
            {
                Contracts.IDType idTypeContract = new Contracts.IDType();
                idTypeContract.Id = idType.ID;
                idTypeContract.Code = idType.Code;
                if (oLoginRequest.language == 1) idTypeContract.Description = idType.Name;
                else idTypeContract.Description = idType.ForeignName;

                idTypeContractList.Add(idTypeContract);
            }

            return idTypeContractList;
        }

        //kept the LoginRequest object to use the language and retrieve the list based on it
        public static List<LP.OMS.Channels.Contracts.Country> GetCountryList(LoginRequest oLoginRequest)
        {
            List<Contracts.Country> oContractCountries = new List<Contracts.Country>();

            if (oCountries == null)
            {
                using (OMSCoreEntities context = new OMSCoreEntities())
                {
                    oCountries = (from countries in context.Countries select countries).Where(c => c.Prefix != null && c.Prefix.Trim() != "966").ToList();
                }
            }

            foreach (Country cntry in oCountries)
            {
                Contracts.Country countryContract = new Contracts.Country();
                countryContract.Id = cntry.ID;
                countryContract.Code = cntry.ID.ToString();
                if (oLoginRequest.language == 1) countryContract.Description = cntry.Name;
                else countryContract.Description = cntry.ForeignName;

                oContractCountries.Add(countryContract);
            }

            List<Contracts.Country> returnedObj = oContractCountries.OrderBy(x => x.Description).ToList();

            return returnedObj;

        }

        //kept the LoginRequest object to use the language and retrieve the list based on it
        public static List<Contracts.Gender> GetGenderList(LoginRequest oLoginRequest)
        {
            List<Contracts.Gender> genderContractList = new List<Contracts.Gender>();

            List<Gender> genderList = null;

            using (Entities context = new Entities())
            {
                genderList = (from genders in context.Genders select genders).ToList();
            }

            genderContractList = new List<Contracts.Gender>(genderList.Count);

            foreach (Gender gender in genderList)
            {
                Contracts.Gender genderContract = new Contracts.Gender();
                genderContract.Code = gender.Code;
                genderContract.Id = gender.ID;

                if (oLoginRequest.language == 1) genderContract.Name = gender.Name;
                else genderContract.Name = gender.ForeignName;

                genderContractList.Add(genderContract);
            }

            return genderContractList;
        }

        //kept the LoginRequest object to use the language and retrieve the list based on it
        public static List<Contracts.Title> GetTitleList(LoginRequest oLoginRequest)
        {
            List<Contracts.Title> titleContractList = new List<Contracts.Title>();
            List<Title> titleList = null;

            using (Entities context = new Entities())
            {
                titleList = (from tittles in context.Titles select tittles).ToList();
            }

            foreach (Title title in titleList)
            {
                Contracts.Title titleContract = new Contracts.Title();
                titleContract.Code = title.Code;
                titleContract.Id = title.ID;
                if (oLoginRequest.language == 1) titleContract.Description = title.Name;
                else titleContract.Description = title.ForeignName;

                titleContractList.Add(titleContract);
            }

            return titleContractList;
        }

        public static bool BlockDealer(string dealerCode, string channelName)
        {
            bool isSuccess = false;

            using (AdministrationServices.AdministrationServicesClient client = new AdministrationServices.AdministrationServicesClient("WSHttpBinding_IAdministrationServices"))
            {
                try
                {
                    client.BlockRequester(dealerCode, "* SEMATI LOGIN EXEEDED in " + channelName);
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("BlockDealer-AdministrationServices", ex, dealerCode);
                    isSuccess = false;
                }
            }
            return isSuccess;
        }

        #endregion

        #region Private methods
        private static List<Contracts.Country> GetNationalityList(OMSLanguages language)
        {
            List<Contracts.Country> NationalityList = new List<Contracts.Country>();
            try
            {
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    var countryList = client.GetNationalityList(language == OMSLanguages.Local ? EngineLanguage.Local : EngineLanguage.Foreign);
                    if (countryList != null)
                    {
                        NationalityList = countryList.Select(c => new Contracts.Country
                        {
                            Id = int.Parse(c.Code),
                            Code = c.Code.ToString(),
                            Description = c.Name
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.ErrorHandling.HandleException("GetNationalityList", ex, "");
            }
            return NationalityList;
        }
        public static CallResponse CheckRequester(LoginRequest oLoginRequest, out AdministrationServices.Requester oRequester, out string userName, bool checkVersion = true)
        {
            CallResponse oCallResponse = new CallResponse() { ESIMData = new Contracts.ESIMData() };
            oRequester = null;
            userName = "";
            //#if (DisableAuthentication)
            //            if (true)
            //            {
            //                oRequester = new AdministrationServices.Requester();
            //                oRequester.DistributerId = 1;
            //                oRequester.Code = "123";
            //                oRequester.Rights = new AdministrationServices.Right[] {
            //                                new AdministrationServices.Right() { Code = "001"},
            //                                new AdministrationServices.Right() { Code = "002"},
            //                                new AdministrationServices.Right() { Code = "003"},
            //                                new AdministrationServices.Right() { Code = "004"},
            //                                new AdministrationServices.Right() { Code = "005"},
            //                                new AdministrationServices.Right() { Code = "006"},
            //                                new AdministrationServices.Right() { Code = "007"},
            //                                new AdministrationServices.Right() { Code = "008"},
            //                                new AdministrationServices.Right() { Code = "009"},
            //                                new AdministrationServices.Right() { Code = "010"},
            //                                new AdministrationServices.Right() { Code = "011"},
            //                                new AdministrationServices.Right() { Code = "012"},
            //                                new AdministrationServices.Right() { Code = "013"},
            //                                new AdministrationServices.Right() { Code = "014"},
            //                                new AdministrationServices.Right() { Code = "015"},
            //                                new AdministrationServices.Right() { Code = "016"},
            //                                new AdministrationServices.Right() { Code = "017"},
            //                                new AdministrationServices.Right() { Code = "018"},
            //                                new AdministrationServices.Right() { Code = "019"},
            //                                new AdministrationServices.Right() { Code = "020"},
            //                                new AdministrationServices.Right() { Code = "021"},
            //                                new AdministrationServices.Right() { Code = "025" },
            //                                new AdministrationServices.Right() { Code = "026" }
            //                            };

            //                return new CallResponse() { IsPassed = true };
            //            }
            //#endif
            if (oLoginRequest == null || string.IsNullOrWhiteSpace(oLoginRequest.Username))
            {
                oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_SessionOutdated", Thread.CurrentThread.CurrentCulture);
                oCallResponse.IsPassed = false;
                return oCallResponse;
            }
            if (oLoginRequest.language == 2)
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            else
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");

            using (AdministrationServices.AdministrationServicesClient client = new AdministrationServices.AdministrationServicesClient("WSHttpBinding_IAdministrationServices"))
            {
                try
                {
                    oRequester = client.AuthenticateRequester(oLoginRequest.Username, oLoginRequest.Password);
                    if (oRequester == null)
                    {
                        oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_UserPassValidation", Thread.CurrentThread.CurrentCulture);
                        oCallResponse.IsPassed = false;

                        return oCallResponse;
                    }
                    else if (!oRequester.IsActive)
                    {
                        oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_NotActiveDealer", Thread.CurrentThread.CurrentCulture);
                        oCallResponse.IsPassed = false;
                        return oCallResponse;
                    }
                    else
                    {
                        // Validate the channel access 
                        string dstChannelAccessCode = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.DSTChannel"].ToString();
                        if (oRequester.Rights == null || !oRequester.Rights.Where(p => p.Code == dstChannelAccessCode).Any())
                        {
                            oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_NotAuthorizedTablet", Thread.CurrentThread.CurrentCulture);
                            oCallResponse.IsPassed = false;
                            return oCallResponse;
                        }
                    }

                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("CheckRequester-AdministrationServices", ex, oLoginRequest.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                    return oCallResponse;
                }
            }
            if (checkVersion)
            {
                string mimimalAppVersionSettingKey = (oLoginRequest.DSTAppOS == DSTAppOS.IOS ? "LP.OMS.Channels.IOSApplicationMinimalVersion" : "LP.OMS.Channels.AndriodApplicationMinimalVersion");
                if (double.Parse(oLoginRequest.ApplicationVersion) < double.Parse(ConfigurationManager.AppSettings[mimimalAppVersionSettingKey].ToString()))
                {
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_VersionValidation", Thread.CurrentThread.CurrentCulture);
                    oCallResponse.IsPassed = false;
                    return oCallResponse;
                }

            }

            oCallResponse.IsPassed = true;
            userName = oRequester.Name;

            return oCallResponse;
        }
        private static Bitmap GetBitmap(byte[] imageBuffer)
        {
            Bitmap pageBitmap = null;
            using (MemoryStream imageMemoryStream = new MemoryStream(imageBuffer))
            {
                Image image = Image.FromStream(imageMemoryStream);

                FrameDimension frameDimension = new FrameDimension(image.FrameDimensionsList[0]);
                int numberOfFrames = image.GetFrameCount(frameDimension);

                if (numberOfFrames > 0)
                {
                    image.SelectActiveFrame(frameDimension, 0);
                    pageBitmap = new Bitmap(image);
                }
            }

            return pageBitmap;
        }
        private static Dictionary<string, string> ToDictionary(byte[] source)
        {
            Dictionary<string, string> dictionaryVal = null;


            BinarySerializer.Deserialize<Dictionary<string, string>>((byte[])source, ref dictionaryVal);


            return dictionaryVal;


        }
        private static DateTime GetPeriodDate(ReportViewMode mode)
        {
            List<String> arrDate = new List<string>();
            //DateTime SystemDate = DateTime.Now;
            DateTime fromDate = DateTime.Now;
            switch (mode)
            {

                case ReportViewMode.Daily:
                    fromDate = fromDate.AddDays(-1);
                    break;
                case ReportViewMode.Weekly:
                    fromDate = fromDate.AddDays(-7);
                    break;
                case ReportViewMode.Monthly:
                    fromDate = fromDate.AddDays(-30);
                    break;
                case ReportViewMode.LanchToDate:
                    fromDate = MinimDateTime;
                    break;
            }
            return fromDate;
        }
        private static bool IsFBORequester(int requesterClassId)
        {
            return _fboRequesterClassIds.Any(p => p == requesterClassId.ToString());
        }
        private static ActivationService.FingerPrint GetActivationServiceFingerprint(Contracts.FingerPrint restFingerprint)
        {
            ActivationService.FingerPrint serviceFP = null;
            if (restFingerprint != null)
            {
                if ((ActivationService.FP_Type)((int)restFingerprint.FP_Type) == ActivationService.FP_Type.Columbo)
                    if (!string.IsNullOrEmpty(restFingerprint.DeviceId))
                        restFingerprint.DeviceId = restFingerprint.DeviceId.ToUpper();

                serviceFP = new ActivationService.FingerPrint
                {
                    DeviceId = restFingerprint.DeviceId,
                    FingerPosition = (ActivationService.FingerPosition)((int)restFingerprint.FingerPosition),
                    FingerPrintText = restFingerprint.FingerPrintText,
                    NFIQValue = restFingerprint.NFIQValue,
                    IsSematiOTPUsed = restFingerprint.IsSematiOTPUsed,
                    SematiOTP = restFingerprint.SematiOTP,
                    FP_Type = (ActivationService.FP_Type)((int)restFingerprint.FP_Type)
                };
            }

            return serviceFP;
        }
        public static string GetAuthenticationManagementMessage(AMErrorCode responseCode)
        {
            if (responseCode == AMErrorCode.LogintimeInvalid)
            {
                string resource = GetResourceManager().GetString("AuthenticationManagement_" + responseCode.ToString(), Thread.CurrentThread.CurrentCulture);
                return string.Format(resource, Helper.GetMessageDetails(Helper.MessageDetailsKeys.StartTime), Helper.GetMessageDetails(Helper.MessageDetailsKeys.EndTime));
            }
            else
            {
                return GetResourceManager().GetString("AuthenticationManagement_" + responseCode.ToString(), Thread.CurrentThread.CurrentCulture);
            }
        }
        public static CallResponse GetSessionData(string sessionToken, int lang, out Session session)
        {
            CallResponse oCallResponse = new CallResponse();

            if (lang == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            AMErrorCode resultCode = AMService.GetSessionData(sessionToken, out session);

            if (resultCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
            }
            else
            {
                oCallResponse.IsPassed = true;
            }

            oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(resultCode);

            return oCallResponse;
        }
        public static CallResponse UpdateSessionData(string sessionToken, int lang, Session session)
        {
            CallResponse oCallResponse = new CallResponse();

            if (lang == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            AMErrorCode resultCode = AMService.UpdateSessionData(sessionToken, session);

            if (resultCode != AMErrorCode.Success)
            {
                oCallResponse.IsPassed = false;
            }
            else
            {
                oCallResponse.IsPassed = true;
            }

            oCallResponse.ResponseMessage = GetAuthenticationManagementMessage(resultCode);

            return oCallResponse;
        }
        public static SematiLoginCallResponse LoginToSemati(LoginRequest request)
        {

            SematiLoginCallResponse oCallResponse = new SematiLoginCallResponse();
            CallResponse oCheckRequesterCallResponse = new CallResponse();

            string name = "";
            AdministrationServices.Requester oRequester = null;
            oCheckRequesterCallResponse = CheckRequester(request, out oRequester, out name, false);
            if (!oCheckRequesterCallResponse.IsPassed)
            {
                return oCallResponse;
            }
            using (var client = new ActivationServiceClient())
            {
                try
                {
                    SematiLoginRequest loginRequest = new SematiLoginRequest()
                    {
                        Fingerprint = new ActivationService.FingerPrint()
                        {
                            // DeviceId = request.FingerPrint.IsSematiOTPUsed ? ConfigurationManager.AppSettings["DefaultFPDeviceID"] : request.FingerPrint.DeviceId,
                            DeviceId = request.FingerPrint.DeviceId,
                            FingerPosition = (LP.OMS.Channels.Engine.ActivationService.FingerPosition)request.FingerPrint.FingerPosition,
                            FingerPrintText = request.FingerPrint.FingerPrintText,
                            NFIQValue = request.FingerPrint.NFIQValue,
                            IsSematiOTPUsed = request.FingerPrint.IsSematiOTPUsed,
                            SematiOTP = request.FingerPrint.SematiOTP,
                        },
                        Latitude = request.Latitude.ToString(),
                        Longitude = request.Longitude.ToString(),

                    };
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        oCallResponse.IsPassed = client.LoginToSemati(loginRequest);
                    }
                    oCallResponse.ResponseMessage = "Success";
                }
                catch (LPBizException LPBizEx)
                {
                    ErrorHandling.HandleException("LoginToSemati", LPBizEx, request.Username);
                    oCallResponse.IsPassed = false;
                    if (LPBizEx.Code == "-1111111")
                    {
                        oCallResponse.ResponseMessage = LPBizEx.Message;
                        oCallResponse.RemainingAttempts = -1; // Show message as is 
                    }
                    else
                    {
                        string[] parts = LPBizEx.Code.Split('_');
                        LPBizEx.Code = parts[0];
                        oCallResponse.RemainingAttempts = parts.Length == 2 ? int.Parse(parts[1]) : -1;
                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
                    }
                }
                catch (FaultException ex)
                {
                    const string CustomError = "-1111111";
                    string[] parts = ex.Code.Name.Split('_');
                    LPBizException LPBizEx = new LPBizException(parts[0], ex.Message);
                    ErrorHandling.HandleException("LoginToSemati", LPBizEx, request.Username);
                    oCallResponse.IsPassed = false;

                    if (ex.Code.Name == CustomError)
                    {
                        oCallResponse.ResponseMessage = LPBizEx.Message;
                        oCallResponse.RemainingAttempts = -1; // Show message as is 
                    }
                    else
                    {
                        oCallResponse.RemainingAttempts = parts.Length == 2 ? int.Parse(parts[1]) : -1;
                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("LoginToSemati", ex, request.Username);
                    oCallResponse.IsPassed = false;
                    oCallResponse.RemainingAttempts = -1;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return oCallResponse;
        }
        public static CallResponse CheckSematiAuthentication(CheckSematiAuthenticationObject checkSematiAuthenticationObject)
        {
            CallResponse oCallResponse = new CallResponse();
            using (var client = new ActivationServiceClient())
            {
                try
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, checkSematiAuthenticationObject.DealerCode, checkSematiAuthenticationObject.DealerCode, DSTChannelValue))
                    {
                        oCallResponse.IsPassed = client.CheckSematiAuthentication(checkSematiAuthenticationObject.FingerPrintSerial);
                    }
                    oCallResponse.ResponseMessage = "Success";
                }
                catch (LPBizException LPBizEx)
                {
                    ErrorHandling.HandleException("CheckSematiAuthentication", LPBizEx, checkSematiAuthenticationObject.SessionToken);
                    oCallResponse.IsPassed = false;
                    if (LPBizEx.Code == "-1111111")
                    {
                        oCallResponse.ResponseMessage = LPBizEx.Message;
                    }
                    else
                    {
                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, 1);// need Language 
                    }
                }
                catch (FaultException ex)
                {
                    const string CustomError = "-1111111";
                    LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                    ErrorHandling.HandleException("CheckSematiAuthentication", LPBizEx, checkSematiAuthenticationObject.SessionToken);
                    oCallResponse.IsPassed = false;

                    if (ex.Code.Name == CustomError)
                    {
                        oCallResponse.ResponseMessage = LPBizEx.Message;
                    }
                    else
                    {
                        oCallResponse.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, 1); // need language
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("CheckSematiAuthentication", ex, checkSematiAuthenticationObject.SessionToken);
                    oCallResponse.IsPassed = false;
                    oCallResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }
            return oCallResponse;


        }
        public static OneSignalResponse GetOneSignalData(LoginRequest loginRequest)
        {
            OneSignalResponse oneSignalResponse = new OneSignalResponse();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                AdministrationServices.Requester oRequester = null;
                string userName = string.Empty;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

                Session session = new Session();
                AMService.GetSessionData(loginRequest.Token, out session);
                if (!oCallResponse.IsPassed)
                {
                    return oneSignalResponse;
                }

                oneSignalResponse.CHANNEL = oRequester.DistributerId;
                oneSignalResponse.REGIONID = oRequester.RegionID;
                oneSignalResponse.CITYID = oRequester.CityID;
                oneSignalResponse.DEALERCODE = oRequester.Code;
                oneSignalResponse.REFILLMSISDN = oRequester.RefillMSISDN;
                oneSignalResponse.CONTACTMSISDN = oRequester.ContactMSISDN;
                oneSignalResponse.GROUPID = oRequester.GroupId;

                return oneSignalResponse;
            }
            catch { return oneSignalResponse; }
        }
        public static GetAllowedIDTypesByIMSIResponse GetAllowedIDTypesByIMSI(GetAllowedIDTypesByIMSIObj request)
        {
            GetAllowedIDTypesByIMSIResponse response = new GetAllowedIDTypesByIMSIResponse();
            response.idTypes = new List<AllowedIDType>();

            try
            {
                CallResponse oCallResponse = new CallResponse();
                AdministrationServices.Requester oRequester = null;
                string userName = string.Empty;
                List<IdTypeContract> idTypelist = new List<IdTypeContract>();
                double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

                if (!oCallResponse.IsPassed)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = "Invalid username or password";
                    return response;
                }


                Contracts.BaseResponse BaseResponse = IsValidDealerEWallet(oRequester.RefillMSISDN, request.LoginRequest.language);

                if (!BaseResponse.IsPassed)
                {
                    return new GetAllowedIDTypesByIMSIResponse()
                    {
                        IsPassed = false,
                        ResponseMessage = BaseResponse.ResponseMessage
                    };
                }

                PropositionType propositionType = PropositionType.Compatibility;
                if (request.Proposition == 1)
                    propositionType = PropositionType.OldProposition;
                else if (request.Proposition == 2)
                    propositionType = PropositionType.NewProposition;

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        idTypelist = client.GetAllowedIDTypesByIMSI(request.IMSI,
                            request.LoginRequest.language == 1 ? EngineLanguage.Local : EngineLanguage.Foreign,
                            (BrandsEnum)request.Brand, propositionType,
                            IsDealerHasRight(ConfigurationManager.AppSettings["ActivateOnlyDigitalRight"],
                            oRequester), request.IsEsimActivation, request.ProductCode,
                            MapDealerRights(oRequester.Rights.ToList()));
                    }
                    foreach (IdTypeContract idType in idTypelist)
                    {
                        AllowedIDType idTypeAllowed = new AllowedIDType();
                        idTypeAllowed.Code = idType.Code;
                        idTypeAllowed.Description = idType.Name;

                        response.idTypes.Add(idTypeAllowed);
                    }
                    response.IsPassed = true;
                }
            }
            catch (LPBizException LPBizEx)
            {
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;

                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAllowedIDTypesByIMSI", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static GetSubscriptionTypesResponse GetSubscriptionTypes(LoginRequest loginRequest, string imsi, string dealerCode, string customerIdType, Brands brand, Contracts.OnboardingType onboardingType, string productCode, bool IsEsimActivation)
        {
            GetSubscriptionTypesResponse response = new GetSubscriptionTypesResponse();
            response.lstSubscriptionTypes = new List<Contracts.Subscription>();
            List<ActivationService.Subscription> activationSubscriptions = new List<ActivationService.Subscription>();

            try
            {
                CallResponse oCallResponse = new CallResponse();
                AdministrationServices.Requester oRequester = null;
                string userName = string.Empty;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

                if (!oCallResponse.IsPassed)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = "Invalid username or password";
                    return response;
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        activationSubscriptions = client.GetSubscriptionTypes(imsi, dealerCode, customerIdType,
                            (EngineLanguage)loginRequest.language, (BrandsEnum)brand,
                            (ActivationService.OnboardingType)onboardingType,
                            GetDealerPrivilegeType(oRequester, brand),
                            IsDealerHasRight(ConfigurationManager.AppSettings["ActivateOnlyDigitalRight"], oRequester),
                            productCode, IsEsimActivation, MapDealerRights(oRequester.Rights.ToList()));
                    }

                    Contracts.Subscription subscription = new Contracts.Subscription();
                    subscription.AddOns = new Contracts.AddOns();
                    subscription.Fields = new Contracts.Fields();
                    response.lstSubscriptionTypes = activationSubscriptions.ConvertAll(x => new Contracts.Subscription
                    {
                        AddOns = MapAddOnObject(x.AddOns),
                        Fields = MapFieldObject(x.Fields),
                        SubscriptionName = x.SubscriptionName,
                        SubscriptionTypeId = x.SubscriptionTypeId,
                        IsDataType = x.IsDataType,
                        IsForceRecharge = x.IsForceRecharge,
                        LoadedSIMDetails = new Contracts.LoadedSIMDetails()
                        {
                            IsLoadedSIM = x.LoadedSIMDetails.IsLoadedSIM,
                            InitialBalance = x.LoadedSIMDetails.InitialBalance
                        },
                        IsPrepaid = x.IsPrepaid
                    });
                    response.IsPassed = true;
                }
            }
            catch (LPBizException LPBizEx)
            {
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, loginRequest.language);
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;

                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, loginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSubscriptionTypes", ex, loginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static ActivationTypePrivilege GetDealerPrivilegeType(AdministrationServices.Requester oRequester, Brands brand)
        {
            ActivationTypePrivilege privilege = ActivationTypePrivilege.NotSet;
            bool hasFRiENDiNormalPrivilege = false;
            bool hasFRiENDiPremiumPrivilege = false;
            bool hasVirginNormalPrivilege = false;
            bool hasVirginPremiumPrivilege = false;

            hasVirginNormalPrivilege = oRequester.Rights.Any(x => x.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginPrepaidActivation"] || x.Code == ConfigurationManager.AppSettings["OpVirginPostpaidBasicActivation"]);
            hasFRiENDiNormalPrivilege = brand == Brands.FRiENDi;
            hasFRiENDiPremiumPrivilege = oRequester.Rights.Any(x => x.Code == ConfigurationManager.AppSettings["OpFRiENDiPostpaidActivation"]);
            hasVirginPremiumPrivilege = oRequester.Rights.Any(x => x.Code == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.RightCode.OpVirginPostpaidActivation"]);

            if (brand == Brands.FRiENDi)
            {
                if (hasFRiENDiNormalPrivilege && hasFRiENDiPremiumPrivilege)
                {
                    privilege = ActivationTypePrivilege.NotRestricted;
                }
                else if (hasFRiENDiNormalPrivilege)
                {
                    privilege = ActivationTypePrivilege.FRiENDiNormal;
                }
                else if (hasFRiENDiPremiumPrivilege)
                {
                    privilege = ActivationTypePrivilege.FRiENDiPremium;
                }
                else
                {
                    throw new LPBizException("182", $"No Privilege Guaranteed, DealerCode {oRequester.Code} Brand {brand.ToString()}");
                }
            }
            else
            {
                if (hasVirginNormalPrivilege && hasVirginPremiumPrivilege)
                {
                    privilege = ActivationTypePrivilege.NotRestricted;
                }
                else if (hasVirginNormalPrivilege)
                {
                    privilege = ActivationTypePrivilege.VirginNormal;
                }
                else if (hasVirginPremiumPrivilege)
                {
                    privilege = ActivationTypePrivilege.VirginPremium;
                }
                else
                {
                    throw new LPBizException("182", $"No Privilege Guaranteed, DealerCode {oRequester.Code} Brand {brand.ToString()}");
                }
            }

            return privilege;
        }

        public static bool IsDealerHasRight(string code, AdministrationServices.Requester oRequester)
        {
            // use it for dealer privaleage
            return oRequester.Rights.Any(x => x.Code == code);
        }

        private static Contracts.AddOns MapAddOnObject(ActivationService.AddOns sub)
        {
            try
            {
                Contracts.None none = new Contracts.None();
                none.Description = sub.None.Description;
                none.IsAllowed = sub.None.IsAllowed;

                Contracts.Topup topup = new Contracts.Topup();
                topup.Description = sub.Topup.Description;
                topup.IsAllowed = sub.Topup.IsAllowed;

                Contracts.Voucher voucher = new Contracts.Voucher();
                voucher.Description = sub.Voucher.Description;
                voucher.IsAllowed = sub.Voucher.IsAllowed;

                Contracts.ProductAddOns product = new Contracts.ProductAddOns();
                product.Description = sub.Product.Description;
                product.IsAllowed = sub.Product.IsAllowed;

                return new Contracts.AddOns()
                {
                    Product = product,
                    Voucher = voucher,
                    Topup = topup,
                    None = none
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Contracts.Fields MapFieldObject(ActivationService.Fields field)
        {
            try
            {
                Contracts.Email email = new Contracts.Email();
                email.IsAllowed = field.Email.IsVisible;
                email.IsMandatory = field.Email.IsMandatory;

                Contracts.Promotion promotion = new Contracts.Promotion();
                promotion.IsAllowed = field.Promotion.IsVisible;
                promotion.IsMandatory = field.Promotion.IsMandatory;

                return new Contracts.Fields()
                {
                    Promotion = promotion,
                    Email = email
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static AvailableDealerBalanceResponse GetAvailableDealerBalance(LoginRequest loginRequest)
        {
            AvailableDealerBalanceResponse objDealerBalanceResponse = new AvailableDealerBalanceResponse();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return objDealerBalanceResponse;
                }
                else
                {
                    int result = -1;
                    double balance = NobillWrapper.GetAccountBalance(oRequester.RefillMSISDN, out result);
                    objDealerBalanceResponse.Balance = Convert.ToDouble(string.Format("{0:0.00}", balance));
                    objDealerBalanceResponse.RefillMSISDN = oRequester.RefillMSISDN;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objDealerBalanceResponse;
        }

        public static GetStocksResponse GetAllStocks(LoginRequest loginRequest, Brands brand)
        {
            GetStocksResponse getStocksResponse = new GetStocksResponse();
            getStocksResponse.Stocks = new List<Contracts.Stock>();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                List<Stock> stocks = new List<Stock>();
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    getStocksResponse.IsSuccessful = false;
                    getStocksResponse.Message = "Not Passed";
                    return getStocksResponse;
                }
                else
                {
                    using (Entities entities = new Entities())
                    {
                        if (brand == Brands.FRiENDi)
                        {
                            stocks = entities.Stocks.Where(a => a.IsActive == true && a.BrandId == (int)AppearanceBasedOnBrand.FRiENDi).ToList();
                        }
                        else
                        {
                            stocks = entities.Stocks.Where(a => a.IsActive == true && a.BrandId == (int)AppearanceBasedOnBrand.Virgin).ToList();
                        }
                        foreach (var item in stocks)
                        {
                            LP.OMS.Channels.Contracts.Stock stock = new LP.OMS.Channels.Contracts.Stock()
                            {
                                BrandId = item.BrandId,
                                CategoryId = item.CategoryId,
                                Description = loginRequest.language == 2 ? item.DescriptionAR : item.Description,
                                Id = item.Id,
                                IsActive = item.IsActive,
                                MaxQuantity = item.MaxQuantity.HasValue ? item.MaxQuantity.Value : -1,
                                MinQuantity = item.MinQuantity.HasValue ? item.MinQuantity.Value : -1,
                                Price = item.Price.HasValue ? item.Price.Value : 0,
                                CommonQuantity = ComputeCommonQuantity(item.CommonOrderQuantity)
                            };
                            getStocksResponse.Stocks.Add(stock);
                        }
                        getStocksResponse.IsSuccessful = true;
                        getStocksResponse.Message = "Successful";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return getStocksResponse;
        }

        public static SendRequestStockResponse SendRequestStock(LoginRequest loginRequest, List<StockInfo> stocksInfo)
        {
            SendRequestStockResponse response = new SendRequestStockResponse();

            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.IsSuccessful = false;
                    response.Message = "Not Passed";
                    return response;
                }
                else
                {
                    using (Entities entities = new Entities())
                    {
                        List<int> stockIDs = new List<int>();
                        StringBuilder emailBody = new StringBuilder();
                        bool isSuccess = false;
                        List<Stock> stocks = new List<Stock>();
                        stockIDs = stocksInfo.Select(s => s.StockId).ToList();
                        stocks = entities.Stocks
                            .Where(x => stockIDs.Contains(x.Id)).ToList();

                        if (stocksInfo.Any())
                        {
                            foreach (StockInfo item in stocksInfo)
                            {
                                var stock = stocks.FirstOrDefault(s => s.Id == item.StockId);
                                string stockBrand = stock.BrandLookup.Brand;
                                if (stock != null)
                                {
                                    if (stock.MaxQuantity.HasValue && stock.MinQuantity.HasValue)
                                    {
                                        if (item.StockQuantity >= stock.MinQuantity
                                            && item.StockQuantity <= stock.MaxQuantity)
                                        {
                                            string EmailMessage = string.Format(ConfigurationManager.AppSettings["StockEmailMessage"], loginRequest.Username, stock.Description, stockBrand, item.StockQuantity.ToString());
                                            emailBody.AppendLine(EmailMessage);
                                            emailBody.AppendLine("<br/>");
                                            StockLog log = new StockLog();
                                            log.DealerCode = loginRequest.Username;
                                            log.StockId = item.StockId;
                                            log.Quantity = item.StockQuantity;
                                            log.OrderDate = DateTime.Now;
                                            entities.StockLogs.Add(log);
                                            isSuccess = true;
                                        }
                                    }
                                    else
                                    {
                                        string EmailMessage = string.Format(ConfigurationManager.AppSettings["StockEmailMessage"], loginRequest.Username, stock.Description, stockBrand, item.StockQuantity.ToString());
                                        emailBody.AppendLine(EmailMessage);
                                        emailBody.AppendLine("<br/>");
                                        StockLog log = new StockLog();
                                        log.DealerCode = loginRequest.Username;
                                        log.StockId = item.StockId;
                                        log.Quantity = item.StockQuantity;
                                        log.OrderDate = DateTime.Now;
                                        entities.StockLogs.Add(log);
                                        isSuccess = true;
                                    }
                                }
                            }

                            if (isSuccess)
                            {
                                List<string> emailRecepients = ConfigurationManager.AppSettings["emailRecepients"].ToString().Split(',').ToList();
                                string EmailSender = ConfigurationManager.AppSettings["EmailSender"];
                                string EmailSubject = ConfigurationManager.AppSettings["StockEmailSubject"] ?? "Stock Request";
                                string EmailSenderDisplayName = ConfigurationManager.AppSettings["StockEmailSenderDisplayName"] ?? "Dealer App";
                                NotificationHelper.SendEmail(loginRequest, emailRecepients, emailBody.ToString(), EmailSender, EmailSubject, EmailSenderDisplayName);
                                entities.SaveChanges();

                                response.IsSuccessful = true;
                                response.Message = "Success.";
                            }
                        }
                        else
                        {
                            response.IsSuccessful = false;
                            response.Message = "Enter stocks to request for.";
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public static int GetTotalActivation(LoginRequest loginRequest)
        {
            int totalActivation = 0;
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return totalActivation;
                }
                else
                {
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        List<ActivationContract> activations = client.GetActivations(loginRequest.Username, DateTime.Today, DateTime.Now);
                        totalActivation = activations.Count;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalActivation;
        }

        public static double GetTotalCommission(LoginRequest loginRequest)
        {
            double totalCommission = 0;
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return totalCommission;
                }
                else
                {
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            List<CommissionReport> commissionReports = client.GetCommissionReport(DateTime.Today, DateTime.Now);
                            foreach (var item in commissionReports)
                            {
                                totalCommission += item.TotalCommissionPaid;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalCommission;
        }

        public static List<SliderImageResponse> GetSliderImages(LoginRequest loginRequest, Brands brand)
        {
            //ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetSliderImages_Controller", "GetSliderImages_Controller",
            //   loginRequest.Username, JsonConvert.SerializeObject(loginRequest) + "Brand: " + brand);

            List<SliderImageResponse> sliderImageResponse = new List<SliderImageResponse>();
            List<AppSliderImage> images = new List<AppSliderImage>();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

                // ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetSliderImagesController", "GetSliderImagesController",
                //loginRequest.Username, JsonConvert.SerializeObject(oCallResponse));

                if (!oCallResponse.IsPassed)
                {
                    return sliderImageResponse;
                }
                else
                {
                    using (Entities context = new Entities())
                    {
                        if (brand == Brands.FRiENDi)
                        {
                            if (context.AppSliderImages != null)
                                images = context.AppSliderImages.Where(i => i.Brand == (int)AppearanceBasedOnBrand.FRiENDi || i.Brand == (int)AppearanceBasedOnBrand.Both).OrderBy(i => i.Ordinal).ToList();
                        }
                        else
                        {
                            if (context.AppSliderImages != null)
                                images = context.AppSliderImages.Where(i => i.Brand == (int)AppearanceBasedOnBrand.Virgin || i.Brand == (int)AppearanceBasedOnBrand.Both).OrderBy(i => i.Ordinal).ToList();
                        }

                        foreach (var item in images)
                        {
                            sliderImageResponse.Add(new SliderImageResponse()
                            {
                                ImageURL = item.URL,
                                Title = loginRequest.language == 2 ? item.TitleAR : item.TitleEN
                            });
                        }
                    }
                }
                // ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetSliderImagesController", "GetSliderImagesController",
                //loginRequest.Username, JsonConvert.SerializeObject(sliderImageResponse[0]) + "Brand: " + brand);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSliderImagesController", ex, loginRequest.Username, true);
                throw ex;
            }

            return sliderImageResponse;
        }

        public static List<AvailableRatePlan> GetAvailableVanityRatePlans(LoginRequest loginRequest, string idType)
        {
            List<AvailableRatePlan> plans = new List<AvailableRatePlan>();
            List<VanityRatePlan> rates = new List<VanityRatePlan>();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return plans;
                }
                else
                {
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            rates = client.GetVanityActivationRatePlan(idType);
                        }

                        plans = rates.ConvertAll(x => new Contracts.AvailableRatePlan
                        {
                            RatePlanId = x.RatePlanId,
                            RatePlanName = loginRequest.language == 1 ? x.RatePlanNameEN : x.RatePlanNameAR,
                            IsPrePaid = x.IsPrePaid
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return plans;

            #endregion
        }

        public static TimeSpan GetRemainingSessionInSeconds(LoginRequest loginRequest)
        {
            //ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetRemainingSessionInSeconds_Controller", "GetRemainingSessionInSeconds_Controller",
            //    loginRequest.Username, JsonConvert.SerializeObject(loginRequest));

            TimeSpan span = new TimeSpan();
            DateTime expires = new DateTime();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName,
                    !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);

                if (!oCallResponse.IsPassed)
                    return span;
                else
                {
                    if (ModernTradeController.IsModernTradeDealer(oRequester.UserName))
                    {
                        var attendance = ModernTradeController.GetDealerActiveCheckIn(oRequester.UserName);
                        if (attendance != null)
                        {
                            span = DateTime.Now.Subtract(attendance.PerformedAt);
                            return span;
                        }
                    }

                    expires = AccessManagementController.GetSessionExpiryDate(loginRequest.Token);
                    if (expires != null && expires != DateTime.MinValue)
                        span = expires.Subtract(DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetRemainingSessionInSeconds_Controller", ex, loginRequest.Username, true);
                throw ex;
            }

            return span;
        }

        public static List<PossibleFailureReason> GetPossibleFailureReasons(LoginRequest loginRequest, Brands brand)
        {
            List<PossibleFailureReason> possibleFailureReasons = new List<PossibleFailureReason>();
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return possibleFailureReasons;
                }
                else
                {
                    using (Entities channelsEntities = new Entities())
                    {
                        List<FailureReason> failureReasons = new List<FailureReason>();
                        if (brand == Brands.FRiENDi)
                        {
                            failureReasons = channelsEntities.FailureReasons.Where(x => x.IsActive && (x.BrandId == (int)AppearanceBasedOnBrand.FRiENDi || x.BrandId == (int)AppearanceBasedOnBrand.Both)).ToList();
                        }
                        else
                        {
                            failureReasons = channelsEntities.FailureReasons.Where(x => x.IsActive && (x.BrandId == (int)AppearanceBasedOnBrand.Virgin || x.BrandId == (int)AppearanceBasedOnBrand.Both)).ToList();
                        }

                        possibleFailureReasons = failureReasons.ConvertAll(r => new PossibleFailureReason()
                        {
                            Id = r.Id,
                            ReasonDisplayText = loginRequest.language == 1 ? r.ReasonEN : r.ReasonAR,
                            IsDealerNoteRequired = r.IsDealerNoteRequired
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return possibleFailureReasons;
        }

        public static bool SubmitDealerFailureReasonAndNote(DealerFailureReasonNote failureReason, LoginRequest loginRequest)
        {
            bool response = false;
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return response;
                }
                else
                {
                    using (Entities channelsEntities = new Entities())
                    {
                        DealerFailureReasonAndNote failureReasonNote = new DealerFailureReasonAndNote()
                        {
                            ReasonId = failureReason.ReasonId,
                            DealerNote = failureReason.DealerNote,
                            DealerCode = loginRequest.Username,
                            TimeStamp = DateTime.Now
                        };

                        channelsEntities.DealerFailureReasonAndNotes.Add(failureReasonNote);
                        channelsEntities.SaveChanges();
                    }

                    response = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public static ActivationGradeDetailsReportResponse GetActivationGradeDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationGradeDetailsReportResponse response = new ActivationGradeDetailsReportResponse()
            {
                GradeSummary = new List<ActivationGradeSummary>()
            };

            string dealerCode = string.Empty;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDealerForReports"]))
            {
                dealerCode = ConfigurationManager.AppSettings["TestDealerForReports"];
            }
            else
            {
                dealerCode = request.loginRequest.Username;
            }
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.ResponseMessage = "Wrong username or password";
                    return response;
                }
                else
                {
                    int originalRecordsBeforePaging = 0;
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        Period period = GetReportPeriod(request.reportViewMode, request.dateFrom, request.dateTo);
                        response.InquiryPeriod = period.GetInquiryPeriod();
                        SearchCriteria searchCriteria = GetActivationGeneralSearchCriteria("grade", request.gradeFilter);
                        var activationResponse = client.GetActivationDetailsReport(dealerCode, searchCriteria, (DisplayBasedOnBrand)request.brand, period.From, period.To);
                        originalRecordsBeforePaging = activationResponse.Count;
                        response.IsPassed = activationResponse.Any();
                        if (response.IsPassed)
                        {
                            response.ResponseMessage = "Success";
                            if (request.pageNumber > 0)
                            {
                                activationResponse = GetPage(activationResponse, request.pageNumber);
                            }
                            response.TotalGradeReportPages = GetReportTotalPages(originalRecordsBeforePaging);
                            if (request.pageNumber > response.TotalGradeReportPages)
                            {
                                response.IsPassed = false;
                                response.ResponseMessage = "You have exceeded the maximum page number";
                                return response;
                            }
                            List<ActivationGradeSummary> activationGrades = new List<ActivationGradeSummary>();
                            FillGrades(ref activationGrades, activationResponse);
                            response.GradeSummary = activationGrades;
                        }
                        else
                        {
                            response.ResponseMessage = "No available data";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }
        public static ActivationRechargeDetailsReportResponse GetActivationRechargeDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationRechargeDetailsReportResponse response = new ActivationRechargeDetailsReportResponse()
            {
                RechargeSummary = new List<RechargeSummary>()
            };

            string dealerCode = string.Empty;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDealerForReports"]))
            {
                dealerCode = ConfigurationManager.AppSettings["TestDealerForReports"];
            }
            else
            {
                dealerCode = request.loginRequest.Username;

            }
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.ResponseMessage = "Wrong username or password";
                    return response;
                }
                else
                {
                    int originalRecordsBeforePaging = 0;
                    using (ReportingService.ReportingServiceClient client = new ReportingService.ReportingServiceClient())
                    {
                        Period period = GetReportPeriod(request.reportViewMode, request.dateFrom, request.dateTo);
                        response.InquiryPeriod = period.GetInquiryPeriod();
                        ReportingService.GeneralSearchCriteria searchCriteria = GetGeneralSearchCriteria("recharge", request.rechargeFilter);
                        var commissionResponseDetails = client.GetCommissionsDetailsReports(dealerCode, searchCriteria, (ReportingService.Brand)request.brand, period.From, period.To);
                        originalRecordsBeforePaging = commissionResponseDetails.CommissionsDetailsDataItems.Count;
                        response.IsPassed = commissionResponseDetails.CommissionsDetailsDataItems.Any();
                        if (response.IsPassed)
                        {
                            response.ResponseMessage = "Success";
                            if (request.pageNumber > 0)
                            {
                                commissionResponseDetails.CommissionsDetailsDataItems = GetPage(commissionResponseDetails.CommissionsDetailsDataItems, request.pageNumber);
                            }
                            response.TotalRechargeReportPages = GetReportTotalPages(originalRecordsBeforePaging);
                            if (request.pageNumber > response.TotalRechargeReportPages)
                            {
                                response.IsPassed = false;
                                response.ResponseMessage = "You have exceeded the maximum page number";
                                return response;
                            }
                            List<RechargeSummary> rechargesSummary = new List<RechargeSummary>();
                            FillRecharges(ref rechargesSummary, commissionResponseDetails.CommissionsDetailsDataItems);
                            response.RechargeSummary = rechargesSummary;
                        }
                        else
                        {
                            response.ResponseMessage = "No available data";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }
        public static ActivationFPEDetailsReportResponse GetActivationFPEDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationFPEDetailsReportResponse response = new ActivationFPEDetailsReportResponse()
            {
                FPESummary = new List<FPESummary>()
            };

            string dealerCode = string.Empty;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDealerForReports"]))
            {
                dealerCode = ConfigurationManager.AppSettings["TestDealerForReports"];
            }
            else
            {
                dealerCode = request.loginRequest.Username;
            }
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.ResponseMessage = "Wrong username or password";
                    return response;
                }
                else
                {
                    int originalRecordsBeforePaging = 0;
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        Period period = GetReportPeriod(request.reportViewMode, request.dateFrom, request.dateTo);
                        response.InquiryPeriod = period.GetInquiryPeriod();
                        SearchCriteria searchCriteria = GetActivationGeneralSearchCriteria("fpe", request.fpeFilter);
                        var activationResponse = client.GetActivationDetailsReport(dealerCode, searchCriteria, (DisplayBasedOnBrand)request.brand, period.From, period.To);
                        originalRecordsBeforePaging = activationResponse.Count;
                        response.IsPassed = activationResponse.Any();
                        if (response.IsPassed)
                        {
                            response.ResponseMessage = "Success";
                            if (request.pageNumber > 0)
                            {
                                activationResponse = GetPage(activationResponse, request.pageNumber);
                            }
                            response.TotalFPEReportPages = GetReportTotalPages(originalRecordsBeforePaging);
                            if (request.pageNumber > response.TotalFPEReportPages)
                            {
                                response.IsPassed = false;
                                response.ResponseMessage = "You have exceeded the maximum page number";
                                return response;
                            }
                            List<FPESummary> fpeSummary = new List<FPESummary>();
                            FillFPEs(ref fpeSummary, activationResponse);
                            response.FPESummary = fpeSummary;
                        }
                        else
                        {
                            response.ResponseMessage = "No available data";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }
        public static CommissionPaymentDetailsReportResponse GetCommissionPaymentDetails(ActivationCommissionReportRequest request)
        {
            CommissionPaymentDetailsReportResponse response = new CommissionPaymentDetailsReportResponse()
            {
                CommissionPaymentSummary = new List<CommissionPaymentSummary>()
            };
            string dealerCode = string.Empty;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDealerForReports"]))
            {
                dealerCode = ConfigurationManager.AppSettings["TestDealerForReports"];
            }
            else
            {
                dealerCode = request.loginRequest.Username;
            }
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.ResponseMessage = "Wrong username or password";
                    return response;
                }
                else
                {
                    int originalRecordsBeforePaging = 0;
                    ReportingService.CommissionsDetailsDataItem brokerDataItem = new ReportingService.CommissionsDetailsDataItem();
                    List<ReportingService.CommissionsDetailsDataItem> lstBrokerDataItem = new List<ReportingService.CommissionsDetailsDataItem>();
                    using (ReportingService.ReportingServiceClient client = new ReportingService.ReportingServiceClient())
                    {
                        Period period = GetReportPeriod(request.reportViewMode, request.dateFrom, request.dateTo);
                        response.InquiryPeriod = period.GetInquiryPeriod();
                        string searchCriteriaKey = GetSearchCriteriaKey(request.paymentStatusFilter);
                        ReportingService.GeneralSearchCriteria searchCriteria = GetGeneralSearchCriteria(searchCriteriaKey, request.commissionElementFilter);
                        var commissionResponseDetails = client.GetCommissionsDetailsReports(dealerCode, searchCriteria, (ReportingService.Brand)request.brand, period.From, period.To);
                        response.IsPassed = commissionResponseDetails.CommissionsDetailsDataItems.Any();
                        if (response.IsPassed)
                        {
                            response.ResponseMessage = "Success";
                            foreach (var commissionDataItem in commissionResponseDetails.CommissionsDetailsDataItems)
                            {
                                foreach (var commissionElement in commissionDataItem.elementCommissions)
                                {
                                    brokerDataItem = new ReportingService.CommissionsDetailsDataItem()
                                    {
                                        IMSI = commissionDataItem.IMSI,
                                        Paid = commissionDataItem.Paid,
                                        ActivationDate = commissionDataItem.ActivationDate,
                                        elementCommissions = new List<ReportingService.ElementCommission>() { commissionElement }
                                    };

                                    lstBrokerDataItem.Add(brokerDataItem);
                                }
                            }
                            lstBrokerDataItem = lstBrokerDataItem.OrderByDescending(o => o.ActivationDate).ToList();
                            lstBrokerDataItem = FilterCommissionDetailsDataItems(lstBrokerDataItem, request.commissionElementFilter, request.brand);
                            originalRecordsBeforePaging = lstBrokerDataItem.Count;
                            if (request.pageNumber > 0)
                            {
                                lstBrokerDataItem = GetPage(lstBrokerDataItem, request.pageNumber);
                            }
                            response.TotalReportPages = GetReportTotalPages(originalRecordsBeforePaging);
                            if (request.pageNumber > response.TotalReportPages)
                            {
                                response.IsPassed = false;
                                response.ResponseMessage = "You have exceeded the maximum page number";
                                return response;
                            }
                            List<CommissionPaymentSummary> commissionPaymentSummary = new List<CommissionPaymentSummary>();
                            FillCommissionPayments(ref commissionPaymentSummary, lstBrokerDataItem, request.paymentStatusFilter);
                            commissionPaymentSummary = commissionPaymentSummary.OrderBy(o => o.CommissionElement).ToList();
                            response.CommissionPaymentSummary = commissionPaymentSummary;
                        }
                        else
                        {
                            response.ResponseMessage = "No available data";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }
        public static ActivationReportSummary GetActivationReportSummary(ActivationCommissionReportRequest request)
        {
            ActivationReportSummary response = new ActivationReportSummary()
            {
                GradeChartSummary = new ChartSummary() { ChartItems = new List<ChartItem>() },
                RechargeChartSummary = new ChartSummary() { ChartItems = new List<ChartItem>() },
                FPEChartSummary = new ChartSummary() { ChartItems = new List<ChartItem>() }
            };

            string dealerCode = string.Empty;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDealerForReports"]))
            {
                dealerCode = ConfigurationManager.AppSettings["TestDealerForReports"];
            }
            else
            {
                dealerCode = request.loginRequest.Username;
            }
            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.ResponseMessage = "Wrong username or password";
                    return response;
                }
                else
                {
                    using (ReportingService.ReportingServiceClient client = new ReportingService.ReportingServiceClient())
                    {
                        using (ActivationServiceClient activationClient = new ActivationServiceClient())
                        {
                            string noneGradeDisplayName = ConfigurationManager.AppSettings["NoneGradeDisplayName"];
                            int totalGrades = 0;
                            int totalRecharges = 0;
                            Period period = GetReportPeriod(request.reportViewMode, request.dateFrom, request.dateTo);
                            response.InquiryPeriod = period.GetInquiryPeriod();
                            var gradeSummary = activationClient.GetActivationGradeSummaryReport(dealerCode, (DisplayBasedOnBrand)request.brand, period.From, period.To);
                            var rechargeSummary = client.GetRechargeSummaryReport(dealerCode, (ReportingService.Brand)request.brand, period.From, period.To);
                            var fpeSummary = activationClient.GetActivationFPESummaryReport(dealerCode, (DisplayBasedOnBrand)request.brand, period.From, period.To);
                            totalGrades = gradeSummary.TotalActivations;
                            totalRecharges = rechargeSummary.TotalActivations - rechargeSummary.TotalNoRecharges;
                            response.IsPassed = (totalGrades > 0 || totalRecharges > 0 || fpeSummary.TotalActivations > 0) ? true : false;
                            if (response.IsPassed)
                            {
                                response.ResponseMessage = "Success";
                                List<ChartItem> gradeChartItems = new List<ChartItem>();
                                AddToChartItems(ref gradeChartItems, "A", gradeSummary.TotalGradeA, totalGrades);
                                AddToChartItems(ref gradeChartItems, "B", gradeSummary.TotalGradeB, totalGrades);
                                AddToChartItems(ref gradeChartItems, "C", gradeSummary.TotalGradeC, totalGrades);
                                AddToChartItems(ref gradeChartItems, "D", gradeSummary.TotalGradeD, totalGrades);
                                AddToChartItems(ref gradeChartItems, noneGradeDisplayName, gradeSummary.TotalNoneGrade, totalGrades);
                                response.GradeChartSummary.ChartItems = gradeChartItems;
                                response.GradeChartSummary.Total = totalGrades.ToString();
                                response.TotalGradeReportPages = GetReportTotalPages(totalGrades);

                                List<ChartItem> rechargeChartItems = new List<ChartItem>();
                                AddToChartItems(ref rechargeChartItems, "1st Recharge", rechargeSummary.TotalFirstRecharges, totalRecharges);
                                AddToChartItems(ref rechargeChartItems, "2nd Recharge", rechargeSummary.TotalSecondRecharges, totalRecharges);
                                AddToChartItems(ref rechargeChartItems, "3rd Recharge", rechargeSummary.TotalThirdRecharges, totalRecharges);
                                response.RechargeChartSummary.ChartItems = rechargeChartItems;
                                response.RechargeChartSummary.Total = totalRecharges.ToString();
                                response.TotalRechargeReportPages = GetReportTotalPages(totalRecharges);

                                List<ChartItem> fpeChartItems = new List<ChartItem>();
                                AddToChartItems(ref fpeChartItems, "FPE", fpeSummary.TotalFirstPayableEvents, fpeSummary.TotalActivations);
                                AddToChartItems(ref fpeChartItems, "Not FPE", (fpeSummary.TotalActivations - fpeSummary.TotalFirstPayableEvents), fpeSummary.TotalActivations);
                                response.FPEChartSummary.ChartItems = fpeChartItems;
                                response.FPEChartSummary.Total = fpeSummary.TotalActivations.ToString();
                                response.TotalFPEReportPages = GetReportTotalPages(fpeSummary.TotalActivations);
                            }
                            else
                            {
                                response.ResponseMessage = "No available data";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }
        public static CommissionReportSummary GetCommissionReportSummary(ActivationCommissionReportRequest request)
        {
            CommissionReportSummary response = new CommissionReportSummary()
            {
                CommissionChartSummary = new ChartSummary(),
                PaidChartSummary = new ChartSummary(),
                PendingChartSummary = new ChartSummary()
            };

            string dealerCode = string.Empty;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TestDealerForReports"]))
            {
                dealerCode = ConfigurationManager.AppSettings["TestDealerForReports"];
            }
            else
            {
                dealerCode = request.loginRequest.Username;
            }

            try
            {
                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.loginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.loginRequest, out oRequester, out userName, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    response.ResponseMessage = "Wrong username or password";
                    return response;
                }
                else
                {
                    using (ReportingService.ReportingServiceClient client = new ReportingService.ReportingServiceClient())
                    {
                        Period period = GetReportPeriod(request.reportViewMode, request.dateFrom, request.dateTo);
                        response.InquiryPeriod = period.GetInquiryPeriod();
                        var commissionResponseSummary = client.GetCommissionPaymentSummaryReport(dealerCode, (ReportingService.Brand)request.brand, period.From, period.To);
                        response.IsPassed = commissionResponseSummary.TotalActivations > 0 ? true : false;
                        if (response.IsPassed)
                        {
                            response.ResponseMessage = "Success";
                            List<ChartItem> commissionChartItems = new List<ChartItem>();
                            List<ChartItem> paidChartItems = new List<ChartItem>();
                            List<ChartItem> pendingChartItems = new List<ChartItem>();
                            AddToChartItems(ref commissionChartItems, "Paid", commissionResponseSummary.TotalPaid, commissionResponseSummary.TotalActivations);
                            AddToChartItems(ref commissionChartItems, "Pending", commissionResponseSummary.TotalPending, commissionResponseSummary.TotalActivations);
                            response.CommissionChartSummary.ChartItems = commissionChartItems;
                            response.CommissionChartSummary.Total = Convert.ToString(commissionResponseSummary.TotalActivations + " SR");

                            AddToChartItems(ref paidChartItems, commissionResponseSummary.PaidElementCommissions, commissionResponseSummary.TotalPaid);
                            AddToChartItems(ref pendingChartItems, commissionResponseSummary.PendingElementCommissions, commissionResponseSummary.TotalPending);
                            response.PaidChartSummary.ChartItems = paidChartItems.OrderBy(o => o.Key).ToList();
                            response.PaidChartSummary.Total = Convert.ToString(commissionResponseSummary.TotalPaid + " SR");
                            response.PendingChartSummary.ChartItems = pendingChartItems.OrderBy(o => o.Key).ToList();
                            response.PendingChartSummary.Total = Convert.ToString(commissionResponseSummary.TotalPending + " SR");

                            var commissionResponseDetails = client.GetCommissionsDetailsReports(dealerCode, ReportingService.GeneralSearchCriteria.AllPayments, (ReportingService.Brand)request.brand, period.From, period.To);
                            response.TotalReportPages = GetReportTotalPages(commissionResponseDetails.CommissionsDetailsDataItems);
                        }
                        else
                        {
                            response.ResponseMessage = "No available data";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public static List<OperatorDetails> GetTelecomOperators(LoginRequest request)
        {
            List<OperatorDetails> response = new List<OperatorDetails>();
            List<TelecomProvider> providers = new List<TelecomProvider>();
            try
            {
                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, request.Username, request.Username, DSTChannelValue))
                    {
                        providers = client.GetServiceProviders();
                    }
                }

                providers.ForEach(x =>
                {
                    response.Add(new OperatorDetails()
                    {
                        ID = x.ID,
                        NameAR = x.NameAr,
                        NameEN = x.NameEn
                    });
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public static CancelMNPPortInResponse CancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            CancelMNPPortInResponse response = new CancelMNPPortInResponse();
            CancelPortInResponse activationResponse = new CancelPortInResponse();
            const string CustomError = "-1111111";

            try
            {
                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, request.LoginRequest.Username, request.LoginRequest.Username, DSTChannelValue))
                    {
                        CancelPortInRequest activationRequest = new CancelPortInRequest()
                        {
                            IDNumber = request.IDNumber,
                            MSISDN = request.MSISDN,
                            Brand = (BrandsEnum)request.Brand
                        };
                        if (request.CancelPortInDocument != null && !string.IsNullOrEmpty(request.CancelPortInDocument.DocumentContent))
                        {
                            activationRequest.CancelPortInDocument = new ActivationService.Document()
                            {
                                FileContent = Convert.FromBase64String(request.CancelPortInDocument.DocumentContent),
                                Type = (ActivationService.DocumentType)request.CancelPortInDocument.DocumentTypeID,
                                Name = request.CancelPortInDocument.RequestDocumentStagingGuid
                            };
                        }
                        activationResponse = client.CancelPortInRequest(activationRequest);
                        response.IsPassed = activationResponse.IsPassed;
                        response.ResponseMessage = activationResponse.ResponseMessage;
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                ErrorHandling.HandleException("CancelMNPPortInRequest", LPBizEx, request.LoginRequest.Username);
                response.IsPassed = false;
                if (LPBizEx.Code == CustomError)
                {
                    response.ResponseMessage = LPBizEx.Message;
                }
                else
                {
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (FaultException ex)
            {
                LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                ErrorHandling.HandleException("CancelMNPPortInRequest", LPBizEx, request.LoginRequest.Username);
                response.IsPassed = false;

                if (ex.Code.Name == CustomError)
                {
                    response.ResponseMessage = LPBizEx.Message;
                }
                else
                {
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelMNPPortInRequest", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static CancelMNPPortInResponse ValidateCancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            CancelMNPPortInResponse response = new CancelMNPPortInResponse();
            CancelPortInResponse activationResponse = new CancelPortInResponse();
            const string CustomError = "-1111111";

            try
            {
                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, request.LoginRequest.Username, request.LoginRequest.Username, DSTChannelValue))
                    {
                        CancelPortInRequest activationRequest = new CancelPortInRequest()
                        {
                            IDNumber = request.IDNumber,
                            MSISDN = request.MSISDN,
                            Brand = (BrandsEnum)request.Brand
                        };
                        activationResponse = client.ValidateCancelPortInRequest(activationRequest);
                        response.IsPassed = activationResponse.IsPassed;
                        response.ResponseMessage = activationResponse.ResponseMessage;
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                ErrorHandling.HandleException("ValidateCancelMNPPortInRequest", LPBizEx, request.LoginRequest.Username);
                response.IsPassed = false;
                if (LPBizEx.Code == CustomError)
                {
                    response.ResponseMessage = LPBizEx.Message;
                }
                else
                {
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (FaultException ex)
            {
                LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                ErrorHandling.HandleException("ValidateCancelMNPPortInRequest", LPBizEx, request.LoginRequest.Username);
                response.IsPassed = false;

                if (ex.Code.Name == CustomError)
                {
                    response.ResponseMessage = LPBizEx.Message;
                }
                else
                {
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateCancelMNPPortInRequest", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        private static List<int> ComputeCommonQuantity(string order)
        {
            List<int> commonQuantity = new List<int>();
            if (string.IsNullOrEmpty(order))
            {
                string common = ConfigurationManager.AppSettings["CommonStockQuantity"];
                commonQuantity = common.Split(',').Select(int.Parse).ToList();
            }
            else
            {
                commonQuantity = order.Split(',').Select(int.Parse).ToList();
            }

            return commonQuantity;
        }
        public static double RoundUp(double input, int places)
        {
            double multiplier = Math.Pow(10, Convert.ToDouble(places));
            return Math.Ceiling(input * multiplier) / multiplier;
        }

        public static bool CheckActivationStatus(string CRMAccountNo)
        {
            bool isActivated = false;
            try
            {
                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    isActivated = client.CheckActivationStatus(CRMAccountNo);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "CheckActivationStatus", "", "");
            }

            return isActivated;
        }

        public static string GetResourceMessage(string key, int lang)
        {
            if (lang == 2)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }
            return objResourceManager.GetString(key, Thread.CurrentThread.CurrentCulture);
        }

        public static string GetBusinessMessage(string code, int lang)
        {
            if (lang == 2)
            {
                return Resources.BLMessages_ar_JO.ResourceManager.GetString(code);
            }
            else
            {
                return Resources.BLMessages.ResourceManager.GetString(code);
            }
        }

        public static Contracts.OnboardingType GetOnboardingType(string productCode, bool isFamilyOnboarding)
        {
            Contracts.OnboardingType onboardingType = Contracts.OnboardingType.NotSet;
            const string CustomError = "-1111111";

            if (string.IsNullOrEmpty(productCode))
            {
                throw new LPBizException(CustomError, "Product code isn't provided");
            }
            if (productCode == "1" && !isFamilyOnboarding)
            {
                onboardingType = Contracts.OnboardingType.Digital;
            }
            else if (productCode == "1" && isFamilyOnboarding)
            {
                onboardingType = Contracts.OnboardingType.FamilySIMOnly;
            }
            else if (productCode == "2")
            {
                onboardingType = Contracts.OnboardingType.FamilySIMOnly;
            }
            else if (productCode == "3")
            {
                onboardingType = Contracts.OnboardingType.FamilySIMWithDevice;
            }
            else
            {
                onboardingType = Contracts.OnboardingType.Digital;
            }

            return onboardingType;
        }


        public static Period GetReportPeriod(ReportViewMode reportViewMode, string customDateFrom, string customDateTo)
        {
            Period period = new Period();
            DateTime baseDate = DateTime.Today;
            var today = baseDate;
            var yesterday = baseDate.AddDays(-1);
            var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
            var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1);
            var lastWeekStart = thisWeekStart.AddDays(-7);
            var lastWeekEnd = thisWeekStart.AddSeconds(-1);
            var thisMonthStart = baseDate.AddDays(1 - baseDate.Day);
            var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
            var lastMonthStart = thisMonthStart.AddMonths(-1);
            var lastMonthEnd = thisMonthStart.AddSeconds(-1);

            switch (reportViewMode)
            {
                case ReportViewMode.Daily:
                    {
                        period.From = baseDate;
                        period.To = period.From.AddDays(1).AddMilliseconds(-1);
                        break;
                    }
                case ReportViewMode.Weekly:
                    {
                        period.From = thisWeekStart;
                        period.To = thisWeekEnd;
                        break;
                    }
                case ReportViewMode.Monthly:
                    {
                        period.From = thisMonthStart;
                        period.To = thisMonthEnd;
                        break;
                    }
                case ReportViewMode.LanchToDate:
                    {
                        period.From = MinimDateTime;
                        period.To = DateTime.Now;
                        break;
                    }
                case ReportViewMode.CustomRequest:
                    {
                        string customDateFormat = ConfigurationManager.AppSettings["ActivationAndCommissionReportsCustomDateTimeFormat"];
                        DateTime customDateTimeFrom = new DateTime();
                        DateTime customDateTimeTo = new DateTime();
                        DateTime.TryParseExact(customDateFrom, customDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out customDateTimeFrom);
                        DateTime.TryParseExact(customDateTo, customDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out customDateTimeTo);
                        if (customDateTimeFrom > customDateTimeTo)
                            throw new Exception(string.Format("Date from {0} can not be larger than date to {1}", customDateTimeFrom.ToString(), customDateTimeTo.ToString()));
                        period.From = customDateTimeFrom;
                        period.To = customDateTimeTo;
                        break;
                    }
                default:
                    {
                        throw new Exception(string.Format("Wrong value for report view mode {0}", reportViewMode.ToString()));
                    }
            }

            return period;
        }
        private static void AddToChartItems(ref List<ChartItem> chartItems, string key, int value, int denominator)
        {
            if (!string.IsNullOrEmpty(key) && value != 0 && denominator != 0)
                chartItems.Add(new ChartItem()
                {
                    Key = key,
                    Value = value.ToString(),
                    Percentage = string.Format("{0:0.00}", (double)value / denominator * 100)
                });
        }
        private static void AddToChartItems(ref List<ChartItem> chartItems, string key, double value, double denominator)
        {
            if (!string.IsNullOrEmpty(key) && value != 0 && denominator != 0)
                chartItems.Add(new ChartItem()
                {
                    Key = key,
                    Value = value.ToString() + " SR",
                    Percentage = string.Format("{0:0.00}", value / denominator * 100)
                });
        }
        private static string GetRecharge(List<ReportingService.ElementCommission> elements)
        {
            if (elements.Any(x => x.Name.ToLower().Replace(" ", "") == "thirdrecharge"))
            {
                return "3rd Recharge";
            }
            if (elements.Any(x => x.Name.ToLower().Replace(" ", "") == "secondrecharge"))
            {
                return "2nd Recharge";
            }
            if (elements.Any(x => x.Name.ToLower().Replace(" ", "") == "firstrecharge"))
            {
                return "1st Recharge";
            }

            return string.Empty;
        }
        private static string GetFPE(ActivationDetailsReport item)
        {
            if (item.FPEDate.HasValue)
                return "FPE";
            else
                return "Not FPE";
        }
        private static string GetCommissionElementName(string element)
        {
            if (element.ToLower().Replace(" ", "") == "thirdmonthbill")
                return "3rd Mon Bill";
            else if (element.ToLower().Replace(" ", "") == "secondmonthbill")
                return "2nd Mon Bill";
            else if (element.ToLower().Replace(" ", "") == "firstmonthbill")
                return "1st Mon Bill";
            else if (element.ToLower().Replace(" ", "") == "thirdrecharge")
                return "3rd Recharge";
            else if (element.ToLower().Replace(" ", "") == "secondrecharge")
                return "2nd Recharge";
            else if (element.ToLower().Replace(" ", "") == "firstrecharge")
                return "1st Recharge";
            else if (element.ToLower().Replace(" ", "") == "firstpayableevent")
                return "FPE";
            else if (element.ToLower().Replace(" ", "") == "contractcollection")
                return "Contract Collection";
            else if (element.ToLower().Replace(" ", "") == "simactivation")
                return "SIM Activation";
            else if (element.ToLower().Replace(" ", "") == "cashback")
                return "Cash Back";
            else if (element.ToLower().Replace(" ", "").Contains("virgin"))
                return "Virgin-Product Selling";
            else if (element.ToLower().Replace(" ", "").Contains("friendi"))
                return "FRiENDi-Product Selling";
            else
                return "none";
        }
        private static void AddToChartItems(ref List<ChartItem> commissionChartItems,
            List<ReportingService.ElementCommission> elementCommissions, double total)
        {
            foreach (var element in elementCommissions)
            {
                if (element.Value > 0)
                {
                    string key = GetCommissionElementName(element.Name);
                    if (key != "none")
                    {
                        ChartItem commissionChartItem = new ChartItem()
                        {
                            Key = key,
                            Percentage = string.Format("{0:0.00}", element.Value / total * 100),
                            Value = element.Value + " SR"
                        };
                        commissionChartItems.Add(commissionChartItem);
                    }
                }
            }
        }
        private static void FillGrades(ref List<ActivationGradeSummary> gradeSummary, List<ActivationDetailsReport> activationDetailsReport)
        {
            string noneGradeDisplayName = ConfigurationManager.AppSettings["NoneGradeDisplayName"];
            string grade = string.Empty;
            List<ActivationGradeDetails> gradeA_Details = new List<ActivationGradeDetails>();
            List<ActivationGradeDetails> gradeB_Details = new List<ActivationGradeDetails>();
            List<ActivationGradeDetails> gradeC_Details = new List<ActivationGradeDetails>();
            List<ActivationGradeDetails> gradeD_Details = new List<ActivationGradeDetails>();
            List<ActivationGradeDetails> gradeNone_Details = new List<ActivationGradeDetails>();

            foreach (var item in activationDetailsReport)
            {
                if (!string.IsNullOrEmpty(item.Grade))
                    grade = item.Grade.ToUpper();
                else
                    grade = string.Empty;

                if (grade == "A")
                {
                    gradeA_Details.Add(new ActivationGradeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        Grade = "A"
                    });
                }
                else if (grade == "B")
                {
                    gradeB_Details.Add(new ActivationGradeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        Grade = "B"
                    });
                }
                else if (grade == "C")
                {
                    gradeC_Details.Add(new ActivationGradeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        Grade = "C"
                    });
                }
                else if (grade == "D")
                {
                    gradeD_Details.Add(new ActivationGradeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        Grade = "D"
                    });
                }
                else
                {
                    gradeNone_Details.Add(new ActivationGradeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        Grade = noneGradeDisplayName
                    });
                }
            }

            if (gradeA_Details.Any())
            {
                gradeSummary.Add(new ActivationGradeSummary()
                {
                    Grade = "A",
                    GradeDetails = new List<ActivationGradeDetails>(gradeA_Details) { }
                });
            }
            if (gradeB_Details.Any())
            {
                gradeSummary.Add(new ActivationGradeSummary()
                {
                    Grade = "B",
                    GradeDetails = new List<ActivationGradeDetails>(gradeB_Details) { }
                });
            }
            if (gradeC_Details.Any())
            {
                gradeSummary.Add(new ActivationGradeSummary()
                {
                    Grade = "C",
                    GradeDetails = new List<ActivationGradeDetails>(gradeC_Details) { }
                });
            }
            if (gradeD_Details.Any())
            {
                gradeSummary.Add(new ActivationGradeSummary()
                {
                    Grade = "D",
                    GradeDetails = new List<ActivationGradeDetails>(gradeD_Details) { }
                });
            }
            if (gradeNone_Details.Any())
            {
                gradeSummary.Add(new ActivationGradeSummary()
                {
                    Grade = noneGradeDisplayName,
                    GradeDetails = new List<ActivationGradeDetails>(gradeNone_Details) { }
                });
            }
        }
        private static int GetReportTotalPages(int count)
        {
            int totalPages = 0;
            int pageSize = 10;
            int.TryParse(ConfigurationManager.AppSettings["ActivationCommissionReportsPageSize"], out pageSize);

            if (count > 0 && pageSize > 0)
            {
                int remainder;
                int quotient = Math.DivRem(count, pageSize, out remainder);
                totalPages = remainder == 0 ? quotient : quotient + 1;
            }

            return totalPages;
        }
        private static int GetReportTotalPages(List<ReportingService.CommissionsDetailsDataItem> commissionDetails)
        {
            int count = 0;
            int totalPages = 0;
            int pageSize = 10;
            int.TryParse(ConfigurationManager.AppSettings["ActivationCommissionReportsPageSize"], out pageSize);

            foreach (var list in commissionDetails)
            {
                foreach (var innerList in list.elementCommissions)
                {
                    count++;
                }
            }

            if (count > 0 && pageSize > 0)
            {
                int remainder;
                int quotient = Math.DivRem(count, pageSize, out remainder);
                totalPages = remainder == 0 ? quotient : quotient + 1;
            }

            return totalPages;
        }
        private static List<T> GetPage<T>(List<T> list, int pageNumber)
        {
            int pageSize = 10;
            int.TryParse(ConfigurationManager.AppSettings["ActivationCommissionReportsPageSize"], out pageSize);

            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }
        private static ReportingService.GeneralSearchCriteria GetGeneralSearchCriteria(string key, string value)
        {
            string noneGradeDisplayName = ConfigurationManager.AppSettings["NoneGradeDisplayName"];
            key = key.ToLower().Replace(" ", "");
            value = value.ToLower().Replace(" ", "");
            ReportingService.GeneralSearchCriteria searchCriteria;
            if (key == "grade")
                searchCriteria = ReportingService.GeneralSearchCriteria.AllGrades;
            else if (key == "recharge")
                searchCriteria = ReportingService.GeneralSearchCriteria.AllRecharges;
            else if (key == "paymentcommission")
                searchCriteria = ReportingService.GeneralSearchCriteria.AllPayments;
            else if (key == "paidcommission")
                searchCriteria = ReportingService.GeneralSearchCriteria.PaidPayments;
            else if (key == "pendingcommission")
                searchCriteria = ReportingService.GeneralSearchCriteria.PendingPayments;
            else
                searchCriteria = ReportingService.GeneralSearchCriteria.AllActivations;

            switch (key)
            {
                case "grade":
                    if (value == "a")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByGradeA;
                    else if (value == "b")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByGradeB;
                    else if (value == "c")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByGradeC;
                    else if (value == "d")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByGradeD;
                    else if (value == noneGradeDisplayName.ToLower())
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByNoneGrade;
                    else
                        searchCriteria = ReportingService.GeneralSearchCriteria.AllGrades;
                    break;
                case "recharge":
                    if (value == "1strecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidFirstRechage;
                    else if (value == "2ndrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidSecondRechage;
                    else if (value == "3rdrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidThirdRechage;
                    else
                        searchCriteria = ReportingService.GeneralSearchCriteria.AllRecharges;
                    break;
                case "fpe":
                    if (value == "fpe")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidFirstPayableEvent;
                    else if (value == "notfpe")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByNotAchivedFirstPayableEvent;
                    else
                        searchCriteria = ReportingService.GeneralSearchCriteria.AllActivations;
                    break;
                case "paymentcommission":
                    if (value == "3rdmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByThirdBill;
                    else if (value == "2ndmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.BySecondBill;
                    else if (value == "1stmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByFirstBill;
                    else if (value == "3rdrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByThirdRechage;
                    else if (value == "2ndrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.BySecondRechage;
                    else if (value == "1strecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByFirstRechage;
                    else if (value == "fpe")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByFirstPayableEvent;
                    else if (value == "contractcollection")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByContractCollection;
                    else
                        searchCriteria = ReportingService.GeneralSearchCriteria.AllPayments;
                    break;
                case "paidcommission":
                    if (value == "3rdmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidThirdBill;
                    else if (value == "2ndmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidSecondBill;
                    else if (value == "1stmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidFirstBill;
                    else if (value == "3rdrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidThirdRechage;
                    else if (value == "2ndrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidSecondRechage;
                    else if (value == "1strecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidFirstRechage;
                    else if (value == "fpe")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidFirstPayableEvent;
                    else if (value == "contractcollection")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPaidContractCollection;
                    else
                        searchCriteria = ReportingService.GeneralSearchCriteria.PaidPayments;
                    break;
                case "pendingcommission":
                    if (value == "3rdmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingThirdBill;
                    else if (value == "2ndmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingSecondBill;
                    else if (value == "1stmonbill")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingFirstBill;
                    else if (value == "3rdrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingThirdRechage;
                    else if (value == "2ndrecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingSecondRechage;
                    else if (value == "1strecharge")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingFirstRechage;
                    else if (value == "fpe")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingFirstPayableEvent;
                    else if (value == "contractcollection")
                        searchCriteria = ReportingService.GeneralSearchCriteria.ByPendingContractCollection;
                    else
                        searchCriteria = ReportingService.GeneralSearchCriteria.PendingPayments;
                    break;
            }

            return searchCriteria;
        }
        private static SearchCriteria GetActivationGeneralSearchCriteria(string key, string value)
        {
            string noneGradeDisplayName = ConfigurationManager.AppSettings["NoneGradeDisplayName"];
            key = key.ToLower().Replace(" ", "");
            value = value.ToLower().Replace(" ", "");
            SearchCriteria searchCriteria = SearchCriteria.AllActivations;
            switch (key)
            {
                case "grade":
                    if (value == "a")
                        searchCriteria = SearchCriteria.ByGradeA;
                    else if (value == "b")
                        searchCriteria = SearchCriteria.ByGradeB;
                    else if (value == "c")
                        searchCriteria = SearchCriteria.ByGradeC;
                    else if (value == "d")
                        searchCriteria = SearchCriteria.ByGradeD;
                    else if (value == noneGradeDisplayName.ToLower())
                        searchCriteria = SearchCriteria.ByNoneGrade;
                    else
                        searchCriteria = SearchCriteria.AllActivations;
                    break;
                case "fpe":
                    if (value == "fpe")
                        searchCriteria = SearchCriteria.ByFPE;
                    else if (value == "notfpe")
                        searchCriteria = SearchCriteria.ByNotFPE;
                    else
                        searchCriteria = SearchCriteria.AllActivations;
                    break;
            }

            return searchCriteria;
        }

        private static void FillRecharges(ref List<RechargeSummary> rechargeSummary, List<ReportingService.CommissionsDetailsDataItem> commissionDetails)
        {
            List<RechargeDetails> firstRecharge_Details = new List<RechargeDetails>();
            List<RechargeDetails> secondRecharge_Details = new List<RechargeDetails>();
            List<RechargeDetails> thirdRecharge_Details = new List<RechargeDetails>();

            commissionDetails.ForEach(item =>
            {
                string rechargeType = GetRecharge(item.elementCommissions);
                if (rechargeType == "1st Recharge")
                {
                    firstRecharge_Details.Add(new RechargeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        RechargeType = rechargeType
                    });
                }
                if (rechargeType == "2nd Recharge")
                {
                    secondRecharge_Details.Add(new RechargeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        RechargeType = rechargeType
                    });
                }
                if (rechargeType == "3rd Recharge")
                {
                    thirdRecharge_Details.Add(new RechargeDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        RechargeType = rechargeType
                    });
                }
            });

            if (firstRecharge_Details.Any())
            {
                rechargeSummary.Add(new RechargeSummary()
                {
                    RechargeType = "1st Recharge",
                    RechargeDetails = new List<RechargeDetails>(firstRecharge_Details) { }
                });
            }
            if (secondRecharge_Details.Any())
            {
                rechargeSummary.Add(new RechargeSummary()
                {
                    RechargeType = "2nd Recharge",
                    RechargeDetails = new List<RechargeDetails>(secondRecharge_Details) { }
                });
            }
            if (thirdRecharge_Details.Any())
            {
                rechargeSummary.Add(new RechargeSummary()
                {
                    RechargeType = "3rd Recharge",
                    RechargeDetails = new List<RechargeDetails>(thirdRecharge_Details) { }
                });
            }
        }
        private static void FillFPEs(ref List<FPESummary> fpeSummary, List<ActivationDetailsReport> activationDetailsReport)
        {
            List<FPEDetails> fpe_Details = new List<FPEDetails>();
            List<FPEDetails> notFPE_Details = new List<FPEDetails>();

            activationDetailsReport.ForEach(item =>
            {
                string fpeType = GetFPE(item);
                if (fpeType == "FPE")
                {
                    fpe_Details.Add(new FPEDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        FPE = fpeType
                    });
                }
                if (fpeType == "Not FPE")
                {
                    notFPE_Details.Add(new FPEDetails()
                    {
                        IMSI = item.IMSI,
                        ActivatedOn = item.ActivationDate.ToString("MMM dd,yyyy"),
                        FPE = fpeType
                    });
                }
            });

            if (fpe_Details.Any())
            {
                fpeSummary.Add(new FPESummary()
                {
                    FPEType = "FPE",
                    FPEDetails = new List<FPEDetails>(fpe_Details) { }
                });
            }
            if (notFPE_Details.Any())
            {
                fpeSummary.Add(new FPESummary()
                {
                    FPEType = "Not FPE",
                    FPEDetails = new List<FPEDetails>(notFPE_Details) { }
                });
            }
        }
        private static void FillCommissionPayments(ref List<CommissionPaymentSummary> commissionPaymentSummary, List<ReportingService.CommissionsDetailsDataItem> commissionDetails, string paymentStatus)
        {
            if (!string.IsNullOrEmpty(paymentStatus))
                paymentStatus = char.ToUpper(paymentStatus[0]) + paymentStatus.Substring(1);

            List<CommissionPaymentDetails> thirdBill_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> secondBill_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> firstBill_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> thirdRecharge_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> secondRecharge_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> firstRecharge_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> fpe_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> cc_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> simActivation_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> cashBack_Details = new List<CommissionPaymentDetails>();
            List<CommissionPaymentDetails> productSelling_Details = new List<CommissionPaymentDetails>();


            commissionDetails.ForEach(item =>
            {
                item.elementCommissions.ForEach(elementItem =>
                {
                    string commissionElement = GetCommissionElementName(elementItem.Name);
                    if (commissionElement.ToLower() != "none")
                    {
                        if (commissionElement == "3rd Mon Bill")
                        {
                            thirdBill_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "2nd Mon Bill")
                        {
                            secondBill_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "1st Mon Bill")
                        {
                            firstBill_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "3rd Recharge")
                        {
                            thirdRecharge_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "2nd Recharge")
                        {
                            secondRecharge_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "1st Recharge")
                        {
                            firstRecharge_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "FPE")
                        {
                            fpe_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "Contract Collection")
                        {
                            cc_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "SIM Activation")
                        {
                            simActivation_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement == "Cash Back")
                        {
                            cashBack_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = commissionElement,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement.ToLower().Replace(" ", "").Contains("virgin"))
                        {
                            productSelling_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = elementItem.Name,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                        if (commissionElement.ToLower().Replace(" ", "").Contains("friendi"))
                        {
                            productSelling_Details.Add(new CommissionPaymentDetails()
                            {
                                IMSI = item.IMSI,
                                Date = item.ActivationDate.ToString("MMM dd,yyyy"),
                                CommissionElement = elementItem.Name,
                                Amount = elementItem.Value.ToString(),
                                IsPaid = item.Paid
                            });
                        }
                    }
                });
            });

            if (thirdBill_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "3rd Mon Bill",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(thirdBill_Details) { }
                });
            }
            if (secondBill_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "2nd Mon Bill",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(secondBill_Details) { }
                });
            }
            if (firstBill_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "1st Mon Bill",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(firstBill_Details) { }
                });
            }
            if (thirdRecharge_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "3rd Recharge",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(thirdRecharge_Details) { }
                });
            }
            if (secondRecharge_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "2nd Recharge",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(secondRecharge_Details) { }
                });
            }
            if (firstRecharge_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "1st Recharge",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(firstRecharge_Details) { }
                });
            }
            if (fpe_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "FPE",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(fpe_Details) { }
                });
            }
            if (cc_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "Contract Collection",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(cc_Details) { }
                });
            }
            if (simActivation_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "SIM Activation",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(simActivation_Details) { }
                });
            }
            if (cashBack_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "CashBack",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(cashBack_Details) { }
                });
            }
            if (productSelling_Details.Any())
            {
                commissionPaymentSummary.Add(new CommissionPaymentSummary()
                {
                    PaymentStatus = paymentStatus,
                    CommissionElement = "Product Selling",
                    CommissionPaymentDetails = new List<CommissionPaymentDetails>(productSelling_Details) { }
                });
            }
        }

        private static string FormatBytes(long bytes, out string unit)
        {
            unit = "";
            string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
            int i;
            double dblSByte = bytes;
            for (i = 0; i < Suffix.Length && bytes >= 1024; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }

            unit = Suffix[i];
            return dblSByte.ToString();
        }

        private static string FormatMinutes(long value, eMeasurementUnit eMeasurementUnit)
        {
            switch (eMeasurementUnit)
            {
                case eMeasurementUnit.MilliSeconds:
                    return (value / 60000).ToString();
                case eMeasurementUnit.Seconds:
                    return (value / 60).ToString();
                case eMeasurementUnit.Minutes:
                    return value.ToString();
            }

            return value.ToString();
        }

        public static void FillPlanUnitAndValue(Contracts.PlanInfo plan, long value,
            eMeasurementUnit eMeasurementUnit, PromotionBenefit promotionBenefit = null)
        {
            string unit = string.Empty;

            switch (plan.Type)
            {
                case (int)eServiceType.VOICE_LCL:
                case (int)eServiceType.VOICE_ON_NET:
                    plan.Unit = "Min";
                    plan.Value = FormatMinutes(value, eMeasurementUnit);
                    break;
                case (int)eServiceType.DATA:
                case (int)eServiceType.DATA_SOCIAL:
                    plan.Value = FormatBytes(value, out unit);
                    plan.Unit = unit;
                    break;
                default:
                    plan.Value = Convert.ToString(promotionBenefit?.FreePlan?.PromotionDuration);
                    plan.Unit = "Days";
                    break;
            }
        }

        public static void FillPlanUnitAndValue(PlanResponse plan, long value,
            eMeasurementUnit eMeasurementUnit)
        {
            string unit = string.Empty;

            switch (plan.ServiceType)
            {
                case (int)eServiceType.VOICE_LCL:
                case (int)eServiceType.VOICE_ON_NET:
                    plan.UnitType = "Min";
                    plan.Balance = FormatMinutes(value, eMeasurementUnit);
                    break;
                case (int)eServiceType.DATA:
                case (int)eServiceType.DATA_SOCIAL:
                    plan.Balance = FormatBytes(value, out unit);
                    plan.UnitType = unit;
                    break;
            }
        }

        public static List<Contracts.PlanInfo> GetPromotionDetails(PlanPrice planPrice,
            List<PromotionRequest> promotions)
        {
            List<Contracts.PlanInfo> planInfos = new List<Contracts.PlanInfo>();
            if (planPrice != null)
            {
                var pfs = planPrice.Promotions.ToList();
                var freeServiceIds = planPrice.Promotions.Select(entity => entity.FreeServiceId);

                if (pfs.Any())
                {
                    var freeServices = POWrapper.GetServices(freeServiceIds.ToList());

                    foreach (var item in pfs)
                    {
                        if (item.FreePlan != null)
                        {
                            var service = freeServices.FirstOrDefault(e => e.Id == item.FreePlan.ID);

                            if (!item.FreePlan.IsOptional || promotions.Any(e => e.PluginCode == item.PluginCode))
                            {
                                Contracts.PlanInfo planInfo = new Contracts.PlanInfo() { };
                                planInfo.Type = (int)service.ServiceType;
                                FillPlanUnitAndValue(planInfo, service.InitialValue, service.Unit, item);
                                planInfo.Description = service.Name;

                                planInfos.Add(planInfo);
                            }
                        }
                    }
                }
            }

            return planInfos;
        }

        private static string GetSearchCriteriaKey(string key)
        {
            key = key.ToLower();
            if (key == "paid")
            {
                return "paidcommission";
            }
            else if (key == "pending")
            {
                return "pendingcommission";
            }
            else
            {
                return "paymentcommission";
            }
        }
        private static List<ReportingService.CommissionsDetailsDataItem> FilterCommissionDetailsDataItems(List<ReportingService.CommissionsDetailsDataItem> commissionsDetailsDataItems, string commissionElementFilter, AppearanceBasedOnBrand brand)
        {
            if (commissionElementFilter.ToLower() == "any")
                return commissionsDetailsDataItems;
            else
            {
                List<ReportingService.CommissionsDetailsDataItem> filteredResult = new List<ReportingService.CommissionsDetailsDataItem>();
                foreach (var item in commissionsDetailsDataItems)
                {
                    if (!commissionElementFilter.ToLower().Replace(" ", "").Contains("productselling"))
                    {
                        if (item.elementCommissions.Any(x => x.Name.ToLower().Replace(" ", "") == GetCommissionElementNameInCommissionEngine(commissionElementFilter)))
                        {
                            filteredResult.Add(item);
                        }
                    }
                    else
                    {
                        if (brand == AppearanceBasedOnBrand.Virgin)
                        {
                            if (item.elementCommissions.Any(x => x.Name.ToLower().Replace(" ", "").Contains("virgin")))
                            {
                                filteredResult.Add(item);
                            }
                        }
                        else if (brand == AppearanceBasedOnBrand.FRiENDi)
                        {
                            if (item.elementCommissions.Any(x => x.Name.ToLower().Replace(" ", "").Contains("friendi")))
                            {
                                filteredResult.Add(item);
                            }
                        }
                        else
                        {
                            if (commissionElementFilter.ToLower().Replace(" ", "").Contains("virgin"))
                            {
                                if (item.elementCommissions.Any(x => x.Name.ToLower().Replace(" ", "").Contains("virgin")))
                                {
                                    filteredResult.Add(item);
                                }
                            }
                            else
                            {
                                if (item.elementCommissions.Any(x => x.Name.ToLower().Replace(" ", "").Contains("friendi")))
                                {
                                    filteredResult.Add(item);
                                }
                            }
                        }
                    }
                }
                return filteredResult;
            }
        }
        private static string GetCommissionElementNameInCommissionEngine(string element)
        {
            if (element.ToLower().Replace(" ", "") == "3rdmonbill")
                return "thirdmonthbill";
            else if (element.ToLower().Replace(" ", "") == "2ndmonbill")
                return "secondmonthbill";
            else if (element.ToLower().Replace(" ", "") == "1stmonbill")
                return "firstmonthbill";
            else if (element.ToLower().Replace(" ", "") == "3rdrecharge")
                return "thirdrecharge";
            else if (element.ToLower().Replace(" ", "") == "2ndrecharge")
                return "secondrecharge";
            else if (element.ToLower().Replace(" ", "") == "1strecharge")
                return "firstrecharge";
            else if (element.ToLower().Replace(" ", "") == "fpe")
                return "firstpayableevent";
            else if (element.ToLower().Replace(" ", "") == "contractcollection")
                return "contractcollection";
            else if (element.ToLower().Replace(" ", "") == "simactivation")
                return "simactivation";
            else if (element.ToLower().Replace(" ", "") == "cashback")
                return "cashback";
            else
                return "none";
        }
        private static MNPPortIn FillMNPPortInRequest(MNPPortInRequest MNPPortInRequest)
        {
            MNPPortIn MNPPortIn = new MNPPortIn();
            if (MNPPortInRequest != null)
            {
                MNPPortIn.IsPortIn = MNPPortInRequest.IsMNPPortIn;
                MNPPortIn.MSISDN = MNPPortInRequest.MSISDN;
                MNPPortIn.OperatorID = MNPPortInRequest.OperatorID;
                MNPPortIn.ContactNumber = MNPPortInRequest.ContactNumber;
            }

            return MNPPortIn;
        }

        public static List<decimal> GetTopupAmounts()
        {
            using (ActivationServiceClient client = new ActivationServiceClient())
            {
                return client.GetTopupAmounts();
            }
        }

        public static ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request)
        {
            return SCWrapper.ValidateAndRegisterFamily(request);
        }

        public static bool IsCustomerEligibleForDSTOnboarding(string email)
        {
            bool isEligible = false;
            using (ActivationServiceClient client = new ActivationServiceClient())
            {
                isEligible = client.IsCustomerEligibleForDSTOnboarding(email);
            }

            return isEligible;
        }

        public static bool IsDealerEligibleForDSTOnboarding(AdministrationServices.Requester oRequester, out FamilyDealerEligibility familyDealerEligibility)
        {
            familyDealerEligibility = FamilyDealerEligibility.NotEligible;
            bool hasSimActivationRight = IsDealerHasRight(familyActivationRightCode, oRequester);
            bool hasDeviceActivationRight = IsDealerHasRight(deviceActivationRightCode, oRequester);

            if (hasSimActivationRight || hasDeviceActivationRight)
            {
                if (hasSimActivationRight && hasDeviceActivationRight)
                {
                    familyDealerEligibility = FamilyDealerEligibility.Both;
                }
                else if (hasSimActivationRight)
                {
                    familyDealerEligibility = FamilyDealerEligibility.SIMOnly;
                }
                else if (hasDeviceActivationRight)
                {
                    familyDealerEligibility = FamilyDealerEligibility.DeviceOnly;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static CancelFamilyRegistrationResponse CancelFamilyRegistration(CancelFamilyRegistrationRequest request)
        {
            return SCWrapper.CancelFamilyRegistration(request.MSISDN);
        }

        public static Contracts.BaseResponse AssociateScannedSim(DealerSimAssociationRequest request)
        {
            Contracts.BaseResponse baseResponse = new Contracts.BaseResponse();
            string userName = string.Empty;
            CallResponse oCallResponse = new CallResponse();
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
            oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName,
                !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                baseResponse.ResponseMessage = oCallResponse.ResponseMessage;
                return baseResponse;
            }

            if (request.IsLoggedInAppSessionInArabic)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            using (ActivationServiceClient client = new ActivationServiceClient())
            {
                try
                {
                    var res = client.AssociateScannedSim(new ShopSimAssociationRequest()
                    {
                        Brand = (BrandsEnum)request.Brand,
                        DealerCode = request.LoginRequest.Username,
                        FromKitId = request.FromKitId,
                        ToKitId = request.ToKitId
                    });

                    baseResponse.IsPassed = res;
                }
                catch (FaultException ex)
                {
                    ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, ex.Message, "AssociateScannedSim", request.LoginRequest.Username, ex.ToString());
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        baseResponse.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("AssociateScannedSim", ex, request.LoginRequest.Username);
                    baseResponse.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return baseResponse;
        }

        public static PagedResult<Contracts.DealerSImAssociation.ShopAssociatedSIMs> GetAssociatedShops(ShopAssociatedSIMsRequest request)
        {
            PagedResult<Contracts.DealerSImAssociation.ShopAssociatedSIMs> result = new PagedResult<Contracts.DealerSImAssociation.ShopAssociatedSIMs>();
            string userName = string.Empty;
            CallResponse oCallResponse = new CallResponse();
            AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
            oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName,
                !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return result;
            }

            if (request.IsLoggedInAppSessionInArabic)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            }

            using (ActivationServiceClient client = new ActivationServiceClient())
            {
                int pageSize = 10;
                try
                {
                    if (ConfigurationManager.AppSettings["SIMsAllocationReportPageSize"] != null)
                    {
                        pageSize = int.Parse(ConfigurationManager.AppSettings["SIMsAllocationReportPageSize"]);
                    }

                    var res = client.GetAssociatedShops(request.Page, pageSize, request.LoginRequest.Username, request.KitID);
                    result.CurrentPage = res.CurrentPage;
                    result.PageCount = res.PageCount;
                    result.PageSize = res.PageSize;
                    result.Results = res.Results.ConvertAll(r => new Contracts.DealerSImAssociation.ShopAssociatedSIMs()
                    {
                        brand = r.brand,
                        id = r.id,
                        iMSI = r.iMSI,
                        timeStamp = r.timeStamp.HasValue ? r.timeStamp.Value.ToString("MMM dd,yyyy") : ""
                    });
                    result.RowCount = res.RowCount;
                }
                catch (FaultException ex)
                {
                    ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, ex.Message, "GetAssociatedShops", request.LoginRequest.Username, ex.ToString());
                    try
                    {
                        throw new LPBizException(ex.Code.Name, ex.Message);
                    }
                    catch (LPBizException LPBizEx)
                    {
                        //ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("GetAssociatedShops", ex, request.LoginRequest.Username);
                    //ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                }
            }

            return result;
        }

        public static string GetVanityPromotionMessage(int? vanityId, int userLanguageId, int EventId)
        {
            using (Entities context = new Entities())
            {
                var promotion = context.VanityPromotions.FirstOrDefault(entity => entity.VanityId == vanityId && entity.EventId == EventId);

                if (promotion != null)
                {
                    if (userLanguageId == 1)
                    {
                        return promotion.EnglishText;
                    }
                    else if (userLanguageId == 2)
                    {
                        return promotion.ArabicText;
                    }
                }
                return null;
            }
        }

        public static void FillMNPData(MNPData mnpData, BookedNumberV2 bookedNumber, int lang)
        {
            mnpData.IsPortInNumber = bookedNumber.IsMNP;
            mnpData.OperatorID = bookedNumber.OperatorId;
            mnpData.Description = GetResourceMessage("MSG_MNP_Description", lang);
        }

        public static RefillDealerEWalletResponse RefillDealerEWallet(RefillDealerEWalletRequest request)
        {
            RefillDealerEWalletResponse response = new RefillDealerEWalletResponse();
            NobillService.Voucher voucher = null;
            NobillService.VoucherRefill voucherRefill = new NobillService.VoucherRefill();

            try
            {
                string name = "";
                CallResponse oCallResponse = new CallResponse() { };
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
                int lang = request.LoginRequest.language;
                string dealerCode = request.LoginRequest.Username;
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out name,
                    !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (!oCallResponse.IsPassed)
                {
                    return new RefillDealerEWalletResponse()
                    {
                        ResponseMessage = oCallResponse.ResponseMessage
                    };
                }

                Contracts.BaseResponse BaseResponse = IsValidDealerEWallet(oRequester.RefillMSISDN, request.LoginRequest.language);

                if (!BaseResponse.IsPassed)
                {
                    return new RefillDealerEWalletResponse()
                    {
                        IsSuccess = false,
                        ResponseMessage = BaseResponse.ResponseMessage
                    };
                }

                if (request.IsVoucherLengthInvalid)
                {
                    return new RefillDealerEWalletResponse()
                    {
                        ResponseMessage = GetResourceMessage("MSG_InvalidVoucher", lang)
                    };
                }

                int voucherDetailsResponse = -1;
                voucher = NobillWrapper.GetVoucherDetails(request.Voucher, dealerCode, out voucherDetailsResponse);
                if (voucher == null || voucherDetailsResponse != 0)
                {
                    response.ResponseMessage = GetResourceMessage("MSG_InvalidVoucher", lang);
                }
                else if (bool.Parse(voucher.Used))
                {
                    response.ResponseMessage = GetResourceMessage("MSG_VoucherUsedBefore", lang);
                }
                else if (voucher.Status == "7")
                {
                    response.ResponseMessage = GetResourceMessage("MSG_BlockedVoucher", lang);
                }
                else if (voucher.Status != "6")
                {
                    response.ResponseMessage = GetResourceMessage("MSG_InvalidVoucher", lang);
                }
                else
                {
                    int voucherRefillResponse = -1;
                    NobillWrapper.VoucherRefill(request.RefillMsisdn, request.Voucher, dealerCode,
                        out voucherRefill, out voucherRefillResponse);
                    if (voucherRefillResponse == 0)
                    {
                        response.IsSuccess = true;
                        response.Balance = decimal.Parse(voucherRefill.balance);
                        response.ResponseMessage = GetResourceMessage("MSG_SuccessfulVoucherRefill", lang);
                    }
                    else
                    {
                        response.ResponseMessage = GetResourceMessage("MSG_FailedVoucherRefill", lang);
                    }
                }

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                ErrorHandling.HandleException("RefillDealerEWallet", ex, request.LoginRequest.Username, false);
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption",
                    Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static Contracts.BaseResponse IsValidDealerEWallet(string refillMSISDN, int language)
        {
            Contracts.BaseResponse response = new Contracts.BaseResponse() { IsPassed = true };

            if (string.IsNullOrEmpty(refillMSISDN) ||
             refillMSISDN == ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.DefaultRefillMSISDN"])
            {
                response.IsPassed = false;
                response.ResponseMessage = GetResourceMessage("MSG_InValidDealerEWallet", language);
            }
            else if (IsDealerEWalletBlocked(refillMSISDN))
            {
                response.IsPassed = false;
                response.ResponseMessage = GetResourceMessage("MSG_BlockedDealerEWallet", language);
            }

            return response;
        }

        public static bool IsDealerEWalletBlocked(string msisdn)
        {
            bool IsDealerEWalletBlocked = true;
            NobillService.SubscribtionData details = null;
            NobillWrapper.GetSubscribtionData(msisdn, out details);

            if (details != null)
            {
                if (details.blocked == NobillService.blocked.notBlocked)
                {
                    IsDealerEWalletBlocked = false;
                }
            }

            return IsDealerEWalletBlocked;
        }

        public static List<Contracts.NumberInfo> GetAllNumbersRealtedByNumberId(string idNumber)
        {
            List<Contracts.NumberInfo> nummberInfoList = new List<Contracts.NumberInfo>();
            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    List<ActivationService.NumberInfo> NumberInfoClientList = client.GetAllNumbersRealtedByNumberId(idNumber);
                    if (NumberInfoClientList != null && NumberInfoClientList.Count > 0)
                    {
                        Contracts.NumberInfo contractNumberInfo = null;
                        foreach (ActivationService.NumberInfo number in NumberInfoClientList)
                        {
                            contractNumberInfo = new Contracts.NumberInfo();
                            contractNumberInfo.MSISDN = number.MSISDN;
                            contractNumberInfo.BrandId = number.BrandId;
                            contractNumberInfo.Email = number.Email;
                            nummberInfoList.Add(contractNumberInfo);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("GetAllNumbersRealtedByNumberId", ex, $"Idnumber {idNumber}", false);
                }
            }

            return nummberInfoList;
        }


        public static Contracts.OTPResponse SendOTP(Contracts.OTPRequest OTPRequest)
        {
            Contracts.OTPResponse response = new Contracts.OTPResponse();
            ActivationService.OTPResponse OTPResponse = new ActivationService.OTPResponse();
            using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
            {
                try
                {
                    ActivationService.OTPRequest request = new ActivationService.OTPRequest();
                    request.BrandId = OTPRequest.BrandId;
                    request.DealerCode = OTPRequest.DealerCode;
                    request.idNumber = OTPRequest.idNumber;
                    request.KitCode = OTPRequest.KitCode;
                    request.SubscriptionTypeId = OTPRequest.SubscriptionTypeId;
                    request.UserEmail = OTPRequest.UserEmail;
                    request.MSISDN = OTPRequest.MSISDN;
                    request.OTPType = OTPRequest.OTPType;
                    request.ProductId = OTPRequest.ProductId;
                    request.BookingCode = !string.IsNullOrEmpty(OTPRequest.BookingCode) ? OTPRequest.BookingCode : string.Empty;
                    request.IsPortInActivation = OTPRequest.IsPortInActivation;
                    request.Topup = OTPRequest.Topup;
                    request.NewOwnerIDNumber = OTPRequest.NewOwnerID;
                    request.ProductDetails = new ActivationService.ProductDetails();
                    request.TerminationRequest = new ActivationService.TerminationRequest();
                    if (OTPRequest.ProductDetails != null)
                    {
                        request.ProductDetails.ServiceDetails = OTPRequest.ProductDetails.ServiceDetails;
                        request.ProductDetails.ProductPrice = OTPRequest.ProductDetails.ProductPrice;
                    }
                    if (OTPRequest.TerminationRequest != null)
                    {
                        request.TerminationRequest.IdNumber = OTPRequest.TerminationRequest.IdNumber;
                        request.TerminationRequest.IdType = OTPRequest.TerminationRequest.IdType;
                        request.TerminationRequest.MSISDN = OTPRequest.TerminationRequest.MSISDN;
                        request.TerminationRequest.NationalityCode = OTPRequest.TerminationRequest.NationalityCode;
                        request.TerminationRequest.TerminationReasonId = OTPRequest.TerminationRequest.TerminationReasonId;
                        request.TerminationRequest.Email = OTPRequest.TerminationRequest.Email;
                        request.TerminationRequest.Longitude = OTPRequest.TerminationRequest.LoginRequest.Longitude.ToString();
                        request.TerminationRequest.Latitude = OTPRequest.TerminationRequest.LoginRequest.Latitude.ToString();
                    }

                    if (OTPRequest.SIMReplacementData != null)
                    {
                        request.SIMReplacementData = new ActivationService.SIMReplacementData()
                        {
                            BrandId = OTPRequest.SIMReplacementData.BrandId,
                            IDNumber = OTPRequest.SIMReplacementData.IDNumber,
                            IDType = OTPRequest.SIMReplacementData.IDType,
                            IsESIM = OTPRequest.SIMReplacementData.IsESIM,
                            SIMCode = OTPRequest.SIMReplacementData.SIMCode,
                            MSISDN = OTPRequest.SIMReplacementData.MSISDN,
                            NationalityCode = OTPRequest.SIMReplacementData.NationalityCode
                        };
                    }

                    OTPResponse = client.SendOTP(request);

                    response.isValid = OTPResponse.isValid;
                    response.ReferenceNo = OTPResponse.ReferenceNo;
                    response.responseMessage = OTPResponse.responseMessage;
                    response.ByPassOTPVerification = OTPResponse.ByPassOTPVerification;
                }
                catch (LPBizException LPBizEx)
                {
                    response.isValid = false;
                    response.responseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, OTPRequest.LoginRequest.language);
                }
                catch (Exception ex)
                {
                    ErrorHandling.HandleException("SendOTP", ex, string.Empty, false);
                }
            }

            return response;
        }

        public static Contracts.OTPResponse ValidateOTP(ValidateOTPRequest validateOTPRequest)
        {
            Contracts.OTPResponse response = new Contracts.OTPResponse();
            ActivationService.OTPResponse OTPResponse = new ActivationService.OTPResponse();
            try
            {
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    OTPResponse = client.ValidateOTP(validateOTPRequest.OTP, validateOTPRequest.referanceNo, validateOTPRequest.OTPType);
                    response.isValid = OTPResponse.isValid;
                    response.ReferenceNo = OTPResponse.ReferenceNo;
                    response.responseMessage = OTPResponse.responseMessage;
                }
            }
            catch (LPBizException LPBizEx)
            {
                response.isValid = false;
                response.responseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, validateOTPRequest.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.isValid = false;
                    response.responseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, validateOTPRequest.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateOTP", ex, validateOTPRequest.LoginRequest.Username);
                response.isValid = false;
                response.responseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;

        }

        public static SendEsimQRImageResponse SendEsimQRImage(SendEsimQRImageRequest request)
        {
            SendEsimQRImageResponse response = new SendEsimQRImageResponse() { IsPassed = false };

            try
            {
                string name = "";
                CallResponse oCallResponse = new CallResponse() { };
                AdministrationServices.Requester oRequester = null;
                double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (oCallResponse.IsPassed == false)
                {
                    return new SendEsimQRImageResponse()
                    {
                        IsPassed = false,
                        ResponseMessage = oCallResponse.ResponseMessage
                    };
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, request.LoginRequest.Username, request.LoginRequest.Username, DSTChannelValue))
                    {
                        response.IsPassed = client.CallNotification("ESIM_QR", new LP.OMS.Channels.Engine.ActivationService.NEPostEventData() { EventParam = new List<LP.OMS.Channels.Engine.ActivationService.EventParam>() { new LP.OMS.Channels.Engine.ActivationService.EventParam() { Name = "QR_String", Value = request.QR_String } } }, request.UniqueDeviceIds);
                        response.ResponseMessage = objResourceManager.GetString("MSG_Success_SendEsimQR", Thread.CurrentThread.CurrentCulture);
                    }
                }
            }
            catch (FaultException ex)
            {
                ErrorHandling.AddToErrorLog("BusinessError", ex.Message, "SendEsimQRImage", request.LoginRequest.Username, ex.ToString());
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendEsimQRImage", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static Contracts.TerminationResponse SIMTermination(Contracts.TerminationRequest terminationRequest)
        {
            Contracts.TerminationResponse response = new Contracts.TerminationResponse();
            try
            {
                CallResponse oCallResponse = new CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = CheckRequester(terminationRequest.LoginRequest, out oRequester, out userName, false);
                if (!oCallResponse.IsPassed)
                {
                    response.isValid = oCallResponse.IsPassed;
                    response.responseMessage = oCallResponse.ResponseMessage;
                    return response;
                }
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                    {
                        ActivationService.TerminationRequest request = new ActivationService.TerminationRequest();

                        request.MSISDN = terminationRequest.MSISDN;
                        request.IdType = terminationRequest.IdType;
                        request.IdNumber = terminationRequest.IdNumber;
                        request.NationalityCode = terminationRequest.NationalityCode;
                        request.TerminationReasonId = terminationRequest.TerminationReasonId;
                        request.ReferenceNo = terminationRequest.ReferenceNo;
                        request.Longitude = terminationRequest.LoginRequest.Longitude.ToString();
                        request.Latitude = terminationRequest.LoginRequest.Latitude.ToString();

                        if (terminationRequest.Fingerprint != null)
                        {
                            request.Fingerprint = new ActivationService.FingerPrint();
                            request.Fingerprint = GetActivationServiceFingerprint(terminationRequest.Fingerprint);
                        }
                        request.LocationId = terminationRequest.LoginRequest.LocationId;

                        ActivationService.TerminationResponse terminationResponse = client.SIMTermination(request);
                        response.isValid = terminationResponse.isValid;
                        response.responseMessage = terminationResponse.responseMessage;
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                response.isValid = false;
                response.responseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, terminationRequest.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.isValid = false;
                    response.responseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, terminationRequest.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SIMTermination", ex, terminationRequest.LoginRequest.Username);
                response.isValid = false;
                response.responseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static Contracts.CustomerActivePlanDetailsResponse GetCustomerSubscriptionInfo(CustomerPlanRequest CustomerPlanRequest)
        {
            Contracts.CustomerActivePlanDetailsResponse CustomerActivePlanDetailsResponse = new Contracts.CustomerActivePlanDetailsResponse();
            List<Contracts.GenericCustomerService> ResponseData = new List<Contracts.GenericCustomerService>();
            try
            {
                using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                {
                    ActivationService.CustomerActivePlanDetailsResponse customerPlanDetails = new ActivationService.CustomerActivePlanDetailsResponse();
                    customerPlanDetails = client.GetCustomerSubscriptionInfo(CustomerPlanRequest.MSISDN);

                    if (customerPlanDetails != null)
                    {
                        if (customerPlanDetails.CustomerServices != null && customerPlanDetails.CustomerServices.Count > 0)
                        {
                            Contracts.GenericCustomerService GenericCustomerService = null;
                            foreach (var item in customerPlanDetails.CustomerServices)
                            {
                                GenericCustomerService = new Contracts.GenericCustomerService();
                                GenericCustomerService.Initial = item.Initial;
                                GenericCustomerService.Remaining = item.Remaining;
                                GenericCustomerService.Unit = GetResourceMessage(item.Unit, CustomerPlanRequest.LoginRequest.language); ;
                                GenericCustomerService.ServiceType = item.ServiceType;
                                GenericCustomerService.ServiceName = GetResourceMessage(item.ServiceName, CustomerPlanRequest.LoginRequest.language);
                                ResponseData.Add(GenericCustomerService);
                            }

                            CustomerActivePlanDetailsResponse.CustomerServices = new List<Contracts.GenericCustomerService>();
                            CustomerActivePlanDetailsResponse.CustomerServices.AddRange(ResponseData);
                            CustomerActivePlanDetailsResponse.RenewalDate = !string.IsNullOrEmpty(customerPlanDetails.RenewalDate) ? customerPlanDetails.RenewalDate : "";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCustomerSubscriptionInfo", ex, CustomerPlanRequest.LoginRequest.Username);
            }

            return CustomerActivePlanDetailsResponse;
        }

        public static Contracts.VerifyChangeSubscriptionTypeResponse VerfityChangeSubscriptionType(Contracts.VerifyChangeSubscriptionTypeRequest request)
        {
            Contracts.VerifyChangeSubscriptionTypeResponse response = new Contracts.VerifyChangeSubscriptionTypeResponse();
            try
            {
                CallResponse oCallResponse = new CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName, false);
                if (oCallResponse.IsPassed)
                {
                    using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            ActivationService.VerifyChangeSubscriptionTypeResponse clientResponse = new ActivationService.VerifyChangeSubscriptionTypeResponse();
                            clientResponse = client.VerfityChangeSubscriptionType(new ActivationService.VerifyChangeSubscriptionTypeRequest
                            {
                                IdNumber = request.idNumber,
                                IdType = request.idType,
                                MSISDN = request.MSISDN,
                                Nationality = request.Nationality,
                                Latitude = request.LoginRequest.Latitude.ToString(),
                                Longitude = request.LoginRequest.Longitude.ToString(),
                                BrandId = request.BrandId,
                                LanguageId = request.LoginRequest.language

                            });

                            if (clientResponse != null)
                            {
                                if (clientResponse.IsPassed)
                                {
                                    response.IsPassed = clientResponse.IsPassed;
                                    response.IsAbshirOTPRequired = clientResponse.IsAbshirOTPRequired;
                                    response.CanChangeToPostPaid = clientResponse.CanChangeToPostPaid;
                                    response.CanChangeToPrePaid = clientResponse.CanChangeToPrePaid;
                                    response.ResponseMessage = clientResponse.ResponseMessage;
                                    response.CurrentSubscriptionType = clientResponse.CurrentSubscriptionType;
                                }
                            }
                        }
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                response.IsPassed = false;
                response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerfityChangeSubscriptionType", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;

        }

        public static Contracts.ChangeSubscriptionTypeResponse ChangeSubscriptionType(Contracts.ChangeSubscriptionTypeRequest request)
        {
            Contracts.ChangeSubscriptionTypeResponse response = new Contracts.ChangeSubscriptionTypeResponse();
            try
            {
                CallResponse oCallResponse = new CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName, false);
                if (oCallResponse.IsPassed)
                {
                    using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            bool clientResponse = client.ChangeSubscriptionTypeDST(request.MSISDN, request.AbshirOTP, request.ReferanceNo, request.TargetSubscriptionType,
                                request.idNumber, request.idType, request.Nationality, request.BrandId,
                                request.LoginRequest.Longitude.ToString(),
                                request.LoginRequest.Latitude.ToString(),
                                request.Address, Convert.FromBase64String(request.SignatureDocument.DocumentContent),
                                request.LoginRequest.LocationId);

                            if (clientResponse)
                            {
                                response.IsPassed = true;
                                response.ResponseMessage = "Done Successfully";
                            }
                        }
                    }
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ChangeSubscriptionType", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static Contracts.ModernTradeResponse SearchDealerModernTrade(Contracts.ModernTradeRequest request)
        {
            Contracts.ModernTradeResponse response = new Contracts.ModernTradeResponse();
            try
            {
                CallResponse oCallResponse = new CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName, false);
                if (oCallResponse.IsPassed)
                {
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            ActivationService.ModernTradeResponse clientResponse = new ActivationService.ModernTradeResponse();
                            clientResponse = client.SearchModernTrade(new ActivationService.ModernTradeRequest { DealerCode = oRequester.Code, ReportType = request.ReportType, Language = request.LoginRequest.language });

                            if (clientResponse != null)
                            {
                                response.AchievedPercent = clientResponse.AchievedPercent;
                                response.AchievedTarget = clientResponse.AchievedTarget;
                                response.ActivationTarget = clientResponse.ActivationTarget;
                                response.AchievedPercent_VM = clientResponse.AchievedPercent_VM;
                                response.AchievedTarget_VM = clientResponse.AchievedTarget_VM;
                                response.ActivationTarget_VM = clientResponse.ActivationTarget_VM;
                                response.PayoutPercent = clientResponse.PayoutPercent.ToString();
                                response.RevenueM2 = clientResponse.RevenueM2;
                                response.RevenueM3 = clientResponse.RevenueM3;
                                response.Sales = clientResponse.Sales;
                                response.TotalPayOut = clientResponse.TotalPayOut;
                                response.Vanity = clientResponse.Vanity;

                                if (request.ReportType == (int)ModernTradeReportType.TwoMonthesBack)
                                {
                                    response.MTDSearchQuery = objResourceManager.GetString("TwoMonthesBackRes", Thread.CurrentThread.CurrentCulture);
                                }
                                else if (request.ReportType == (int)ModernTradeReportType.ThreeMonthesBack)
                                {
                                    response.MTDSearchQuery = objResourceManager.GetString("ThreeMonthesBackRes", Thread.CurrentThread.CurrentCulture);
                                }
                                else
                                {
                                    response.MTDSearchQuery = clientResponse.MTDSearchQuery;
                                }

                                response.IsPassed = true;
                                response.ResponseMessage = "Done Successfully";
                            }
                        }
                    }
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SearchDealerModernTrade", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;

        }


        public static Contracts.MNPQuarantineStatusResponse IsNumberQuarantined(Contracts.MNPQuarantineStatusRequest request)
        {
            Contracts.MNPQuarantineStatusResponse response = new Contracts.MNPQuarantineStatusResponse();

            try
            {
                CallResponse oCallResponse = new CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = CheckRequester(request.LoginRequest, out oRequester, out userName, false);
                if (oCallResponse.IsPassed)
                {
                    using (ActivationService.ActivationServiceClient client = new ActivationService.ActivationServiceClient())
                    {
                        using (var userScope = new UserContextScope(client.InnerChannel, oRequester.UserName, oRequester.Code, DSTChannelValue))
                        {
                            response.IsNumberInQuarantined = client.IsNumberQuarantined(new ActivationService.MNPQuarantineStatusRequest { IdNumber = request.IdNumber, MSISDN = request.MSISDN });
                            response.IsPassed = true;
                            response.ResponseMessage = "Done Successfully";
                        }
                    }
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsNumberQuarantined", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        private static bool IsIndirectSalesChannel(int requesterClassId)
        {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["IndirectSalesChannelRequesterClassIds"]))
            {
                return false;
            }
            List<int> requesterClassIds = ConfigurationManager.AppSettings["IndirectSalesChannelRequesterClassIds"].Split(',').Select(e => int.Parse(e)).ToList();
            return requesterClassIds.Contains(requesterClassId);
        }

        public static string TranslateString(string str)
        {
            int lang = 2;
            if (str != null)
            {
                switch (str.ToLower())
                {
                    case "free":
                        return GetResourceMessage("Vanity_Type_Free", lang);
                    case "value":
                        return GetResourceMessage("Vanity_Type_Value", lang);
                    case "rare":
                        return GetResourceMessage("Vanity_Type_Rare", lang);
                    case "legendary":
                        return GetResourceMessage("Vanity_Type_Legendary", lang);
                    case "exotic":
                        return GetResourceMessage("Vanity_Type_Exotic", lang);
                    case "30days":
                        return GetResourceMessage("Vanity_Commitment_30days", lang);
                    case "refill":
                        return GetResourceMessage("Extra_Type_Refill", lang);
                    case "familyrecharge":
                        return GetResourceMessage("Extra_Type_FamilyRecharge", lang);
                    case "free 1 month":
                        return GetResourceMessage("Vanity_Commitment_1_Month", lang);
                    case "free 2 months":
                        return GetResourceMessage("Vanity_Commitment_2_Month", lang);
                    case "free 3 months":
                        return GetResourceMessage("Vanity_Commitment_3_Month", lang);
                    default:
                        return str;
                }
            }

            return str;
        }

        public static VanityTypesResponse GetVanityTypes(LoginRequest request)
        {
            VanityTypesResponse response = new VanityTypesResponse()
            {
                IsPassed = false,
                ResponseMessage = string.Empty,
                VanityTypes = new List<VanityType>()
            };

            try
            {
                string name = "";
                AdministrationServices.Requester dealer = null;
                double appVersion = double.Parse(request.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    var vanitytypes = client.GetVanityTypes();
                    vanitytypes.ForEach(v =>
                    {
                        response.VanityTypes.Add(new VanityType()
                        {
                            ID = v.ID,
                            NameEn = v.NameEN,
                            NameAR = v.NameAR,
                            Price = v.Price,
                            IsFRiENDi = v.IsFRiENDi
                        });
                    });
                }
                response.IsPassed = true;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetVanityTypes", ex, request.Username);
            }

            return response;
        }

        public static AvailableNumbersResponse GetAvailableNumbers(AvailableNumbersRequest request)
        {
            AvailableNumbersResponse response = new AvailableNumbersResponse()
            {
                IsPassed = false,
                ResponseMessage = string.Empty,
                AvailableNumbers = new List<Contracts.AvailableNumber>()
            };

            try
            {
                string name = "";
                AdministrationServices.Requester dealer = null;
                double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request.oLoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    var availableNumbers = client.GetAvailableNumbers(new AvailableNumberRequest()
                    {
                        SearchPattern = request.searchPattern,
                        IsFRiENDi = request.IsFRiENDi,
                        HasVanityRight =
                            IsDealerHasRight(ConfigurationManager.AppSettings["VirginVanityBookingRight"], dealer),
                        PageNumber = request.pageNumber,
                        HasVirginNormalRight =
                            IsDealerHasRight(ConfigurationManager.AppSettings["VirginBookingRight"], dealer)
                    });
                    availableNumbers.ForEach(num =>
                    {
                        response.AvailableNumbers.Add(new Contracts.AvailableNumber
                        {
                            MSISDN = num.MSISDN,
                            VanityType = new VanityType()
                            {
                                ID = num.vanityTypeDetails.ID,
                                NameEn = num.vanityTypeDetails.NameEN,
                                NameAR = num.vanityTypeDetails.NameAR,
                                Price = num.vanityTypeDetails.Price
                            }
                        });
                    });
                }
                response.IsPassed = true;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableNumbers", ex, request.oLoginRequest.Username);
            }

            return response;
        }

        public static Contracts.ValidateIDBookingResponse ValidateIDBooking(Contracts.ValidateIDBookingRequest request)
        {
            Contracts.ValidateIDBookingResponse response = new Contracts.ValidateIDBookingResponse()
            {
                IsPassed = false,
                ResponseMessage = string.Empty,
                AvailableNumber = new Contracts.AvailableNumber() { MSISDN = string.Empty, VanityType = new VanityType() },
                IDBookingStatus = Contracts.IDBookingStatus.There_Is_No_Booking,
                IDBookingStatusMessage = string.Empty
            };

            try
            {
                string name = "";
                AdministrationServices.Requester dealer = null;
                double appVersion = double.Parse(request.LoginRequest.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request.LoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    var validateIDBookingResponse = client.ValidateIDBooking
                        (new ActivationService.ValidateIDBookingRequest()
                        {
                            CustomerID = request.CustomerID,
                            DealerCode = dealer.Code,
                            Brand = (int)request.Brand
                        });

                    if (validateIDBookingResponse.AvailableNumber != null)
                    {
                        response.AvailableNumber.MSISDN = validateIDBookingResponse.AvailableNumber.MSISDN;
                        response.AvailableNumber.VanityType.ID = validateIDBookingResponse.AvailableNumber.vanityTypeDetails.ID;
                        response.AvailableNumber.VanityType.NameEn = validateIDBookingResponse.AvailableNumber.vanityTypeDetails.NameEN;
                        response.AvailableNumber.VanityType.NameAR = validateIDBookingResponse.AvailableNumber.vanityTypeDetails.NameAR;
                        response.AvailableNumber.VanityType.Price = validateIDBookingResponse.AvailableNumber.vanityTypeDetails.Price;
                    }
                    response.IDBookingStatus = GetIDBookingStatus(validateIDBookingResponse.IDBookingStatus);
                    response.IDBookingStatusMessage = (response.IDBookingStatus.ToString()).Replace('_', ' ');
                }

                response.IsPassed = true;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateIDBooking", ex, request.LoginRequest.Username);
            }

            return response;
        }

        private static Contracts.IDBookingStatus GetIDBookingStatus(ActivationService.IDBookingStatus iDBookingStatus)
        {
            switch (iDBookingStatus)
            {
                case ActivationService.IDBookingStatus.Success:
                    return Contracts.IDBookingStatus.Success;
                case ActivationService.IDBookingStatus.MSISDN_Unavailable:
                    return Contracts.IDBookingStatus.MSISDN_Unavailable;
                case ActivationService.IDBookingStatus.Expired_Still_Available:
                    return Contracts.IDBookingStatus.Expired_Still_Available;
                case ActivationService.IDBookingStatus.Brand_Mismatch:
                    return Contracts.IDBookingStatus.Brand_Mismatch;
                default:
                    return Contracts.IDBookingStatus.There_Is_No_Booking;
            }
        }

        public static BookNumberResponse BookNumber(Contracts.BookNumberRequest request)
        {
            BookNumberResponse response = new BookNumberResponse()
            {
                IsSuccess = false,
                IsPassed = false,
                ResponseMessage = string.Empty
            };

            try
            {
                string name = "";
                AdministrationServices.Requester dealer = null;
                double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request.oLoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    var isSuccess = client.BookNumber(new ActivationService.BookNumberRequest()
                    {
                        BookedMSISDN = request.BookedMSISDN,
                        Vanity = request.Vanity,
                        CustomerID = request.CustomerID,
                        Email = request.Email,
                        IDTypeCode = request.IDTypeCode,
                        DealerCode = dealer.Code
                    });

                    response.IsSuccess = isSuccess;
                }

                response.IsPassed = true;
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
                }
            }
            catch (LPBizException LPBizEx)
            {
                response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, request.oLoginRequest.language);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("BookNumber", ex, request.oLoginRequest.Username);
            }

            return response;
        }

        public static CancelBookingResponse CancelBookingNumber(Contracts.CancelBookingNumberRequest request)
        {
            CancelBookingResponse response = new CancelBookingResponse()
            {
                IsSuccess = false,
                IsPassed = false,
                ResponseMessage = string.Empty
            };

            try
            {
                string name = "";
                AdministrationServices.Requester dealer = null;
                double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request.oLoginRequest, out dealer, out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }

                using (ActivationServiceClient client = new ActivationServiceClient())
                {
                    var isSuccess = client.CancelBookingNumber(new ActivationService.CancelBookingNumberRequest()
                    {
                        BookedMSISDN = request.BookedMSISDN,
                        CustomerID = request.CustomerID,
                        DealerCode = dealer.Code
                    });

                    response.IsSuccess = isSuccess;
                }

                response.IsPassed = true;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelBookingNumber", ex, request.oLoginRequest.Username);
            }

            return response;
        }

        public static bool CanPOSActivateDigital(AdministrationServices.Requester requester)
        {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["WalletDeductionRequesterClassIds"]))
            {
                return false;
            }
            List<int> requesterClassIds = ConfigurationManager.AppSettings["WalletDeductionRequesterClassIds"]
                .Split(',').Select(e => int.Parse(e)).ToList();
            return requesterClassIds.Contains(requester.DistributerId)
                && IsDealerHasRight(ConfigurationManager.AppSettings["WalletDeductionRight"], requester);
        }

        public static bool CheckPaymentStatus(int? paymentTransactionID, string email = "")
        {
            bool isPaid = false;

            try
            {
                if (paymentTransactionID.HasValue && paymentTransactionID.Value != 0)
                {
                    PaymentAPIs.PaymentTransaction transaction;

                    using (PaymentAPIs.PaymentAPIsClient client = new PaymentAPIs.PaymentAPIsClient())
                    {
                        client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["PaymentServiceUsername"];
                        client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["PaymentServicePassword"];
                        transaction = client.CheckPaymentStatus(paymentTransactionID.Value);
                    }

                    if (transaction != null)
                    {
                        isPaid = transaction.PaymentStatus == PaymentAPIs.PaymentStatus.PaymentConfirmed ? true : false;
                        ErrorHandling.AddToErrorLog("CheckPaymentStatus", Convert.ToString(paymentTransactionID.Value),
                               "CheckPaymentStatus", email, $"PaymentStatus {transaction.PaymentStatus}, isPaid {isPaid}");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog("CheckPaymentStatus-Ex", ex.Message,
                               "CheckPaymentStatus", email, paymentTransactionID.HasValue ? Convert.ToString(paymentTransactionID.Value) : "NoPaymentId");

                throw new LPBizException(ErrorCodes.GenericError, "CheckPaymentStatus failed");
            }

            return isPaid;
        }

        public static bool IsWalletDeductionEnabled(AdministrationServices.Requester requester,
          bool isDigital, bool isDigitalOnboardingPaid)
        {
            bool enabled = false;

            if (isDigital)
                return !isDigitalOnboardingPaid && CanPOSActivateDigital(requester);

            return enabled;
        }

        public static List<DealerRightsDTO> MapDealerRights(List<AdministrationServices.Right> omsRights)
        {
            List<DealerRightsDTO> dealerRightsDTO = new List<DealerRightsDTO>();
            foreach (var omsRight in omsRights)
            {
                DealerRightsDTO dealerRightDTO = new DealerRightsDTO()
                {
                    Code = omsRight.Code,
                    Name = omsRight.Name
                };

                dealerRightsDTO.Add(dealerRightDTO);
            }

            return dealerRightsDTO;
        }

        public static IsAttendanceVerificationRequiredResponse IsAttendanceVerificationRequired(LoginRequest request)
        {
            IsAttendanceVerificationRequiredResponse response = new IsAttendanceVerificationRequiredResponse() { IsPassed = true };
            try
            {
                double appVersion = double.Parse(request.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request, out AdministrationServices.Requester dealer,
                    out string name, !(appVersion >= CurrentAndriodApplicationMinimalVersion));
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }

                bool IsModernTrade = ModernTradeController.IsModernTradeDealer(request.Username);
                if (IsModernTrade)
                {
                    using (AttendenceEntities context = new AttendenceEntities())
                    {
                        ObjectParameter result = new ObjectParameter("Result", typeof(int));
                        context.IsVerificationRequired(request.Username, result);

                        response.IsAttendanceVerificationRequired = (int)result.Value == 1;
                        response.ResponseMessage = "OK";
                    }
                }
                else
                {
                    response.IsAttendanceVerificationRequired = false;
                    response.ResponseMessage = "Not Modern Trade Dealer";
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsAttendanceVerificationRequired", ex, request.Username);
            }

            return response;
        }

        public static STCCLoginResponse STCCLogin(STCCLoginRequest request)
        {
            STCCLoginResponse oLoginResponse = new STCCLoginResponse();

            try
            {
                string sessionToken = string.Empty;
                ExtendedService.Response extendedResponse = new ExtendedService.Response() { IsSuccess = true };
                var oRequester = OMSProxy.GetRequesterByNameWithoutCache(request.STCCEmployeeUserName);

                if (oRequester != null && !oRequester.IsActive)
                {
                    STCC_Trace_Log(request, "Requester Is not Active",
                           "oRequester.Address: " + oRequester.Address + ", request.EmployeeCivilId: " + request.EmployeeCivilId);
                    return new STCCLoginResponse() { ResponseCode = 998 };
                }

                if (oRequester == null)
                {
                    using (ExtendedService.ExtendedServiceClient client = new ExtendedService.ExtendedServiceClient())
                    {
                        extendedResponse = client.InsertRequester(new ExtendedService.Requester()
                        {
                            Code = request.STCCEmployeeUserName,
                            ContactMSISDN = request.EmployeeContactNumber,
                            Name = request.STCCEmployeeUserName,
                            Email = request.EmployeeOrgId,
                            FaxNumber = request.EmployeeOrgId,
                            Address = request.EmployeeCivilId,
                            ContactPerson = "System",
                            RefillMSISDN = ConfigurationManager.AppSettings["STCC_RefillMSISDN"],
                            ContactNos = request.STCCUserReferenceNumber,
                            SalesPersons = request.STCCUserReferenceNumber,
                            UserName = request.STCCEmployeeUserName,
                            CreatedBy = "System",
                            AccessProfileID = int.Parse(ConfigurationManager.AppSettings["STCC_AccessProfileID"]),
                            AccessProfileStartDate = DateTime.Now,
                            IsActive = true,
                            GroupId = int.Parse(ConfigurationManager.AppSettings["STCC_GroupId"]),
                            RegionID = int.Parse(ConfigurationManager.AppSettings["STCC_RegionID"]),
                            Location = ",",
                            RequesterClassName = ConfigurationManager.AppSettings["STCC_RequesterClassName"],
                            PoSTypeID = int.Parse(ConfigurationManager.AppSettings["STCC_PoSTypeID"]),
                            ChannelTypeID = 1,
                            DistributerId = int.Parse(ConfigurationManager.AppSettings["STCC_RequesterClassName"]),
                            CityID = request.RegionCode
                        });
                    }
                }
                else
                {
                    if (oRequester.Address != request.EmployeeCivilId)
                    {
                        STCC_Trace_Log(request, "Address of request not equal to exist address",
                            "oRequester.Address: " + oRequester.Address + ", request.EmployeeCivilId: " + request.EmployeeCivilId);
                        return new STCCLoginResponse() { ResponseCode = 999 };
                    }
                }

                if (!extendedResponse.IsSuccess)
                {
                    STCC_Trace_Log(request, "Insert Requester Failed", JsonConvert.SerializeObject(extendedResponse));

                    return new STCCLoginResponse()
                    {
                        ResponseCode = MapOMSResponseCode(extendedResponse.ResponseCode),
                        ResponseMsgEN = extendedResponse.Message,
                        ResponseMsgAR = extendedResponse.Message
                    };
                }

                AMErrorCode responseCode = AMService.StartSession(request.STCCEmployeeUserName, request.STCCEmployeeUserName,
                    string.Empty, string.Empty, request.LiveLocation_Lat,
                    request.LiveLocation_Lon, "STCC",
                    Thread.CurrentThread.CurrentCulture.Name == "en-GB" ? "English" : "Arabic", "",
                    "", CurrentAndriodApplicationVersion, CurrentIOSApplicationVersion,
                    out sessionToken, out int nextSessionTimeCheck, out User user);

                if (responseCode != AMErrorCode.Success)
                {
                    oLoginResponse.ResponseCode = (int)responseCode;
                    oLoginResponse.ResponseMsgEN = oLoginResponse.ResponseMsgAR = GetAuthenticationManagementMessage(responseCode);

                    STCC_Trace_Log(request, "Start Session Failed", JsonConvert.SerializeObject(oLoginResponse));
                    return oLoginResponse;
                }
                else
                {
                    using (var client = new ActivationServiceClient())
                    {
                        ActivateSIMSTCCSTCCLoginToSematiRequest loginRequest = new ActivateSIMSTCCSTCCLoginToSematiRequest()
                        {
                            Fingerprint = new ActivationService.FingerPrint()
                            {
                                DeviceId = "",
                                FingerPosition = ActivationService.FingerPosition.NotSet,
                                FingerPrintText = "",
                                NFIQValue = 0,
                                IsSematiOTPUsed = !string.IsNullOrEmpty(request.IAM_OTP),
                                SematiOTP = request.IAM_OTP,
                            },
                            Latitude = request.LiveLocation_Lat.ToString(),
                            Longitude = request.LiveLocation_Lon.ToString(),
                            Channel = "STCC",
                            DealerCode = request.STCCEmployeeUserName,
                            SessionToken = sessionToken
                        };

                        if (client.STCCLoginToSemati(loginRequest))
                        {
                            oLoginResponse.ResponseCode = (int)AMErrorCode.Success;
                            oLoginResponse.SessionToken = sessionToken;
                        }
                        else
                        {
                            oLoginResponse.ResponseCode = (int)AMErrorCode.SessionInvalid;
                            oLoginResponse.ResponseMsgEN = oLoginResponse.ResponseMsgAR
                                = GetAuthenticationManagementMessage(AMErrorCode.SessionInvalid);

                            STCC_Trace_Log(request, "STCC Login To Semati failed", JsonConvert.SerializeObject(oLoginResponse));
                        }
                    }
                }
            }
            catch (LPBizException LPBizEx)
            {
                oLoginResponse.ResponseCode = Convert.ToInt32(LPBizEx.Code);
                oLoginResponse.ResponseMsgEN = ExceptionHandling.HandleBackendException(LPBizEx, 1);

                STCC_Trace_Log(request, "LPBizException", LPBizEx.ToString() + ", LoginResponse: " +
                    JsonConvert.SerializeObject(oLoginResponse));
            }
            catch (FaultException ex)
            {
                STCC_Trace_Log(request, "FaultException", ex.ToString() + ", LoginResponse: " +
                    JsonConvert.SerializeObject(oLoginResponse));

                //LPBizException LPBizEx = new LPBizException(ex.Code.Name, ex.Message);
                //oLoginResponse.ResponseCode = Convert.ToInt32(LPBizEx.Code);
                //oLoginResponse.ResponseMsgEN = ExceptionHandling.HandleBackendException(LPBizEx, 1);

                const string CustomError = "-1111111";
                string[] parts = ex.Code.Name.Split('_');
                LPBizException LPBizEx = new LPBizException(parts[0], ex.Message);
                ErrorHandling.HandleException("LoginToSemati", LPBizEx, request.STCCEmployeeUserName);

                oLoginResponse.ResponseCode = Convert.ToInt32(parts[0]);
                if (ex.Code.Name == CustomError)
                {
                    oLoginResponse.ResponseMsgEN = LPBizEx.Message;
                    oLoginResponse.RemainingAttempts = -1; // Show message as is 
                }
                else
                {
                    oLoginResponse.RemainingAttempts = parts.Length == 2 ? int.Parse(parts[1]) : -1;
                    oLoginResponse.ResponseMsgEN = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, (int)eLanguage.English);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("STCCLogin", ex, request.STCCEmployeeUserName);
                oLoginResponse.ResponseCode = -1000;
                oLoginResponse.ResponseMsgEN = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);

                STCC_Trace_Log(request, "Exception", ex.ToString() + ", LoginResponse: " +
                    JsonConvert.SerializeObject(oLoginResponse));
            }

            return oLoginResponse;
        }

        public static void STCC_Trace_Log(STCCLoginRequest request = null, string description = "", string extraDetails = "")
        {
            ErrorHandling.AddToErrorLog(ErrorTypes.Information, description, "STCC_Trace_Log", request.STCCEmployeeUserName,
                "Request: " + JsonConvert.SerializeObject(request) + ", Extra Details: " + extraDetails);
        }

        public static GetSubscriptionTypeResponse GetSubscriptionType(GetSubscriptionTypeRequest request)
        {
            GetSubscriptionTypeResponse response = new GetSubscriptionTypeResponse() { IsPassed = true };
            try
            {
                double appVersion = double.Parse(request.oLoginRequest.ApplicationVersion);
                CallResponse oCallResponse = CheckRequester(request.oLoginRequest, out AdministrationServices.Requester dealer,
                    out string name, !(appVersion >= CurrentAndriodApplicationMinimalVersion));
                if (oCallResponse.IsPassed == false)
                {
                    response.ResponseMessage = oCallResponse.ResponseMessage;
                    return response;
                }


                using (var client = new ActivationServiceClient())
                {
                    var actResponse = client.GetSubscriptionType(request.MSISDN);
                    response.IsPrepaid = actResponse.IsPrepaid;
                    response.DisplayName = actResponse.DisplayName;
                    response.DisplayNameAr = actResponse.DisplayNameAr;
                    response.InitialBalance = actResponse.InitialBalance;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSubscriptionType", ex, request.oLoginRequest.Username);
            }

            return response;
        }

        public static int MapOMSResponseCode(int code)
        {
            switch (code)
            {
                case 0:
                    return 0;
                case 1:
                case 6:
                    return 1;
                case 2:
                    return -1000;
                case 3:
                    return -1001;
                case 4:
                    return -1002;
                case 5:
                    return -1003;
                default:
                    return 1;
            }
        }
    }
}



