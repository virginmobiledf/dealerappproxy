﻿using LP.Core.Utilities.Exceptions;
using LP.OMS.Channels.Engine.Models;
using LP.OMS.Channels.Engine.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Resources;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using LP.OMS.Channels.Contracts;
using System.Globalization;
using LP.OMS.Channels.Engine.ActivationService;
using LP.OMS.Channels.Engine.Proxies;
using LP.OMS.Channels.Engine.Notification;
using static LP.OMS.Channels.Engine.Enums;

namespace LP.OMS.Channels.Engine
{
    public static class ModernTradeController
    {
        #region Configurations Initialization

        private static ResourceManager objResourceManager;
        public static bool IS_ATTENDANCE_ENABLED;
        public static string MT_RESEND_LOCATION_PERIOD;
        public static int LOCATION_TRACKING_MODE;
        public static List<string> requesterIdsGroupsIds;
        public static bool EnableMTTrackProcessMode;
        public static string VirginGeoLocationBaseAddress;
        public static List<string> DealersWithStaticLongitudeLatitudeValues;
        public static List<string> DefaultLongitudeLatitudeLocations;
        public static bool EnableCheckInLcationHotfix;
        public static string MTLocationsConfiguration;
        public static int MT_NumberOfVerfications;
        public static int MT_FirstReminderVerficationsInMin;
        public static int MT_SecondVerficationsInMin;
        public static int MT_VerficationsDueTimeInMin;
        public static int AttendanceWorkingHours;

        static ModernTradeController()
        {
            Assembly objAssembly = Assembly.GetExecutingAssembly();
            objResourceManager = new ResourceManager("LP.OMS.Channels.Engine.Resources.OMSResources", objAssembly);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["IsAttendanceEnabled"]))
                IS_ATTENDANCE_ENABLED = Convert.ToBoolean(ConfigurationManager.AppSettings["IsAttendanceEnabled"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MTResendLocationPeriod"]))
                MT_RESEND_LOCATION_PERIOD = ConfigurationManager.AppSettings["MTResendLocationPeriod"];

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ModernTradeRequesterIDsGroupIDs"]))
                requesterIdsGroupsIds = ConfigurationManager.AppSettings["ModernTradeRequesterIDsGroupIDs"].Split('-').ToList();

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LocationTrackingMode"]))
                LOCATION_TRACKING_MODE = Convert.ToInt32(ConfigurationManager.AppSettings["LocationTrackingMode"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableMTTrackProcessMode"]))
                EnableMTTrackProcessMode = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableMTTrackProcessMode"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["VirginGeoLocationBaseAddress"]))
                VirginGeoLocationBaseAddress = ConfigurationManager.AppSettings["VirginGeoLocationBaseAddress"];

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DealersWithStaticLongitudeLatitudeValues"]))
                DealersWithStaticLongitudeLatitudeValues = ConfigurationManager.AppSettings["DealersWithStaticLongitudeLatitudeValues"].Split(',').ToList();

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultLongitudeLatitudeLocations"]))
                DefaultLongitudeLatitudeLocations = ConfigurationManager.AppSettings["DefaultLongitudeLatitudeLocations"].Split('-').ToList();

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EnableCheckInLcationHotfix"]))
                EnableCheckInLcationHotfix = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableCheckInLcationHotfix"]);

            MTLocationsConfiguration = ConfigurationManager.AppSettings["MT_Locations_Configuration"];

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MT_NumberOfVerfications"]))
                MT_NumberOfVerfications = Convert.ToInt32(ConfigurationManager.AppSettings["MT_NumberOfVerfications"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MT_FirstReminderVerficationsInMin"]))
                MT_FirstReminderVerficationsInMin = Convert.ToInt32(ConfigurationManager.AppSettings["MT_FirstReminderVerficationsInMin"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MT_SecondVerficationsInMin"]))
                MT_SecondVerficationsInMin = Convert.ToInt32(ConfigurationManager.AppSettings["MT_SecondVerficationsInMin"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MT_VerficationsDueTimeInMin"]))
                MT_VerficationsDueTimeInMin = Convert.ToInt32(ConfigurationManager.AppSettings["MT_VerficationsDueTimeInMin"]);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Attendance.WorkingHours"]))
                AttendanceWorkingHours = Convert.ToInt32(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Attendance.WorkingHours"]);
        }

        #endregion

        #region Methods

        //
        // Summary:
        //     Represents Is Dealer MT user.
        public static bool IsModernTradeDealer(string requesterUsername)
        {
            bool IsModernTrade = false;

            if (requesterIdsGroupsIds != null && requesterIdsGroupsIds.Count > 0)
            {
                using (OMSCommonServices.CommonServicesClient client = new OMSCommonServices.CommonServicesClient())
                {
                    using (UserContextScope userScope = new UserContextScope(client.InnerChannel, ""))
                    {
                        OMSCommonServices.Requester requester = client.GetRequesterByName(requesterUsername);
                        if (requester != null)
                        {
                            string requesterID = string.Empty;
                            string groupId = string.Empty;
                            foreach (string item in requesterIdsGroupsIds)
                            {
                                requesterID = item.Split(',')[0];
                                groupId = item.Split(',')[1];
                                if (requester.DistributerId.ToString() == requesterID &&
                                    requester.GroupId.ToString() == groupId && requester.IsActive)
                                {
                                    IsModernTrade = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return IsModernTrade;
        }

        //
        // Summary:
        //     Represents If Dealer have to Check In before continue.
        public static bool IsDealerCheckInRequired(User user, string dealerCode, bool IsModernTradeuser, string sessionToken,
            out string locationID)
        {
            bool IsCheckInRequired = false;
            locationID = string.Empty;

            try
            {
                if (IS_ATTENDANCE_ENABLED && IsModernTradeuser)
                {
                    using (AttendenceEntities context = new AttendenceEntities())
                    {
                        if (((DateTime.Now >= user.StartTimePeriod1 && DateTime.Now < user.EndTimePeriod1)
                             || (DateTime.Now >= user.StartTimePeriod2 && DateTime.Now < user.EndTimePeriod2)))
                        {
                            var result = context.GetActiveAttendance_V2(dealerCode).FirstOrDefault();

                            if (result != null)
                            {
                                Session userSession = AccessManagementController.GetSession(sessionToken);
                                locationID = result.LocationID;
                                string AttendanceSessionID = result.SessionID;

                                if (userSession != null)
                                {
                                    if (userSession.SessionToken.ToLower() != AttendanceSessionID.ToLower())
                                    {
                                        // link session with new attendance ID
                                        userSession.AttendanceId = result.ID.ToString();
                                        AMErrorCode code = AMService.UpdateSessionData(sessionToken, userSession);

                                        if (EnableMTTrackProcessMode)
                                            ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, string.Format("UpdateSessionData : code : {0}", code), "MT-GetActiveAttendance_V2-UpdateSessionData", dealerCode);

                                        if (code == AMErrorCode.Success)
                                        {
                                            context.UpdateActiveAttendanceSessionID(result.ID, userSession.SessionToken);
                                        }
                                    }
                                }
                            }
                            else // No Active Record
                            {
                                IsCheckInRequired = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsDealerCheckInRequired", ex, dealerCode);
            }

            return IsCheckInRequired;
        }

        //
        // Summary:
        //     GetNearestLocations From Geo Location API.
        public static Contracts.NearestLocationsResponse GetNearestLocations(Contracts.LoginRequest LoginRequest)
        {
            string API_URL = "Geo/GetNearestStores?sLatitude={0}&sLongitude={1}";
            Contracts.NearestLocationsResponse response = new Contracts.NearestLocationsResponse();
            response.Locations = new List<Contracts.Location>();
            try
            {
                Contracts.CallResponse oCallResponse = new Contracts.CallResponse();

                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = Controller.CheckRequester(LoginRequest, out oRequester, out userName, false);

                if (oCallResponse.IsPassed)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(VirginGeoLocationBaseAddress);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        if (DealersWithStaticLongitudeLatitudeValues != null && DealersWithStaticLongitudeLatitudeValues.Count > 0)
                        {
                            if (DealersWithStaticLongitudeLatitudeValues.Contains(oRequester.UserName.ToLower()))
                            {
                                LoginRequest.Latitude = 24.64625;
                                LoginRequest.Longitude = 46.71757;
                            }
                        }

                        HttpResponseMessage ApiResponse = client.GetAsync(string.Format(API_URL, LoginRequest.Latitude, LoginRequest.Longitude)).Result;

                        if (ApiResponse.IsSuccessStatusCode)
                        {
                            var apiResponseContent = ApiResponse.Content.ReadAsStringAsync().Result;
                            List<dynamic> result = JsonConvert.DeserializeObject<List<dynamic>>(apiResponseContent);

                            if (result != null && result.Count > 0)
                            {
                                foreach (var item in result)
                                {
                                    response.Locations.Add(new Contracts.Location
                                    {
                                        FriendiSale = item.FriendiSale == "Yes" ? true : false,
                                        VirginSale = item.VirginSale == "Yes" ? true : false,
                                        Latitude = item.Latitude,
                                        Longitude = item.Longitude,
                                        LocationName = (LoginRequest.language == 2 && item.LocationName_ar != null) ? item.LocationName_ar : item.LocationName_en,
                                        LocationId = item.Location_id
                                    });
                                }
                            }
                        }
                    }

                    response.IsPassed = true;
                    response.ResponseMessage = "Done Successfully";
                }

                if (EnableMTTrackProcessMode)
                {
                    ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, string.Format("IsPassed value : {0}", response.IsPassed), "MT-GetNearestLocations", LoginRequest.Username);
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetNearestLocations", ex, LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        //
        // Summary:
        //     Attendance Tracking For All Processes.
        public static Contracts.BaseResponse AttendanceTracking(Contracts.AttendanceTrackingRequest request)
        {
            Contracts.BaseResponse response = new Contracts.BaseResponse();
            DateTime startShift;
            DateTime performedAt;
            startShift = performedAt = DateTime.Now;
            DateTime endShift = startShift.AddHours(AttendanceWorkingHours);

            try
            {
                Contracts.CallResponse oCallResponse = new Contracts.CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = Controller.CheckRequester(request.LoginRequest, out oRequester, out userName, false);
                AMErrorCode code;

                if (oCallResponse.IsPassed)
                {
                    try
                    {
                        using (AttendenceEntities context = new AttendenceEntities())
                        {
                            context.AddAuditLog("AttendanceTracking", "Enter",
                                $"userName:{oRequester.UserName},request.AttendanceProcessType:{request.AttendanceProcessType},CheckRequester:true");
                            ObjectParameter AttendanceId = new ObjectParameter("iD", typeof(int));
                            Session userSession = AccessManagementController.GetSession(request.LoginRequest.Token);
                            var lastActiveRecord = context.GetActiveAttendance_V2(oRequester.UserName).FirstOrDefault();
                            var lastInActiveRecord = context.GetLastInActiveAttendance(oRequester.UserName).FirstOrDefault();

                            if (lastActiveRecord != null)
                            {
                                if (request.AttendanceProcessType != (int)AttendanceProcessType.CheckIn)
                                {
                                    startShift = lastActiveRecord.WorkingHours_Start;
                                    endShift = lastActiveRecord.WorkingHours_End;
                                }
                            }
                            else
                            {
                                if (lastInActiveRecord != null)
                                {
                                    if (request.AttendanceProcessType == (int)AttendanceProcessType.CheckIn)
                                    {
                                        if (DateTime.Now < lastInActiveRecord.PerformedAt.AddHours(AttendanceWorkingHours))
                                        {
                                            startShift = lastInActiveRecord.WorkingHours_Start;
                                            endShift = lastInActiveRecord.WorkingHours_End;
                                        }
                                    }
                                }
                            }

                            switch (request.AttendanceProcessType)
                            {
                                case (int)Contracts.AttendanceProcessType.CheckIn:
                                    if (EnableCheckInLcationHotfix)
                                    {
                                        request.Longitude = request.LoginRequest.Longitude;
                                        request.Latitude = request.LoginRequest.Latitude;
                                    }
                                    else
                                    {
                                        //if (DefaultLongitudeLatitudeLocations != null && DefaultLongitudeLatitudeLocations.Count > 0)
                                        //{
                                        //    foreach (var item in DefaultLongitudeLatitudeLocations)
                                        //    {
                                        //        double Longitude = Convert.ToDouble(item.Split(',')[0]);
                                        //        double Latitude = Convert.ToDouble(item.Split(',')[1]);

                                        //        if (request.Longitude == Longitude && request.Latitude == Latitude)
                                        //        {
                                        //            request.Longitude = request.LoginRequest.Longitude;
                                        //            request.Latitude = request.LoginRequest.Latitude;
                                        //            break;
                                        //        }
                                        //    }
                                        //}
                                    }
                                    context.Insert_Attendance_V2(oRequester.UserName, request.LoginRequest.LocationId, request.Latitude.ToString(),
                                    request.Longitude.ToString(), request.AttendanceProcessType, performedAt,
                                    startShift, endShift,
                                    request.LoginRequest.Token, request.LoginRequest.MobileDeviceId, null,
                                    false, IsModernTradeDealer(oRequester.UserName), false, oRequester.DistributerId.ToString(), request.AttendanceDoneThrough, AttendanceId);

                                    userSession.AttendanceId = AttendanceId.Value.ToString();
                                    code = AMService.UpdateSessionData(request.LoginRequest.Token, userSession); // link with session table
                                    context.AddAuditLog("AttendanceTracking", "Insert_Attendance",
                                        $"userName:{oRequester.UserName},request.AttendanceProcessType:{request.AttendanceProcessType}," +
                                        $"AttendanceId:{userSession.AttendanceId},UpdateSessionDataCode:{code}");
                                    if (code != AMErrorCode.Success)
                                    {
                                        response.ResponseMessage = Controller.GetAuthenticationManagementMessage(code);
                                        response.IsPassed = false;
                                    }
                                    else
                                    {
                                        var result = context.GetActiveNotificationAttendance_V2(oRequester.UserName).FirstOrDefault();
                                        string str_result = result == null ? "Null" : result.Id.ToString();
                                        context.AddAuditLog("AttendanceTracking", "GetActiveNotificationAttendance",
                                      $"userName:{oRequester.UserName},request.AttendanceProcessType:{request.AttendanceProcessType}," +
                                      $"AttendanceId:{userSession.AttendanceId},GetActiveNotificationAttendance:{str_result}");
                                        if (result == null)
                                        {
                                            bool addVerifications = false;

                                            if (lastInActiveRecord != null)
                                            {
                                                if (DateTime.Now > lastInActiveRecord.PerformedAt.AddHours(AttendanceWorkingHours))
                                                    addVerifications = true;
                                            }
                                            else
                                                addVerifications = true;

                                            if (addVerifications)
                                                PopulateVerficationNotifications(context, AttendanceId, userSession, performedAt);
                                        }

                                        HRMS_Proxy.AddAttendanceEntry(new AddAttendanceRequest()
                                        {
                                            AttendanceDetails = new AttendanceDetails()
                                            {
                                                EntryType = "1",
                                                Location = GetLocationByID(request.LoginRequest.LocationId, 1,
                                                    oRequester.UserName)?.LocationName,
                                                UserCode = oRequester.FaxNumber
                                            }
                                        });
                                    }

                                    if (EnableMTTrackProcessMode)
                                        ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, string.Format("AttendanceTracking-UpdateSessionData code : {0} , AttendanceId {1}  , Process ID  {2}", code, AttendanceId.Value, request.AttendanceProcessType), "AttendanceTracking", request.LoginRequest.Username);

                                    response.ResponseMessage = "Done Successfully";
                                    response.IsPassed = true;
                                    context.AddAuditLog("AttendanceTracking", "Exist",
                                        $"userName:{oRequester.UserName},request.AttendanceProcessType:{request.AttendanceProcessType}," +
                                        $"AttendanceId:{userSession.AttendanceId},code:{code}");
                                    break;

                                case (int)Contracts.AttendanceProcessType.CheckOut:
                                    if (lastActiveRecord.LocationID == request.LoginRequest.LocationId)
                                    {
                                        context.Insert_Attendance_V2(oRequester.UserName, request.LoginRequest.LocationId, request.Latitude.ToString(),
                                        request.Longitude.ToString(), request.AttendanceProcessType, performedAt, startShift,
                                        endShift, request.LoginRequest.Token, request.LoginRequest.MobileDeviceId, null,
                                        false, IsModernTradeDealer(oRequester.UserName), false, oRequester.DistributerId.ToString(), null, AttendanceId);

                                        context.UpdateDealerActiveAttendanceForCheckOut(request.LoginRequest.Username, AttendanceId.Value.ToString());
                                        userSession.AttendanceId = string.Empty;
                                        code = AMService.UpdateSessionData(request.LoginRequest.Token, userSession); // remove attendance id from session table

                                        if (code != AMErrorCode.Success)
                                        {
                                            response.ResponseMessage = Controller.GetAuthenticationManagementMessage(code);
                                            response.IsPassed = false;
                                        }

                                        HRMS_Proxy.AddAttendanceEntry(new AddAttendanceRequest()
                                        {
                                            AttendanceDetails = new AttendanceDetails()
                                            {
                                                EntryType = "2",
                                                Location = GetLocationByID(request.LoginRequest.LocationId, 1,
                                                    oRequester.UserName)?.LocationName,
                                                UserCode = oRequester.FaxNumber
                                            }
                                        });

                                        response.ResponseMessage = "Done Successfully";
                                        response.IsPassed = true;
                                    }
                                    else
                                    {
                                        response.ResponseMessage = Controller.GetResourceMessage("Wrong_Location_QR",
                                            request.LoginRequest.language);
                                        response.IsPassed = false;
                                    }
                                    break;

                                case (int)Contracts.AttendanceProcessType.Login:
                                case (int)Contracts.AttendanceProcessType.Logout:
                                case (int)Contracts.AttendanceProcessType.GeoLocationTracking:
                                case (int)Contracts.AttendanceProcessType.CheckInAttemptOutsideAllowedRadius:
                                case (int)Contracts.AttendanceProcessType.CheckOutAttemptOutsideAllowedRadius:
                                case (int)Contracts.AttendanceProcessType.VerifyAttemptOutsideAllowedRadius:

                                    context.Insert_Attendance_V2(oRequester.UserName, request.LoginRequest.LocationId, request.Latitude.ToString(),
                                    request.Longitude.ToString(), request.AttendanceProcessType, performedAt,
                                    startShift, endShift,
                                    request.LoginRequest.Token, request.LoginRequest.MobileDeviceId, null,
                                    false, IsModernTradeDealer(oRequester.UserName), false, oRequester.DistributerId.ToString(), null, AttendanceId);

                                    response.ResponseMessage = "Done Successfully";
                                    response.IsPassed = true;
                                    break;

                                case (int)Contracts.AttendanceProcessType.Verfication:

                                    lastActiveRecord = context.GetActiveAttendance_V2(oRequester.UserName).FirstOrDefault();

                                    if (lastActiveRecord.LocationID == request.LoginRequest.LocationId)
                                    {
                                        context.Insert_Attendance_V2(oRequester.UserName, request.LoginRequest.LocationId, request.Latitude.ToString(),
                                                                          request.Longitude.ToString(), request.AttendanceProcessType, performedAt,
                                                                          startShift, endShift,
                                                                          request.LoginRequest.Token, request.LoginRequest.MobileDeviceId, null,
                                                                          false, IsModernTradeDealer(oRequester.UserName), false, oRequester.DistributerId.ToString(), null, AttendanceId);

                                        string result = context.MarkRecordAsVerified(oRequester.UserName, (int)AttendanceId.Value).SingleOrDefault();
                                        response.IsPassed = string.IsNullOrEmpty(result);
                                        if (response.IsPassed)
                                            response.ResponseMessage = "Done Successfully";
                                        else
                                            response.ResponseMessage = result;

                                    }
                                    else
                                    {
                                        response.ResponseMessage = Controller.GetResourceMessage("Wrong_Location_QR", request.LoginRequest.language);
                                        response.IsPassed = false;
                                    }

                                    break;
                                default:
                                    response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                                    response.IsPassed = false;
                                    break;
                            }

                            if (EnableMTTrackProcessMode)
                            {
                                ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError,
                                string.Format("AttendanceTracking-Response Is Passed : {0} , Process Type : {1} , Attendance ID : {2} , ResponseMessage : {3}", response.IsPassed,
                                request.AttendanceProcessType, AttendanceId.Value, response.ResponseMessage),
                                "AttendanceTracking", request.LoginRequest.Username);
                            }

                            if (IsModernTradeDealer(oRequester.UserName))
                            {
                                string contactMSISDN = OMSProxy.GetDealerContactMSISDN(oRequester.UserName);
                                NEProxy.UpdateCustomerPreferences(
                                    new UpdateCustomerPreferencesRequest()
                                    {
                                        AndroidToken = new List<string>() { request.LoginRequest.FirebaseToken },
                                        MSISDN = new List<string>() { contactMSISDN },
                                        Lang = oRequester.LanguagePreferenceIsEnglish ? "en" : "ar",
                                        DeviceId = new List<string>() { $"DeviceId-{contactMSISDN}" },
                                        OperationType = "Login",
                                        Email = $"email_{contactMSISDN}@virginmobile.sa",
                                        WaitResponse = false,
                                    }, (int)AttendanceId.Value);
                            }

                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "AttendanceTracking", request.LoginRequest.Username, ex.ToString());
                    }
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AttendanceTracking", ex, request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        private static void PopulateVerficationNotifications(AttendenceEntities context, ObjectParameter AttendanceId, Session userSession, DateTime currentDate)
        {
            if (MT_NumberOfVerfications > 0)
            {
                int workingHours = AttendanceWorkingHours;
                List<DateTime> verificationDates = context.Database.SqlQuery<DateTime>("SP_GetRandomVerificationDates_No12PMlimit {0},{1},{2}", new object[] { currentDate, MT_NumberOfVerfications, workingHours }).ToList();
                int index = 1;
                var expectedCheckoutDate = DateTime.Now.AddHours(workingHours);

                verificationDates.ForEach(vDate =>
                {
                    context.VerificationNotifications.Add(new VerificationNotification()
                    {
                        CheckInId = (int)AttendanceId.Value,
                        DateTime = currentDate,
                        ExpectedCheckOut = expectedCheckoutDate,
                        FirstReminderTime = vDate.AddMinutes(MT_FirstReminderVerficationsInMin),
                        SecondReminderTime = vDate.AddMinutes(MT_SecondVerficationsInMin),
                        NotificationSentAt = vDate,
                        VerificationStatus = 1,//Pending.
                        NotificationStatus = 1,//Pending.
                        ResponseDueOnTime = vDate.AddMinutes(MT_VerficationsDueTimeInMin),
                    });
                    index++;
                });
                if (context.SaveChanges() > 0)
                {
                    context.AddAuditLog("AttendanceTracking", "PopulateVerficationNotifications",
                                       $"userName:{userSession.User.UserName},NumberOfverificationDates:{verificationDates.Count.ToString()}," +
                                       $"AttendanceId:{userSession.AttendanceId}");
                };
            }
        }

        //
        // Summary:
        //     Attendance Tracking For All Processes.
        public static Contracts.GetLocationByIDExternalResponse GetLocationByIDExternal(Contracts.LoginRequest request)
        {
            Contracts.GetLocationByIDExternalResponse response = new Contracts.GetLocationByIDExternalResponse();

            try
            {
                Contracts.CallResponse oCallResponse = new Contracts.CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = Controller.CheckRequester(request, out oRequester, out userName, false);

                if (oCallResponse.IsPassed)
                {
                    response.IsPassed = true;
                    response.ResponseMessage = "";
                    response.Location = GetLocationByID(request.LocationId, request.language, request.Username);
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = Utilities.ExceptionHandling.HandleBackendException(LPBizEx, request.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AttendanceTracking", ex, request.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        //
        // Summary:
        //     GetMTDealerAchievementReport.
        public static DealerAchievementResponse GetMTDealerAchievementReport(DealerAchievementRequest request)
        {
            DealerAchievementResponse response = new DealerAchievementResponse()
            {
                MT_Report = new List<MT_Report>()
            };
            List<ActivationService.MTDataCustomeMTReportResponse> clientResponse
                            = new List<ActivationService.MTDataCustomeMTReportResponse>();
            List<Attendence> attendences = new List<Attendence>();
            Dictionary<string, string> locations = new Dictionary<string, string>();

            try
            {
                Period period = Controller.GetReportPeriod(ReportViewMode.CustomRequest, request.Start,
                       request.End);
                period.To = period.To.AddDays(1).AddMinutes(-1);

                CallResponse oCallResponse = new CallResponse();
                string userName = string.Empty;
                AdministrationServices.Requester oRequester = null;
                oCallResponse = Controller.CheckRequester(request?.LoginRequest,
                    out oRequester, out userName, false);

                if (oCallResponse.IsPassed)
                {
                    using (ActivationServiceClient client = new ActivationServiceClient())
                    {
                        clientResponse = client.GetTotalSIMsCashCollection(new MTDataMTReportRequest()
                        {
                            DealerCode = request?.LoginRequest?.Username,
                            Page = -1,
                            PageSize = -1,
                            Start = period.From,
                            End = period.To
                        });
                    }

                    using (AttendenceEntities db = new AttendenceEntities())
                    {
                        attendences = db.Attendences
                           .Where(e => e.DealerCode == request.LoginRequest.Username)
                           .Where(e => e.ProcessID == (int)AttendanceProcessType.CheckIn)
                           .Where(e => e.PerformedAt >= period.From && e.PerformedAt <= period.To)
                           .ToList();

                        foreach (var attendence in attendences)
                        {
                            foreach (var item in clientResponse)
                            {
                                if (attendence.PerformedAt.Day == item.Date.Day
                                    && attendence.PerformedAt.Month == item.Date.Month
                                    && response.MT_Report.FirstOrDefault(e => e.CheckInDateTime.Value.Day
                                    == attendence.PerformedAt.Day
                                    && e.CheckInDateTime.Value.Month == attendence.PerformedAt.Month) == null)
                                {
                                    var temp = new MT_Report()
                                    {
                                        CacheCollectionVatExcluded = (double)item.CostVATExcluded,
                                        CacheCollectionVatIncluded = (double)item.CostVATIncluded,
                                        CheckInDateTime = GetAttendenceDateTime(attendences,
                                        attendence.PerformedAt.Day, AttendanceProcessType.CheckIn,
                                        attendence.PerformedAt.Month),
                                        CheckOutDateTime = GetAttendenceDateTime(attendences,
                                        attendence.PerformedAt.Day, AttendanceProcessType.CheckOut,
                                        attendence.PerformedAt.Month),
                                        TotalActivationsCount = Convert.ToInt32(item.TotalActivationsCount)
                                    };

                                    if (locations.Any())
                                    {
                                        string locationName = string.Empty;
                                        locations.TryGetValue(attendence.LocationID, out locationName);

                                        if (!string.IsNullOrEmpty(locationName))
                                            temp.SalesLocationName = locationName;
                                        else
                                            temp.SalesLocationName = GetLocationByID(attendence.LocationID, 1,
                                                attendence.DealerCode)?.LocationName;
                                    }

                                    response.MT_Report.Add(temp);
                                    if (!locations.Any(e => e.Key == attendence.LocationID))
                                        locations.Add(attendence.LocationID ?? "", temp.SalesLocationName ?? "");
                                }
                            }

                            if (response.MT_Report.FirstOrDefault(e => e.CheckInDateTime.Value.Day
                                 == attendence.PerformedAt.Day
                                 && e.CheckInDateTime.Value.Month == attendence.PerformedAt.Month) == null)
                            {
                                var temp = new MT_Report()
                                {
                                    CacheCollectionVatExcluded = 0,
                                    CacheCollectionVatIncluded = 0,
                                    CheckInDateTime = GetAttendenceDateTime(attendences,
                                       attendence.PerformedAt.Day, AttendanceProcessType.CheckIn,
                                       attendence.PerformedAt.Month),
                                    CheckOutDateTime = GetAttendenceDateTime(attendences,
                                       attendence.PerformedAt.Day, AttendanceProcessType.CheckOut,
                                       attendence.PerformedAt.Month),
                                    TotalActivationsCount = 0,
                                    SalesLocationName = GetLocationByID(attendence.LocationID, 1,
                                    attendence.DealerCode)?.LocationName
                                };

                                response.MT_Report.Add(temp);
                                if (!locations.Any(e => e.Key == attendence.LocationID))
                                    locations.Add(attendence.LocationID ?? "", temp.SalesLocationName ?? "");
                            }
                        }

                        foreach (var item in response.MT_Report)
                        {
                            var dealer = db.GetMissedVerificationDealersByDate(item.CheckInDateTime,
                                request?.LoginRequest?.Username).FirstOrDefault();
                            if (string.IsNullOrEmpty(dealer))
                                item.IsVerified = true;
                        }
                    }

                    response.IsPassed = true;
                    response.ResponseMessage = "Done Successfully";
                }
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx,
                        request.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetMTDealerAchievementReport", ex,
                    request.LoginRequest.Username);
                response.IsPassed = false;
                response.ResponseMessage = objResourceManager.GetString("MSG_General_Execption",
                    Thread.CurrentThread.CurrentCulture);
            }

            return response;
        }

        public static DateTime? GetAttendenceDateTime(List<Attendence> attendences, int day,
            AttendanceProcessType attendanceProcess, int month)
        {
            var order = attendences.Where(e => e.PerformedAt.Day == day && e.PerformedAt.Month == month)
                .OrderByDescending(e => e.PerformedAt);

            if (order.Any())
            {
                if (attendanceProcess == AttendanceProcessType.CheckIn)
                    return order.LastOrDefault().PerformedAt;

                if (attendanceProcess == AttendanceProcessType.CheckOut)
                {
                    order = order.Where(e => e.CheckoutID != null && e.CheckoutID != 0)
                        .OrderByDescending(e => e.PerformedAt);

                    if (order.Any())
                    {
                        using (AttendenceEntities db = new AttendenceEntities())
                        {
                            int checkoutID = order.FirstOrDefault().CheckoutID.Value;
                            var checkout = db.Attendences.FirstOrDefault(e => e.ID == checkoutID);

                            return checkout.PerformedAt;
                        }
                    }
                }
            }

            return null;
        }

        //
        // Summary:
        //     GetDealerDefaultLocationDetails.
        public static Contracts.Location GetDealerDefaultLocationDetails(string requesterUsername, int language)
        {
            Contracts.Location location = new Contracts.Location();

            try
            {
                string API_URL = "Geo/GetLocationByDealerCode?dealerCode={0}";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(VirginGeoLocationBaseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage ApiResponse = client.GetAsync(string.Format(API_URL, requesterUsername)).Result;

                    if (ApiResponse.IsSuccessStatusCode)
                    {
                        var apiResponseContent = ApiResponse.Content.ReadAsStringAsync().Result;
                        List<dynamic> result = JsonConvert.DeserializeObject<List<dynamic>>(apiResponseContent);

                        if (result != null && result.Count > 0)
                        {
                            dynamic dealerlocationDetails = result.FirstOrDefault();
                            location.FriendiSale = dealerlocationDetails.FriendiSale == "Yes" ? true : false;
                            location.VirginSale = dealerlocationDetails.VirginSale == "Yes" ? true : false;
                            location.Latitude = dealerlocationDetails.Latitude;
                            location.Longitude = dealerlocationDetails.Longitude;
                            location.LocationName = (language == 2 && dealerlocationDetails.LocationName_ar != null) ? dealerlocationDetails.LocationName_ar : dealerlocationDetails.LocationName_en;
                            location.LocationId = dealerlocationDetails.Location_id;
                        }

                        if (EnableMTTrackProcessMode)
                        {
                            ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, string.Format("ApiResponse : {0}", ApiResponse.IsSuccessStatusCode), "GetDealerDefaultLocationDetails", requesterUsername);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "GetDealerDefaultLocationDetails", requesterUsername);
            }

            return location;

        }

        //
        // Summary:
        //     GetDealerLocationID.
        public static string GetDealerLocationID(string requesterUsername, int language)
        {
            string locationId = string.Empty;
            if (IS_ATTENDANCE_ENABLED)
            {
                if (IsModernTradeDealer(requesterUsername))
                {
                    var result = GetDealerActiveCheckIn(requesterUsername);

                    if (result != null)
                    {
                        locationId = result.LocationID;
                    }
                    else
                    {
                        var defaultLocation = GetDealerDefaultLocationDetails(requesterUsername, language);

                        if (defaultLocation != null)
                        {
                            locationId = defaultLocation.LocationId;
                        }
                    }
                }
                else
                {
                    var defaultLocation = GetDealerDefaultLocationDetails(requesterUsername, language);

                    if (defaultLocation != null)
                    {
                        locationId = defaultLocation.LocationId;
                    }
                }
            }

            return locationId;
        }

        //
        // Summary:
        //     GetDealerActiveCheckIn.
        public static GetActiveAttendance_V2_Result GetDealerActiveCheckIn(string dealerCode)
        {
            GetActiveAttendance_V2_Result result = null;
            using (AttendenceEntities context = new AttendenceEntities())
            {
                result = context.GetActiveAttendance_V2(dealerCode).FirstOrDefault();
            }

            return result;
        }

        //
        // Summary:
        //     GetLocationByID.
        public static Contracts.Location GetLocationByID(string locationID, int language, string dealerCode)
        {
            Contracts.Location location = new Contracts.Location();

            try
            {
                string API_URL = "Geo/GetLocationById?location_id={0}";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(VirginGeoLocationBaseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage ApiResponse = client.GetAsync(string.Format(API_URL, locationID)).Result;

                    if (ApiResponse.IsSuccessStatusCode)
                    {
                        var apiResponseContent = ApiResponse.Content.ReadAsStringAsync().Result;
                        dynamic objResult = JsonConvert.DeserializeObject<dynamic>(apiResponseContent);

                        if (objResult != null)
                        {
                            location.FriendiSale = objResult.FriendiSale == "Yes" ? true : false;
                            location.VirginSale = objResult.VirginSale == "Yes" ? true : false;
                            location.Latitude = objResult.Latitude;
                            location.Longitude = objResult.Longitude;
                            location.LocationName = objResult.LocationName_en;
                            location.LocationId = objResult.Location_id;
                        }

                        if (EnableMTTrackProcessMode)
                        {
                            ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, string.Format("ApiResponse : {0}", ApiResponse.IsSuccessStatusCode), "GetLocationByID location ID = " + locationID, dealerCode);
                        }
                    }
                    else
                    {
                        ErrorHandling.AddToErrorLog(ErrorTypes.BusinessError, string.Format("GeoApiResponse : {0}", ApiResponse.IsSuccessStatusCode), "GetLocationByID location ID = " + locationID, dealerCode);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "GetLocationByID location ID = " + locationID, dealerCode);
            }

            return location;
        }

        //
        // Summary:
        //     DealerIsVerified.
        public static Contracts.BaseResponse DealerIsVerified(string sessionToken)
        {
            Contracts.BaseResponse response = new Contracts.BaseResponse();
            Session session = null;
            try
            {
                session = AccessManagementController.GetSession(sessionToken);
                if (session != null)
                {
                    using (var context = new AttendenceEntities())
                    {
                        response.IsPassed = context.DealerIsVerified(session.User.UserName) == 0;
                        response.ResponseMessage = response.IsPassed ? "Verified" : "Not Verified";
                    }
                }
                else
                {
                    response.IsPassed = false;
                    response.ResponseMessage = "Not Verified";
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message, "Calling DealerIsVerified", session != null ? session.User.UserName : sessionToken);
            }

            return response;
        }


        public static Dictionary<int, bool> GetMTLocationsConfiguration()
        {
            Dictionary<int, bool> locationsConfiguration = null;
            try
            {
                if (!string.IsNullOrEmpty(MTLocationsConfiguration))
                {
                    locationsConfiguration = new Dictionary<int, bool>();
                    var configs = MTLocationsConfiguration.Split(',');

                    foreach (var config in configs)
                    {
                        var location = config.Split(':');
                        locationsConfiguration.Add(int.Parse(location[0]), bool.Parse(location[1]));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.AddToErrorLog(ErrorTypes.GenericError, ex.Message,
                    "GetLocationsConfiguration", string.Empty);
            }

            return locationsConfiguration;
        }
        #endregion
    }
}
