﻿using LP.Core.Utilities.Exceptions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Web;

namespace LP.OMS.Channels.Engine
{
    public static class AccessManagementController
    {
        static Random RandomGenerator = new Random();

        const string OMSCoreConnectionName = "OMSCore";
        const string OMSChannelsConnectionName = "OMSChannels";

        //public static bool GeofenceEnabled { get; set; }
        public static bool DeviceValidationEnabled { get; set; }
        //public static bool FingerprintDeviceValidationEnabled { get; set; }
        //public static bool LoginGeofenceEnabled { get; set; }
        //public static bool LoginFingerprintDeviceValidationEnabled { get; set; }
        public static bool TimeAccessProfileEnabled { get; set; }
        public static bool DifferentChannelsAtSameTimeEnabled { get; set; }
        public static int SessionIdleTimeout { get; set; }
        public static int SessionNotificationIdleTimeout { get; set; }
        //public static long GeofenceRadius { get; set; }
        public static int ConcurrentSessionsPerUser { get; set; }
        public static string MasterSessionCode { get; set; }
        public static string ChannelsToSkipUsedSessionCodeCondition { get; set; }

        static AccessManagementController()
        {
            //GeofenceEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.GeofenceEnabled"]);
            DeviceValidationEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.DeviceValidationEnabled"]);
            //FingerprintDeviceValidationEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.FingerprintDeviceValidationEnabled"]);
            //LoginGeofenceEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.LoginGeofenceEnabled"]);
            //LoginFingerprintDeviceValidationEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.LoginFingerprintDeviceValidationEnabled"]);
            TimeAccessProfileEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.TimeAccessProfileEnabled"]);
            DifferentChannelsAtSameTimeEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.DifferentChannelsAtSameTimeEnabled"]);
            ConcurrentSessionsPerUser = Int32.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.ConcurrentSessionsPerUser"]);
            SessionIdleTimeout = Int32.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.SessionIdleTimeout"]);
            SessionNotificationIdleTimeout = Int32.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.SessionNotificationIdleTimeout"]);
            //GeofenceRadius = Int32.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.GeofenceRadius"]);
            MasterSessionCode = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.AccessManagement.MasterSessionCode"];
            ChannelsToSkipUsedSessionCodeCondition = ConfigurationManager.AppSettings["ChannelsToSkipUsedSessionCodeCondition"];
        }

        public static User GetUser(string userName, string password)
        {
            User user = new User();

            string weekDay = DateTime.Now.DayOfWeek.ToString().Substring(0, 2);

            string query =
@"SELECT    r.UserName, r.IsActive, r.Location, r.ContactMSISDN,r.DeviceMAC, r.DeviceIMEI, r.FPDeviceSerial, r.GeofenceRadius ,
			DATEADD(s, DATEDIFF(s, Convert(datetime, '1900-01-01 00:00:00'), ISNULL([" + weekDay + @"1Start], '1900-01-01 00:00:00')), Convert(datetime, Convert(date, GetDate()))) StartTimePeriod1,
			DATEADD(s, DATEDIFF(s, Convert(datetime, '1900-01-01 00:00:00'), ISNULL([" + weekDay + @"2Start], ISNULL([" + weekDay + @"1Start], '1900-01-01 00:00:00'))), Convert(datetime, Convert(date, GetDate()))) StartTimePeriod2,
			DATEADD(s, DATEDIFF(s, Convert(datetime, '1900-01-01 00:00:00'), ISNULL([" + weekDay + @"1End], '1900-01-01 23:59:59.999')), Convert(datetime, Convert(date, GetDate()))) EndTimePeriod1,
			DATEADD(s, DATEDIFF(s, Convert(datetime, '1900-01-01 00:00:00'), ISNULL([" + weekDay + @"2End], ISNULL([" + weekDay + @"1End], '1900-01-01 23:59:59.999'))), Convert(datetime, Convert(date, GetDate()))) EndTimePeriod2
FROM        Requester AS r LEFT JOIN
            RequesterAccessProfile AS ra ON r.Id = ra.RequesterID LEFT JOIN
            AccessProfile AS a ON ra.AccessProfileID = a.ID
WHERE     ([UserName] = @userName AND [Password] = @password) AND (ra.EndDate IS NULL OR ra.StartDate < GETDATE()) AND (ra.EndDate IS NULL OR ra.EndDate >= GETDATE())";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSCoreConnectionName))
            {
                dynamic dbUser = db.FirstOrDefault<dynamic>(query, new { @userName = userName, @password = password });

                if (dbUser == null)
                {
                    return null;
                }

                user.UserName = dbUser.UserName;
                user.IsActive = dbUser.IsActive;
                user.ContactMSISDN = dbUser.ContactMSISDN;
                user.DeviceImei = dbUser.DeviceIMEI;
                user.DeviceMacAddress = dbUser.DeviceMAC;
                user.FingerprintSerial = dbUser.FPDeviceSerial;
                user.Lat = string.IsNullOrWhiteSpace(dbUser.Location) || dbUser.Location == "," ? 0 : double.Parse(dbUser.Location.ToString().Split(',')[0]);
                user.Lon = string.IsNullOrWhiteSpace(dbUser.Location) || dbUser.Location == "," ? 0 : double.Parse(dbUser.Location.ToString().Split(',')[1]);
                user.GeofenceRadius = dbUser.GeofenceRadius;
                user.StartTimePeriod1 = dbUser.StartTimePeriod1;
                user.EndTimePeriod1 = dbUser.EndTimePeriod1;
                user.StartTimePeriod2 = dbUser.StartTimePeriod2;
                user.EndTimePeriod2 = dbUser.EndTimePeriod2;
            }

            return user;
        }

        public static AMErrorCode CheckExistingSession(string userName, string password, string deviceId, string channel, out string sessionToken, out int nextSessionTimeCheck, bool needToExtendExistingSession = true)
        {
            bool ExcludeSameSession = false;
            string oldestSessionToken = string.Empty;
            sessionToken = "";
            if (DifferentChannelsAtSameTimeEnabled)
            {
                nextSessionTimeCheck = 0;
                return AMErrorCode.Success;
            }

            Session session = new Session();

            string query =
@"
SELECT  [SessionToken] AS SessionToken,
        [UserName] AS UserName,
        [DeviceMacAddress] AS DeviceMacAddress,
        [DeviceImei] AS DeviceImei,
        [Created] AS Created,
        [Expires] AS Expires,
        [Timeout] AS Timeout,
        [Lat] AS Lat,
        [Lon] AS Lon,
        [StartTimePeriod1] AS StartTimePeriod1,
        [EndTimePeriod1] AS EndTimePeriod1,
        [StartTimePeriod2] AS StartTimePeriod2,
        [EndTimePeriod2] AS EndTimePeriod2,
        [SessionCode] AS SessionCode,
        [ContactMSISDN] AS ContactMSISDN,
        [FingerprintSerial] AS FingerprintSerial,
        [Channel] AS Channel
FROM    [AccessManagementSession]
WHERE   UserName = @userName" +
                              (AccessManagementController.DeviceValidationEnabled ? "        AND ( DeviceMacAddress = @deviceId OR DeviceImei = @deviceId )" : "") +
@"        AND [Expires] > GETDATE() AND [UsedSessionCode] IS NOT NULL ORDER BY [Expires] DESC";

            var skipList = new List<string>(ChannelsToSkipUsedSessionCodeCondition.Split(new char[] { ',' }));
            if (skipList.Contains(channel))
                query = query.Replace("AND [UsedSessionCode] IS NOT NULL ", "");

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                List<dynamic> dbSessionList = db.Fetch<dynamic>(query, new { @userName = userName, @deviceId = deviceId });
                foreach (dynamic _session in dbSessionList)
                {
                    if ((!string.IsNullOrWhiteSpace(_session.DeviceImei) && _session.DeviceImei == deviceId)
                        || (!string.IsNullOrWhiteSpace(_session.DeviceMacAddress) && _session.DeviceMacAddress == deviceId)
                        || string.IsNullOrEmpty(_session.DeviceImei)
                        || string.IsNullOrEmpty(_session.DeviceMacAddress))
                    {
                        if (needToExtendExistingSession)
                        {
                            string _sessionToken = _session.SessionToken == null ? string.Empty : _session.SessionToken.ToString();
                            ExtendSession(_sessionToken);
                            UpdateAndSendNewCodeForExistingSession(userName, password, _sessionToken);
                            sessionToken = _sessionToken;
                            nextSessionTimeCheck = AMService.GetNextSessionCheckTimeByExpiry(_session.Expires);
                            return AMErrorCode.BypassCreateNewSession;
                        }
                        else
                            ExcludeSameSession = true;
                    }
                    oldestSessionToken = _session.SessionToken == null ? string.Empty : _session.SessionToken.ToString();
                }
                if (dbSessionList.Count - Convert.ToInt32(ExcludeSameSession) < ConcurrentSessionsPerUser)
                {
                    nextSessionTimeCheck = 0;//Check it with Ibraheem
                    return AMErrorCode.Success;
                }
                else if (channel == "Portal" && string.IsNullOrWhiteSpace(deviceId) && !string.IsNullOrWhiteSpace(oldestSessionToken))
                {
                    nextSessionTimeCheck = 0;//Check it with Ibraheem
                    EndSession(oldestSessionToken);
                    return AMErrorCode.Success;
                }
                else
                {
                    if (AccessManagementController.DeviceValidationEnabled)
                    {
                        if (ConcurrentSessionsPerUser == 1)
                        {
                            foreach (dynamic dbSession in dbSessionList)
                            {
                                if (dbSession.DeviceMacAddress == deviceId || dbSession.DeviceImei == deviceId)
                                {

                                    nextSessionTimeCheck = AMService.GetNextSessionCheckTimeByExpiry(dbSession.Expires);
                                    return AMErrorCode.Success;
                                }
                            }
                        }
                        nextSessionTimeCheck = 0;
                        Utilities.ErrorHandling.AddToErrorLog("BusinessLoginError", "Dealer: " + userName + " ,Device ID: " + deviceId + " ,Channel: " + (channel == "Portal" ? channel : "DST-" + channel), "CheckExistingSession", userName, string.Empty);
                        return AMErrorCode.UserAlreadyLoggedIn;
                    }
                    else
                    {
                        nextSessionTimeCheck = 0;
                        Utilities.ErrorHandling.AddToErrorLog("BusinessLoginError", "Dealer: " + userName + " ,Device ID: " + deviceId + " ,Channel: " + (channel == "Portal" ? channel : "DST-" + channel), "CheckExistingSession", userName, string.Empty);
                        return AMErrorCode.UserAlreadyLoggedIn;
                    }
                }
            }
        }

        public static void UpdateAndSendNewCodeForExistingSession(string username, string password, string sessionToken)
        {
            string _sessionCode = RandomGenerator.Next(100000, 1000000).ToString().Substring(1);
            string query =
@"
UPDATE  [Session]
SET     [SessionData7] = @sessionCode
WHERE   [SessionId] = @sessionToken";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                dynamic dbSession = db.Execute(query, new { @sessionCode = _sessionCode, @sessionToken = sessionToken });
            }

            User user = GetUser(username, password);
            if (user != null)
            {
                if (bool.Parse(ConfigurationManager.AppSettings["OTPVisabilty"]) == true && !string.IsNullOrWhiteSpace(user.ContactMSISDN))
                {
                    string[] messages = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessage"].Split(';');
                    bool SendSMSResult = false;
                    if (Thread.CurrentThread.CurrentCulture.Name == "en-GB")
                    {
                        SendSMSResult = NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"], user.ContactMSISDN, string.Format(messages[0], _sessionCode), true, thowExceptionOnFailure: false);
                    }
                    else
                    {
                        SendSMSResult = NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"], user.ContactMSISDN, string.Format(messages[1], _sessionCode), true, thowExceptionOnFailure: false);
                    }

                    if (!SendSMSResult)
                        throw new LPBizException("FailedSendingVerificationCode");
                }
            }
        }

        public static string CreateSessionAndSendCode(User user, string channel, string language, string deviceId)
        {
            string sessionToken = Guid.NewGuid().ToString();
            string sessionCode = RandomGenerator.Next(100000, 1000000).ToString().Substring(1);

            if (user.EndTimePeriod1 == new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 0))
            {
                user.EndTimePeriod1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);
            }

            if (user.EndTimePeriod2 == new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 0))
            {
                user.EndTimePeriod2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);
            }

            string query =
@"
INSERT INTO [Session]
           ([SessionId]
           ,[SessionKey1]
           ,[SessionKey2]
           ,[SessionKey3]
           ,[Created]
           ,[Expires]
           ,[Timeout]
           ,[SessionData1]
           ,[SessionData2]
           ,[SessionData3]
           ,[SessionData4]
           ,[SessionData5]
           ,[SessionData6]
           ,[SessionData7]
           ,[SessionData8]
           ,[SessionData9]
           ,[SessionData10]
           ,[SessionData11]
           ,[SessionData13])
 VALUES
		   (@SessionId
		   ,@SessionKey1
		   ,@SessionKey2
		   ,@SessionKey3
		   ,GETDATE()
		   ,DATEADD (mi , @Timeout , GETDATE())
		   ,@Timeout
		   ,@SessionData1
		   ,@SessionData2
		   ,@SessionData3
		   ,@SessionData4
		   ,@SessionData5
		   ,@SessionData6
		   ,@SessionData7
		   ,@SessionData8
		   ,@SessionData9
		   ,@SessionData10
		   ,@SessionData11
           ,@SessionData13)";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                db.Execute(query, new
                {
                    @SessionId = sessionToken,
                    @SessionKey1 = user.UserName,
                    // Save used values, not configured values
                    @SessionKey2 = deviceId,// user.DeviceMacAddress,
                    @SessionKey3 = deviceId,// user.DeviceImei,
                    @Timeout = SessionIdleTimeout,
                    @SessionData1 = user.Lat,
                    @SessionData2 = user.Lon,
                    @SessionData3 = user.StartTimePeriod1.ToString("yyyyMMddHHmmssfff"),
                    @SessionData4 = user.EndTimePeriod1.ToString("yyyyMMddHHmmssfff"),
                    @SessionData5 = user.StartTimePeriod2.ToString("yyyyMMddHHmmssfff"),
                    @SessionData6 = user.EndTimePeriod2.ToString("yyyyMMddHHmmssfff"),
                    @SessionData7 = sessionCode,
                    @SessionData8 = user.ContactMSISDN,
                    @SessionData9 = user.FingerprintSerial,
                    @SessionData10 = language,
                    @SessionData11 = channel,
                    @SessionData13 = user.GeofenceRadius
                });
            }

            if (ConfigurationManager.AppSettings["OTPVisabilty"].ToLower() == "true" && !String.IsNullOrWhiteSpace(user.ContactMSISDN))
            {
                string[] messages = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessage"].Split(';');
                bool SendSMSResult = false;
                if (Thread.CurrentThread.CurrentCulture.Name == "en-GB")
                {
                    SendSMSResult = NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"], user.ContactMSISDN, string.Format(messages[0], sessionCode), true, thowExceptionOnFailure: false);
                }
                else
                {
                    SendSMSResult = NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"], user.ContactMSISDN, string.Format(messages[1], sessionCode), true, thowExceptionOnFailure: false);
                }

                if (!SendSMSResult)
                    throw new LPBizException("FailedSendingVerificationCode");
            }
            return sessionToken;
        }

        public static void ExtendSession(string sessionToken)
        {
            string query =
@"
UPDATE  [Session]
SET     [Expires] = DATEADD (mi , Timeout , GETDATE())
WHERE   [SessionId] = @sessionToken";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                dynamic dbSession = db.Execute(query, new { @sessionToken = sessionToken });
            }
        }

        public static Session GetSession(string sessionToken)
        {
            Session session;

            if (WcfOperationContext.Current.Items.ContainsKey("Session") && WcfOperationContext.Current.Items["Session"] != null)
            {
                session = (Session)WcfOperationContext.Current.Items["Session"];

                if (session.SessionToken.ToLower() == sessionToken.ToLower())
                {
                    return session;
                }
                else
                {
                    throw new Exception("Unexpected Invalid Session -" + session.SessionToken);
                }
            }
            else
            {
                string query =
@"
SELECT  [SessionToken] AS SessionToken,
        [UserName] AS UserName,
        [DeviceMacAddress] AS DeviceMacAddress,
        [DeviceImei] AS DeviceImei,
        [Created] AS Created,
        [Expires] AS Expires,
        [Timeout] AS Timeout,
        [Lat] AS Lat,
        [Lon] AS Lon,
        [StartTimePeriod1] AS StartTimePeriod1,
        [EndTimePeriod1] AS EndTimePeriod1,
        [StartTimePeriod2] AS StartTimePeriod2,
        [EndTimePeriod2] AS EndTimePeriod2,
        [SessionCode] AS SessionCode,
        [ContactMSISDN] AS ContactMSISDN,
        [FingerprintSerial] AS FingerprintSerial,
        [Language] AS Language,
        [Channel] AS Channel,
        [GeofenceRadius] AS GeofenceRadius,
        [UsedFingerprintSerial] AS UsedFingerprintSerial,
        [UsedLat] AS UsedLat,
        [UsedLon] AS UsedLon,
        [UsedSematiToken] AS UsedSematiToken,
		[LoginAttemps] AS LoginAttemps,
        [MinimumAllowedDenomination] AS MinimumAllowedDenomination,
        [MinimumVirginAllowedDenomination] AS MinimumVirginAllowedDenomination
FROM    [AccessManagementSession]
WHERE   SessionToken = @sessionToken";

                using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
                {
                    dynamic dbSession = db.SingleOrDefault<dynamic>(query, new { @sessionToken = sessionToken });

                    if (dbSession == null)
                    {
                        return null;
                    }

                    session = new Session();

                    session.User = new User();
                    session.User.UserName = dbSession.UserName;
                    session.User.ContactMSISDN = dbSession.ContactMSISDN;
                    session.User.DeviceImei = dbSession.DeviceImei;
                    session.User.DeviceMacAddress = dbSession.DeviceMacAddress;
                    session.User.EndTimePeriod1 = DateTime.ParseExact(dbSession.EndTimePeriod1, "yyyyMMddHHmmssfff", CultureInfo.InvariantCulture);
                    session.User.EndTimePeriod2 = DateTime.ParseExact(dbSession.EndTimePeriod2, "yyyyMMddHHmmssfff", CultureInfo.InvariantCulture);
                    session.User.FingerprintSerial = dbSession.FingerprintSerial;
                    session.User.IsActive = true;
                    session.User.Lat = Double.Parse(dbSession.Lat);
                    session.User.Lon = Double.Parse(dbSession.Lon);
                    session.User.StartTimePeriod1 = DateTime.ParseExact(dbSession.StartTimePeriod1, "yyyyMMddHHmmssfff", CultureInfo.InvariantCulture);
                    session.User.StartTimePeriod2 = DateTime.ParseExact(dbSession.StartTimePeriod2, "yyyyMMddHHmmssfff", CultureInfo.InvariantCulture);
                    session.SessionCode = dbSession.SessionCode;
                    session.SessionToken = dbSession.SessionToken.ToString();
                    session.Language = dbSession.Language;
                    session.Channel = dbSession.Channel;
                    session.Expires = dbSession.Expires;

                    session.User.GeofenceRadius = int.Parse(dbSession.GeofenceRadius);
                    session.UsedSematiToken = dbSession.UsedSematiToken;
                    session.UsedFingerprintSerial = dbSession.UsedFingerprintSerial;
                    session.UsedLat = dbSession.UsedLat;
                    session.UsedLon = dbSession.UsedLon;
                    session.LoginAttemps = dbSession.LoginAttemps;
                    session.MinimumFRiENDiAllowedDenomination = dbSession.MinimumAllowedDenomination == null ? 0 : dbSession.MinimumAllowedDenomination;
                    session.MinimumVirginAllowedDenomination = dbSession.MinimumVirginAllowedDenomination == null ? 0 : dbSession.MinimumVirginAllowedDenomination;

                    DateTime endDay = DateTime.ParseExact("19000101235959999", "yyyyMMddHHmmssfff", CultureInfo.InvariantCulture);
                    DateTime startDay = DateTime.ParseExact("19000101000000000", "yyyyMMddHHmmssfff", CultureInfo.InvariantCulture);

                    if ((session.User.EndTimePeriod1 - session.User.StartTimePeriod1) == (endDay - startDay))
                    {
                        session.Is24HourBranch = true;
                    }
                    else
                    {
                        session.Is24HourBranch = false;
                    }

                }

                WcfOperationContext.Current.Items["Session"] = session;

                return session;
            }
        }

        public static void EndSession(string sessionToken)
        {
            string query =
@"
INSERT INTO [SessionHistory]
SELECT *
FROM [Session]
WHERE [SessionId] = @sessionToken

DELETE [Session]
WHERE [SessionId] = @sessionToken
";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                db.Execute(query, new { @sessionToken = sessionToken });
            }
        }

        public static void EndLastActiveSessionBySessionKey(string sessionValue)
        {
            string query =
@"
INSERT INTO [SessionHistory]
SELECT *
FROM [Session]
Where [SessionKey1]=@DealerCode
order by Expires desc

DELETE [Session]
Where SessionId IN (
SELECT TOP 1 SessionId FROM [Session]
Where [SessionKey1]=@DealerCode
order by Expires desc
)
";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                db.Execute(query, new { @DealerCode = sessionValue });
            }
        }

        public static AMErrorCode ResendSessionCode(string sessionToken)
        {
            Session session = GetSession(sessionToken);

            if (session != null && !String.IsNullOrWhiteSpace(session.User.ContactMSISDN))
            {
                session.SessionCode = RandomGenerator.Next(100000, 1000000).ToString().Substring(1);

                string query =
    @"
UPDATE  [Session]
SET [SessionData7] = @sessionCode
WHERE   [SessionId] = @sessionToken";

                using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
                {
                    db.Execute(query, new { @sessionCode = session.SessionCode, @sessionToken = sessionToken });
                }

                string[] messages = ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessage"].Split(';');
                bool SendSMSResult = false;
                if (Thread.CurrentThread.CurrentCulture.Name == "en-GB")
                {
                    SendSMSResult = NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"], session.User.ContactMSISDN, string.Format(messages[0], session.SessionCode), true, thowExceptionOnFailure: false);
                }
                else
                {
                    SendSMSResult = NobillWrapper.SendSMS(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.OneTimePasswordMessageSender"], session.User.ContactMSISDN, string.Format(messages[1], session.SessionCode), true, thowExceptionOnFailure: false);
                }

                if (!SendSMSResult)
                    throw new LPBizException("FailedSendingVerificationCode");

                return AMErrorCode.Success;
            }
            else
                return AMErrorCode.SessionInvalid;
        }

        public static void SaveUsedSessionCode(string sessionToken, string sessionCode)
        {
            string query =
@"
UPDATE  [Session]
SET [SessionData12] = @sessionCode
WHERE   [SessionId] = @sessionToken";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                db.Execute(query, new { @sessionCode = sessionCode, @sessionToken = sessionToken });
            }
        }

        public static void UpdateUsedSessionData(string sessionToken, Session session)
        {
            string query =
@"
UPDATE  [Session] SET ";

            query += session.UsedFingerprintSerial == "KEEP" ? "" : "[SessionData14] = @usedFingerprintSerial,";
            query += session.UsedLat == "KEEP" ? "" : "  [SessionData15] = @usedLat,";
            query += session.UsedLon == "KEEP" ? "" : " [SessionData16] = @usedLon,";
            query += session.UsedSematiToken == "KEEP" ? "" : " [SessionData17] = @usedSematiToken,";
            query += session.LoginAttemps == "KEEP" ? "" : " [SessionData18] = @loginAttemps,";
            query += session.AttendanceId == "KEEP" ? "" : " [SessionData20] = @AttendanceId,";

            query = query.TrimEnd(',');
            query += " WHERE   [SessionId] = @sessionToken";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                db.Execute(query, new
                {
                    @usedFingerprintSerial = session.UsedFingerprintSerial,
                    @usedLat = session.UsedLat,
                    @usedLon = session.UsedLon,
                    @usedSematiToken = session.UsedSematiToken,
                    @loginAttemps = session.LoginAttemps,
                    @sessionToken = sessionToken,
                    @AttendanceId = session.AttendanceId
                });
            }
        }

        public static void SaveLowestAllowedDenomination(string token, int lowestFRiENDiDenomination, int lowestVirginDenomination)
        {
            string query =
@"
UPDATE  [Session]
SET     [SessionData19] = @lowestFRiENDiDenomination,
        [SessionData21] = @lowestVirginDenomination
WHERE   [SessionId] = @sessionToken";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                dynamic dbSession = db.Execute(query, new { @lowestFRiENDiDenomination = lowestFRiENDiDenomination, @lowestVirginDenomination = lowestVirginDenomination, @sessionToken = token });
            }
        }

        public static DateTime GetSessionExpiryDate(string token)
        {
            DateTime expires = new DateTime();
            string query =
@"
SELECT  [SessionToken] AS SessionToken,
        [UserName] AS UserName,
        [DeviceMacAddress] AS DeviceMacAddress,
        [DeviceImei] AS DeviceImei,
        [Created] AS Created,
        [Expires] AS Expires,
        [Timeout] AS Timeout,
        [Lat] AS Lat,
        [Lon] AS Lon,
        [StartTimePeriod1] AS StartTimePeriod1,
        [EndTimePeriod1] AS EndTimePeriod1,
        [StartTimePeriod2] AS StartTimePeriod2,
        [EndTimePeriod2] AS EndTimePeriod2,
        [SessionCode] AS SessionCode,
        [ContactMSISDN] AS ContactMSISDN,
        [FingerprintSerial] AS FingerprintSerial,
        [Language] AS Language,
        [Channel] AS Channel,
        [GeofenceRadius] AS GeofenceRadius,
        [UsedFingerprintSerial] AS UsedFingerprintSerial,
        [UsedLat] AS UsedLat,
        [UsedLon] AS UsedLon,
        [UsedSematiToken] AS UsedSematiToken,
		[LoginAttemps] AS LoginAttemps,
        [MinimumAllowedDenomination] AS MinimumAllowedDenomination
FROM    [AccessManagementSession]
WHERE   SessionToken = @sessionToken";

            using (PetaPoco.Database db = new PetaPoco.Database(OMSChannelsConnectionName))
            {
                dynamic dbSession = db.SingleOrDefault<dynamic>(query, new { @sessionToken = token });

                if (dbSession != null)
                {
                    expires = dbSession.Expires;
                }
            }

            return expires;
        }
    }

    public class WcfOperationContext : IExtension<OperationContext>
    {
        private readonly IDictionary<string, object> items;

        private WcfOperationContext()
        {
            items = new Dictionary<string, object>();
        }

        public IDictionary<string, object> Items
        {
            get { return items; }
        }

        public static WcfOperationContext Current
        {
            get
            {
                WcfOperationContext context = OperationContext.Current.Extensions.Find<WcfOperationContext>();

                if (context == null)
                {
                    context = new WcfOperationContext();
                    OperationContext.Current.Extensions.Add(context);
                }

                return context;
            }
        }

        public void Attach(OperationContext owner)
        {
        }

        public void Detach(OperationContext owner)
        {
        }
    }

    public static class Helper
    {
        public enum MessageDetailsKeys
        {
            StartTime,
            EndTime
        }

        public static void SetMessageDetails(MessageDetailsKeys key, object value)
        {
            WcfOperationContext.Current.Items["MessageDetails" + key.ToString()] = value;
        }

        public static object GetMessageDetails(MessageDetailsKeys key)
        {
            return WcfOperationContext.Current.Items["MessageDetails" + key.ToString()];
        }
    }
}