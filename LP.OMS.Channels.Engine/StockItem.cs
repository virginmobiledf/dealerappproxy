//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.Engine
{
    using System;
    using System.Collections.Generic;
    
    public partial class StockItem
    {
        public StockItem()
        {
            this.StockRequests = new HashSet<StockRequest>();
        }
    
        public int ID { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string ForgeinName { get; set; }
        public string CategoryForgeinName { get; set; }
        public Nullable<int> CategoryID { get; set; }
    
        public virtual ICollection<StockRequest> StockRequests { get; set; }
    }
}
