﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LP.OMS.Channels.Engine.Models;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (AttendenceEntities context = new AttendenceEntities())
            {
                ObjectParameter result = new ObjectParameter("Result", typeof(int));
                context.IsVerificationRequired("CUS-200025", result);
                
                var xx = (int)result.Value;
            }
                
        }
    }
}
