﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.MT.ReminderManager.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class AttendenceEntities : DbContext
    {
        public AttendenceEntities()
            : base("name=AttendenceEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<AttendenceProcessType> AttendenceProcessTypes { get; set; }
        public virtual DbSet<NotificationLog> NotificationLogs { get; set; }
        public virtual DbSet<Attendence> Attendences { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
    
        public virtual ObjectResult<GetActiveAttendance_Result> GetActiveAttendance(string daelerCode)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetActiveAttendance_Result>("GetActiveAttendance", daelerCodeParameter);
        }
    
        public virtual ObjectResult<string> GetActiveLocationIDByDealerCode(string daelerCode)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetActiveLocationIDByDealerCode", daelerCodeParameter);
        }
    
        public virtual int Insert_Attendance(string daelerCode, string locationID, string location_Lat, string location_Lon, Nullable<int> processID, Nullable<System.DateTime> perfomedAt, Nullable<System.DateTime> workingHours_Strat, Nullable<System.DateTime> workingHours_End, string sessionID, string deviceID, Nullable<int> checkoutID, Nullable<bool> isAutomatic, Nullable<bool> isMTDealer, Nullable<bool> isCheckoutReminderSent, string channelID, ObjectParameter iD)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            var locationIDParameter = locationID != null ?
                new ObjectParameter("LocationID", locationID) :
                new ObjectParameter("LocationID", typeof(string));
    
            var location_LatParameter = location_Lat != null ?
                new ObjectParameter("Location_Lat", location_Lat) :
                new ObjectParameter("Location_Lat", typeof(string));
    
            var location_LonParameter = location_Lon != null ?
                new ObjectParameter("Location_Lon", location_Lon) :
                new ObjectParameter("Location_Lon", typeof(string));
    
            var processIDParameter = processID.HasValue ?
                new ObjectParameter("ProcessID", processID) :
                new ObjectParameter("ProcessID", typeof(int));
    
            var perfomedAtParameter = perfomedAt.HasValue ?
                new ObjectParameter("PerfomedAt", perfomedAt) :
                new ObjectParameter("PerfomedAt", typeof(System.DateTime));
    
            var workingHours_StratParameter = workingHours_Strat.HasValue ?
                new ObjectParameter("WorkingHours_Strat", workingHours_Strat) :
                new ObjectParameter("WorkingHours_Strat", typeof(System.DateTime));
    
            var workingHours_EndParameter = workingHours_End.HasValue ?
                new ObjectParameter("WorkingHours_End", workingHours_End) :
                new ObjectParameter("WorkingHours_End", typeof(System.DateTime));
    
            var sessionIDParameter = sessionID != null ?
                new ObjectParameter("SessionID", sessionID) :
                new ObjectParameter("SessionID", typeof(string));
    
            var deviceIDParameter = deviceID != null ?
                new ObjectParameter("DeviceID", deviceID) :
                new ObjectParameter("DeviceID", typeof(string));
    
            var checkoutIDParameter = checkoutID.HasValue ?
                new ObjectParameter("CheckoutID", checkoutID) :
                new ObjectParameter("CheckoutID", typeof(int));
    
            var isAutomaticParameter = isAutomatic.HasValue ?
                new ObjectParameter("IsAutomatic", isAutomatic) :
                new ObjectParameter("IsAutomatic", typeof(bool));
    
            var isMTDealerParameter = isMTDealer.HasValue ?
                new ObjectParameter("IsMTDealer", isMTDealer) :
                new ObjectParameter("IsMTDealer", typeof(bool));
    
            var isCheckoutReminderSentParameter = isCheckoutReminderSent.HasValue ?
                new ObjectParameter("IsCheckoutReminderSent", isCheckoutReminderSent) :
                new ObjectParameter("IsCheckoutReminderSent", typeof(bool));
    
            var channelIDParameter = channelID != null ?
                new ObjectParameter("ChannelID", channelID) :
                new ObjectParameter("ChannelID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_Attendance", daelerCodeParameter, locationIDParameter, location_LatParameter, location_LonParameter, processIDParameter, perfomedAtParameter, workingHours_StratParameter, workingHours_EndParameter, sessionIDParameter, deviceIDParameter, checkoutIDParameter, isAutomaticParameter, isMTDealerParameter, isCheckoutReminderSentParameter, channelIDParameter, iD);
        }
    
        public virtual int UpdateActiveAttendanceSessionID(Nullable<int> iD, string sessionID)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var sessionIDParameter = sessionID != null ?
                new ObjectParameter("SessionID", sessionID) :
                new ObjectParameter("SessionID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateActiveAttendanceSessionID", iDParameter, sessionIDParameter);
        }
    
        public virtual int UpdateDealerActiveAttendanceForCheckOut(string daelerCode, string checkoutID)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            var checkoutIDParameter = checkoutID != null ?
                new ObjectParameter("CheckoutID", checkoutID) :
                new ObjectParameter("CheckoutID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateDealerActiveAttendanceForCheckOut", daelerCodeParameter, checkoutIDParameter);
        }
    
        public virtual int AddNotificationLog(Nullable<System.DateTime> creationDate, string request, string response, Nullable<bool> isSuccess, string apiName, string apiUri)
        {
            var creationDateParameter = creationDate.HasValue ?
                new ObjectParameter("CreationDate", creationDate) :
                new ObjectParameter("CreationDate", typeof(System.DateTime));
    
            var requestParameter = request != null ?
                new ObjectParameter("Request", request) :
                new ObjectParameter("Request", typeof(string));
    
            var responseParameter = response != null ?
                new ObjectParameter("Response", response) :
                new ObjectParameter("Response", typeof(string));
    
            var isSuccessParameter = isSuccess.HasValue ?
                new ObjectParameter("IsSuccess", isSuccess) :
                new ObjectParameter("IsSuccess", typeof(bool));
    
            var apiNameParameter = apiName != null ?
                new ObjectParameter("ApiName", apiName) :
                new ObjectParameter("ApiName", typeof(string));
    
            var apiUriParameter = apiUri != null ?
                new ObjectParameter("ApiUri", apiUri) :
                new ObjectParameter("ApiUri", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddNotificationLog", creationDateParameter, requestParameter, responseParameter, isSuccessParameter, apiNameParameter, apiUriParameter);
        }
    
        public virtual ObjectResult<GetAllActiveAttendances_Result> GetAllActiveAttendances()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllActiveAttendances_Result>("GetAllActiveAttendances");
        }
    
        public virtual int SetAsReminderedAttendance(Nullable<int> iD)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SetAsReminderedAttendance", iDParameter);
        }
    
        public virtual ObjectResult<GetAttendancesForCheckOutSMS_Result> GetAttendancesForCheckOutSMS(Nullable<System.TimeSpan> remainingTimeForSendSMS)
        {
            var remainingTimeForSendSMSParameter = remainingTimeForSendSMS.HasValue ?
                new ObjectParameter("RemainingTimeForSendSMS", remainingTimeForSendSMS) :
                new ObjectParameter("RemainingTimeForSendSMS", typeof(System.TimeSpan));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAttendancesForCheckOutSMS_Result>("GetAttendancesForCheckOutSMS", remainingTimeForSendSMSParameter);
        }
    }
}
