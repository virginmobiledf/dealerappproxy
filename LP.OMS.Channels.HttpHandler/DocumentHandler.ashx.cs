﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LP.OMS.Channels.Engine;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Configuration;
using System.IO;

namespace LP.OMS.Channels.HttpHandler
{
    /// <summary>
    /// Summary description for DocumentHandler
    /// </summary>
    public class DocumentHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            byte[] file = null;
            bool skipTerms = false;
            context.Response.ContentType = "application/pdf";

            if (!string.IsNullOrEmpty(context.Request.QueryString["id"]))
            {
                if (string.IsNullOrEmpty(context.Request.QueryString["Brand"]))
                {
                    file = DownloadPDF(context);
                }
                else if (!string.IsNullOrEmpty(context.Request.QueryString["Brand"]))
                {
                    if (!string.IsNullOrEmpty(context.Request.QueryString["skipterms"]))
                        bool.TryParse(context.Request.QueryString["skipterms"], out skipTerms);

                    file = DownloadPDFByBrand(context, skipTerms);
                }

                context.Response.BinaryWrite(file);
            }
        }

        static private byte[] DownloadPDF(HttpContext context)
        {
            string base64 = DMSProxy.GetDocumentById(context.Request.QueryString["id"]).Base64;
            byte[] newBytes = Convert.FromBase64String(base64);
            return newBytes;
        }

        static private byte[] DownloadPDFByBrand(HttpContext context, bool skipTerms)
        {
            var base64AsString = DMSProxy.GetDocumentById(context.Request.QueryString["id"]).Base64;
            byte[] base64AsByte = Convert.FromBase64String(base64AsString);
            byte[] newBytes = base64AsByte;

            if (!string.IsNullOrEmpty(base64AsString) && !skipTerms)
            {
                newBytes = ConvertTif2Pdf(base64AsByte, context.Request.QueryString["Brand"], skipTerms);
            }
            return newBytes;
        }

        static private byte[] ConvertTif2Pdf(byte[] tifFile, string brand, bool skipTerms)
        {
            byte[] pdfBytes = null;

            using (PdfDocument pdfDoc = new PdfDocument())
            {
                pdfDoc.Pages.Add(new PdfPage());
                XGraphics xgFirstPage = XGraphics.FromPdfPage(pdfDoc.Pages[0]);
                Stream stream = new MemoryStream(tifFile);
                XImage imgFirstPage = XImage.FromStream(stream);
                xgFirstPage.DrawImage(imgFirstPage, 0, 0, 610, 800);

                if (!skipTerms)
                {
                    string termsAndCondsPath = string.Empty;
                    switch (int.Parse(brand))
                    {
                        case 1:
                            termsAndCondsPath = ConfigurationManager.AppSettings["TermsAndConditionsForFRiENDiPath"];
                            break;
                        case 2:
                            termsAndCondsPath = ConfigurationManager.AppSettings["TermsAndConditionsForVirginPath"];
                            break;
                    }

                    pdfDoc.Pages.Add(new PdfPage());
                    XGraphics xgSecondPage = XGraphics.FromPdfPage(pdfDoc.Pages[1]);
                    XImage imgSecondPage = XImage.FromFile(termsAndCondsPath);
                    xgSecondPage.DrawImage(imgSecondPage, 0, 0, 610, 800);
                }

                MemoryStream stream2 = new MemoryStream();
                pdfDoc.Save(stream2);
                pdfBytes = stream2.ToArray();
            }

            return pdfBytes;
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}