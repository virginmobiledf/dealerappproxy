﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.Enums;
using LP.OMS.Channels.Engine.ReportingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LP.OMS.Channels.WCFServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDealerHiringServices" in both code and config file together.
    [ServiceContract]
    public interface IDealerHiringServices
    {
        [OperationContract]
        DealerHiringLoginResponse Login(DHLoginRequest request);

        [OperationContract]
        DealerCreationResponse AddDealerCreationRequest(DealerCreationContractRequest request);

        [OperationContract]
        LookupsList GetLookups(LookupsRequest request);

        [OperationContract]
        MyRequestsResponse GetMyRequests(MyRequestsRequest request);

        [OperationContract]
        CreationDetailsResponse GetCreationRequestDetails(CreationDetailsRequest request);

        [OperationContract]
        UpdatingDetailsResponse GetUpdatingRequestDetails(UpdatingDetailsRequest request);

        [OperationContract]
        DeActivateRequestResponse DeActivateRequest(DeActivateRequest request);

        [OperationContract]
        ReActivateResponse ReActivateRequest(ReActivateRequest request);

        [OperationContract]
        SearchDealerResponse SearchDealerRequest(SearchDealerRequest request);

        [OperationContract]
        ResetPasswordResponse ResetPassword(DHResetPasswordRequest request);

        [OperationContract]
        DiscardResponse DiscardRequest(DiscardRequest request);

        [OperationContract]
        DHBaseResponse UpdateSaudiID(UpdateSaudiIDRequest request);

        [OperationContract]
        DHBaseResponse UpdateOTPMSISDN(UpdateOTPMSISDNRequest request);

        [OperationContract]
        DescendantActivationsResponse GetDescendantActivations(LoginRequest request);

        [OperationContract]
        DescendantCommissionsResponse GetDescendantCommissions(LoginRequest request);

        [OperationContract]
        ActivationSummaryReportResponse GetActivationSummaryReport(LoginRequest request, string dealerCode, DateSearchCriteria dateSearchCriteria);
        [OperationContract]
        RechargeSummaryReportResponse GetRechargeSummaryReport(LoginRequest request, string dealerCode, DateSearchCriteria dateSearchCriteria);

        [OperationContract]
        CommissionsDetailsReportResponse GetCommissionsDetailsReports(LoginRequest request, string dealerCode, DHGeneralSearchCriteria generalSearchCriteria, DateSearchCriteria dateSearchCriteria);

        [OperationContract]
        AddLocationResponse AddLocation(AddLocationRequest request);

        [OperationContract]
        SearchDealerResponse SearchDealerRequestNew(SearchDealerRequest request);
    }
}
