﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;
using Newtonsoft.Json;

namespace LP.OMS.Channels.WCFServices
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GeneralInspectorAttribute : Attribute, IParameterInspector, IServiceBehavior
    {
        public object BeforeCall(string operationName, object[] inputs)
        {
            LogRequestForTesting(operationName, inputs);
            return null;
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                if (channelDispatcher == null)
                {
                    continue;
                }

                foreach (var endPoint in channelDispatcher.Endpoints)
                {
                    if (endPoint == null)
                    {
                        continue;
                    }

                    foreach (var opertaion in endPoint.DispatchRuntime.Operations)
                    {
                        opertaion.ParameterInspectors.Add(this);
                    }
                }
            }
        }

        private static void LogRequestForTesting(string operationName, object[] inputs)
        {
            try
            {
                bool enableRequestLogging = false;
                var configVal = ConfigurationManager.AppSettings["EnableRequestLogging"];

                if (configVal != null)
                {
                    bool.TryParse(configVal, out enableRequestLogging);
                    if (enableRequestLogging)
                    {
                        string requestData = operationName + Environment.NewLine;
                        foreach (var item in inputs)
                        {
                            requestData += JsonConvert.SerializeObject(item) + Environment.NewLine;
                        }

                        string logsFolder = System.Web.Hosting.HostingEnvironment.MapPath("~/RequestLogs");
                        if (!System.IO.Directory.Exists(logsFolder))
                        {
                            System.IO.Directory.CreateDirectory(logsFolder);
                        }

                        string filePath = System.IO.Path.Combine(logsFolder, string.Format("{0}_{1}.txt", operationName, DateTime.Now.ToString("yyyyMMddhhmmss")));
                        System.IO.File.WriteAllText(filePath, requestData);
                    }
                }
            }
            catch { }
        }
    }
}