﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.Enums;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Engine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LP.OMS.Channels.WCFServices
{
    public class DealerHiringServices : IDealerHiringServices
    {
        public DealerHiringLoginResponse Login(DHLoginRequest request)
        {
            DealerHiringLoginResponse response = new DealerHiringLoginResponse();

            try
            {
                response = DealerHiringController.Login(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("Login", ex, request.Username, true);
            }

            return response;
        }

        public DealerCreationResponse AddDealerCreationRequest(DealerCreationContractRequest request)
        {
            DealerCreationResponse response = new DealerCreationResponse();
            try
            {
                response = DealerHiringController.AddDealerCreationRequest(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("AddDealerCreationRequest", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public LookupsList GetLookups(LookupsRequest request)
        {
            LookupsList response = new LookupsList();
            try
            {
                response = DealerHiringController.GetLookups(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetLookups", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public MyRequestsResponse GetMyRequests(MyRequestsRequest request)
        {
            MyRequestsResponse response = new MyRequestsResponse();
            try
            {
                response = DealerHiringController.GetMyRequests(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("GetMyRequests", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public CreationDetailsResponse GetCreationRequestDetails(CreationDetailsRequest request)
        {
            CreationDetailsResponse response = new CreationDetailsResponse();
            try
            {
                response = DealerHiringController.GetCreationRequestDetails(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("GetCreationRequestDetails", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public UpdatingDetailsResponse GetUpdatingRequestDetails(UpdatingDetailsRequest request)
        {
            UpdatingDetailsResponse response = new UpdatingDetailsResponse();
            try
            {
                response = DealerHiringController.GetUpdatingRequestDetails(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("GetUpdatingRequestDetails", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public DeActivateRequestResponse DeActivateRequest(DeActivateRequest request)
        {
            DeActivateRequestResponse response = new DeActivateRequestResponse();
            try
            {
                response = DealerHiringController.DeActivateRequest(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("DeActivateRequest", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public ReActivateResponse ReActivateRequest(ReActivateRequest request)
        {
            ReActivateResponse response = new ReActivateResponse();
            try
            {
                response = DealerHiringController.ReActivateRequest(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("ReActivateRequest", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public SearchDealerResponse SearchDealerRequest(SearchDealerRequest request)
        {
            SearchDealerResponse response = new SearchDealerResponse();
            try
            {
                response = DealerHiringController.SearchDealerRequest(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("SearchDealerRequest", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public SearchDealerResponse SearchDealerRequestNew(SearchDealerRequest request)
        {
            SearchDealerResponse response = new SearchDealerResponse();
            try
            {
                response = DealerHiringController.SearchDealerRequestNew(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("SearchDealerRequestNew", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public ResetPasswordResponse ResetPassword(DHResetPasswordRequest request)
        {
            ResetPasswordResponse response = new ResetPasswordResponse();
            try
            {
                response = DealerHiringController.ResetPassword(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("ResetPassword", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public DiscardResponse DiscardRequest(DiscardRequest request)
        {
            DiscardResponse response = new DiscardResponse();
            try
            {
                response = DealerHiringController.DiscardRequest(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleDealerHiringException("DiscardRequest", ex, request.oLoginRequest.Username, true);
            }
            return response;
        }

        public DHBaseResponse UpdateSaudiID(UpdateSaudiIDRequest request)
        {
            DHBaseResponse response = new DHBaseResponse();
            try
            {
                response = DealerHiringController.UpdateSaudiID(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("UpdateSaudiID", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }

        public DHBaseResponse UpdateOTPMSISDN(UpdateOTPMSISDNRequest request)
        {
            DHBaseResponse response = new DHBaseResponse();
            try
            {
                response = DealerHiringController.UpdateOTPMSISDN(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("UpdateOTPMSISDN", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }

        public DescendantActivationsResponse GetDescendantActivations(LoginRequest request)
        {
            DescendantActivationsResponse response = new DescendantActivationsResponse()
            {
                Result = new List<DescendantActivations>()
            };

            try
            {
                response = DealerHiringController.GetDescendantActivations(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDescendantActivations", ex, request.Username, true);
            }

            return response;
        }

        public DescendantCommissionsResponse GetDescendantCommissions(LoginRequest request)
        {
            DescendantCommissionsResponse response = new DescendantCommissionsResponse()
            {
                Result = new List<DescendantCommissions>()
            };

            try
            {
                response = DealerHiringController.GetDescendantCommissions(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDescendantCommissions", ex, request.Username, true);
            }

            return response;
        }

        public ActivationSummaryReportResponse GetActivationSummaryReport(LoginRequest request, string dealerCode, DateSearchCriteria dateSearchCriteria)
        {
            ActivationSummaryReportResponse response = new ActivationSummaryReportResponse()
            {
                Result = new ActivationSummaryReport()
            };

            try
            {
                response = DealerHiringController.GetActivationSummaryReport(request, dealerCode, dateSearchCriteria);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationSummaryReport", ex, request.Username, true);
            }

            return response;
        }

        public RechargeSummaryReportResponse GetRechargeSummaryReport(LoginRequest request, string dealerCode, DateSearchCriteria dateSearchCriteria)
        {
            RechargeSummaryReportResponse response = new RechargeSummaryReportResponse()
            {
                Result = new RechargeSummaryReport()
            };

            try
            {
                response = DealerHiringController.GetRechargeSummaryReport(request, dealerCode, dateSearchCriteria);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetRechargeSummaryReport", ex, request.Username, true);
            }

            return response;
        }

        public CommissionsDetailsReportResponse GetCommissionsDetailsReports(LoginRequest request, string dealerCode, DHGeneralSearchCriteria generalSearchCriteria, DateSearchCriteria dateSearchCriteria)
        {
            CommissionsDetailsReportResponse response = new CommissionsDetailsReportResponse()
            {
                Result = new CommissionsDetailsReport()
                {
                    CommissionsDetailsDataItems = new List<CommissionsDetailsDataItem>(),
                    Headers = new List<string>()
                }
            };

            try
            {
                response = DealerHiringController.GetCommissionsDetailsReport(request, dealerCode, generalSearchCriteria, dateSearchCriteria);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCommissionsDetailsReports", ex, request.Username, true);
            }

            return response;
        }

        public AddLocationResponse AddLocation(AddLocationRequest request)
        {
            AddLocationResponse response = new AddLocationResponse();

            try
            {
                response = LocationController.AddLocation(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AddLocation", ex, request.Login.Username, true);
            }

            return response;
        }
    }
}
