using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.DealerSImAssociation;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Engine.OMSCommonServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LP.OMS.Channels.WCFServices
{
    [ServiceContract]
    public interface IOnlineChannelServices
    {
        [OperationContract]
        CustomerPlanDetailsResponse GetCustomerDetails(GetCustomerDetailsRequest customerDetails);
        [OperationContract]
        LP.OMS.Channels.Contracts.CallResponse SendRequest(LP.OMS.Channels.Contracts.SubmitRequest request);
        [OperationContract]
        List<LP.OMS.Channels.Contracts.Country> GetCountryList(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.LoginResponse Login(LP.OMS.Channels.Contracts.LoginRequest request);
        [OperationContract]
        List<LP.OMS.Channels.Contracts.IDType> GetIDTypes(LP.OMS.Channels.Contracts.LoginRequest request);
        [OperationContract]
        List<LP.OMS.Channels.Contracts.Title> GetTitleList(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.Lookups GetLookups(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.DealerActivitiesResponseColelction GetDealerActivitiesList(LP.OMS.Channels.Contracts.AndroidReportRequest oAndroidReportRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.ResubmissionCallResponse GetFailedRequests(LP.OMS.Channels.Contracts.FaildActivationRequest oFaildActivationRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.CallResponse ChangePassword(LP.OMS.Channels.Contracts.HashingRequest oHashingRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.CallResponse ResubmitRequest(LP.OMS.Channels.Contracts.ResubmitRequestObj request);
        [OperationContract]
        LP.OMS.Channels.Contracts.StatusCheckResponse StatusCheck(LP.OMS.Channels.Contracts.StatusCheckRequest objRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.EloadResponse Eload(LP.OMS.Channels.Contracts.EloadRequest objRequest);
        [OperationContract]
        void HandleException(string methodName, string message, string extradetails);
        [OperationContract]
        LP.OMS.Channels.Contracts.ActivationReportCounters GetActivationReportCounters(LoginRequest objLogin);
        [OperationContract]
        List<LP.OMS.Channels.Contracts.ActivationResponse> GetDealerActivations(Contracts.CommissionDataByDealerRequest objActivationReportRequest);
        [OperationContract]
        List<CommissionReportResponse> GetCommissionReport(Contracts.CommissionDataByDealerRequest objCommissionDataByDealerRequest);
        [OperationContract]
        LP.OMS.Channels.Contracts.CallResponse SubmitVirginActivation(VirginActivationRequest request);
        [OperationContract]
        LP.OMS.Channels.Contracts.NotifyApplicationLogResponse NotifyLocalApplicationLog(NotifyApplicationLogRequest request);
        [OperationContract]
        LP.OMS.Channels.Contracts.PhysicalFormCheckResponse PhysicalFormCollectionCheck(LP.OMS.Channels.Contracts.PhysicalFormCheckRequest request);
        [OperationContract]
        LP.OMS.Channels.Contracts.CallResponse PhysicalFormsCollectionSubmit(LP.OMS.Channels.Contracts.PhysicalFormCollectionRequest request);
        [OperationContract]
        VirginVanityActivationCheckResponse VerifyVirginVanityNumber(VirginVanityActivationCheckRequest objVirginVanityActivationCheckRequest);
        [OperationContract]
        CallResponse ActivateVirginVanityNumber(VirginVanityActivationActivationRequest objVirginVanityActivationActivationRequest);
        [OperationContract]
        DealerBalanceResponse GetDealerBalance(LoginRequest objLoginRequest);


        [OperationContract]
        CallResponse CheckCustomerId(CheckCustomerIdRquest objCheckCustomerIdRquest);
        [OperationContract]
        CheckCustomerSubscriptionsResponse CheckCustomerSubscriptions(CheckCustomerIdRquest objCheckCustomerIdRquest);
        [OperationContract]
        VerifyCustomerResponse VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest);
        [OperationContract]
        CheckVirginCustomerIdResponse CheckVirginCustomerId(CheckVirginCustomerIdRequest objCheckVirginCustomerIdRquest);

        [OperationContract]
        CallResponse LogFingerprintNFIQTrials(LogFingerprintNFIQTrialsRequest logNFIQTrialsRequest);

        [OperationContract]
        CallResponse VerifyOwnershipChange(OwnerChangeRequest request);

        [OperationContract]
        CallResponse SubmitOwnershipChange(OwnerChangeRequest request);

        [OperationContract]
        CallResponse AddStockRequest(StockItemRequest sRequest);

        [OperationContract]
        BundleResponse GetBundle(BundlesRequest request);

        [OperationContract]
        CallResponse SubmitBundle(SubmitBundleRequest SubmitBundleRequest);

        [OperationContract]
        void LogError(DSTErrorLog dstErrorLog);

        [OperationContract]
        CallResponse ReplaceSIM(SIMReplacementRequest Request);

        [OperationContract]
        CallResponse ValidateReplaceSIM(SIMReplacementRequest Request);

        [OperationContract]
        int SelfcareResubmitRequest(string msisdn, string idNumber, int brand, int documentType, string additionalInfo, List<byte[]> lstResubmissionDocuments);

        //[OperationContract]
        //CallResponse PasswordResetGetVerificationCode(PasswordResetGetVerificationCodeRequest request);

        //[OperationContract]
        //CheckVerificationResetPasswordResponse CheckVerificationResetPassword(CheckVerificationResetPasswordRequest request);

        [OperationContract]
        AvailableProductsResponse GetAvailableProducts(ProductSellingRequest request);

        [OperationContract]
        ProductSellingResponse SubmitProductSelling(ProductSellingRequest request);

        [OperationContract]
        CallResponse CheckProductSellingEligibility(CheckProductSellingEligibilityRequest request);

        [OperationContract]
        CallResponse VerfiyLoginCode(VerfiyLoginCode request);

        [OperationContract]
        CallResponse ResendLoginCode(LoginRequest request);

        [OperationContract]
        CallResponse CheckSessionValidity(string sessionToken, int lang, bool ExtendSession = true);

        [OperationContract]
        CallResponse Logout(LoginRequest request);

        [OperationContract]
        CallResponse ValidateSessionTransaction(LoginRequest request);

        [OperationContract]
        CallResponse GetSessionData(string sessionToken, int lang, out Session session);

        [OperationContract]
        CallResponse UpdateSessionData(string sessionToken, int lang, Session session);

        [OperationContract]
        bool BlockDealer(string dealerCode, string channelName);

        [OperationContract]
        SematiLoginCallResponse LoginToSemati(LoginRequest request);

        [OperationContract]
        CallResponse CheckSematiAuthentication(CheckSematiAuthenticationObject checkSematiAuthenticationObject);

        [OperationContract]
        OneSignalResponse GetOneSignalData(LoginRequest loginRequest);

        [OperationContract]
        NextSessionCheckTimeResponse GetNextSessionCheckTime(LoginRequest loginRequest, string sessionToken);

        [OperationContract]
        ExtendSessionRequest ExtendSession(LoginRequest loginRequest, string sessionToken);

        [OperationContract]
        GetAllowedIDTypesByIMSIResponse GetAllowedIDTypesByIMSI(GetAllowedIDTypesByIMSIObj request);

        [OperationContract]
        GetSubscriptionTypesResponse GetSubscriptionTypes(GetSubscriptionTypeObj request);

        [OperationContract]
        List<SliderImageResponse> GetSliderImages(LoginRequest loginRequest, Brands brand);

        [OperationContract]
        int GetTotalActivation(LoginRequest loginRequest);

        [OperationContract]
        double GetTotalCommission(LoginRequest loginRequest);

        [OperationContract]
        AvailableDealerBalanceResponse GetAvailableDealerBalance(LoginRequest loginRequest);

        [OperationContract]
        AvailableProductsResponse GetAvailableProductsBySubscriptionType(ProductSellingBySubscriptionTypeRequest productSellingRequest);

        [OperationContract]
        SendRequestStockResponse SendRequestStock(SendRequestStockRequest stockRequest);

        [OperationContract]
        GetStocksResponse GetStocks(LoginRequest loginRequest, Brands brand);


        [OperationContract]
        List<AvailableRatePlan> GetAvailableVanityRatePlans(LoginRequest loginRequest, string idType);

        [OperationContract]
        double GetRemainingSessionInSeconds(LoginRequest loginRequest);

        [OperationContract]
        List<PossibleFailureReason> GetPossibleFailureReasons(LoginRequest loginRequest, Brands brand);

        [OperationContract]
        bool SubmitDealerFailureReasonAndNote(DealerFailureReasonNote failureReason);

        [OperationContract]
        ActivationGradeDetailsReportResponse GetActivationGradeDetailsReport(ActivationCommissionReportRequest request);

        [OperationContract]
        ActivationRechargeDetailsReportResponse GetActivationRechargeDetailsReport(ActivationCommissionReportRequest request);

        [OperationContract]
        ActivationFPEDetailsReportResponse GetActivationFPEDetailsReport(ActivationCommissionReportRequest request);

        [OperationContract]
        CommissionPaymentDetailsReportResponse GetCommissionPaymentDetails(ActivationCommissionReportRequest request);

        [OperationContract]
        ActivationReportSummary GetActivationReportSummary(ActivationCommissionReportRequest request);

        [OperationContract]
        CommissionReportSummary GetCommissionReportSummary(ActivationCommissionReportRequest request);

        [OperationContract]
        List<OperatorDetails> GetTelecomOperators(LoginRequest request);

        [OperationContract]
        CancelMNPPortInResponse CancelMNPPortInRequest(CancelMNPPortInRequest request);

        [OperationContract]
        CancelMNPPortInResponse ValidateCancelMNPPortInRequest(CancelMNPPortInRequest request);

        [OperationContract]
        List<decimal> GetTopupAmounts(LoginRequest request);

        [OperationContract]
        ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request);

        [OperationContract]
        CancelFamilyRegistrationResponse CancelFamilyRegistration(CancelFamilyRegistrationRequest request);

        [OperationContract]
        BaseResponse AssociateScannedSim(DealerSimAssociationRequest request);

        [OperationContract]
        Contracts.PagedResult<Contracts.DealerSImAssociation.ShopAssociatedSIMs> GetAssociatedShops(Contracts.DealerSImAssociation.ShopAssociatedSIMsRequest request);

        [OperationContract]
        RefillDealerEWalletResponse RefillDealerEWallet(RefillDealerEWalletRequest request);

        [OperationContract]
        List<NumberInfo> GetAllNumbersRealtedByNumberId(string idNumber);

        [OperationContract]
        OTPResponse SendOTP(OTPRequest OTPRequest);

        [OperationContract]
        OTPResponse ValidateOTP(ValidateOTPRequest validateOTPRequest);

        [OperationContract]
        SendEsimQRImageResponse SendEsimQRImage(SendEsimQRImageRequest request);

        [OperationContract]
        TerminationResponse SIMTermination(TerminationRequest TerminationRequest);

        [OperationContract]
        CustomerActivePlanDetailsResponse GetCustomerSubscriptionInfo(CustomerPlanRequest CustomerPlanRequest);

        [OperationContract]
        VerifyChangeSubscriptionTypeResponse VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest VerifyChangeSubscriptionTypeRequest);

        [OperationContract]
        ChangeSubscriptionTypeResponse ChangeSubscriptionType(Contracts.ChangeSubscriptionTypeRequest request);

        [OperationContract]
        ModernTradeResponse SearchDealerModernTrade(ModernTradeRequest request);

        [OperationContract]
        NearestLocationsResponse GetNearestLocations(LoginRequest LoginRequest);

        [OperationContract]
        BaseResponse AttendanceTracking(AttendanceTrackingRequest request);

        [OperationContract]
        GetLocationByIDExternalResponse GetLocationByIDExternal(LoginRequest request);

        [OperationContract]
        BaseResponse DealerIsVerified(string sessionToken);

        [OperationContract]
        DealerAchievementResponse GetMTDealerAchievementReport(DealerAchievementRequest request);

        [OperationContract]
        MNPQuarantineStatusResponse IsNumberQuarantined(MNPQuarantineStatusRequest request);

        [OperationContract]
        VanityTypesResponse GetVanityTypes(LoginRequest loginRequest);

        [OperationContract]
        AvailableNumbersResponse GetAvailableNumbers(AvailableNumbersRequest request);

        [OperationContract]
        BookNumberResponse BookNumber(BookNumberRequest request);

        [OperationContract]
        ValidateIDBookingResponse ValidateIDBooking(ValidateIDBookingRequest request);

        [OperationContract]
        RebookResponse Rebook(BookNumberRequest request);

        [OperationContract]
        CancelBookingResponse CancelBookingNumber(CancelBookingNumberRequest request);

        [OperationContract]
        IsAttendanceVerificationRequiredResponse IsAttendanceVerificationRequired(LoginRequest request);

        [OperationContract] STCCLoginResponse STCCLogin(STCCLoginRequest request);

        [OperationContract]
        GetSubscriptionTypeResponse GetSubscribtionType(GetSubscriptionTypeRequest request);
    }
}
