﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using LP.Core.Utilities.Exceptions;
using System.Configuration;
using System.Drawing;
using System.IO;
using LP.Core.Utilities.ServiceModel;
using LP.Core.Utilities.Logging;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine.Utilities;
using LP.OMS.Channels.Engine.SCExternalApisService;
using LP.OMS.Channels.Contracts.DealerSImAssociation;
using LP.OMS.Channels.Engine.PlanServiceReference;

namespace LP.OMS.Channels.WCFServices
{
    [GeneralInspector]
    public class OnlineChannelServices : IOnlineChannelServices
    {
        public CustomerPlanDetailsResponse GetCustomerDetails(GetCustomerDetailsRequest customerDetails)
        {
            decimal VATPercentage = decimal.Parse(ConfigurationManager.AppSettings["VAT"]) / 100;
            decimal multiplier = (decimal)Math.Pow(10, 2);
            CustomerPlanDetailsResponse response = new CustomerPlanDetailsResponse()
            {
                IsPassed = true,
                ResponseMessage = "Success",
                CustomerSummary = new List<CustomerSummary>() { }
            };
            List<PlanResponse> PlanList = new List<PlanResponse>();
            SavedPlanResponse SavedPlan = null;
            bool enableDealerBalanceCheck = false;
            int scFlowStatus = 2;
            if (ConfigurationManager.AppSettings["SCFlowStatus"] != null)
            {
                scFlowStatus = int.Parse(ConfigurationManager.AppSettings["SCFlowStatus"]);
            }
            bool.TryParse(ConfigurationManager.AppSettings["EnableCheckingDealerBalanceInDigitalActivation"], out enableDealerBalanceCheck);
            List<Relative> pendingRelatives = new List<Relative>();
            bool isFamilyOnboarding = false;
            bool enableDSTOnboarding = false;
            FamilyDealerEligibility familyDealerEligibility = FamilyDealerEligibility.NotEligible;
            bool.TryParse(ConfigurationManager.AppSettings["EnableDSTOnboarding"], out enableDSTOnboarding);
            double CurrentAndriodApplicationMinimalVersion = double.Parse(ConfigurationManager.AppSettings["LP.OMS.Channels.AndriodApplicationMinimalVersion"]);
            string name = "";
            CallResponse oCallResponse = new CallResponse();
            Engine.AdministrationServices.Requester oRequester = null;
            double appVersion = double.Parse(customerDetails.LoginRequest.ApplicationVersion);
            bool isArabic = customerDetails?.LoginRequest?.language == 2;
            List<PromotionRequest> promotions = new List<PromotionRequest>();
            bool canPOSActivateDigital = false;

            oCallResponse = Controller.CheckRequester(customerDetails.LoginRequest, out oRequester,
                out name, !(appVersion >= CurrentAndriodApplicationMinimalVersion) ? true : false);
            if (oCallResponse.IsPassed == false)
            {
                return new CustomerPlanDetailsResponse()
                {
                    IsPassed = false,
                    ResponseMessage = "Failed on authenticate requester"
                };
            }

            try
            {
                CustomerDetailsResponse Customer = SCWrapper.GetCustomerDetails(customerDetails.Email);
                if (Customer == null || Customer.Response == null || Customer.Response.Code != 8)
                {
                    if (enableDSTOnboarding)
                    {
                        if (!Controller.IsDealerEligibleForDSTOnboarding(oRequester, out familyDealerEligibility))
                        {
                            return new CustomerPlanDetailsResponse()
                            {
                                IsPassed = false,
                                ResponseMessage = Controller.GetResourceMessage("MSG_DealerNotAuthorizedToProceed", customerDetails.LoginRequest.language)
                            };
                        }
                        if (Controller.IsCustomerEligibleForDSTOnboarding(customerDetails.Email))
                        {
                            response.ShowRegistrationAlert = true;
                            response.IsPassed = true;
                            response.ResponseMessage = Controller.GetResourceMessage("MSG_SelfCareRegistrationNeeded", customerDetails.LoginRequest.language);
                            return response;
                        }
                    }
                    else
                    {
                        response.IsPassed = false;
                        response.ResponseMessage = Controller.GetResourceMessage("MSG_InvalidEmail", customerDetails.LoginRequest.language);
                        return response;
                    }
                }

                if (Customer.Relatives != null && Customer.Relatives.Any()) // Has family onboarding
                {
                    if (customerDetails.Channel == ChannelType.WEB) // Prevent access family onboarding from WEB channel
                    {
                        response.IsPassed = false;
                        response.ResponseMessage = Controller.GetResourceMessage("MSG_ProhibitedWebAccess", customerDetails.LoginRequest.language);
                        return response;
                    }

                    if (Customer.Relatives.Any(x => x.Type == RelativeType.Parent)) // One of relatives isn't family member
                    {
                        response.IsPassed = false;
                        response.ResponseMessage = Controller.GetResourceMessage("MSG_ProhibitedChildAccess", customerDetails.LoginRequest.language);
                        return response;
                    }
                    else
                    {
                        pendingRelatives = Customer.Relatives.Where(r => r.Status == RelativeStatus.Pending).ToList();
                        if (!pendingRelatives.Any() && !enableDSTOnboarding)
                        {
                            response.IsPassed = false;
                            response.ResponseMessage = Controller.GetResourceMessage("MSG_NoBookingAvailable", customerDetails.LoginRequest.language);
                            return response;
                        }
                        else
                        {
                            // Check activation status for parent
                            if (!Controller.CheckActivationStatus(Customer.AccountID))
                            {
                                return new CustomerPlanDetailsResponse()
                                {
                                    IsPassed = false,
                                    ResponseMessage = Controller.GetResourceMessage("MSG_MainAccountInactive", customerDetails.LoginRequest.language)
                                };
                            }
                            isFamilyOnboarding = true;
                        }
                    }
                }
                else
                {
                    // Digital activation without family onboarding
                    isFamilyOnboarding = false;
                }

                var BookedNumbers = SCWrapper.GetUserBookedNumbers(customerDetails.Email, isFamilyOnboarding)
                        .ListResult;

                if (!Controller.IsDealerEligibleForDSTOnboarding(oRequester, out familyDealerEligibility) && isFamilyOnboarding)
                {
                    return new CustomerPlanDetailsResponse()
                    {
                        IsPassed = false,
                        ResponseMessage = Controller.GetResourceMessage("MSG_DealerNotAuthorizedToProceed", customerDetails.LoginRequest.language)
                    };
                }

                if (BookedNumbers == null || !BookedNumbers.Any())
                {
                    if (enableDSTOnboarding)
                    {
                        if (Controller.IsCustomerEligibleForDSTOnboarding(customerDetails.Email))
                        {
                            if (familyDealerEligibility == FamilyDealerEligibility.Both)
                            {
                                response.ShowAddNewSIMButton = true;
                                response.ShowAddNewDeviceButton = true;
                            }
                            else if (familyDealerEligibility == FamilyDealerEligibility.SIMOnly)
                            {
                                response.ShowAddNewSIMButton = true;
                            }
                            else if (familyDealerEligibility == FamilyDealerEligibility.DeviceOnly)
                            {
                                response.ShowAddNewDeviceButton = true;
                            }

                            if (response.ShowAddNewDeviceButton || response.ShowAddNewSIMButton)
                            {
                                response.IsPassed = true;
                                response.ResponseMessage = "Success";
                                return response;
                            }
                            else
                            {
                                response.IsPassed = false;
                                response.ResponseMessage = Controller.GetResourceMessage("MSG_DealerNotAuthorizedToProceed", customerDetails.LoginRequest.language);
                                return response;
                            }
                        }
                    }
                    else
                    {
                        response.IsPassed = false;
                        response.ResponseMessage = Controller.GetResourceMessage("MSG_NoBookingAvailable", customerDetails.LoginRequest.language);
                        return response;
                    }
                }

                response.CRMAccountNo = Customer.AccountID;

                foreach (BookedNumberV2 BookedNumber in BookedNumbers)
                {
                    PlanPrice planPrice = null;
                    if (Customer.FlowStatus > scFlowStatus)
                    {
                        List<string> PlanIds = new List<string>();
                        List<PlanResponse> UserPlans = new List<PlanResponse>();
                        CustomerSummary summary = new CustomerSummary()
                        {
                            Extras = new List<Extras>(),
                            Plans = new List<PlanResponse>(),
                            MNPData = new MNPData()
                        };

                        if (BookedNumber.IsMNP)
                        {
                            Controller.FillMNPData(summary.MNPData, BookedNumber,
                                customerDetails.LoginRequest.language);
                        }

                        summary.isDealerRegistration = BookedNumber.IsDealerReg;
                        summary.OnboardingType = Controller.GetOnboardingType(BookedNumber.ProductCode, isFamilyOnboarding);

                        if (summary.OnboardingType == OnboardingType.FamilySIMOnly)
                        {
                            if ((familyDealerEligibility != FamilyDealerEligibility.Both)
                                && (familyDealerEligibility != FamilyDealerEligibility.SIMOnly))
                            {
                                if (BookedNumbers.Count == 1)
                                {
                                    response.IsPassed = false;
                                    response.ResponseMessage = Controller.GetResourceMessage("MSG_DealerNotAuthorizedToProceed", customerDetails.LoginRequest.language);
                                    break;
                                }
                                continue;
                            }
                        }
                        else if (summary.OnboardingType == OnboardingType.FamilySIMWithDevice)
                        {
                            if ((familyDealerEligibility != FamilyDealerEligibility.Both)
                               && (familyDealerEligibility != FamilyDealerEligibility.DeviceOnly))
                            {
                                if (BookedNumbers.Count == 1)
                                {
                                    response.IsPassed = false;
                                    response.ResponseMessage = Controller.GetResourceMessage("MSG_DealerNotAuthorizedToProceed", customerDetails.LoginRequest.language);
                                    break;
                                }
                                continue;
                            }
                        }

                        SavedPlan = SCWrapper.GetSavedPlan(BookedNumber.MSISDN, true);
                        if (SavedPlan != null)
                        {
                            if (SavedPlan.Plan != null)
                                PlanIds = SavedPlan.Plan.Ids.Split(',').ToList();

                            if (SavedPlan.ExtrasSummary != null && SavedPlan.ExtrasSummary.Extras.Any())
                            {
                                foreach (Extra extra in SavedPlan.ExtrasSummary.Extras)
                                {
                                    summary.Extras.Add(new Extras()
                                    {
                                        Title = isArabic ? Controller.TranslateString(extra.Type) :
                                        (extra.Type == "FamilyRecharge" ? "Recharge" : extra.Type),
                                        Price = Convert.ToDecimal(extra.Value)
                                    });
                                }
                                summary.TotalExtrasPrice = SavedPlan.ExtrasSummary.CostVATex;
                            }

                            if (SavedPlan.PromotionIDs != null && SavedPlan.PromotionIDs.Any())
                            {
                                PromotionRequest reqPromotion = null;
                                foreach (var item in SavedPlan.PromotionIDs)
                                {
                                    reqPromotion = new PromotionRequest();
                                    reqPromotion.PluginCode = item.PluginCode;
                                    reqPromotion.PromotionId = item.PromotionId;
                                    promotions.Add(reqPromotion);
                                }
                            }
                        }

                        if (PlanIds != null && PlanIds.Any())
                        {
                            PlanList = POWrapper.GetServicesByIDs(PlanIds.Select(int.Parse).ToArray());
                            UserPlans = PlanList.Where(z => PlanIds.Contains(z.ID)).ToList();
                        }

                        if (!UserPlans.Any())
                            summary.TotalPlansPrice = 0;
                        else
                        {
                            planPrice = POWrapper.GetPackagePrice(Convert.ToInt32(BookedNumber.ProductCode),
                                PlanIds.Select(int.Parse).ToList(), BookedNumber.MSISDN, promotions);
                            decimal totalPlansPrice = planPrice.NetCost;
                            summary.TotalPlansPrice = Math.Ceiling(totalPlansPrice * multiplier) / multiplier;
                        }

                        summary.MSISDN = BookedNumber.MSISDN;
                        summary.PlanDesc = "";
                        summary.Plans = UserPlans.ToList();
                        summary.VanityPrice = BookedNumber.VanityPrice;
                        summary.VanityType = isArabic ? Controller.TranslateString(BookedNumber.VanityDesc)
                            : BookedNumber.VanityDesc;
                        summary.VanityPromotion = Controller.GetVanityPromotionMessage(BookedNumber.VanityID, customerDetails.LoginRequest.language, BookedNumber.EventId);
                        summary.IsActivated = BookedNumber.StatusID == ReservationType.Activated ? true : false;
                        summary.IsEsim = BookedNumber.IsEsim;
                        summary.EsimPriceVATExcluded = BookedNumber.SimPrice;

                        if (!summary.IsActivated)
                        {
                            response.Amount += Math.Ceiling((summary.TotalPlansPrice + summary.VanityPrice + summary.TotalExtrasPrice + BookedNumber.SimPrice + BookedNumber.DevicePrice) * multiplier) / multiplier;
                        }

                        summary.ProductCode = BookedNumber.ProductCode;
                        summary.Email = BookedNumber.Email;
                        summary.VanityCommitmentPeriod = isArabic
                            ? Controller.TranslateString(BookedNumber.VanityCommitmentPeriod)
                            : BookedNumber.VanityCommitmentPeriod;
                        summary.Promotions = Controller.GetPromotionDetails(planPrice, promotions);

                        response.CustomerSummary.Add(summary);

                        response.VAT = Math.Ceiling((response.Amount * VATPercentage) * multiplier) / multiplier;
                        response.AmountVATIncluded = Math.Ceiling((response.Amount * VATPercentage + response.Amount) * multiplier) / multiplier;
                        response.IsPaid = SavedPlan.PaymentTransactionId == null ? false : Controller.CheckPaymentStatus(SavedPlan.PaymentTransactionId, customerDetails.Email);

                        if (!response.IsPaid)
                        {
                            if (!Controller.IsDealerHasRight(ConfigurationManager.AppSettings["VanityCashCollection"], oRequester))
                            {
                                canPOSActivateDigital = Controller.CanPOSActivateDigital(oRequester);
                                if (!canPOSActivateDigital)
                                {
                                    string notAuthorizedForCashCollectionCode = "52";
                                    throw new LPBizException(notAuthorizedForCashCollectionCode,
                                         string.Format("Dealer {0} does not have cash collection / wallet deduction privilege",
                                         customerDetails.LoginRequest.Username));
                                }
                            }
                        }
                    }
                    else
                    {
                        response.IsPassed = false;
                        response.ResponseMessage = Controller.GetResourceMessage("MSG_ExpiredBooking", customerDetails.LoginRequest.language);
                    }
                }

                if ((enableDealerBalanceCheck && response.IsPassed) ||
                    (canPOSActivateDigital && !response.IsPaid))
                {
                    AvailableDealerBalanceResponse balance =
                    Controller.GetAvailableDealerBalance(customerDetails.LoginRequest);
                    if (response.AmountVATIncluded > Convert.ToDecimal(balance.Balance))
                    {
                        response = new CustomerPlanDetailsResponse()
                        {
                            IsPassed = false,
                            ResponseMessage = Controller.GetResourceMessage("MSG_InsufficientDealerBalance", customerDetails.LoginRequest.language)
                        };
                        return response;
                    }
                }
            }

            catch (LPBizException LPBizEx)
            {
                response.IsPassed = false;
                response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, customerDetails.LoginRequest.language);
            }
            catch (FaultException ex)
            {
                try
                {
                    throw new LPBizException(ex.Code.Name, ex.Message);
                }
                catch (LPBizException LPBizEx)
                {
                    response.IsPassed = false;
                    response.ResponseMessage = ExceptionHandling.HandleBackendException(LPBizEx, customerDetails.LoginRequest.language);
                }
            }
            catch (Exception ex)
            {
                response.IsPassed = false;
                ErrorHandling.HandleException("GetCustomerDetails", ex, customerDetails.Email, true);
            }

            return response;
        }

        public List<LP.OMS.Channels.Contracts.Country> GetCountryList(LoginRequest oLoginRequest)
        {
            List<LP.OMS.Channels.Contracts.Country> countryList = new List<LP.OMS.Channels.Contracts.Country>();
            try
            {
                countryList = Controller.GetCountryList(oLoginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCountryList", ex, oLoginRequest.Username, true);
            }

            return countryList;
        }

        public LP.OMS.Channels.Contracts.CallResponse SendRequest(SubmitRequest request)
        {
            LP.OMS.Channels.Contracts.CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.SendRequest(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendRequest", ex, request.oLoginRequest.Username, true);
            }
            return oCallResponse;
        }
        public LP.OMS.Channels.Contracts.CallResponse SubmitVirginActivation(VirginActivationRequest request)
        {
            LP.OMS.Channels.Contracts.CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.SubmitVirginActivation(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitVirginActivation", ex, request.oLoginRequest.Username, true);
            }
            return oCallResponse;
        }
        public LoginResponse Login(LoginRequest request)
        {
            LoginResponse response = new LoginResponse();

            try
            {
                response = Controller.Login(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Login", ex, request.Username, true);
            }

            return response;
        }

        public List<IDType> GetIDTypes(LoginRequest request)
        {
            List<IDType> idTypes = new List<IDType>();

            try
            {
                idTypes = Controller.GetIdTypeList(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetIDTypes", ex, request.Username, true);
            }

            return idTypes;
        }

        public List<LP.OMS.Channels.Contracts.Title> GetTitleList(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest)
        {
            List<LP.OMS.Channels.Contracts.Title> titleList = new List<LP.OMS.Channels.Contracts.Title>();
            try
            {
                titleList = Controller.GetTitleList(oLoginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTitleList", ex, oLoginRequest.Username, true);
            }
            return titleList;
        }

        public Lookups GetLookups(LoginRequest oLoginRequest)
        {
            Lookups lookups = new Lookups();

            try
            {
                lookups = Controller.GetLookups(oLoginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetLookups", ex, oLoginRequest.Username, true);
            }
            return lookups;
        }

        public DealerActivitiesResponseColelction GetDealerActivitiesList(AndroidReportRequest oAndroidReportRequest)
        {
            DealerActivitiesResponseColelction oCallResponse = new DealerActivitiesResponseColelction();
            oCallResponse.DealerActivitiesResponseList = new List<DealerActivitiesResponse>();

            try
            {
                oCallResponse = Controller.GetDealerActivitiesList(oAndroidReportRequest);

            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDealerActivitiesList", ex, oAndroidReportRequest.oLoginRequest.Username);
                oCallResponse = new DealerActivitiesResponseColelction();
                oCallResponse.IsPassed = false;

                if (oAndroidReportRequest.oLoginRequest.language == 2)
                    oCallResponse.ResponseMessage = "هناك خطأ اثناء تنفيذ العملية";
                else
                    oCallResponse.ResponseMessage = "Error While executing activities";

                return oCallResponse;
            }

            return oCallResponse;
        }

        public ResubmissionCallResponse GetFailedRequests(FaildActivationRequest oFaildActivationRequest)
        {

            ResubmissionCallResponse oCallResponse = new ResubmissionCallResponse();

            try
            {
                oCallResponse.CallResponseObj = new CallResponse();
                oCallResponse = Controller.GetFailedRequests(oFaildActivationRequest);

            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetFailedRequests", ex, oFaildActivationRequest.oLoginRequest.Username);
                oCallResponse.CallResponseObj = new CallResponse();

                oCallResponse.CallResponseObj.IsPassed = false;
                oCallResponse.CallResponseObj.ResponseMessage = "Error While executing activities";
                return oCallResponse;
            }

            return oCallResponse;
        }

        public CallResponse ChangePassword(HashingRequest oHashingRequest)
        {
            CallResponse oCallResponse = null;

            try
            {
                oCallResponse = Controller.ChangePassword(oHashingRequest);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ChangePassword", ex, oHashingRequest.LoginRequest.Username);
                oCallResponse = new CallResponse();
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = "Error While executing activities";
                return oCallResponse;
            }

            return oCallResponse;
        }

        public CallResponse ResubmitRequest(ResubmitRequestObj request)
        {
            CallResponse oCallResponse = null;

            try
            {
                oCallResponse = Controller.ResubmitRequest(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ResubmitRequest", ex, request.LoginRequest.Username, true);
            }
            return oCallResponse;
        }

        public StatusCheckResponse StatusCheck(StatusCheckRequest objRequest)
        {
            StatusCheckResponse objResponse = new StatusCheckResponse();

            try
            {
                objResponse = Controller.StatusCheck(objRequest);

            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("StatusCheck", ex, objRequest.LoginRequest.Username);

                objResponse.IsPassed = false;
                objResponse.ResponseMessage = "Error While executing activities";

            }
            return objResponse;

        }

        public EloadResponse Eload(EloadRequest objRequest)
        {
            EloadResponse objResponse = new EloadResponse();

            try
            {
                objResponse = Controller.Eload(objRequest);

            }
            catch (Exception ex)
            {
                objResponse.IsPassed = false;
                objResponse.ResponseMessage = "General Error.";
            }
            return objResponse;
        }

        public void HandleException(string methodName, string message, string extradetails)
        {
            ErrorHandling.AddToErrorLog("Error", message, methodName, extradetails);
        }

        public ActivationReportCounters GetActivationReportCounters(LoginRequest objLogin)
        {
            ActivationReportCounters objActivationReportCounters = new ActivationReportCounters();
            try
            {
                objActivationReportCounters = Controller.GetActivationReportCounters(objLogin);
            }
            catch (Exception ex)
            {
                objActivationReportCounters.IsPassed = false;
                objActivationReportCounters.ResponseMessage = ex.Message;
            }

            return objActivationReportCounters;
        }

        public List<Contracts.ActivationResponse> GetDealerActivations(Contracts.CommissionDataByDealerRequest objActivationReportRequest)
        {
            List<Contracts.ActivationResponse> lstActivations = null;
            try
            {
                lstActivations = Controller.GetDealerActivations(objActivationReportRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDealerActivations", ex, objActivationReportRequest.objLogin.Username, true);
            }

            return lstActivations;
        }

        public List<CommissionReportResponse> GetCommissionReport(Contracts.CommissionDataByDealerRequest objCommissionDataByDealerRequest)
        {
            List<CommissionReportResponse> objResponse = new List<CommissionReportResponse>();
            try
            {
                objResponse = Controller.GetCommissionReport(objCommissionDataByDealerRequest);

            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCommissionReport", ex, objCommissionDataByDealerRequest.objLogin.Username, true);
            }

            return objResponse;
        }

        public LP.OMS.Channels.Contracts.NotifyApplicationLogResponse NotifyLocalApplicationLog(NotifyApplicationLogRequest request)
        {
            NotifyApplicationLogResponse objResponse = new NotifyApplicationLogResponse();
            try
            {
                Controller.NotifyLocalApplicationLog(request);
                objResponse.IsPassed = true;
            }
            catch (Exception ex)
            {
                objResponse.IsPassed = false;
                ErrorHandling.HandleException("NotifyLocalApplicationLog", ex, request.oLoginRequest.Username);
            }

            return objResponse;
        }

        public PhysicalFormCheckResponse PhysicalFormCollectionCheck(PhysicalFormCheckRequest request)
        {
            PhysicalFormCheckResponse objPhysicalFormCheckResponse = new PhysicalFormCheckResponse();

            try
            {
                objPhysicalFormCheckResponse = Controller.PhysicalFormCollectionCheck(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("PhysicalFormCollectionCheck", ex, request.LoginRequest.Username, true);
            }
            return objPhysicalFormCheckResponse;
        }

        public CallResponse PhysicalFormsCollectionSubmit(PhysicalFormCollectionRequest request)
        {
            LP.OMS.Channels.Contracts.CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.PhysicalFormsCollectionSubmit(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("PhysicalFormsCollectionSubmit", ex, request.LoginRequest.Username, true);
            }
            return oCallResponse;
        }

        public VirginVanityActivationCheckResponse VerifyVirginVanityNumber(VirginVanityActivationCheckRequest objVirginVanityActivationCheckRequest)
        {
            LP.OMS.Channels.Contracts.VirginVanityActivationCheckResponse oVirginVanityActivationCheckResponse = new VirginVanityActivationCheckResponse();

            try
            {
                oVirginVanityActivationCheckResponse = Controller.VerifyVirginVanityNumber(objVirginVanityActivationCheckRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyVirginVanityNumber", ex, objVirginVanityActivationCheckRequest.LoginRequest.Username, true);
            }
            return oVirginVanityActivationCheckResponse;
        }

        public CallResponse ActivateVirginVanityNumber(VirginVanityActivationActivationRequest objVirginVanityActivationActivationRequest)
        {
            LP.OMS.Channels.Contracts.CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.ActivateVirginVanityNumber(objVirginVanityActivationActivationRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ActivateVirginVanityNumber", ex, objVirginVanityActivationActivationRequest.LoginRequest.Username, true);
            }
            return oCallResponse;
        }

        public DealerBalanceResponse GetDealerBalance(LoginRequest objLoginRequest)
        {
            LP.OMS.Channels.Contracts.DealerBalanceResponse oDealerBalanceResponse = new DealerBalanceResponse();

            try
            {
                oDealerBalanceResponse = Controller.GetDealerBalance(objLoginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDealerBalance", ex, objLoginRequest.Username, true);
            }
            return oDealerBalanceResponse;
        }

        public CallResponse CheckCustomerId(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            LP.OMS.Channels.Contracts.CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.CheckCustomerId(objCheckCustomerIdRquest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ActivateVirginVanityNumber", ex, objCheckCustomerIdRquest.LoginRequest.Username, true);
            }
            return oCallResponse;
        }

        public CheckCustomerSubscriptionsResponse CheckCustomerSubscriptions(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            CheckCustomerSubscriptionsResponse objCheckCustomerSubscriptionsResponse = new CheckCustomerSubscriptionsResponse();

            try
            {
                objCheckCustomerSubscriptionsResponse = Controller.CheckCustomerSubscriptions(objCheckCustomerIdRquest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckCustomerSubscriptions", ex, objCheckCustomerIdRquest.LoginRequest.Username, true);
            }
            return objCheckCustomerSubscriptionsResponse;
        }

        public VerifyCustomerResponse VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest)
        {
            VerifyCustomerResponse objVerifyCustomerResponse = new VerifyCustomerResponse();

            try
            {
                objVerifyCustomerResponse = Controller.VerifyCustomerSubscriptions(objVerifyCustomerRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyCustomerSubscriptions", ex, objVerifyCustomerRequest.LoginRequest.Username, true);
            }
            return objVerifyCustomerResponse;
        }

        public CheckVirginCustomerIdResponse CheckVirginCustomerId(CheckVirginCustomerIdRequest objCheckVirginCustomerIdRquest)
        {
            CheckVirginCustomerIdResponse objCheckVirginCustomerIdResponse = new CheckVirginCustomerIdResponse();

            try
            {
                objCheckVirginCustomerIdResponse = Controller.CheckVirginCustomerId(objCheckVirginCustomerIdRquest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckVirginCustomerId", ex, objCheckVirginCustomerIdRquest.LoginRequest.Username, true);
            }
            return objCheckVirginCustomerIdResponse;
        }

        public CallResponse LogFingerprintNFIQTrials(LogFingerprintNFIQTrialsRequest logNFIQTrialsRequest)
        {
            CallResponse result = new CallResponse();

            try
            {
                result = Controller.LogFingerprintNFIQTrials(logNFIQTrialsRequest);
            }
            catch (Exception)
            {
                result.IsPassed = false;
            }

            return result;
        }

        public CallResponse VerifyOwnershipChange(OwnerChangeRequest request)
        {
            var response = new CallResponse();

            try
            {
                response = Controller.VerifyOwnershipChange(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyOwnershipChange", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        public CallResponse SubmitOwnershipChange(OwnerChangeRequest request)
        {
            var response = new CallResponse();

            try
            {
                response = Controller.SubmitOwnershipChange(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitOwnershipChange", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        //Anas Alzube
        public CallResponse ReplaceSIM(SIMReplacementRequest Request)
        {
            var response = new CallResponse();
            try
            {
                response = Controller.ReplaceSIM(Request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
                response.IsPassed = false;
            }
            catch (FaultException)
            {
                response.IsPassed = false;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ReplaceSIM", ex, "");
                response.IsPassed = false;
            }
            return response;
        }

        //Anas Alzube
        public CallResponse ValidateReplaceSIM(SIMReplacementRequest Request)
        {
            var response = new CallResponse();
            try
            {
                response = Controller.ValidateReplaceSIM(Request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
                response.IsPassed = false;
            }
            catch (FaultException)
            {
                response.IsPassed = false;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateReplaceSIM", ex, "");
                response.IsPassed = false;
            }
            return response;
        }
        //Anas Alzube
        public CallResponse AddStockRequest(StockItemRequest sRequest)
        {
            CallResponse oCallresponse = new CallResponse();
            try
            {
                oCallresponse = Controller.AddStockRequest(sRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AddStockRequest", ex, "");
                oCallresponse.IsPassed = false;
                oCallresponse.ResponseMessage = ex.Message;
            }
            return oCallresponse;
        }
        //Anas Alzube
        public BundleResponse GetBundle(BundlesRequest request)
        {
            BundleResponse oBundleResponse = new BundleResponse();
            try
            {
                oBundleResponse = Controller.GetBundle(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetBundle", ex, "");
                oBundleResponse.IsPassed = false;
                oBundleResponse.ResponseMessage = ex.Message;
            }
            return oBundleResponse;
        }
        //Anas Alzube
        public CallResponse SubmitBundle(SubmitBundleRequest SubmitBundleRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                oCallResponse = Controller.SubmitBundle(SubmitBundleRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitBundle", ex, "");
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public void LogError(DSTErrorLog dstErrorLog)
        {
            try
            {
                Controller.LogError(dstErrorLog);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("DST LogError", ex, string.Empty, true);
            }
        }

        public int SelfcareResubmitRequest(string msisdn, string idNumber, int brand, int documentType, string additionalInfo, List<byte[]> lstResubmissionDocuments)
        {
            int ResponseCode = (int)ResubmissionResponseCodes.UNKNOWN_ERROR;
            try
            {
                ResponseCode = Controller.SelfcareResubmitRequest(msisdn, idNumber, brand, documentType, additionalInfo, lstResubmissionDocuments);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SelfcareResubmitRequest", ex, "Selfcare", false);
            }
            return ResponseCode;
        }

        public AvailableProductsResponse GetAvailableProducts(ProductSellingRequest request)
        {
            AvailableProductsResponse oAvailableResponse = new AvailableProductsResponse();
            try
            {
                oAvailableResponse = Controller.GetAvailableProducts(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableProducts", ex, request.oLoginRequest.Username, true);
            }
            return oAvailableResponse;
        }

        public ProductSellingResponse SubmitProductSelling(ProductSellingRequest request)
        {
            ProductSellingResponse ProductSellingResponse = new ProductSellingResponse();
            try
            {
                ProductSellingResponse = Controller.SubmitProductSelling(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitProductSelling", ex, request.oLoginRequest.Username, true);
            }
            return ProductSellingResponse;
        }

        public CallResponse CheckProductSellingEligibility(CheckProductSellingEligibilityRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                oCallResponse = Controller.CheckProductSellingEligibility(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckProductSellingEligibility", ex, request.oLoginRequest.Username, true);
            }
            return oCallResponse;
        }

        public CallResponse VerfiyLoginCode(VerfiyLoginCode request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.VerfiyLoginCode(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerfiyLoginCode", ex, request.oLoginRequest.Username, true);
            }

            return oCallResponse;
        }

        public CallResponse CheckSessionValidity(string sessionToken, int lang, bool ExtendSession = true)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.CheckSessionValidity(sessionToken, lang, ExtendSession);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckSessionValidity", ex, sessionToken.ToString(), true);
            }

            return oCallResponse;
        }

        public CallResponse ResendLoginCode(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.ResendLoginCode(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ResendLoginCode", ex, request.Username, true);
            }

            return oCallResponse;
        }

        public CallResponse Logout(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.Logout(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Logout", ex, request.Username, true);
            }

            return oCallResponse;
        }

        public CallResponse ValidateSessionTransaction(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                oCallResponse = Controller.ValidateSessionTransaction(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateSessionTransaction", ex, request.Username, true);
            }
            return oCallResponse;
        }

        public CallResponse GetSessionData(string sessionToken, int lang, out Session session)
        {
            session = null;
            CallResponse oCallResponse = new CallResponse();
            try
            {
                oCallResponse = Controller.CheckSessionValidity(sessionToken, lang);

                if (oCallResponse.IsPassed == true)
                {
                    Controller.GetSessionData(sessionToken, lang, out session);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSessionData", ex, sessionToken.ToString(), true);
            }

            return oCallResponse;
        }

        public CallResponse UpdateSessionData(string sessionToken, int lang, Session session)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                if (sessionToken != session.SessionToken)
                {
                    oCallResponse.IsPassed = false;
                    return oCallResponse;
                }
                oCallResponse = Controller.CheckSessionValidity(sessionToken, lang);

                if (oCallResponse.IsPassed == true)
                {
                    Controller.UpdateSessionData(sessionToken, lang, session);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("UpdateSessionData", ex, sessionToken.ToString(), true);
            }

            return oCallResponse;
        }

        public bool BlockDealer(string dealerCode, string channelName)
        {
            return Controller.BlockDealer(dealerCode, channelName);
        }

        public SematiLoginCallResponse LoginToSemati(LoginRequest request)
        {
            var response = new SematiLoginCallResponse();

            try
            {
                response = Controller.LoginToSemati(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("LoginToSemati", ex, request.Username, true);
            }

            return response;
        }

        public CallResponse CheckSematiAuthentication(CheckSematiAuthenticationObject checkSematiAuthenticationObject)
        {
            var response = new CallResponse();

            try
            {
                response = Controller.CheckSematiAuthentication(checkSematiAuthenticationObject);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckSematiAuthentication", ex, checkSematiAuthenticationObject.DealerCode, true);
            }

            return response;
        }

        public OneSignalResponse GetOneSignalData(LoginRequest loginRequest)
        {
            var response = new OneSignalResponse();

            try
            {
                response = Controller.GetOneSignalData(loginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetOneSignalData", ex, loginRequest.Username, true);
            }

            return response;
        }

        public NextSessionCheckTimeResponse GetNextSessionCheckTime(LoginRequest loginRequest, string sessionToken)
        {
            var response = new NextSessionCheckTimeResponse();
            try
            {
                return Controller.GetNextSessionCheckTime(sessionToken);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetNextSessionCheckTime", ex, loginRequest.Username, true);
            }

            return response;
        }

        public ExtendSessionRequest ExtendSession(LoginRequest loginRequest, string sessionToken)
        {
            try
            {
                return Controller.ExtendSession(sessionToken);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ExtendSession", ex, loginRequest.Username, true);
            }
            ExtendSessionRequest oExtendSessionRequest = new ExtendSessionRequest() { AMErrorCode = (int)AMErrorCode.SessionInvalid };
            return oExtendSessionRequest;
        }

        public GetAllowedIDTypesByIMSIResponse GetAllowedIDTypesByIMSI(GetAllowedIDTypesByIMSIObj request)
        {
            GetAllowedIDTypesByIMSIResponse response = new GetAllowedIDTypesByIMSIResponse();
            response.idTypes = new List<AllowedIDType>();

            try
            {
                response = Controller.GetAllowedIDTypesByIMSI(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAllowedIDTypesByIMSI", ex, request.IMSI, true);
            }

            return response;
        }

        public GetSubscriptionTypesResponse GetSubscriptionTypes(GetSubscriptionTypeObj request)
        {
            GetSubscriptionTypesResponse response = new GetSubscriptionTypesResponse();
            response.lstSubscriptionTypes = new List<Subscription>();

            try
            {
                response = Controller.GetSubscriptionTypes(request.LoginRequest, request.IMSI, request.DealerCode, request.CustomerIdType, request.Brand, request.OnboardingType, request.ProductCode, request.IsEsimActivation);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSubscriptionTypes", ex, request.DealerCode, true);
            }

            return response;
        }

        public List<SliderImageResponse> GetSliderImages(LoginRequest loginRequest, Brands brand)
        {
            List<SliderImageResponse> sliderImageResponse = new List<SliderImageResponse>();
            try
            {
                //ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetSliderImagesControllerB", "GetSliderImagesControllerB",
               //loginRequest.Username, Newtonsoft.Json.JsonConvert.SerializeObject(sliderImageResponse) + "Brand: " + brand);
                sliderImageResponse = Controller.GetSliderImages(loginRequest, brand);
                //ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetSliderImagesControllerA", "GetSliderImagesControllerA",
               //loginRequest.Username, Newtonsoft.Json.JsonConvert.SerializeObject(sliderImageResponse[0]) + "Brand: " + brand);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
              //  ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetSliderImages", "GetSliderImages",
              //loginRequest.Username, Newtonsoft.Json.JsonConvert.SerializeObject(ex));

                ErrorHandling.HandleException("GetSliderImages", ex, loginRequest.Username, true);
            }

            return sliderImageResponse;
        }

        public int GetTotalActivation(LoginRequest loginRequest)
        {
            int totalActivation = 0;
            try
            {
                totalActivation = Controller.GetTotalActivation(loginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTotalActivation", ex, loginRequest.Username, true);
            }

            return totalActivation;
        }

        public double GetTotalCommission(LoginRequest loginRequest)
        {
            double totalCommission = 0;
            try
            {
                totalCommission = Controller.GetTotalCommission(loginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTotalCommission", ex, loginRequest.Username, true);
            }

            return totalCommission;
        }

        public AvailableDealerBalanceResponse GetAvailableDealerBalance(LoginRequest loginRequest)
        {
            AvailableDealerBalanceResponse oDealerBalanceResponse = new AvailableDealerBalanceResponse();

            try
            {
                oDealerBalanceResponse = Controller.GetAvailableDealerBalance(loginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableDealerBalance", ex, loginRequest.Username, true);
            }
            return oDealerBalanceResponse;
        }

        public GetStocksResponse GetStocks(LoginRequest loginRequest, Brands brand)
        {
            GetStocksResponse response = new GetStocksResponse();
            try
            {
                response = Controller.GetAllStocks(loginRequest, brand);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetStocks", ex, loginRequest.Username, true);
            }
            return response;
        }

        public AvailableProductsResponse GetAvailableProductsBySubscriptionType(ProductSellingBySubscriptionTypeRequest productSellingRequest)
        {
            AvailableProductsResponse response = new AvailableProductsResponse();
            try
            {

                response = Controller.GetAvailableProductsBySubscriptionType(productSellingRequest);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableProductsBySubscriptionType", ex, productSellingRequest.oLoginRequest.Username, true);
            }
            return response;
        }

        public SendRequestStockResponse SendRequestStock(SendRequestStockRequest stockRequest)
        {
            SendRequestStockResponse response = new SendRequestStockResponse();
            try
            {
                response = Controller.SendRequestStock(stockRequest.LoginRequest, stockRequest.StockInfo);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendRequestStock", ex, stockRequest.LoginRequest.Username, true);
            }
            return response;
        }

        public List<AvailableRatePlan> GetAvailableVanityRatePlans(LoginRequest loginRequest, string idType)
        {
            List<AvailableRatePlan> plans = new List<AvailableRatePlan>();
            try
            {
                plans = Controller.GetAvailableVanityRatePlans(loginRequest, idType);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableVanityRatePlans", ex, loginRequest.Username, true);
            }
            return plans;
        }

        public double GetRemainingSessionInSeconds(LoginRequest loginRequest)
        {
            TimeSpan span = new TimeSpan();
            double totalRemainingSeconds = 0;
            try
            {
                //ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetRemainingSessionInSecondsB", "GetRemainingSessionInSecondsB",
             //loginRequest.Username, Newtonsoft.Json.JsonConvert.SerializeObject(span));
                span = Controller.GetRemainingSessionInSeconds(loginRequest);
                //ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetRemainingSessionInSecondsA", "GetRemainingSessionInSecondsA",
             //loginRequest.Username, Newtonsoft.Json.JsonConvert.SerializeObject(span));
                totalRemainingSeconds = span.TotalSeconds;
            }
            catch (Exception ex)
            {
             //   ErrorHandling.AddToErrorLog(ErrorTypes.Information, "GetRemainingSessionInSeconds", "GetRemainingSessionInSeconds",
             //loginRequest.Username, Newtonsoft.Json.JsonConvert.SerializeObject(ex));

                ErrorHandling.HandleException("GetRemainingSessionInSeconds", ex, loginRequest.Username, true);
            }
            return totalRemainingSeconds;
        }

        public List<PossibleFailureReason> GetPossibleFailureReasons(LoginRequest loginRequest, Brands brand)
        {
            List<PossibleFailureReason> possibleFailureReasons = new List<PossibleFailureReason>();
            try
            {
                possibleFailureReasons = Controller.GetPossibleFailureReasons(loginRequest, brand);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetPossibleFailureReasons", ex, loginRequest.Username, true);
            }
            return possibleFailureReasons;
        }

        public bool SubmitDealerFailureReasonAndNote(DealerFailureReasonNote failureReason)
        {
            bool response = false;
            try
            {
                response = Controller.SubmitDealerFailureReasonAndNote(failureReason, failureReason.LoginRequest);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitDealerFailureReasonAndNote", ex, failureReason.LoginRequest.Username, true);
            }
            return response;
        }

        #region New Activation & Commission Reports

        public ActivationGradeDetailsReportResponse GetActivationGradeDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationGradeDetailsReportResponse response = new ActivationGradeDetailsReportResponse();
            try
            {
                response = Controller.GetActivationGradeDetailsReport(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationGradeDetailsReport", ex, request.loginRequest.Username, true);
            }
            return response;
        }

        public ActivationRechargeDetailsReportResponse GetActivationRechargeDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationRechargeDetailsReportResponse response = new ActivationRechargeDetailsReportResponse();
            try
            {
                response = Controller.GetActivationRechargeDetailsReport(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationRechargeDetailsReport", ex, request.loginRequest.Username, true);
            }
            return response;
        }

        public ActivationFPEDetailsReportResponse GetActivationFPEDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationFPEDetailsReportResponse response = new ActivationFPEDetailsReportResponse();
            try
            {
                response = Controller.GetActivationFPEDetailsReport(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationFPEDetailsReport", ex, request.loginRequest.Username, true);
            }
            return response;
        }

        public CommissionPaymentDetailsReportResponse GetCommissionPaymentDetails(ActivationCommissionReportRequest request)
        {
            CommissionPaymentDetailsReportResponse response = new CommissionPaymentDetailsReportResponse();
            try
            {
                response = Controller.GetCommissionPaymentDetails(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCommissionPaymentDetails", ex, request.loginRequest.Username, true);
            }
            return response;
        }

        public ActivationReportSummary GetActivationReportSummary(ActivationCommissionReportRequest request)
        {
            ActivationReportSummary response = new ActivationReportSummary();
            try
            {
                response = Controller.GetActivationReportSummary(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationReportSummary", ex, request.loginRequest.Username, true);
            }
            return response;
        }

        public CommissionReportSummary GetCommissionReportSummary(ActivationCommissionReportRequest request)
        {
            CommissionReportSummary response = new CommissionReportSummary();
            try
            {
                response = Controller.GetCommissionReportSummary(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationReportSummary", ex, request.loginRequest.Username, true);
            }
            return response;
        }

        #endregion

        public List<OperatorDetails> GetTelecomOperators(LoginRequest request)
        {
            List<OperatorDetails> response = new List<OperatorDetails>();
            try
            {
                response = Controller.GetTelecomOperators(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTelecomOperators", ex, request.Username, true);
            }
            return response;
        }

        public CancelMNPPortInResponse CancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            CancelMNPPortInResponse response = new CancelMNPPortInResponse();
            try
            {
                response = Controller.CancelMNPPortInRequest(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelMNPPortInRequest", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public CancelMNPPortInResponse ValidateCancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            CancelMNPPortInResponse response = new CancelMNPPortInResponse();
            try
            {
                response = Controller.ValidateCancelMNPPortInRequest(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateCancelMNPPortInRequest", ex, request.LoginRequest.Username, true);
            }
            return response;
        }


        ////Anas Alzube 
        //public CallResponse PasswordResetGetVerificationCode(PasswordResetGetVerificationCodeRequest request)
        //{

        //    CallResponse oCallResponse = new CallResponse();
        //    try
        //    {
        //        oCallResponse = Controller.PasswordResetGetVerificationCode(request);
        //    }
        //    catch (LPBizException lpBizException)
        //    {
        //        lpBizException.PuplishFault();
        //    }
        //    catch (FaultException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("ResetPassword", ex, "", false);
        //        oCallResponse.IsPassed = false;
        //        oCallResponse.ResponseMessage = ex.Message;
        //    }
        //    return oCallResponse;
        //}

        //public CheckVerificationResetPasswordResponse CheckVerificationResetPassword(CheckVerificationResetPasswordRequest request)
        //{
        //    CheckVerificationResetPasswordResponse response = new CheckVerificationResetPasswordResponse();
        //    try
        //    {
        //        response = Controller.CheckVerificationResetPassword(request);
        //    }
        //    catch (LPBizException lpBizException)
        //    {
        //        lpBizException.PuplishFault();
        //    }
        //    catch (FaultException)
        //    {
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("CheckVerificationResetPassword", ex, "", false);
        //        response.oCallResponse.IsPassed = false;
        //        response.oCallResponse.ResponseMessage = ex.Message;
        //    }
        //    return response;
        //}

        public List<decimal> GetTopupAmounts(LoginRequest request)
        {
            List<decimal> response = new List<decimal>();
            try
            {
                response = Controller.GetTopupAmounts();
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTopupAmounts", ex, request.Username, true);
            }
            return response;
        }

        public ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request)
        {
            ValidateAndRegisterFamilyResponse response = new ValidateAndRegisterFamilyResponse();
            try
            {
                response = Controller.ValidateAndRegisterFamily(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateAndRegisterFamily", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public CancelFamilyRegistrationResponse CancelFamilyRegistration(CancelFamilyRegistrationRequest request)
        {
            CancelFamilyRegistrationResponse response = new CancelFamilyRegistrationResponse();
            try
            {
                response = Controller.CancelFamilyRegistration(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelFamilyRegistration", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public BaseResponse AssociateScannedSim(DealerSimAssociationRequest request)
        {
            Contracts.BaseResponse response = new Contracts.BaseResponse();
            try
            {
                response = Controller.AssociateScannedSim(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AssociateScannedSim", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public PagedResult<ShopAssociatedSIMs> GetAssociatedShops(ShopAssociatedSIMsRequest request)
        {
            PagedResult<ShopAssociatedSIMs> response = new PagedResult<ShopAssociatedSIMs>();
            try
            {
                response = Controller.GetAssociatedShops(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAssociatedShops", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public RefillDealerEWalletResponse RefillDealerEWallet(RefillDealerEWalletRequest request)
        {
            RefillDealerEWalletResponse response = new RefillDealerEWalletResponse();
            try
            {
                response = Controller.RefillDealerEWallet(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("RefillDealerEWallet", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public List<NumberInfo> GetAllNumbersRealtedByNumberId(string idNumber)
        {
            List<NumberInfo> numberInfoList = new List<NumberInfo>();
            try
            {
                numberInfoList = Controller.GetAllNumbersRealtedByNumberId(idNumber);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAllNumbersRealtedByNumberId", ex, "", true);
            }
            return numberInfoList;
        }

        public OTPResponse ValidateOTP(ValidateOTPRequest validateOTPRequest)
        {
            OTPResponse response = new OTPResponse();
            try
            {
                response = Controller.ValidateOTP(validateOTPRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateOTP", ex, validateOTPRequest.LoginRequest.Username, true);
            }
            return response;

        }

        public OTPResponse SendOTP(OTPRequest OTPRequest)
        {
            OTPResponse response = new OTPResponse();
            try
            {
                response = Controller.SendOTP(OTPRequest);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendOTP", ex, "", true);
            }

            return response;
        }

        public SendEsimQRImageResponse SendEsimQRImage(SendEsimQRImageRequest request)
        {
            SendEsimQRImageResponse response = new SendEsimQRImageResponse();
            try
            {
                response = Controller.SendEsimQRImage(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendEsimQRImage", ex, request.LoginRequest.Username, true);
            }
            return response;
        }

        public TerminationResponse SIMTermination(TerminationRequest TerminationRequest)
        {
            TerminationResponse response = new TerminationResponse();
            try
            {
                response = Controller.SIMTermination(TerminationRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SIMTermination", ex, TerminationRequest.LoginRequest.Username, true);
            }

            return response;
        }

        public CustomerActivePlanDetailsResponse GetCustomerSubscriptionInfo(CustomerPlanRequest CustomerPlanRequest)
        {
            CustomerActivePlanDetailsResponse response = new CustomerActivePlanDetailsResponse();
            try
            {
                response = Controller.GetCustomerSubscriptionInfo(CustomerPlanRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCustomerSubscriptionInfo", ex, CustomerPlanRequest.LoginRequest.Username, true);
            }

            return response;
        }

        public VerifyChangeSubscriptionTypeResponse VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest VerifyChangeSubscriptionTypeRequest)
        {
            VerifyChangeSubscriptionTypeResponse response = new VerifyChangeSubscriptionTypeResponse();
            try
            {
                response = Controller.VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerfityChangeSubscriptionType", ex, VerifyChangeSubscriptionTypeRequest.LoginRequest.Username, true);
            }

            return response;
        }

        public ChangeSubscriptionTypeResponse ChangeSubscriptionType(ChangeSubscriptionTypeRequest request)
        {
            ChangeSubscriptionTypeResponse response = new ChangeSubscriptionTypeResponse();
            try
            {
                response = Controller.ChangeSubscriptionType(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ChangeSubscriptionType", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        public ModernTradeResponse SearchDealerModernTrade(ModernTradeRequest request)
        {
            ModernTradeResponse response = new ModernTradeResponse();
            try
            {
                response = Controller.SearchDealerModernTrade(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SearchDealerModernTrade", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        public NearestLocationsResponse GetNearestLocations(LoginRequest LoginRequest)
        {
            NearestLocationsResponse response = new NearestLocationsResponse();
            try
            {
                response = ModernTradeController.GetNearestLocations(LoginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetNearestLocations", ex, LoginRequest.Username, true);
            }

            return response;
        }

        public BaseResponse AttendanceTracking(AttendanceTrackingRequest request)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response = ModernTradeController.AttendanceTracking(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AttendanceTracking", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        public GetLocationByIDExternalResponse GetLocationByIDExternal(LoginRequest request)
        {
            GetLocationByIDExternalResponse response = new GetLocationByIDExternalResponse();
            try
            {
                response = ModernTradeController.GetLocationByIDExternal(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetLocationByIDExternal", ex, request.Username, true);
            }

            return response;
        }

        public BaseResponse DealerIsVerified(string sessionToken)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                response = ModernTradeController.DealerIsVerified(sessionToken);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("DealerIsVerified", ex, sessionToken, true);
            }

            return response;
        }

        public DealerAchievementResponse GetMTDealerAchievementReport(DealerAchievementRequest request)
        {
            DealerAchievementResponse response = new DealerAchievementResponse();
            try
            {
                response = ModernTradeController.GetMTDealerAchievementReport(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetMTDealerAchievementReport", ex,
                    request?.LoginRequest?.Username, true);
            }

            return response;
        }


        public MNPQuarantineStatusResponse IsNumberQuarantined(MNPQuarantineStatusRequest request)
        {
            MNPQuarantineStatusResponse response = new MNPQuarantineStatusResponse();
            try
            {
                response = Controller.IsNumberQuarantined(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsNumberQuarantined", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        public VanityTypesResponse GetVanityTypes(LoginRequest loginRequest)
        {
            VanityTypesResponse response = new VanityTypesResponse();
            try
            {
                response = Controller.GetVanityTypes(loginRequest);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetVanityTypes", ex, loginRequest.Username, true);
            }

            return response;
        }

        public AvailableNumbersResponse GetAvailableNumbers(AvailableNumbersRequest request)
        {
            AvailableNumbersResponse response = new AvailableNumbersResponse();
            try
            {
                response = Controller.GetAvailableNumbers(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableNumbers", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }

        public BookNumberResponse BookNumber(BookNumberRequest request)
        {
            BookNumberResponse response = new BookNumberResponse();
            try
            {
                response = Controller.BookNumber(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("BookNumber", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }

        public ValidateIDBookingResponse ValidateIDBooking(ValidateIDBookingRequest request)
        {
            ValidateIDBookingResponse response = new ValidateIDBookingResponse();
            try
            {
                response = Controller.ValidateIDBooking(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateIDBooking", ex, request.LoginRequest.Username, true);
            }

            return response;
        }

        public RebookResponse Rebook(BookNumberRequest request)
        {
            RebookResponse response = new RebookResponse();
            try
            {
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Rebook", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }

        public CancelBookingResponse CancelBookingNumber(CancelBookingNumberRequest request)
        {
            CancelBookingResponse response = new CancelBookingResponse();
            try
            {
                response = Controller.CancelBookingNumber(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelBooking", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }

        public IsAttendanceVerificationRequiredResponse IsAttendanceVerificationRequired(LoginRequest request)
        {
            IsAttendanceVerificationRequiredResponse response = new IsAttendanceVerificationRequiredResponse();
            try
            {
                response = Controller.IsAttendanceVerificationRequired(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsAttendanceVerificationRequired", ex, request.Username, true);
            }

            return response;
        }

        public STCCLoginResponse STCCLogin(STCCLoginRequest request)
        {
            STCCLoginResponse response = new STCCLoginResponse();

            try
            {
                response = Controller.STCCLogin(request);
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("STCCLogin", ex, request.STCCEmployeeUserName, true);
            }

            return response;
        }

        public GetSubscriptionTypeResponse GetSubscribtionType(GetSubscriptionTypeRequest request)
        {
            GetSubscriptionTypeResponse response = new GetSubscriptionTypeResponse();
            try
            {
                response = Controller.GetSubscriptionType(request);
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSubscribtionType", ex, request.oLoginRequest.Username, true);
            }

            return response;
        }
    }
}
