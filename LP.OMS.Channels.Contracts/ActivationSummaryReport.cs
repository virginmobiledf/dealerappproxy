﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationSummaryReport
    {
        [DataMember]
        public int TotalActivations { get; set; }
        [DataMember]
        public int TotalFirstPayableEvents { get; set; }
    }
}
