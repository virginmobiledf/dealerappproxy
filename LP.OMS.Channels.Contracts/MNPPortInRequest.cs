﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class MNPPortInRequest
    {
        [DataMember]
        public bool IsMNPPortIn { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public int OperatorID { get; set; }
        [DataMember]
        public string ContactNumber { get; set; }
    }

    [DataContract]
    public class OperatorDetails
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string NameEN { get; set; }
        [DataMember]
        public string NameAR { get; set; }
    }
}
