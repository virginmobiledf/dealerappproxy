﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CommissionReportSummary : BaseReportResponse
    {
        [DataMember]
        public ChartSummary CommissionChartSummary { get; set; }
        [DataMember]
        public ChartSummary PaidChartSummary { get; set; }
        [DataMember]
        public ChartSummary PendingChartSummary { get; set; }
        [DataMember]
        public int TotalReportPages { get; set; }
    }
}
