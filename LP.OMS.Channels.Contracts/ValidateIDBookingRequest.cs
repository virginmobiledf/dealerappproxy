﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ValidateIDBookingRequest
    {
        [DataMember]
        public LoginRequest LoginRequest;

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public Brands Brand { get; set; }
    }
}
