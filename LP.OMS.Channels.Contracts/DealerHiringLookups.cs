﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class LookupsList
    {
        [DataMember]
        public List<DealerHiringLookup> Lookups { get; set; }
    }

    [DataContract]
    public class DealerHiringLookup
    {
        [DataMember]
        public int LookupId { get; set; }

        [DataMember]
        public string LookupName { get; set; }

        [DataMember]
        public string LookupName2 { get; set; }

        [DataMember]
        public int LookupType { get; set; }

        [DataMember]
        public List<LookupItems> LookupItems { get; set; }
    }

    [DataContract]
    public class LookupItems
    {
        [DataMember]
        public int LookupItemId { get; set; }

        [DataMember]
        public string LookupItemName { get; set; }

        [DataMember]
        public string LookupItemName2 { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string ReferenceIDs { get; set; }
    }

    public enum SystemLookups
    {
        lookup1 = 1,
        lookup2 = 2,
    } 
}
