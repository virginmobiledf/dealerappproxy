﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AvailableNumbersRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string searchPattern { get; set; }

        [DataMember]
        public int pageNumber { get; set; }

        [DataMember]
        public bool IsFRiENDi { get; set; }
    }
}
