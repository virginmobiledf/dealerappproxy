﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class TerminationRequest
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string IdNumber { get; set; }

        [DataMember]
        public string IdType { get; set; }

        [DataMember]
        public string TerminationReasonId { get; set; }

        [DataMember]
        public string NationalityCode { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public FingerPrint Fingerprint { get; set; }

        [DataMember]
        public LoginRequest LoginRequest { get; set; }

    }

    [DataContract]
    public class TerminationResponse
    {
        [DataMember]
        public bool isValid { get; set; }

        [DataMember]
        public string responseMessage { get; set; }

    }

    [DataContract]
    public class TerminationReason
    {
        [DataMember]
        public string key { get; set; }

        [DataMember]
        public string value { get; set; }

    }
}
