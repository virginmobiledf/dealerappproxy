﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class UpdateOTPMSISDNRequest: IAuthenticatableRequest
    {
        public UpdateOTPMSISDNRequest()
        {
            documents = new List<Documents>();
        }

        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public int reason { set; get; }

        [DataMember]
        public int id { set; get; }

        [DataMember]
        public string note { set; get; }

        [DataMember]
        public string code { set; get; }

        [DataMember]
        public string contactMSISDN { set; get; }

        [DataMember]
        public List<Documents> documents { set; get; }

        public bool IsResubmitted
        {
            get
            {
                return id > 0;
            }
        }
    }
}
