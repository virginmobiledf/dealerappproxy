﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DescendantCommissions
    {
        [DataMember]
        public string DescendantCode { get; set; }
        [DataMember]
        public double TotalYesterdayCommissions { get; set; }
        [DataMember]
        public double TotalTodayCommissions { get; set; }
        [DataMember]
        public double TotalLastMonthCommissions { get; set; }
    }
}