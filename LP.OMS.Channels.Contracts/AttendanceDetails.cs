﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class AttendanceDetails
    {
        public string ClientKey { get; set; }
        public string UserCode { get; set; }
        public string EntryTime { get; set; }
        public string ReaderType { get; set; }
        public string EntryType { get; set; }
        public string Location { get; set; }
    }

    public class AddAttendanceResponse
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }

    public class AddAttendanceRequest
    {
        public AttendanceDetails AttendanceDetails { get; set; }
    }
}
