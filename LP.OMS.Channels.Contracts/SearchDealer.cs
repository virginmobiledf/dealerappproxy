﻿using LP.OMS.Channels.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class SearchDealerResponse : DHBaseResponse
    {
        [DataMember]
        public List<SearchDealerResult> Result { get; set; }
    }

    [DataContract]
    public class SearchDealerResult : BaseDealerEntities
    {
        [DataMember]
        public string CreationDate { set; get; }
        [DataMember]
        public string BlockReason { set; get; }
        [DataMember]
        public string BlockDate { set; get; }
        [DataMember]
        public bool? UpdatedInCAS { set; get; }
        [DataMember]
        public bool? CleanData { set; get; }
        [DataMember]
        public string LastActionDate { set; get; }
        [DataMember]
        public string Password { set; get; }
        [DataMember]
        public string UserName { set; get; }
        [DataMember]
        public int? ParentId { set; get; }
        [DataMember]
        public string Hierarchy { set; get; }
        [DataMember]
        public string EmployeeID { set; get; }
        [DataMember]
        public DHLocation LocationDetails { set; get; }

    }

    [DataContract]
    public class FullDealerRequestDetails : SearchDealerResult
    {
        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class SearchDealerRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string SaudiID { get; set; }
    }
}
