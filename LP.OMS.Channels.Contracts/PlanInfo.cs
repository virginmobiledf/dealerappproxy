﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class PlanInfo
    {
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Unit { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public class PromotionsDTO
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}
