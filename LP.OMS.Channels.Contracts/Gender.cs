﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class Gender
    {
        int _Id;
        string _code;
        string _name = string.Empty;


        [DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

}
