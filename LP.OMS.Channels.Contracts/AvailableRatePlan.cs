﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AvailableRatePlan
    {
        [DataMember]
        public string RatePlanName { get; set; }
        [DataMember]
        public int RatePlanId { get; set; }
        [DataMember]
        public bool IsPrePaid { get; set; }
    }
}
