﻿using LP.OMS.Channels.Contracts.Enums;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CommissionsDetailsReportRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public DHGeneralSearchCriteria GeneralSearchCriteria { get; set; }
        [DataMember]
        public DateSearchCriteria DateSearchCriteria { get; set; }
        
    }
}
