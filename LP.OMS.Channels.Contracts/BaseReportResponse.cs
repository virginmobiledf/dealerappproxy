﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class BaseReportResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public string InquiryPeriod { get; set; }
    }

    [DataContract]
    public class BaseReportDetailsResponse : BaseReportResponse
    {
        [DataMember]
        public int IsPassed { get; set; }
    }
}
