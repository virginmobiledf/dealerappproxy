﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class BookNumberRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public int IDType { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string BookedMSISDN { get; set; }

        [DataMember]
        public int Vanity { get; set; }

        [DataMember]
        public string IDTypeCode { get; set; }
    }
}
