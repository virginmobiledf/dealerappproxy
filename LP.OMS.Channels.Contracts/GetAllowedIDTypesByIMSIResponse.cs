﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class GetAllowedIDTypesByIMSIResponse : CallResponse
    {
        public List<AllowedIDType> idTypes { get; set; }
    }
}
