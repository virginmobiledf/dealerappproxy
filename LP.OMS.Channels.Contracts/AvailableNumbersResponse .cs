﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AvailableNumbersResponse : BaseResponse
    {
        [DataMember]
        public List<AvailableNumber> AvailableNumbers { get; set; }
    }

    [DataContract]
    public class AvailableNumber
    {
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public VanityType VanityType { get; set; }
    }
}
