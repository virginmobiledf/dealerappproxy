﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class VirginSubscriptionType
    {
        public string SubscriptionType { get; set; }

        public string SubscriptionTypeName { get; set; }

        public string SubscriptionTypeNameAr { get; set; }
    }
}