﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationCommissionReportRequest
    {
        [DataMember]
        public AppearanceBasedOnBrand brand { get; set; }
        [DataMember]
        public ReportViewMode reportViewMode { get; set; }
        [DataMember]
        public string dateFrom { get; set; }
        [DataMember]
        public string dateTo { get; set; }
        [DataMember]
        public LoginRequest loginRequest { get; set; }
        [DataMember]
        public string gradeFilter { get; set; }
        [DataMember]
        public string rechargeFilter { get; set; }
        [DataMember]
        public string fpeFilter { get; set; }
        [DataMember]
        public string paymentStatusFilter { get; set; }
        [DataMember]
        public string commissionElementFilter { get; set; }
        [DataMember]
        public int pageNumber { get; set; }
    }
}
