﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class OwnerChangeRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public int ProcessID { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string CurrentOwnerIDType { get; set; }

        [DataMember]
        public string CurrentOwnerIDNumber { get; set; }

        [DataMember]
        public string NewOwnerIDType { get; set; }

        [DataMember]
        public string NewOwnerIDNumber { get; set; }

        [DataMember]
        public FingerPrint Fingerprint { get; set; }

        [DataMember]
        public List<Document> Documents { get; set; }

        [DataMember]
        public string CurrentCountryCode { get; set; }

        [DataMember]
        public string NewCountryCode { get; set; }

        [DataMember]
        public string SourceBrand { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public string CurrentOwnerAddress { get; set; }

        [DataMember]
        public string NewOwnerAddress { get; set; }

        [DataMember]
        public Document CurrentOwnerSignature { get; set; }

        [DataMember]
        public Document NewOwnerSignature { get; set; }

        [DataMember]
        public Document DealerSignature { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }
    }
}
