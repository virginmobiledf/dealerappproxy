﻿using LP.OMS.Channels.Contracts.Enums;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class RechargeSummaryReportRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public DateSearchCriteria DateSearchCriteria { get; set; }
    }
}
