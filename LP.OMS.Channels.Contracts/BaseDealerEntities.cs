﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LP.OMS.Channels.Contracts.Enums;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class BaseDealerEntities
    {
        public BaseDealerEntities()
        {
            DocumentsList = new List<Documents>();
        }

        [DataMember]
        public int Id { set; get; }

        [DataMember]
        public int? UserTypeId { set; get; }

        [DataMember]
        public string Name { set; get; }

        [DataMember]
        public string Email { set; get; }

        [DataMember]
        public string FaxNumber { set; get; }

        [DataMember]
        public int RegionID { set; get; }

        [DataMember]
        public string Code { set; get; } //Dealer Code

        [DataMember]
        public bool IsActive { set; get; }

        [DataMember]
        public string SaudiID { set; get; }//SaudiIdNumber

        [DataMember]
        public string Address { set; get; }

        [DataMember]
        public string ContactPerson { set; get; }//ContactPerson

        [DataMember]
        public int RequesterClassId { set; get; } //User Type

        [DataMember]
        public bool LanguagePreferenceIsEnglish { set; get; }

        [DataMember]
        public string ContactNos { set; get; }//ContactNumber

        [DataMember]
        public bool IsKeyAccount { set; get; }

        [DataMember]
        public int PoSTypeID { set; get; }

        [DataMember]
        public string CreatedBy { set; get; }

        [DataMember]
        public string LastActionBy { set; get; }

        [DataMember]
        public int DistributerID { set; get; }

        [DataMember]
        public bool HasCommission { set; get; }

        [DataMember]
        public int CityId { set; get; }

        [DataMember]
        public string Location { set; get; }

        [DataMember]
        public int GroupId { set; get; }

        [DataMember]
        public string ContactMSISDN { set; get; } //PhoneNumber

        [DataMember]
        public string DeviceIMEI { set; get; }

        [DataMember]
        public string DeviceMAC { set; get; }

        [DataMember]
        public string FPDeviceSerial { set; get; }

        [DataMember]
        public int GeofenceRadius { set; get; }

        [DataMember]
        public bool MandatoryCreditLimit { set; get; }

        [DataMember]
        public bool OverRideOverDuePayment { set; get; }

        [DataMember]
        public int CreditLimit { set; get; }

        [DataMember]
        public string PaymentTermsCode { set; get; }

        [DataMember]
        public string DefaultSalesPersonCode { set; get; }//PromoterName

        [DataMember]
        public int GrCuClassClass { set; get; }

        [DataMember]
        public int PartnerName { set; get; }//PartnerName

        [DataMember]
        public string ParentCode { set; get; }

        [DataMember]
        public string MunicipalityLicenseCode { set; get; } // LicenseRentalAgreement

        [DataMember]
        public string StoreOwnerCode { set; get; }

        [DataMember]
        public List<Documents> DocumentsList { set; get; }

        [DataMember]
        public string OwnerSaudiIdNumber { set; get; }

        [DataMember]
        public int SubChannelTypeID { set; get; }

        [DataMember]
        public RequestStatus RequestStatus { set; get; }

        [DataMember]
        public RequestType RequestType { set; get; }

        [DataMember]
        public string RefillMSISDN { get; set; }

        [DataMember]
        public string PreviousCode { get; set; }

        [DataMember]
        public string LandMarkClass { get; set; } //LandlineNumberShop

        [DataMember]
        public int AddressCode { get; set; }

        [DataMember]
        public int ChannelTypeID { get; set; } //Channel

        [DataMember]
        public string OwnerContactNumber { get; set; }

        [DataMember]
        public string SalesPerson { get; set; }

        [DataMember]
        public string CRNumber { get; set; }

        [DataMember]
        public string RejectionReason { get; set; }
    }

    [DataContract]
    public class Documents
    {
        [DataMember]
        public string DocumentReferenceID { set; get; }

        [DataMember]
        public int DocumentType { set; get; }

        [DataMember]
        public string DocumentContent { get; set; }

        [DataMember]
        public string DocumentUrl { get; set; }
    }
}
