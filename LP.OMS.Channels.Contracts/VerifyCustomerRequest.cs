﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class VerifyCustomerRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public List<VerifyCustomerSubscription> VerifyCustomerSubscriptions { get; set; }
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public FingerPrint FingerPrint { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
    }
    [DataContract]
    public class VerifyCustomerSubscription
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string Priority { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public int ActivationId { get; set; }

    }
    [DataContract]
    public class VerifyCustomerResponse
    {
        [DataMember]
        public List<VerifyCustomerSubscription> VerifyCustomerSubscriptions { get; set; }
        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }
    }

}
