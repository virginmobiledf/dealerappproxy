﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ExtendSessionRequest
    {

        [DataMember]
        public int AMErrorCode { get; set; }

        [DataMember]
        public int NextSessionCheckTime { get; set; }

        public ExtendSessionRequest()
        {
            AMErrorCode = 11;//SessionInvalid
        }

    }

}
