﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AndroidReportRequest
    {
        //[DataMember]
        //public int RequesterId { get; set; }

        [DataMember] 
        public int StatusId { get; set; }

        [DataMember]
        public string lstDocumentTypeIds { get; set; }

        [DataMember]
        public string Channel { get; set; }

        [DataMember]
        public DateTime FromDate { get; set; }

        [DataMember]
        public DateTime ToDate { get; set; }

        [DataMember]
        public int StartIndex { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public int Resubmission { get; set; }
    }

    public enum ResubmissionEnum : int
    {
        NoResubmission = 100, // Check box unchecked
        IncludeResubmission = 200, // Check box checked
        OnlyResubmission = 300 // Resubmission selected in Document type drop down
    }
}
