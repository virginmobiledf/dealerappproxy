﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class Period
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string GetInquiryPeriod()
        {
            return this.From.ToString("MMM dd,yyyy") + " - " + this.To.ToString("MMM dd,yyyy");
        }
    }
}
