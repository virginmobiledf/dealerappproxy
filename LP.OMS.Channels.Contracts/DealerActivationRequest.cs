﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    class DealerActivationRequest
    {
            [DataMember]
            ReportViewMode reportViewMode{set; get;}
            [DataMember]
            string dealerCode{set; get;}
            [DataMember]
            DateTime dateFrom{set; get;}
            [DataMember]
            DateTime dateTo { set; get; }
    }
}
