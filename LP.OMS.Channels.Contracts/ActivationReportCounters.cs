﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationReportCounters
    {
        [DataMember]
        public int DailyActivations { set; get; }

        [DataMember]
        public int WeeklyActivations { set; get; }

        [DataMember]
        public int MonthlyActivations { set; get; }
        [DataMember]
        public int LanchToDateActivations { set; get; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }
    }
}
