﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationRechargeDetailsReportResponse : BaseReportResponse
    {
        [DataMember]
        public int TotalRechargeReportPages { get; set; }
        [DataMember]
        public List<RechargeSummary> RechargeSummary { get; set; }
    }

    [DataContract]
    public class RechargeSummary
    {
        [DataMember]
        public string RechargeType { get; set; }
        [DataMember]
        public List<RechargeDetails> RechargeDetails { get; set; }
    }

    [DataContract]
    public class RechargeDetails
    {
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string ActivatedOn { get; set; }
        [DataMember]
        public string RechargeType { get; set; }
    }
}
