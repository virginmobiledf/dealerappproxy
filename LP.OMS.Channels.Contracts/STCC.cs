﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class STCCBaseResponse
    {
        [DataMember] public int ResponseCode { get; set; }
        [DataMember] public string ResponseMsgEN { get; set; }
        [DataMember] public string ResponseMsgAR { get; set; }
    }

    [DataContract]
    public class STCCLoginRequest
    {
        [DataMember] public string EmployeeCivilId { get; set; }
        [DataMember] public string EmployeeOrgId { get; set; }
        [DataMember] public string EmployeeContactNumber { get; set; }
        [DataMember] public double LiveLocation_Lat { get; set; }
        [DataMember] public double LiveLocation_Lon { get; set; }
        [DataMember] public string IAM_OTP { get; set; }
        [DataMember] public string STCCUserReferenceNumber { get; set; }
        [DataMember] public string EmployeeUserName { get; set; }
        [DataMember] public int RegionCode { get; set; }

        public string STCCEmployeeUserName
        {
            get
            {
                return $"{ConfigurationManager.AppSettings["STCC_Users_Prefix"]}{STCCUserReferenceNumber}";
            }
        }
    }

    [DataContract]
    public class STCCLoginResponse : STCCBaseResponse
    {
        [DataMember] public string SessionToken { get; set; }
        [DataMember]
        public int RemainingAttempts { get; set; }
    }
}
