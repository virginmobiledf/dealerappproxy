﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class GetSubscriptionTypeObj
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public string CustomerIdType { get; set; }
        [DataMember]
        public Brands Brand { get; set; }
        [DataMember]
        public OnboardingType OnboardingType { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public bool IsEsimActivation { get; set; }
    }
}
