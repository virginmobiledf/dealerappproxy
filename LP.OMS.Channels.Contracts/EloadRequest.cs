﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class EloadRequest
    {
        [DataMember]
        public LoginRequest LoginRequest;

        [DataMember]
        public String MSISDN;
        
        [DataMember]
        public String ID;
        
        [DataMember]
        public String Amount;

        [DataMember]
        public string PIN { get; set; }
    }
}
