﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DealerHiringLoginResponse
    {
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public bool NewApplicationVersionExists { get; set; }
        [DataMember]
        public string ApplicationApkUrl { get; set; }
        [DataMember]
        public bool hasMinimalAppVersion { get; set; }
        [DataMember]
        public List<int> UserPrivilageList { get; set; }
        [DataMember]
        public string WellcomeMessage { get; set; }
        [DataMember]
        public string DefaultSalesPersonCode { get; set; }
        [DataMember]
        public int UserTypeID { get; set; }
        [DataMember]
        public int ChannelTypeID { get; set; }
        [DataMember]
        public bool LanguageIsEnglish { get; set; }
        [DataMember]
        public int RegionID { get; set; }
        [DataMember]
        public int CityID { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string ParentCode { get; set; }
        [DataMember]
        public string[] DelegatedBy { get; set; }
        [DataMember]
        public string UserConfiguration { get; set; }
        [DataMember]
        public string CRNumber { get; set; }

        
    }
}
