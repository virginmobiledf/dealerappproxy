﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class HashingRequest
    {
 
        [DataMember]
        public string NewPassword { get; set; }
        
            [DataMember]
        public string OldPassword { get; set; }

        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        
    }

  
}
