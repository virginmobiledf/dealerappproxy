﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class FaildActivationRequest
    {
        [DataMember]
        public string Msisdn{ get; set; }
        
        [DataMember]
        public int StatusId { get; set; }
         
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

    }
}
