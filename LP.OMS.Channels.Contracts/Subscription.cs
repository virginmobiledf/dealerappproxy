﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class Subscription
    {
        [DataMember]
        public int SubscriptionTypeId { get; set; }
        [DataMember]
        public string SubscriptionName { get; set; }
        [DataMember]
        public AddOns AddOns { get; set; }
        [DataMember]
        public Fields Fields { get; set; }
        [DataMember]
        public bool IsDataType { get; set; }
        [DataMember]
        public bool IsForceRecharge { get; set; }
        [DataMember]
        public LoadedSIMDetails LoadedSIMDetails { get; set; }
        [DataMember]
        public bool IsPrepaid { get; set; }
    }
}
