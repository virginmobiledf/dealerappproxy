﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class EloadResponse
    {

        [DataMember]
        public Boolean IsPassed { get; set; }

        [DataMember]
        public String ResponseMessage { get; set; }

        [DataMember]
        public float CurrentBalance { get; set; }
    }
}
