﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts.DealerSImAssociation
{
    [DataContract]
    public class ShopAssociatedSIMs
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string iMSI { get; set; }

        [DataMember]
        public string timeStamp { get; set; }

        [DataMember]
        public string brand { get; set; }
    }
}
