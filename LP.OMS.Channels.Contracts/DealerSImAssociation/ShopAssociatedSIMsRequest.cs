﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts.DealerSImAssociation
{
    [DataContract]
    public class ShopAssociatedSIMsRequest
    {
        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public string KitID { get; set; }

        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        public bool IsLoggedInAppSessionInArabic
        {
            get
            {
                return LoginRequest.language == 2;
            }
        }
    }
}
