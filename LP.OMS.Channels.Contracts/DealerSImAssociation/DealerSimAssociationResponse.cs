﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts.DealerSImAssociation
{
    [DataContract]
    public class DealerSimAssociationResponse
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string FromKitId { get; set; }

        [DataMember]
        public string ToKitId { get; set; }

        public bool IsSingleKitAssociation
        {
            get
            {
                return string.IsNullOrWhiteSpace(ToKitId);
            }
        }
    }
}
