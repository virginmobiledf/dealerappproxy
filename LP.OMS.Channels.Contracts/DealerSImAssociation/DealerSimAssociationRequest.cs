﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts.DealerSImAssociation
{
    [DataContract]
    public class DealerSimAssociationRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string FromKitId { get; set; }

        [DataMember]
        public string ToKitId { get; set; }

        [DataMember]
        public Brands Brand { get; set; }

        public bool IsSingleKitAssociation
        {
            get
            {
                return string.IsNullOrWhiteSpace(ToKitId);
            }
        }

        public bool IsLoggedInAppSessionInArabic
        {
            get
            {
                return LoginRequest.language == 2;
            }
        }

        public string Display
        {
            get
            {
                if(!string.IsNullOrWhiteSpace(FromKitId) && !string.IsNullOrWhiteSpace(ToKitId))
                {
                    return $"from {FromKitId} to {ToKitId}";
                }
                return $"for {FromKitId}";
            }
        }
    }
}
