﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class OnlineRequest
    {

        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string SecondName { get; set; }
        [DataMember]
        public string ThirdName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public DateTime DoB { get; set; }
        [DataMember]
        public string NationalityCode/*CountryCode*/ { get; set; }//send PrefixField field to WCF rest.
        [DataMember]
        public string NationalityID/*CountryCode*/ { get; set; }//send PrefixField field to WCF rest.
        [DataMember]
        public int IdNo/*CivilId*/ { get; set; }
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public int LanguageID { get; set; }
        [DataMember]
        public int TitleID { get; set; }
        [DataMember]
        public DateTime IDExpiryDate { get; set; }
        [DataMember]
        public int GenderID { get; set; }
        [DataMember]
        public  Document IdCopyDocument { get; set; }
        [DataMember]
        public Document SignatureDocument { get; set; }
    }

}
