﻿using LP.OMS.Channels.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class MyRequestsRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
       
        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public RequestStatus Status { get; set; }
    }

    [DataContract]
    public class MyRequestsResponse : DHBaseResponse
    {
        [DataMember]
        public List<MyRequestsResult> Result { get; set; }
    }

    [DataContract]
    public class MyRequestsResult /*: BaseDealerEntities*/
    {
        [DataMember]
        public string CreationDate { set; get; }
        [DataMember]
        public int? RequestSubType { set; get; }
        [DataMember]
        public int? UserTypeId { set; get; }
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string CreatedBy { set; get; }
        [DataMember]
        public string ParentCode { set; get; }
        [DataMember]
        public int? ChannelTypeID { set; get; }
        [DataMember]
        public int? SubChannelTypeID { set; get; }
        [DataMember]
        public string SaudiID { set; get; }
        [DataMember]
        public string Code { set; get; }
        [DataMember]
        public RequestStatus RequestStatus { get; set; }
        [DataMember]
        public RequestType RequestType { get; set; }

    }
}
