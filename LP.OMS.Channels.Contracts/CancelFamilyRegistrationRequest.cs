﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CancelFamilyRegistrationRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
    }
}
