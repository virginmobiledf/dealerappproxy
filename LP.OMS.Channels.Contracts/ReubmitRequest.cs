﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ResubmitRequestObj
    {

        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string IDNumber { get; set; }

        [DataMember]
        public Document IdCopyDocument { get; set; }

    }
}
