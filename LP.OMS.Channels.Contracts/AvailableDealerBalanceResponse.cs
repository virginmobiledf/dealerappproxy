﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AvailableDealerBalanceResponse
    {
        [DataMember]
        public string RefillMSISDN { get; set; }
        [DataMember]
        public double Balance { get; set; }
    }
}
