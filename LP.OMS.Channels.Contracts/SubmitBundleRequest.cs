﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    //Anas Alzube
    [DataContract]
    public class SubmitBundleRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public int BundleId { get; set; }

        [DataMember]
        public string Msisdn { get; set; }
    }
}
