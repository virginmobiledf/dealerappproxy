﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AddOns 
    {
        [DataMember]
        public Topup Topup { get; set; }
        [DataMember]
        public Voucher Voucher { get; set; }
        [DataMember]
        public None None { get; set; }
        [DataMember]
        public ProductAddOns Product { get; set; }
    }
}
