﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class BaseResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }
    }

    [DataContract]
    public class DHBaseResponse
    {
        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
