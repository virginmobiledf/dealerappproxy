﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CallResponse
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }

        [DataMember]
        public string RechargeMessage { get; set; }

        [DataMember]
        public ESIMData ESIMData { get; set; }

    }

    [DataContract]
    public class SematiLoginCallResponse : CallResponse
    {
        [DataMember]
        public int RemainingAttempts { get; set; }

    }
}
