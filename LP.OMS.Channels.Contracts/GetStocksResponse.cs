﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class GetStocksResponse
    {
        [DataMember]
        public List<Stock> Stocks { get; set; }
        [DataMember]
        public bool IsSuccessful { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class Stock
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int CategoryId { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public int BrandId { get; set; }
        [DataMember]
        public int MinQuantity { get; set; }
        [DataMember]
        public int MaxQuantity { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public List<int> CommonQuantity { get; set; }
    }
}

