﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class RequesterRight
    {
        [DataMember]
        public int RightID
        {
            get;
            set;
        }

        [DataMember]
        public int RequesterID
        {
            get;
            set;
        }
    }
}
