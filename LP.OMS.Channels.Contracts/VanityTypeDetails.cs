﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class VanityTypeDetails
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string NameEN { get; set; }
        [DataMember]
        public string NameAR { get; set; }
        [DataMember]
        public double Price { get; set; }

    }
}
