﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DescendantActivationsResponse
    {
        [DataMember]
        public List<DescendantActivations> Result { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }
}
