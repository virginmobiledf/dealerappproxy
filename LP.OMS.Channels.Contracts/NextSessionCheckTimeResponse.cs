﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
   public class NextSessionCheckTimeResponse
    {
        [DataMember]
        public int SessionExpireAfter { get; set; }

        [DataMember]
        public int NextSessionCheckTime { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
