﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckCustomerResponse
    {
        [DataMember]
        public int CustomerID { get; set; }

        [DataMember]
        public bool IsCustomerExist { get; set; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public bool IsIDExpired { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }
 
    }
}
