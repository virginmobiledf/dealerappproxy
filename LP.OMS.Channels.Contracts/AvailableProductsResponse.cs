﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AvailableProductsResponse :CallResponse
    {
        [DataMember]
        public List<ProductCategory> ProductCategory { get; set; }
    }
}
