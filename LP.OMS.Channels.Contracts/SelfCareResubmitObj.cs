﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
   public class SelfCareResubmitObj
    {

        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public int Brand { get; set; }
        [DataMember]
        public int DocumentType { get; set; }
        [DataMember]
        public string AdditionalInfo { get; set; }
        [DataMember]
        public string IDType { get; set; }
        [DataMember]
        public List<byte[]> LstResubmissionDocuments { get; set; }
    }
}
