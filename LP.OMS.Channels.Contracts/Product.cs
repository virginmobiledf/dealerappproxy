﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class Product
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<string> Details { get; set; }

        [DataMember]
        public double Price { get; set; }

        [DataMember]
        public string Validity { get; set; }

        [DataMember]
        public double DealerCommission { get; set; }

        [DataMember]
        public double PriceWithoutVAT { get; set; }

        [DataMember]
        public List<PlanInfo> Promotions { get; set; }

        [DataMember]
        public List<PlanInfo> NewPropositionDetails { get; set; }

        [DataMember]
        public double OldPrice { get; set; }

        [DataMember]
        public List<PromotionsDTO> PromotionsDTO { get; set; }


    }
}
