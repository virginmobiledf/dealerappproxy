﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class Lookups
    {
        [DataMember]
        public List<Country> Countries { get; set; }

        [DataMember]
        public List<Title>  Titles{ get; set; }

        [DataMember]
        public List<Gender> Genders { get; set; }

        [DataMember]
        public List<IDType> IDTypes { get; set; }

        [DataMember]
        public List<IDType> DealerActivationIDTypes { get; set; }

        [DataMember]
        public List<ReplacementReason> ReplacementReasons { get; set; }

        //TODO: to be comminted in case of deploying a build for phase 1
        [DataMember]
        public List<WFActivityStatus> WFActivityStatuses { get; set; }

        [DataMember]
        public CurrentHijriDate currentHijriDate { set; get; }

        [DataMember]
        public string PoorNFIQMessage { get; set; }

        [DataMember]
        public Dictionary<string, string> AppMessages { get; set; }

        //Anas Alzube
        [DataMember]
        public List<Domination> Dominations { get; set; }

        //Anas Alzube
        [DataMember]
        public List<StockItem> Items { get; set; }

        [DataMember]
        public List<Country> NationalityList { get; set; }

        [DataMember]
        public string SaudiCountryCode { get; set; }

        [DataMember]
        public string ResendTime { get; set; }

        [DataMember]
        public string MessageCodeTime { get; set; }

        [DataMember]
        public string OTPVisabilty { get; set; }

        [DataMember]
        public List<VirginSubscriptionType> PremiumSubscriptionTypes { get; set; }

        [DataMember]
        public List<IDType> VanityActivationIDTypes { get; set; }

        [DataMember]
        public List<IDType> OwnershipIDTypes { get; set; }

        [DataMember]
        public string ActivationAndCommissionReportsDateTimeFormat { get; set; }

        [DataMember]
        public List<City> Cities { get; set; }

        [DataMember]
        public List<Domination> ELoadDenominations { get; set; }

        [DataMember]
        public string VAT { get; set; }

        [DataMember]
        public int ValidVoucherLength { get; set; }

        [DataMember]
        public decimal SimPriceOrSubscriptionFee { get; set; }

        [DataMember]
        public List<Domination> VirginDenominations { get; set; } // Activation denominations for Virgin

        [DataMember]
        public List<TerminationReason> TerminationReasons { get; set; }

        [DataMember]
        public decimal EsimPriceVATExcluded { get; set; }

        [DataMember]
        public string MTResendLocationPeriod { get; set; }

        [DataMember]
        public int LocationTrackingMode { get; set; }// 1-all dealers / 2-MT

        [DataMember]
        public MTConfiguration MTConfiguration { get; set; }

        [DataMember]
        public bool EnableTCCOTPVerificationMechanism { get; set; }
    }
}
