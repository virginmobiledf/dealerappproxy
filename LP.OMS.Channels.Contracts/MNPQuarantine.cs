﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class MNPQuarantineStatusRequest
    {
        [DataMember]
        public LoginRequest LoginRequest;

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string IdNumber { get; set; }
    }

    [DataContract]
    public class MNPQuarantineStatusResponse : BaseResponse
    {
        [DataMember]
        public bool IsNumberInQuarantined { get; set; }
    }
}
