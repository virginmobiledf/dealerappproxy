﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DealerActivitiesResponse
    {
        [DataMember]
        public string StatusName { get; set; }

        [DataMember]
        public string StatusForeignName { get; set; }
        
        [DataMember]
        public string ChannelName { get; set; }
        
        [DataMember]
        public string RequestDate { get; set; }
        
        [DataMember]
        public string Imsi { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public int DocumentTypeId { get; set; }


        [DataMember]
        public string DocumentTypeDescription { get; set; }

        [DataMember]
        public bool IsResubmit { get; set; }  
    }
}
