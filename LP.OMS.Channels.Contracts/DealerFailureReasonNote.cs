﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DealerFailureReasonNote
    {
        [DataMember]
        public int ReasonId { get; set; }
        [DataMember]
        public string DealerNote { get; set; }
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
    }
}
