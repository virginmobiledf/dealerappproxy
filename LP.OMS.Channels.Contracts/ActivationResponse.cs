﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationResponse
    {
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public DateTime ActivationDate { get; set; }
        [DataMember]
        public Grade Grade { get; set; }
        [DataMember]
        public bool PhysicalFormDelivered { get; set; }
        [DataMember]
        public bool FirstPayableEventReceived { get; set; }
    }

    public enum Grade : int
    {
        NotSet = -1,
        A = 0,
        B = 1,
        C = 2,
        D = 4
    }
    public enum ReportViewMode : int
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
        LanchToDate = 3,
        CustomRequest = 4
    }
}
