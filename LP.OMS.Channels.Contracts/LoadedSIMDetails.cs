﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class LoadedSIMDetails
    {
        [DataMember]
        public bool IsLoadedSIM { get; set; }
        [DataMember]
        public decimal InitialBalance { get; set; }
    }
}
