﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class VirginActivationRequest
    {
        //login
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
        [DataMember]
        public FingerPrint FingerPrint { get; set; }
        //request
        [DataMember]
        public string IdNo { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int IDTypeId { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public int LanguageID { get; set; }
        [DataMember]
        public string NationalityCode/*CountryCode*/ { get; set; }//send PrefixField field to WCF rest.
        [DataMember]
        public int NationalityID/*CountryCode*/ { get; set; }//send PrefixField field to WCF rest.
        [DataMember]
        public CurrentHijriDate IDExpiryDate { get; set; }
        [DataMember]
        public CurrentHijriDate VisaIssueDate { get; set; }
        [DataMember]
        public CurrentHijriDate DateOfBirth { get; set; }
        [DataMember]
        public Document IdCopyDocument { get; set; }
        [DataMember]
        public Document VisaCopyDocument { get; set; }
        [DataMember]
        public Document PassportCopyDocument { get; set; }
        [DataMember]
        public Document SignatureDocument { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string VoucherPinCode { get; set; }
        [DataMember]
        public VirginSubscriptionType VirginSubscriptionType { get; set; }

        [DataMember]
        public bool IsPrimarySuscription { get; set; }
        [DataMember]
        public string ActivationPlanCode { get; set; }
        [DataMember]
        public string PlanVoucherNumber { get; set; }

        [DataMember]
        public int RechargeDenominationId
        {
            get; set;
        }
        [DataMember]
        public string BundleVoucherCode { get; set; }
        [DataMember]
        public string ReferralCode { get; set; }

        [DataMember]
        public int SubscriptionTypeId { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string KitCode { get; set; }

        [DataMember]
        public bool IsDigitalActivation { get; set; }

        [DataMember]
        public bool IsFamilyOnboarded { get; set; }

        [DataMember]
        public string FamilyMemberMSISDN { get; set; }
        public MNPPortInRequest MNPPortInRequest { get; set; }
        [DataMember]
        public Document DealerSignatureDocument { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public bool IsEsimActivation { get; set; }
        [DataMember]
        public ActivationPackageDetails ActivationPackageDetails { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public DealerAppOnboardedDetailsDTO DealerAppOnboarded { get; set; }

        [DataMember]
        public bool IsDigitalOnboardingPaid { get; set; }
    }

    [DataContract]
    public enum VirginSubscriptionType : int
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        PrePaid = 1,
        [EnumMember]
        Premium = 2,
        [EnumMember]
        Standard = 3,
        [EnumMember]
        Basic = 4
    }

    [DataContract]
    public enum FRiENDiSubscriptionType : int
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        PrePaid = 1,
        [EnumMember]
        PostpaidBasic = 2
    }

    [DataContract]
    public class ActivationPackageDetails
    {
        [DataMember]
        public List<string> ServiceDetails { get; set; }
    }
}
