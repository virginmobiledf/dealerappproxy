﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class LoginRequest
    {
 
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int language { get; set; }

        // Ismail11
        [DataMember]
        public string ApplicationVersion { get; set; }

        [DataMember]
        public DSTAppOS DSTAppOS { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string MobileDeviceId { get; set; }

        [DataMember]
        public string FigerPrintSerial { get; set; }

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public FingerPrint FingerPrint { get; set; }

        [DataMember]
        public string LocationId { get; set; }

        [DataMember]
        public string FirebaseToken { get; set; }

    }

    [DataContract]
    public class DHLoginRequest : IAuthenticatableRequest
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int language { get; set; }

        [DataMember]
        public string ApplicationVersion { get; set; }

        [DataMember]
        public DSTAppOS DSTAppOS { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string MobileDeviceId { get; set; }

        [DataMember]
        public string FigerPrintSerial { get; set; }

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public FingerPrint FingerPrint { get; set; }

        public bool IsRequestLanguageArabic
        {
            get
            {
                return language == 2 ? true : false;
            }
        }

        public LoginRequest oLoginRequest
        {
            get
            {
                return (LoginRequest)this.MemberwiseClone();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public enum DSTAppOS : int
    {
        NotDefined = 0,
        Android = 1,
        IOS = 2
    }

}
