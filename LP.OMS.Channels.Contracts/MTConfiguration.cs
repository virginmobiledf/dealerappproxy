﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class MTConfiguration
    {
        [DataMember]
        public Dictionary<int, bool> LocationsConfiguration { get; set; }

        [DataMember]
        public List<int> AllowedAppFeatures { get; set; }
    }
}