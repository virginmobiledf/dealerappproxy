﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DescendantActivations
    {
        [DataMember]
        public string DescendantCode { get; set; }
        [DataMember]
        public int TotalYesterdayActivations { get; set; }
        [DataMember]
        public int TotalTodayActivations { get; set; }
        [DataMember]
        public int TotalLastMonthActivations { get; set; }
    }
}
