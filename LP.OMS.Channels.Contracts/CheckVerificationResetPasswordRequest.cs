﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckVerificationResetPasswordRequest
    {
        [DataMember]
        public string VerificationCode { get; set; }

        [DataMember]
        public string Username { get; set; }
    }
}
