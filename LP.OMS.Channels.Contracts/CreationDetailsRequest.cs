﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CreationDetailsRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public bool IsApprovedDealer { get; set; }

        [DataMember]
        public string Code { get; set; }
        
    }

    [DataContract]
    public class CreationDetailsResponse : BaseDealerEntities
    {
        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string CreationDate { set; get; }

        [DataMember]
        public DHLocation LocationDetails { get; set; }

        [DataMember]
        public string EmployeeID { set; get; }
    }
}
