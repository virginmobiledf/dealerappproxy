﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ReActivateRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string code { get; set; }

        [DataMember]
        public int reason { get; set; }

        [DataMember]
        public string note { get; set; }

        [DataMember]
        public int id { get; set; }

        [DataMember]
        public List<Documents> documents { set; get; }

        public bool IsResubmitted
        {
            get
            {
                return id > 0;
            }
        }
    }
}
