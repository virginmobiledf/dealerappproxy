﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    //Anas Alzube
    [DataContract]
    public class BundleResponse
    {
        [DataMember]
        public List<Bundle> Bundles { get; set; }
        [DataMember]
        public bool IsPassed { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
    }
}
