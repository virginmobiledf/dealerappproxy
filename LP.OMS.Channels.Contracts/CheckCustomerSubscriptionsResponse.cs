﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckCustomerSubscriptionsResponse
    {
        [DataMember]
        public List<CustomerSubscription> MSISDNs { get; set; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }
    }
    [DataContract]
    public class CustomerSubscription
    {
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string Brand { get; set; }
        [DataMember]
        public string SubscriptionType { get; set; }
        [DataMember]
        public string IdentityVerificationStatus { get; set; }
        [DataMember]
        public int ActivationId { get; set; }

    }

}
