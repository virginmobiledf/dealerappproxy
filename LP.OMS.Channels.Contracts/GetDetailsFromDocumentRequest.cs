﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class GetDetailsFromDocumentRequest
    {

        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
        [DataMember]
        public Document Document { get; set; }
        
    }

}
