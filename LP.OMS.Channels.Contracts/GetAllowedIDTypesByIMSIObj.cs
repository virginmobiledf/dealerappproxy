﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class GetAllowedIDTypesByIMSIObj
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public Brands Brand { get; set; }
        [DataMember]
        public int? Proposition { get; set; }
        [DataMember]
        public bool IsEsimActivation { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
    }
}
