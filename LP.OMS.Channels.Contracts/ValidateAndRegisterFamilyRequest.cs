﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ValidateAndRegisterFamilyRequest
    {
        [DataMember]
        public string AccountType { get; set; }
        [DataMember]
        public decimal AllowanceAmount { get; set; }
        [DataMember]
        public bool IsMonthlyAllowance { get; set; }
        [DataMember]
        public string MainAccountEmail { get; set; }
        [DataMember]
        public string SubAccountEmail { get; set; }
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
    }
}
