﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class OTPRequest
    {
        [DataMember]
        public string KitCode { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public int SubscriptionTypeId { get; set; }

        [DataMember]
        public string idNumber { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string UserEmail { get; set; }

        [DataMember]
        public int BrandId { get; set; }

        [DataMember]
        public int OTPType { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string BookingCode { get; set; }

        [DataMember]
        public bool IsPortInActivation { get; set; }

        [DataMember]
        public string Topup { get; set; }
        [DataMember]
        public string NewOwnerID { get; set; }

        [DataMember]
        public ProductDetails ProductDetails { get; set; }

        [DataMember]
        public TerminationRequest TerminationRequest { get; set; }

        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public SIMReplacementData SIMReplacementData { get; set; }
    }

    [DataContract]
    public class OTPResponse
    {
        [DataMember]
        public bool isValid { get; set; }

        [DataMember]
        public string responseMessage { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public bool ByPassOTPVerification { get; set; }
    }

    [DataContract]
    public class NumberInfo
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public int BrandId { get; set; }

        [DataMember]
        public string Email { get; set; }

    }

    [DataContract]
    public class ValidateOTPRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string OTP { get; set; }

        [DataMember]
        public string referanceNo { get; set; }

        [DataMember]
        public int OTPType { get; set; }

    }

    
    [DataContract]
    public class ProductDetails
    {
        [DataMember]
        public string ServiceDetails { get; set; }

        [DataMember]
        public string ProductPrice { get; set; }
    }

    [DataContract]
    public class SIMReplacementData
    {
        [DataMember]
        public string IDType { get; set; }
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public int BrandId { get; set; }
        [DataMember]
        public string NationalityCode { get; set; }
        [DataMember]
        public bool IsESIM { get; set; }
        [DataMember]
        public string SIMCode { get; set; }

    }
}
