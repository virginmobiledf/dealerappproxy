﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class RefillDealerEWalletResponse
    {
        [DataMember]
        public bool IsSuccess { get; set; }
        [DataMember]
        public decimal Balance { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
    }
}
