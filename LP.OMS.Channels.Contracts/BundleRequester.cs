﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    //Anas Alzube
    [DataContract]
    public class BundleRequester
    {
        [DataMember]
        public int RequesterID { get; set; }

        [DataMember]
        public int BundleID { get; set; }
    }
}
