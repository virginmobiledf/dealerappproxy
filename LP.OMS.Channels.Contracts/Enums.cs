﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public enum Brands : int
    {
        [EnumMember]
        FRiENDi = 1,
        [EnumMember]
        Virgin = 2
    }

    public enum BusinessDocumentTypeEnum
    {
        Unknown = 1,
        ApplicationForm = 2,
        IDCopy = 3
    }

    public enum NawrasIdType : int
    {
        OMANI_CIVIL_ID = 1,
        OMANI_PASSPORT = 2,
        GCC_ID = 7,
        GCC_PASSPORT = 13,
        OTHER_NATIONALITY_PASSPORT = 14,
        DIPLOMATIC_ID_CARD = 12,
        OMANI_MINISTRY_ID = 5,
        OMANI_RESIDENT_CARD = 6,
    }

    [DataContract]
    public enum OMSLanguages : int
    {
        [EnumMember]
        Local = 1,
        [EnumMember]
        Foreign = 2,
    }


    public enum DocumentTypes
    {
        //Activation = 2,
        //Replace_SIM = 29,
        Activation = 3,
        Replace_SIM = 5,

    }


    public enum IDNumberResultStatus
    {
        SUCCESS = 0,
        CIVIL_ID_IS_NOT_ATCHING = 2,
        UNKNOWN_ERROR = 3,
        UNKNOWN_ERROR1 = 4,
    }

    [DataContract]
    public enum ResubmissionResponseCodes
    {
        [EnumMember]
        SUCCESS = 0,
        [EnumMember]
        FAILED = 1,
        [EnumMember]
        UNKNOWN_ERROR = 2,
    }

    [DataContract]
    public enum AppearanceBasedOnBrand : int
    {
        [EnumMember]
        FRiENDi = 1,
        [EnumMember]
        Virgin = 2,
        [EnumMember]
        Both = 3
    }

    [DataContract]
    public enum ChannelType : int
    {
        [EnumMember]
        DST = 1,
        [EnumMember]
        WEB = 2
    }

    [DataContract]
    [Flags]
    public enum OnboardingType : int
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        Normal = 1,
        [EnumMember]
        Digital = 2,
        [EnumMember]
        FamilySIMOnly = 3,
        [EnumMember]
        FamilySIMWithDevice = 4
    }

    [DataContract]
    public enum QualityGrade : int
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        A = 1,
        [EnumMember]
        B = 2,
        [EnumMember]
        C = 3,
        [EnumMember]
        D = 4
    }

    [DataContract]
    public enum RechargeType : int
    {
        [EnumMember]
        NoRecharge = 0,
        [EnumMember]
        FirstRecharge = 1,
        [EnumMember]
        SecondRecharge = 2,
        [EnumMember]
        ThirdRecharge = 3
    }

    [DataContract]
    public enum PaidElement : int
    {
        [EnumMember]
        None = 0, // Will not exist at all
        [EnumMember]
        FPE = 1,
        [EnumMember]
        FirstRecharge = 2,
        [EnumMember]
        SecondRecharge = 3,
        [EnumMember]
        ThirdRecharge = 4,
        [EnumMember]
        FirstMonthBill = 5,
        [EnumMember]
        SecondMonthBill = 6,
        [EnumMember]
        ThirdMonthBill = 7,
        [EnumMember]
        ContractCollection = 8
    }

    [DataContract]
    public enum FamilyDealerEligibility : int
    {
        [EnumMember]
        NotEligible = 0,
        [EnumMember]
        SIMOnly = 1,
        [EnumMember]
        DeviceOnly = 2,
        [EnumMember]
        Both = 3
    }
}
