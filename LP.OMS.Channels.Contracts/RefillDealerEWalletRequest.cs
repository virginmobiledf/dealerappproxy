﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class RefillDealerEWalletRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string RefillMsisdn { get; set; }
        [DataMember]
        public string Voucher { get; set; }

        public bool IsVoucherLengthInvalid
        {
            get
            {
                int validVoucherLength = 14;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ValidVoucherLength"]))
                {
                    validVoucherLength = int.Parse(ConfigurationManager.AppSettings["ValidVoucherLength"]);
                }

                return Voucher.Trim().Length != validVoucherLength;
            }
        }
    }
}
