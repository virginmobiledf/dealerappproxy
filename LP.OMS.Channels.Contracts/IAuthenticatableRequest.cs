﻿namespace LP.OMS.Channels.Contracts
{
    public interface IAuthenticatableRequest
    {
        // People who still prefix property names with data type must die o__o
        LoginRequest oLoginRequest { get; set; }
    }
}
