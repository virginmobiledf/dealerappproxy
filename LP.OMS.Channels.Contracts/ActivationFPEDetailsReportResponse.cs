﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationFPEDetailsReportResponse : BaseReportResponse
    {
        [DataMember]
        public int TotalFPEReportPages { get; set; }
        [DataMember]
        public List<FPESummary> FPESummary { get; set; }
    }

    [DataContract]
    public class FPESummary
    {
        [DataMember]
        public string FPEType { get; set; }
        [DataMember]
        public List<FPEDetails> FPEDetails { get; set; }
    }

    [DataContract]
    public class FPEDetails
    {
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string ActivatedOn { get; set; }
        [DataMember]
        public string FPE { get; set; }
    }
}
