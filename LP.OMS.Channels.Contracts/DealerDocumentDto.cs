﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class DealerDocumentDto
    {
        public string FileTypeId { get; set; }
        public string FileTypeName { get; set; }
        public string FileId { get; set; }
    }
}
