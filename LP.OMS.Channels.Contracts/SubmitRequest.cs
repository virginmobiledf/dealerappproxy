﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class SubmitRequest
    {
        //login
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
        [DataMember]
        public FingerPrint FingerPrint { get; set; }
        //request
        [DataMember]
        public string IdNo/*CivilId*/ { get; set; }
        [DataMember]
        public string Email/*CivilId*/ { get; set; }
        //[DataMember]
        //public DateTime DoB { get; set; }
        [DataMember]
        public int IDTypeId { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public int LanguageID { get; set; }
        [DataMember]
        public string NationalityCode/*CountryCode*/ { get; set; }//send PrefixField field to WCF rest.
        [DataMember]
        public int NationalityID/*CountryCode*/ { get; set; }//send PrefixField field to WCF rest.
        [DataMember]
        public CurrentHijriDate IDExpiryDate { get; set; }
        [DataMember]
        public CurrentHijriDate VisaIssueDate { get; set; }
        [DataMember]
        public CurrentHijriDate DateOfBirth { get; set; }
        [DataMember]
        public Document IdCopyDocument { get; set; }
        [DataMember]
        public Document VisaCopyDocument { get; set; }
        [DataMember]
        public Document PassportCopyDocument { get; set; }
        [DataMember]
        public Document SignatureDocument { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public bool IsDealerActivation { get; set; }
        [DataMember]
        public string latitude { get; set; }
        [DataMember]
        public string longitude { get; set; }
        [DataMember]
        public string VoucherPinCode { get; set; }
        [DataMember]
        public string VanityMSISDN { get; set; }
        [DataMember]
        public string VanityBookingCode { get; set; }

        [DataMember]
        public FRiENDiSubscriptionType SubscriptionType { get; set; }

        //[DataMember]
        //public int CustomerID { get; set; }
        //[DataMember]
        //public string FirstName { get; set; }
        //[DataMember]
        //public string SecondName { get; set; }
        //[DataMember]
        //public string ThirdName { get; set; }
        //[DataMember]
        //public string LastName { get; set; }  
        [DataMember]
        public string IMSI { get; set; }
        //[DataMember]
        //public string MSISDN { get; set; }


        //[DataMember]
        //public int TitleID { get; set; }

        //[DataMember]
        //public int GenderID { get; set; }

        ////[DataMember]
        ////public bool IsOfflineLogin{ get; set; }

        [DataMember]
        public int RechargeDenominationId
        {
            get; set;
        }

        [DataMember]
        public string BundleVoucherCode { get; set; }
        [DataMember]
        public string ReferralCode { get; set; }

        [DataMember]
        public int SubscriptionTypeId { get; set; }

        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public MNPPortInRequest MNPPortInRequest { get; set; }
        [DataMember]
        public Document DealerSignatureDocument { get; set; }

        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public ActivationPackageDetails ActivationPackageDetails { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public bool IsEsimActivation { get; set; }

        [DataMember]
        public DealerAppOnboardedDetailsDTO DealerAppOnboarded { get; set; }
    }
}
