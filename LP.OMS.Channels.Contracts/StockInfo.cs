﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class StockInfo
    {
        [DataMember]
        public int StockId { get; set; }
        [DataMember]
        public int StockQuantity { get; set; }
    }
}
