﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    //Anas Alzube
    [DataContract]
    public class BundlesRequest
    {
        [DataMember]
        public string Msisdn { get; set; }
    }
}
