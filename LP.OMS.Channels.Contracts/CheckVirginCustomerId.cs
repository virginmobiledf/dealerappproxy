﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckVirginCustomerIdRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string IdNumber { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public FingerPrint FingerPrint { get; set; }
        //[DataMember]
        //public string VisaNumber { get; set; }
        [DataMember]
        public string ActivationPlanCode { get; set; }
        [DataMember]
        public string PlanVoucherNumber { get; set; }
    }

    [DataContract]
    public class CheckVirginCustomerIdResponse
    {

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public string NotificationMessage { get; set; }

    }
}
