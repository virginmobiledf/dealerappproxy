﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class GetSubscriptionTypeResponse : BaseResponse
    {
        [DataMember]
        public bool IsPrepaid { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string DisplayNameAr { get; set; }
        [DataMember]
        public decimal InitialBalance { get; set; }
    }
}
