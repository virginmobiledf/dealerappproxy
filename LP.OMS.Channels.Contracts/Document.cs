﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class Document
    {
        [DataMember]
        public int DocumentTypeID { get; set; }

        [DataMember]
        //public sbyte[] DocumentContent { get; set; }
        public string DocumentContent { get; set; }

        [DataMember]
        public string DocumentExtension { get; set; }
        
        [DataMember]
        public string RequestDocumentStagingGuid { get; set; }
    }
}
