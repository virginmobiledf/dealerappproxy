﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ReplacementRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { set; get; }

        [DataMember]
        public String MSISDN { set; get; }


        [DataMember]
        public String OldICCID { set; get; }

        [DataMember]
        public String NewICCID { set; get; }

        [DataMember]
        public String ReplacementReason { set; get; }

        [DataMember]
        public int ReplacementReasonId { set; get; }
        [DataMember]
        public Document IdCopyDocument { set; get; }

        [DataMember]
        public Document SignatureDocument { set; get; }

        [DataMember]
        public int CustomerId { set; get; }

    }
}
