﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckCustomerIdRquest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string IdNumber { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public FingerPrint FingerPrint { get; set; }
        //[DataMember]
        //public string VisaNumber { get; set; }
    }

}
