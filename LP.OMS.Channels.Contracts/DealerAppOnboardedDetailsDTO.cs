﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DealerAppOnboardedDetailsDTO
    {
        [DataMember]
        public bool IsDealerAppOnboarded { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public VanityTypeDetails VanityTypeDetails { get; set; }
    }
}
