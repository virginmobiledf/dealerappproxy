﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
   public class DescendantCommissionsResponse
    {
        [DataMember]
        public List<DescendantCommissions> Result { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }
}
