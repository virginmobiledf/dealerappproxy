﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DealerActivitiesResponseColelction
    {
        [DataMember]
        public List<DealerActivitiesResponse> DealerActivitiesResponseList { get; set; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }

        [DataMember]
        public int NumberOfRecords { get; set; }

    }
}
