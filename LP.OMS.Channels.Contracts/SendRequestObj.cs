﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class SendRequestObj
    {
        [DataMember]
        public OnlineRequest oOnlineRequest { get; set; }

        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
 
    }
}
