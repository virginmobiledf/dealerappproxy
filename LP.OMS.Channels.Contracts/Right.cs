﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class Right
    {
        [DataMember]
        public int ID
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Code
        {
            get;
            set;
        }

        [DataMember]
        public string FroeignName
        {
            get;
            set;
        }
    }
}
