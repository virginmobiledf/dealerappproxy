﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationGradeDetailsReportResponse : BaseReportResponse
    {
        [DataMember]
        public int TotalGradeReportPages { get; set; }
        [DataMember]
        public List<ActivationGradeSummary> GradeSummary { get; set; }
    }

    [DataContract]
    public class ActivationGradeSummary
    {
        [DataMember]
        public string Grade { get; set; }
        [DataMember]
        public List<ActivationGradeDetails> GradeDetails { get; set; }
    }

    [DataContract]
    public class ActivationGradeDetails
    {
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string ActivatedOn { get; set; }
        [DataMember]
        public string Grade { get; set; }
    }
}
