﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ProductSellingResponse : CallResponse
    {
        [DataMember]
        public bool ShowChangePlanConfirmationPage { get; set; }

        [DataMember]
        public string ProductSellingTransactionId { get; set; }
    }
}
