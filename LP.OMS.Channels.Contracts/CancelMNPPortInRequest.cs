﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CancelMNPPortInRequest
    {
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public Brands Brand { get; set; }
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public Document CancelPortInDocument { get; set; }
    }
}
