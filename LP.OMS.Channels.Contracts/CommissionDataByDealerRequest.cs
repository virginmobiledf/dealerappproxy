﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CommissionDataByDealerRequest
    {
        [DataMember]
        public DateTime dateFrom{ get; set; }

        [DataMember]
        public DateTime dateTo { get; set; }

        [DataMember]
        public ReportViewMode reportViewMode { get; set; }

        [DataMember]
        public LoginRequest objLogin { get; set; }
    }
}
