﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class SendEsimQRImageRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string QR_String { get; set; }

        [DataMember]
        public List<string> UniqueDeviceIds { get; set; }
    }

    [DataContract]
    public class SendEsimQRImageResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
    }
}

