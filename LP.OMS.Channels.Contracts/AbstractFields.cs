﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AbstractFields
    {
        [DataMember]
        public bool IsAllowed { get; set; }
        [DataMember]
        public bool IsMandatory { get; set; }
    }
}
