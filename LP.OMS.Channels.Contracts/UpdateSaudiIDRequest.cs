﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class UpdateSaudiIDRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public int reason { set; get; }

        [DataMember]
        public int id { set; get; }

        [DataMember]
        public string note { set; get; }

        [DataMember]
        public string code { set; get; }

        [DataMember]
        public string saudiID { set; get; }

        [DataMember]
        public string name { set; get; }

        [DataMember]
        public string contactMSISDN { set; get; }

        [DataMember]
        public string contactNos { set; get; }

        [DataMember]
        public List<Documents> DocumentsList { set; get; }

        public bool IsResubmitted
        {
            get
            {
                return id > 0;
            }
        }
    }
}
