﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class GetDetailsFromDocumentResponse
    {
        [DataMember]//CallResponse
        public CallResponse CallResponseObj { get; set; }
        [DataMember]
        public int CustomerID { get; set; } // In case customer Id was not set to 0, then the customer is already exists and the details would not be filled for security purposes
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string SecondName { get; set; }
        [DataMember]
        public string ThirdName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public String DoB { get; set; }
        [DataMember]
        public string NationalityCode { get; set; }// Country code extracted from the document
        [DataMember]
        public int NationalityID { get; set; } //Country Id in CAS lookups
        [DataMember]
        public string IdNo { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public int TitleID { get; set; }
        [DataMember]
        public string IDExpiryDate { get; set; }
        [DataMember]
        public int GenderID { get; set; }
        [DataMember]
        public bool IsIDExpired { get; set; } // For existing customers, it would be true in case the id expired.
        [DataMember]
        public string ResponseMessage { get; set; } // It would have the backend error message in case of any.
        [DataMember]
        public string RequestDocumentStagingGuid { get; set; }

    }

}
