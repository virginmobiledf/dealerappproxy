﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class RechargeSummaryReportResponse
    {
        [DataMember]
        public RechargeSummaryReport Result { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }
}
