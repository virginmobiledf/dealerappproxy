﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ResetPasswordRequest
    {
        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string MSISDN { get; set; }
    }


    [DataContract]
    public class DHResetPasswordRequest : IAuthenticatableRequest
    {
        [DataMember]
        public string code { get; set; }
        [DataMember]
        public int reason { get; set; }
        [DataMember]
        public string note { get; set; }
        [DataMember]
        public List<Documents> documents { get; set; }
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
    }
}
