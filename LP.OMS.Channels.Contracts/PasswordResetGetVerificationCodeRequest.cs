﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
     public class PasswordResetGetVerificationCodeRequest
    {
        [DataMember]
        public string IDNumber { get; set; }

        [DataMember]
        public string UserName { get; set; }
    }
}
