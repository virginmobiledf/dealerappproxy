﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
     [DataContract]
    public class CheckSIMReplacementResponse
    {
       
        [DataMember]//CallResponse
        public CallResponse CallResponseObj { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string ICCID { get; set; }

        [DataMember]
        public int CustomerId { get; set; }
 
    }
    
}
