﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CancelFamilyRegistrationResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
    }
}
