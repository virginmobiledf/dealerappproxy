﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class SliderImageResponse
    {
        [DataMember]
        public string ImageURL { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}
