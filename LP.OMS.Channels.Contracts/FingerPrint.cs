﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class FingerPrint
    {
        [DataMember]
        public string FingerPrintText { get; set; }
        [DataMember]
        public string DeviceId { get; set; }
        [DataMember]
        public FingerPosition FingerPosition { get; set; }

        [DataMember]
        public int NFIQValue { get; set; }

        [DataMember]
        public string MobileDeviceId { get; set; }

        [DataMember]
        public bool IsSematiOTPUsed { get; set; }

        [DataMember]
        public string SematiOTP { get; set; }

        [DataMember]
        public FP_Type FP_Type { get; set; }

    }
    [Serializable]
    public enum FingerPosition : int
    {
        NotSet = 0,
        Right_Thumb = 1,
        Right_Index = 2,
        Right_Middle = 3,
        Right_Ring = 4,
        Right_Little = 5,
        Left_Thumb = 6,
        Left_Index = 7,
        Left_Middle = 8,
        Left_Ring = 9,
        Left_Little = 10
    }

    [Serializable]
    public enum FP_Type : int
    {
        NotSet = 0,
        SecuGen = 1,
        Columbo = 2
    }
}
