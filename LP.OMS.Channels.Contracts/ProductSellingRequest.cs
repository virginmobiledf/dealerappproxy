﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ProductSellingRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string IDNumber { get; set; }

        [DataMember]
        public Brands SourceBrand { get; set; }

        [DataMember]
        public int SourceLanguage { get; set; }

        [DataMember]
        public int ProductID { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public bool IsOnActivation { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }

        [DataMember]
        public string ProductSellingTransactionId { get; set; }

        [DataMember]
        public bool ForceSubscribe { get; set; }
    }     
}
