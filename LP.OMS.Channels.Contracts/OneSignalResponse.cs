﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class OneSignalResponse
    {
        [DataMember]
        public int CHANNEL { get; set; }
        [DataMember]
        public int REGIONID { get; set; }
        [DataMember]
        public int CITYID { get; set; }
        [DataMember]
        public string DEALERCODE { get; set; }
        [DataMember]
        public int GROUPID { get; set; }
        [DataMember]
        public string REFILLMSISDN { get; set; }
        [DataMember]
        public string CONTACTMSISDN { get; set; }
        [DataMember]
        public string DEVICEIMEI { get; set; }
    }
}
