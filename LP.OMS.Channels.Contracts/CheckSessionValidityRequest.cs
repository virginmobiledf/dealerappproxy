﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckSessionValidityRequest
    {
        [DataMember]
        public string SessionToken { get; set; }
        [DataMember]
        public int Lang { get; set; }
        [DataMember]
        public bool ExtendSession { get; set; }
    }
}
