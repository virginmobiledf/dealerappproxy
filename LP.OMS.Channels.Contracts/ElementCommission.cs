﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ElementCommission
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public double Value { get; set; }
    }
}
