﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [Serializable]
    public class DSTErrorLog
    {
        [DataMember]
        public String error;
    }
}
