﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ChangeSubscriptionTypeRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string idNumber { get; set; }

        [DataMember]
        public string idType { get; set; }

        [DataMember]
        public string Nationality { get; set; }

        [DataMember]
        public int BrandId { get; set; }

        [DataMember]
        public string AbshirOTP { get; set; }

        [DataMember]
        public string ReferanceNo { get; set; }

        [DataMember]
        public int TargetSubscriptionType { get; set; }

        [DataMember]
        public Document SignatureDocument { get; set; }

        [DataMember]
        public string Address { get; set; }
    }

    [DataContract]
    public class ChangeSubscriptionTypeResponse
    {
        [DataMember]
        public bool IsPassed;

        [DataMember]
        public string ResponseMessage;
    }


    [DataContract]
    public class VerifyChangeSubscriptionTypeRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string idNumber { get; set; }

        [DataMember]
        public string idType { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string Nationality { get; set; }

        [DataMember]
        public int BrandId { get; set; }
    }

    [DataContract]
    public class VerifyChangeSubscriptionTypeResponse
    {
        [DataMember]
        public bool CanChangeToPostPaid;

        [DataMember]
        public bool CanChangeToPrePaid;

        [DataMember]
        public bool IsAbshirOTPRequired;

        [DataMember]
        public bool IsPassed;

        [DataMember]
        public string ResponseMessage;

        [DataMember]
        public string CurrentSubscriptionType;
    }
}
