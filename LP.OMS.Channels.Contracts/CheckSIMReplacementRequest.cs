﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckSIMReplacementRequest
    {
        [DataMember]
        public string Msisdn { get; set; }

        [DataMember]
        public string IdNo { get; set; }

        [DataMember]
        public int IDTypeId { get; set; }

        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

    }
}
