﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CancelBookingResponse : BaseResponse
    {
        [DataMember]
        public bool IsSuccess { get; set; }
    }
}
