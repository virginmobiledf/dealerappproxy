﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ChartSummary
    {
        [DataMember]
        public List<ChartItem> ChartItems { get; set; }
        [DataMember]
        public string Total { get; set; }
    }
}
