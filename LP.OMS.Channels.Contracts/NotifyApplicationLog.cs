﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{

    [DataContract]
    public class NotifyApplicationLogRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
        [DataMember]
        public DateTime AppLogTimestamp { get; set; }
        [DataMember]
        public string LogContent { get; set; }
        [DataMember]
        public string DeviceManufacturer { get; set; }
        [DataMember]
        public string DeviceModel { get; set; }
        [DataMember]
        public string IMEI { get; set; }
    }

    [DataContract]
    public class NotifyApplicationLogResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }
    }
}
