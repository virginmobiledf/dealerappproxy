﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class LogFingerprintNFIQTrialsRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public List<FingerprintNFIQTrial> Trials { get; set; }
    }

    [DataContract]
    public class FingerprintNFIQTrial
    {
        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public int NFIQValue { get; set; }
    }
}
