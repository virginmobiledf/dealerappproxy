﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DealerCreationContractRequest : BaseDealerEntities, IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        //Creator ID
        [DataMember]
        public int? ParentId { set; get; }

        public bool IsResubmitted
        {
            get
            {
                return Id > 0;
            }
        }

        [DataMember]
        public string EmployeeID { get; set; }

        [DataMember]
        public string LocationID { get; set; }

    }
}
