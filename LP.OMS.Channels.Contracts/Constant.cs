﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Engine
{
    class Constant
    {
        public class RequestOperationType
        {

            public const string Activation = "Activation";

            public const string Resubmission = "Resubmission";

            public const string Replacement = "Replacement";
        }

    }
}
