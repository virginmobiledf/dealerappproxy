﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class GetSubscriptionTypesResponse : CallResponse
    {
        public List<Subscription> lstSubscriptionTypes { get; set; }
    }
}
