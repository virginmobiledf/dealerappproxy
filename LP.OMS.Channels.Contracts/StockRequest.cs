﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    //Anas Alzube
    [DataContract]
    public class StockRequest
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int RequesterId { get; set; }

        [DataMember]
        public int StockItemId { get; set; }

        [DataMember]
        public int RequestAmount { get; set; }

        [DataMember]
        public string StockRequestStatus { get; set; }

        [DataMember]
        public DateTime LastActionDate { get; set; }

        [DataMember]
        public DateTime RequesterDate { get; set; }

    }
}
