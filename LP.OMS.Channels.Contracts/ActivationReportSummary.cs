﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ActivationReportSummary : BaseReportResponse
    {
        [DataMember]
        public ChartSummary GradeChartSummary { get; set; }
        [DataMember]
        public ChartSummary RechargeChartSummary { get; set; }
        [DataMember]
        public ChartSummary FPEChartSummary { get; set; }
        [DataMember]
        public int TotalGradeReportPages { get; set; }
        [DataMember]
        public int TotalRechargeReportPages { get; set; }
        [DataMember]
        public int TotalFPEReportPages { get; set; }
    }
}
