﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{

    [DataContract]
    public class CommissionReportResponse
    {
        public CommissionReportResponse()
        {
            CycleSchemas = new List<Schema>();
        }
        [DataMember]
        public string CycleName { get; set; }
        [DataMember]
        public DateTime CycleDate { get; set; }
        [DataMember]
        public List<Schema> CycleSchemas { get; set; }

    }
    [DataContract]
    public class Schema
    {
        public Schema()
        {
            CommissionElements = new List<CommissionSchemaElement>();
        }

        [DataMember]
        public string SchemaName { get; set; }
        [DataMember]
        public string PaymentStatus { get; set; }
        [DataMember]
        public DateTime? PaymentDate { get; set; }
        [DataMember]
        public List<CommissionSchemaElement> CommissionElements { get; set; }
    }
    [DataContract]
    public class CommissionSchemaElement
    {
        public CommissionSchemaElement()
        {
            CommissionDetails = new Dictionary<string, CommissionPayment>();
        }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string UIdentifier { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public string Requester { get; set; }
        [DataMember]
        public string Product { get; set; }
        [DataMember]
        public Dictionary<string, CommissionPayment> CommissionDetails { get; set; }
    }
    [DataContract]
    public class CommissionPayment
    {
        [DataMember]
        public double CommissionPaidAmount { get; set; }

    }


}
