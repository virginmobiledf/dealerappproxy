﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CommissionPaymentDetailsReportResponse : BaseReportResponse
    {
        [DataMember]
        public int TotalReportPages { get; set; }
        [DataMember]
        public List<CommissionPaymentSummary> CommissionPaymentSummary { get; set; }
    }

    [DataContract]
    public class CommissionPaymentSummary
    {
        [DataMember]
        public string PaymentStatus { get; set; }
        [DataMember]
        public string CommissionElement { get; set; }
        [DataMember]
        public List<CommissionPaymentDetails> CommissionPaymentDetails { get; set; }
    }

    [DataContract]
    public class CommissionPaymentDetails
    {
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string CommissionElement { get; set; }
        [DataMember]
        public string Amount { get; set; }
        [DataMember]
        public bool IsPaid { get; set; }
    }
}
