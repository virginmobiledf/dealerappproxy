﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class PossibleFailureReasonRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public Brands Brand { get; set; }
    }
}
