﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckCustomerObj
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int language { get; set; }

        [DataMember]
        public string idNumber { get; set; }

        [DataMember]
        public int nawrasIdType { get; set; }

        [DataMember]
        public String ApplicationVersion { get; set; }

    }
}
