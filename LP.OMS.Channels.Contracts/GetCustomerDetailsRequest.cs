﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class GetCustomerDetailsRequest
    {
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public ChannelType Channel { get; set; }
    }
}
