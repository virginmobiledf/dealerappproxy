﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ValidateIDBookingResponse : BaseResponse
    {
        [DataMember]
        public AvailableNumber AvailableNumber { get; set; }

        [DataMember]
        public IDBookingStatus IDBookingStatus { get; set; }

        [DataMember]
        public string IDBookingStatusMessage { get; set; }
    }

    public enum IDBookingStatus
    {
        Success = 0,
        There_Is_No_Booking = 1,
        MSISDN_Unavailable = 2,
        Expired_Still_Available = 3,
        Activated = 4,
        Brand_Mismatch = 5
    }
}
