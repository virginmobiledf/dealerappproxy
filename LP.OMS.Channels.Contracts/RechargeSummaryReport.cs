﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class RechargeSummaryReport
    {
        [DataMember]
        public int TotalActivations { get; set; }
        [DataMember]
        public int TotalNoRecharges { get; set; }
        [DataMember]
        public int TotalFirstRecharges { get; set; }
        [DataMember]
        public int TotalSecondRecharges { get; set; }
        [DataMember]
        public int TotalThirdRecharges { get; set; }
    }
}
