﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class LoginResponse
    {
        [DataMember]
        public int CustomerID { get; set; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }

        [DataMember]
        public string UserName { get; set; }

        //HusseiniI
        [DataMember]
        public bool NewApplicationVersionExists { get; set; }
        [DataMember]
        public string ApplicationApkUrl { get; set; }
        [DataMember]
        public bool hasMinimalAppVersion { get; set; }

        //Mahmoud 
        [DataMember]
        public int msisdnLength { get; set; }
        [DataMember]
        public String msisdnStartWith { get; set; }
        [DataMember]
        public int iccidLength { get; set; }
        [DataMember]
        public String iccidStartWith { get; set; }

        [DataMember]
        public int imsiLength { get; set; }
        [DataMember]
        public String imsiStartWith { get; set; }

        [DataMember]
        public int vaildAge { get; set; }

        [DataMember]
        public int OpActivation { get; set; }

        [DataMember]
        public int OpAddonproducts { get; set; }

        [DataMember]
        public int OpAddonproductsVirgin { get; set; }

        [DataMember]
        public int OpELoad { get; set; }
        [DataMember]
        public int OpOnlyResubmission { get; set; }
        [DataMember]
        public int OpDealerActivation { get; set; }
        [DataMember]
        public int OpActivationReport { get; set; }
        [DataMember]
        public List<int> UserPrivilageList { get; set; }
        [DataMember]
        public List<String> MandatoryField { get; set; }
        [DataMember]
        public int OpVirginPrepaidActivation { get; set; }
        [DataMember]
        public int OpVirginPostpaidActivation { get; set; }
        [DataMember]
        public int OpVirginVanityActivation { get; set; }
        [DataMember]
        public bool ConvertActivationDocsToGrayScale { get; set; }
        [DataMember]
        public bool LogApplicationCrashes { get; set; }
        [DataMember]
        public bool NotifyApplicationLogs { get; set; }
        [DataMember]
        public int ApplicationLogExpiryHours { get; set; }
        [DataMember]
        public int OpVerifyCustomerDetails { get; set; }

        [DataMember]
        public int OpFNDIResubmission { get; set; }
        [DataMember]
        public int OpVirginResubmission { get; set; }
        [DataMember]
        public int OpCommissionReport { get; set; }
        [DataMember]
        public bool HasAnyVirginOperation { get; set; }
        [DataMember]
        public bool HasAnyFriendiOperation { get; set; }

        [DataMember]
        public int ApplicationFormImageQuality { get; set; }
        [DataMember]
        public int IdCopyImageQuality { get; set; }

        [DataMember]
        public int KitIDLength { get; set; }
        [DataMember]
        public string KitIDStartWith { get; set; }
        [DataMember]
        public string WellcomeMessage { get; set; }

        [DataMember]
        public int OpPhysicalFormCollection { get; set; }
        [DataMember]
        public int MaxPhysicalFormCollectionBatchSize { get; set; }
        [DataMember]
        public bool IsRequesterFBO { get; set; }
        [DataMember]
        public bool CanUseDateOfBirthFriendi { get; set; }
        [DataMember]
        public bool CanUseDateOfBirthVirgin { get; set; }
        [DataMember]
        public int MinFriendiPrepaidActivationYears { get; set; }
        [DataMember]
        public int MinVirginPrepaidActivationYears { get; set; }
        [DataMember]
        public int MinVirginPostpaidActivationYears { get; set; }
        [DataMember]
        public int MaximumCustomerAge { get; set; }
        [DataMember]
        public bool CanUsePlanActivationCodePrepaid { get; set; }
        [DataMember]
        public bool CanUsePlanVoucherPrepaid { get; set; }

        [DataMember]
        public int MinAllowedNFIQFingerprintValue { get; set; }

        [DataMember]
        public int OpOwnershipManagement { get; set; }

        [DataMember]
        public bool CanUpdateIDForOwnership { get; set; }

        [DataMember]
        public string RefillMSISDN { get; set; }

        [DataMember]
        public int OpFRiENDiPostpaidActivation { get; set; }

        [DataMember]
        public int OpVirginPostpaidBasicActivation { get; set; }

        [DataMember]
        public int OpSIMReplacement { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public bool CanActivateWithBundle { get; set; }

        [DataMember]
        public bool CanActivateWithNoAdditions { get; set; }

        [DataMember]
        public int NextSessionCheckTime { get; set; }

        [DataMember]
        public int OpFRiENDiDataSimActivation { get; set; }

        [DataMember]
        public int OpVirginDataSimActivation { get; set; }

        [DataMember]
        public bool EnableFRiENDiReferralCodeFeature { get; set; }

        [DataMember]
        public bool EnableVirginReferralCodeFeature { get; set; }

        [DataMember]
        public bool EnableFRiENDiPostpaidBasicDataSimActivation { get; set; }

        [DataMember]
        public bool EnableVirginPostpaidBasicDataSimActivation { get; set; }

        [DataMember]
        public bool CanUseFRiENDiSematiOTP { get; set; }

        [DataMember]
        public bool CanUseVirginSematiOTP { get; set; }

        [DataMember]
        public int OpDigitalActivation { get; set; }

        [DataMember]
        public int OpELoadVirgin { get; set; }

        [DataMember]
        public int OpActivationReportVirgin { get; set; }

        [DataMember]
        public int OpCommissionReportVirgin { get; set; }

        [DataMember]
        public int OpVerifyCustomerDetailsVirgin { get; set; }

        [DataMember]
        public int OpOwnershipManagementVirgin { get; set; }

        [DataMember]
        public bool CanUpdateIDForOwnershipVirgin { get; set; }

        [DataMember]
        public int OpSIMReplacementVirgin { get; set; }

        [DataMember]
        public int OpStockFRiENDi { get; set; }

        [DataMember]
        public int OpStockVirgin { get; set; }

        [DataMember]
        public int OpStatusCheckFRiEDNi { get; set; }

        [DataMember]
        public int OpStatusCheckVirgin { get; set; }

        [DataMember]
        public int OpPortInFRiENDi { get; set; }
        [DataMember]
        public int OpPortInVirgin { get; set; }
        [DataMember]
        public int OpCancelPortInFRiENDi { get; set; }
        [DataMember]
        public int OpCancelPortInVirgin { get; set; }
        [DataMember]
        public string PortInMSISDNStartWith { get; set; }

        [DataMember]
        public int OpAssociateScannedSim { get; set; }

        [DataMember]
        public int OpAssociateScannedSimVM { get; set; }

        [DataMember]
        public int OpEsimActivation { get; set; }
        [DataMember]
        public int OpRefillDealerEWalletFRiENDi { get; set; }
        [DataMember]
        public int OpRefillDealerEWalletVirgin { get; set; }
        [DataMember]
        public bool EnableFingerprintDeviceLogging { get; set; }
        [DataMember]
        public int OpAcquisitionMode { get; set; }
        [DataMember]
        public bool EnableAcquisitionMode { get; set; }
        [DataMember]
        public bool EnableOTPValidationMode { get; set; }
        [DataMember]
        public int OpSimTerminationVirgin { get; set; }
        [DataMember]
        public int OpSimTerminationFRiENDi { get; set; }
        [DataMember]
        public int OpEsimActivationFRiENDi { get; set; }
        [DataMember]
        public bool EnablePostPaidBasicFPAuth { get; set; }
        [DataMember]
        public int OpChangeSubscriptionTypeVirgin { get; set; }
        [DataMember]
        public int OpChangeSubscriptionTypeFRiENDi { get; set; }
        [DataMember]
        public bool IsModernTrade { get; set; }
        [DataMember]
        public bool IsCheckInRequired { get; set; }
        [DataMember]
        public bool IsAttendanceEnabled { get; set; }
        [DataMember]
        public string LocationID { get; set; }

        [DataMember]
        public bool IsVPNDetectionEnabled { get; set; }

        [DataMember]
        public bool IsMockLocationDetectionEnabled { get; set; }

        [DataMember]
        public string CurrentLocation { get; set; }

        [DataMember]
        public bool EnableFPDealerLoginToSEMATI { get; set; }

        [DataMember]
        public bool EnableIAMOTPDealerLoginToSEMATI { get; set; }

        [DataMember]
        public int OpDealerHiring { get; set; }

        [DataMember]
        public int AllowedAreaRadius { get; set; }

        [DataMember]
        public bool EnableInDirectSaleZeroSIMActivationCR { get; set; }

        [DataMember]
        public bool isIndirectSalesChannel { get; set; }

        [DataMember]
        public bool EnableColumboFP { get; set; }

        [DataMember]
        public bool EnableSecuGenFP { get; set; }

        [DataMember]
        public int FRiENDiBookingRight { get; set; }

        [DataMember]
        public int VirginBookingRight { get; set; }

        [DataMember]
        public int VirginVanityBookingRight { get; set; }

        [DataMember]
        public bool CanPOSActivateDigital { get; set; }

        [DataMember]
        public string FP_Bitrate { get; set; }

        [DataMember]
        public bool IsAttendanceVerificationRequired { get; set; }
    }
}
