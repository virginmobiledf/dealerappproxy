﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DHLocation
    {
        [DataMember]
        public bool VirginSale { get; set; }

        [DataMember]
        public bool FriendiSale { get; set; }

        [DataMember]
        public string LocationName { get; set; }

        [DataMember]
        public string LocationId { get; set; }

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public string RegionNameEn { get; set; }

        [DataMember]
        public string RegionNameAr { get; set; }

        [DataMember]
        public string GovernorateNameEn { get; set; }

        [DataMember]
        public string GovernorateNameAr { get; set; }

        [DataMember]
        public string CityNameEn { get; set; }

        [DataMember]
        public string CityNameAr { get; set; }

        [DataMember]
        public int CityId { get; set; }

        [DataMember]
        public int RegionId { get; set; }

    }

    [DataContract]
    public class AddLocationRequest
    {
        [DataMember]
        public LoginRequest Login { get; set; }

        [DataMember]
        public bool VirginSale { get; set; }

        [DataMember]
        public bool FriendiSale { get; set; }

        [DataMember]
        public string LocationNameEn { get; set; }

        [DataMember]
        public string LocationNameAr { get; set; }

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string LocationType { get; set; }

        [DataMember]
        public double stdev_km { get; set; }

    }

    [DataContract]
    public class AddLocationResponse : DHBaseResponse
    {
        [DataMember]
        public string LocationID { get; set; }
    }
}
