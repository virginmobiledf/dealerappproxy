﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class SIMReplacementRequest
    {
        [DataMember]
        public string IDType { get; set; }

        [DataMember]
        public string IDNumber { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public FingerPrint Fingerprint { get; set; }

        [DataMember]
        public List<Document> Documents { get; set; }

        [DataMember]
        public int SourceBrand { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string NationalityCode { get; set; }

        [DataMember]
        public bool IsESIM { get; set; }

        [DataMember]
        public string SIMCode { get; set; }

        [DataMember]
        public string ReferenceNo { get; set; }
    }
}
