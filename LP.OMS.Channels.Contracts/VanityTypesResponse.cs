﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class VanityTypesResponse : BaseResponse
    {
        [DataMember]
        public List<VanityType> VanityTypes { get; set; }
    }

    [DataContract]
    public class VanityType
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string NameEn { get; set; }
        [DataMember]
        public string NameAR { get; set; }
        [DataMember]
        public double Price { get; set; }

        [DataMember]
        public bool IsFRiENDi { get; set; }
    }
}
