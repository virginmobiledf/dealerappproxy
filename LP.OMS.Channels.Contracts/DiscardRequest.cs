﻿using LP.OMS.Channels.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DiscardRequest : IAuthenticatableRequest
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public RequestType RequestTypeID { get; set; }
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }
    }
}
