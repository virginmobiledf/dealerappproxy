﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CommissionsDetailsReportResponse
    {
        [DataMember]
        public CommissionsDetailsReport Result { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }
}
