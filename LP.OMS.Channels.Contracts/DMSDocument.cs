﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DMSDocument
    {
        [DataMember]
        public string Base64 { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public List<string> Tags { get; set; }
        [DataMember]
        public string Extention { get; set; }
    }

    [DataContract]
    public class DocumentDetails
    {
        [DataMember]
        public bool IsSuccess { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string DocumentId { get; set; }
    }
}
