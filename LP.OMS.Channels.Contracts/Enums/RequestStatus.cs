﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts.Enums
{
    public enum RequestStatus
    {
        NotSet= 0,
        Pending= 1,
        Rejected = 2,
        Approved = 3,
        Automated = 4,
        Discarded = 5,
    }

    public enum RequestType
    {
        NotSet=0,
        DealerCreateRequest = 1,
        DealerUpdateRequest = 2,
        DealerCreationRequestResubmission = 3
    }

    public enum RequestUpdateType
    {
        NotSet=0,
        Deactivate=1,
        Reactivate=2,
        MSISDN_OTP=3,
        SaudiID=4,
        Discard = 5,
        SaudiIDResubmission = 6,
        DeactivateResubmission = 7,
        ReactivateResubmission = 8,
        ResetPassword = 9
    }

    public enum DealerHiringErrorCode
    {
        Success = 0,
        SaudiId_Already_Exist = 1,
        Dealer_Already_Exist = 2,
        No_Available_Data = 3,
        De_Activate_Pending_Request_Already_exist = 4,
        Dealer_Already_Deactivated = 5,
        Re_Activate_Pending_Request_Already_exist = 6,
        Dealer_Already_Activated = 7,
        Update_SaudiID_Pending_Request_Already_exist = 8,
        Update_OTP_MSISDN_Pending_Request_Already_exist = 9,
        Discard_Pending_Request_Already_Exist = 10,
        Process_Already_Approved = 11,
        No_Descendant_To_Retrieve_Activation_Report = 12,
        No_Descendant_To_Retrieve_Commission_Report = 13,
        Not_Valid_SaudiID = 14
    }
}
