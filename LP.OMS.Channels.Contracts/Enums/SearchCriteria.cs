﻿using System.Runtime.Serialization;


namespace LP.OMS.Channels.Contracts.Enums
{
    [DataContract]
    public enum DHGeneralSearchCriteria
    {
        [EnumMember]
        ByFirstPayableEvent = 1,
        [EnumMember]
        ByNotAchivedFirstPayableEvent = 2,
        [EnumMember]
        ByNoRecharge = 3,
        [EnumMember]
        ByFirstRechage = 4,
        [EnumMember]
        BySecondRechage = 5,
        [EnumMember]
        ByThirdRechage = 6
    }
    [DataContract]
    public enum DateSearchCriteria
    {
        [EnumMember]
        Today = 1,
        [EnumMember]
        Yesterday = 2,
        [EnumMember]
        LastMonth = 3
    }
}
