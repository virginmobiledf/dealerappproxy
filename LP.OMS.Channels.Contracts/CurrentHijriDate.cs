﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CurrentHijriDate
    {

        [DataMember]
        public String Day { get; set; }
        [DataMember]
        public String Month { get; set; }
        [DataMember]
        public String Year { get; set; }
    }
}
