﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class UpdateCustomerPreferencesRequest
    {
        [DataMember]
        public bool? WaitResponse { get; set; }
        [DataMember]
        public string CRMID { get; set; }
        [DataMember]
        public string SponsorEmail { get; set; }
        [DataMember]
        public string SponsorMSISDN { get; set; }
        [DataMember]
        public string ExternalMSISDN { get; set; }
        [DataMember]
        public string OperationType { get; set; }
        [DataMember]
        public List<string> DeviceId { get; set; }
        [DataMember]
        public List<string> AndroidToken { get; set; }
        [DataMember]
        public List<string> IOSToken { get; set; }
        [DataMember]
        public List<string> MSISDN { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Lang { get; set; }
        [DataMember]
        public string IMEI { get; set; }
        [DataMember]
        public UserChannelPermission userChannelPermission { get; set; }
    }

    public class UpdateCustomerPreferencesResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }

    public class UserChannelPermission
    {
        public UserChannelPermission()
        {
            MSISDN = "";
            Email = "";
            Informative_PushIsEnable = true;
            Informative_SMSIsEnable = true;
            Informative_EmailIsEnable = true;
            Campaign_PushIsEnable = true;
            Campaign_SMSIsEnable = true;
            Campaign_EmailIsEnable = true;

        }
        public string MSISDN { get; set; }
        public string Email { get; set; }
        public bool Informative_PushIsEnable { get; set; }
        public bool Informative_SMSIsEnable { get; set; }
        public bool Informative_EmailIsEnable { get; set; }

        public bool Campaign_PushIsEnable { get; set; }
        public bool Campaign_SMSIsEnable { get; set; }
        public bool Campaign_EmailIsEnable { get; set; }
    }
}
