﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class PhysicalFormCheckRequest
    {
        [DataMember]
        public string FormSerial { get; set; }
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string Reference { get; set; }

    }
    public enum PhysicalFormCheckStatus
    {
        NotSet = 0,
        NoActivation = 1,
        AlreadySalesDelivered = 2,
        AlreadyOfficeDelivered = 3
    }
    [DataContract]
    public class PhysicalFormCheckResponse
    {
        [DataMember]
        public PhysicalFormCheckStatus PhysicalFormCheckStatus { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool IsValid { get; set; }

        [DataMember]
        public string Reference { get; set; }

    }
    [DataContract]
    public class PhysicalFormCollectionRequest
    {
        [DataMember]
        public List<string> FormSerials { get; set; }
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
    }
    public enum PhysicalFormCollectionChannel : int
    {
        Scanner = 1,
        CAS = 2,
        DST = 3,
        ExcelUploades = 4
    }
    public enum PhysicalFormCollectionCategory : int
    {
        Successful = 1,
        NoImsi = 2,
        NoActivation = 3,
        Duplicated = 4
    }
}
