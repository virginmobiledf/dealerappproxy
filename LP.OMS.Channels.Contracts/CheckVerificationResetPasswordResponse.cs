﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CheckVerificationResetPasswordResponse
    {
        [DataMember]
        public CallResponse oCallResponse { get; set; }

        [DataMember]
        public string Password { get; set; }

    }
}
