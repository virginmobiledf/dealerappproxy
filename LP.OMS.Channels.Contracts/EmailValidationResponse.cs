﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CustomerPlanDetailsResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public bool IsPaid { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public decimal AmountVATIncluded { get; set; }
        [DataMember]
        public decimal VAT { get; set; }
        [DataMember]
        public List<CustomerSummary> CustomerSummary { get; set; }
        [DataMember]
        public string CRMAccountNo { get; set; }
        [DataMember]
        public bool ShowAddNewSIMButton { get; set; }
        [DataMember]
        public bool ShowAddNewDeviceButton { get; set; }
        [DataMember]
        public bool ShowRegistrationAlert { get; set; }
    }

    [DataContract]
    public class PlanResponse : PlanResponseExtended
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Unit { get; set; }
        [DataMember]
        public double Value { get; set; }
        [DataMember]
        public string ID { get; set; }
    }

    [DataContract]
    public class Extras
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public decimal Price { get; set; }
    }

    [DataContract]
    public class CustomerSummary
    {
        [DataMember]
        public string VanityType { get; set; }
        [DataMember]
        public decimal VanityPrice { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public decimal TotalPlansPrice { get; set; }
        [DataMember]
        public List<PlanResponse> Plans { get; set; }
        [DataMember]
        public string PlanDesc { get; set; }
        [DataMember]
        public decimal TotalExtrasPrice { get; set; }
        [DataMember]
        public List<Extras> Extras { get; set; }
        [DataMember]
        public bool IsActivated { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string ProductCode { get; set; }
        [DataMember]
        public string VanityCommitmentPeriod { get; set; }
        [DataMember]
        public OnboardingType OnboardingType { get; set; }
        [DataMember]
        public bool isDealerRegistration { get; set; }
        [DataMember]
        public string VanityPromotion { get; set; }
        [DataMember]
        public MNPData MNPData { get; set; }
        [DataMember]
        public List<PlanInfo> Promotions { get; set; }
        [DataMember]
        public bool IsEsim { get; set; }
        [DataMember]
        public decimal EsimPriceVATExcluded { get; set; }
    }

    [DataContract]
    public class MNPData
    {
        [DataMember]
        public bool IsPortInNumber { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int OperatorID { get; set; }
    }

    [DataContract]
    public class PlanResponseExtended
    {
        [DataMember]
        public int ServiceType { get; set; }
        [DataMember]
        public string UnitType { get; set; }
        [DataMember]
        public string Balance { get; set; }
    }
}
