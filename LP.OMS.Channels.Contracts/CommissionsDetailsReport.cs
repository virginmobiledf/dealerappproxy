﻿using System.Collections.Generic;
using System.Runtime.Serialization;


namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CommissionsDetailsReport
    {
        [DataMember]
        public List<string> Headers { get; set; }
        [DataMember]
        public List<CommissionsDetailsDataItem> CommissionsDetailsDataItems { get; set; }
    }

    [DataContract]
    public class CommissionsDetailsDataItem
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string IMSI { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public double TotalCommissions { get; set; }
        [DataMember]
        public List<ElementCommission> elementCommissions { get; set; }
    }
}
