﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ESIMData
    {
        [DataMember]
        public string SMDP_URL { get; set; }
    }
}
