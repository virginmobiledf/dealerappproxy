﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class AbstractAddOn
    {
        [DataMember]
        public bool IsAllowed { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
