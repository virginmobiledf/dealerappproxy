﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class StatusCheckResponse
    {
        [DataMember]
        public Boolean IsPassed { set; get; }
        [DataMember]
        public String ResponseMessage { set; get; }
        [DataMember]
        public Boolean IMSIStatus { set; get; }
        [DataMember]
        public Boolean KitStatus { set; get; }
    }
}
