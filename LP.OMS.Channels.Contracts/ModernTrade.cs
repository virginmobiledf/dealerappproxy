﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ModernTradeRequest
    {
        [DataMember]
        public LoginRequest LoginRequest;

        [DataMember]
        public int ReportType { get; set; }
    }

    [DataContract]
    public class ModernTradeResponse
    {
        [DataMember]
        public string PayoutPercent { get; set; }

        [DataMember]
        public double Vanity { get; set; }

        [DataMember]
        public double Sales { get; set; }

        [DataMember]
        public double RevenueM2 { get; set; }

        [DataMember]
        public double RevenueM3 { get; set; }

        [DataMember]
        public double AchievedPercent { get; set; }

        [DataMember]
        public string AchievedTarget { get; set; }

        [DataMember]
        public string ActivationTarget { get; set; }
        [DataMember]
        public double AchievedPercent_VM { get; set; }

        [DataMember]
        public string AchievedTarget_VM { get; set; }

        [DataMember]
        public string ActivationTarget_VM { get; set; }

        [DataMember]
        public string MTDSearchQuery { get; set; }

        [DataMember]
        public string TotalPayOut { get; set; }

        [DataMember]
        public bool IsPassed { get; set; }

        [DataMember]
        public string ResponseMessage { get; set; }

    }

    [DataContract]
    public class NearestLocationsResponse : BaseResponse
    {
        [DataMember]
        public List<Location> Locations { get; set; }
    }

    [DataContract]
    public class Location
    {
        [DataMember]
        public bool VirginSale { get; set; }

        [DataMember]
        public bool FriendiSale { get; set; }

        [DataMember]
        public string LocationName { get; set; }

        [DataMember]
        public string LocationId { get; set; }

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public double Latitude { get; set; }

    }


    [DataContract]
    public class DealerAchievementResponse : BaseResponse
    {
        [DataMember]
        public List<MT_Report> MT_Report { get; set; }
    }

    [DataContract]
    public class DealerAchievementRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }

        [DataMember]
        public string Start { get; set; }

        [DataMember]
        public string End { get; set; }
    }

    [DataContract]
    public class MT_Report
    {
        [DataMember]
        public string SalesLocationName { get; set; }

        [DataMember]
        public int TotalActivationsCount { get; set; }

        [DataMember]
        public double CacheCollectionVatIncluded { get; set; }

        [DataMember]
        public double CacheCollectionVatExcluded { get; set; }

        [DataMember]
        public DateTime? CheckInDateTime { get; set; }

        [DataMember]
        public DateTime? CheckOutDateTime { get; set; }

        [DataMember]
        public bool IsVerified { get; set; }
    }

    [DataContract]
    public class AttendanceTrackingRequest
    {
        [DataMember]
        public LoginRequest LoginRequest;

        [DataMember]
        public double Longitude { get; set; }

        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public int AttendanceProcessType { get; set; }

        [DataMember]
        public int AttendanceDoneThrough { get; set; }
    }

 

    [DataContract]
    public class GetLocationByIDExternalResponse : BaseResponse
    {
        [DataMember]
        public Location Location { get; set; }
    }
    /*------------------------Enums -----------------------*/

    public enum ModernTradeReportType : int
    {
        MonthToDate = 1,
        PreviousMonth = 2,
        TwoMonthesBack = 3,
        ThreeMonthesBack = 4,
    }

    public enum AttendanceProcessType : int
    {
        CheckIn = 1,
        CheckOut = 2,
        Login = 3,
        Logout = 4,
        GeoLocationTracking = 5,
        CheckInAttemptOutsideAllowedRadius = 6,
        CheckOutAttemptOutsideAllowedRadius = 7,
        Verfication=8,
        VerifyAttemptOutsideAllowedRadius=9,
        CheckOut_MissedVerfication=10
    }

    public enum AttendanceDoneThrough : int
    {
        Map = 1,
        Event = 2,
        Undefined = 3,
        QR = 4,
    }
}
