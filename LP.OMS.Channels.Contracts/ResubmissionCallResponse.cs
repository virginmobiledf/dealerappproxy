﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class ResubmissionCallResponse
    {
        [DataMember]//CallResponse
        public CallResponse CallResponseObj { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string SecondName { get; set; }

        [DataMember]
        public string RejectionReason { get; set; }

        [DataMember]
        public int RequestedTypeId { get; set; }

        [DataMember]
        public string RequestedDate { get; set; }

        [DataMember]
        public string RejectedDate { get; set; }

        [DataMember]
        public int WFActivityId { get; set; }
 
    }
}
