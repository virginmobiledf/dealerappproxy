﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class CustomerPlanRequest
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public LoginRequest LoginRequest { get; set; }
    }

    [DataContract]
    public class GenericCustomerService
    {
        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public long Initial { get; set; }

        [DataMember]
        public long Remaining { get; set; }

        [DataMember]
        public int ServiceType { get; set; }

        [DataMember]
        public string Unit { get; set; }

    }

    [DataContract]
    public class CustomerActivePlanDetailsResponse
    {
        [DataMember]
        public string RenewalDate { get; set; }

        [DataMember]
        public List<GenericCustomerService> CustomerServices { get; set; }

    }

}
