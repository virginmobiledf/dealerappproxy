﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
   public class CheckSematiAuthenticationObject
    {
        public string DealerCode { get; set; }

        public string SessionToken { get; set; }

        public string FingerPrintSerial { get; set; }
    }
}
