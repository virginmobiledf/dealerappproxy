﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class VirginVanityActivationCheckRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string BookingCode { get; set; }
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public FingerPrint FingerPrint { get; set; }
        [DataMember]
        public string ActivationPlanCode { get; set; }
        [DataMember]
        public string PlanVoucherNumber { get; set; }
        [DataMember]
        public string NationalityCode { get; set; }
    }
    [DataContract]
    public class VirginVanityActivationCheckResponse
    {
        [DataMember]
        public bool IsPassed { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public VirginSubscriptionType ExpectedSubscriptionType { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public bool IsPaid { get; set; }
        [DataMember]
        public string Price { get; set; }
        [DataMember]
        public List<VirginSubscriptionType> AvailableSubscriptionTypes { get; set; }

    }
    [DataContract]
    public class VirginVanityActivationActivationRequest
    {
        [DataMember]
        public LoginRequest LoginRequest { get; set; }
        [DataMember]
        public string BookingCode { get; set; }
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public string IDTypeCode { get; set; }
        [DataMember]
        public VirginActivationRequest VirginActivationRequest { set; get; }
    }
}
