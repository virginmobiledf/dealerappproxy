﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class PossibleFailureReason
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public bool IsDealerNoteRequired { get; set; }
        [DataMember]
        public string ReasonDisplayText { get; set; }
    }
}
