﻿using System.Runtime.Serialization;

namespace LP.OMS.Channels.Contracts
{
    [DataContract]
    public class DeActivateRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public int Reason { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public int Id { get; set; }

        public bool IsResubmitted
        {
            get
            {
                return Id > 0;
            }
        }
    }
}
