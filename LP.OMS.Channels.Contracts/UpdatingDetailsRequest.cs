﻿using LP.OMS.Channels.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace LP.OMS.Channels.Contracts
{
    public class UpdatingDetailsRequest : IAuthenticatableRequest
    {
        [DataMember]
        public LoginRequest oLoginRequest { get; set; }

        [DataMember]
        public int Id { get; set; }
    }

    [DataContract]
    public class UpdatingDetailsResponse /*: BaseDealerEntities*/
    {
        [DataMember]
        public string Code { set; get; }
        [DataMember]
        public string ContactMSISDN { set; get; }
        [DataMember]
        public string ContactNos { set; get; }
        [DataMember]
        public string CreatedBy { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string SaudiID { set; get; }
        [DataMember]
        public RequestStatus RequestStatus { set; get; }
        [DataMember]
        public string RequestDetails { set; get; }
        [DataMember]
        public string RejectionReason { set; get; }
        [DataMember]
        public List<Documents> DocumentsList { set; get; }
        [DataMember]
        public int RequestUpdateType { set; get; }
        [DataMember]
        public int RequestReason { set; get; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string CreationDate { set; get; }
        [DataMember]
        public int Id { set; get; }
    }
}
