﻿using LP_HRMS_Integration.Contracts;
using LP_HRMS_Integration.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace LP_HRMS_Integration.Controllers
{
    public class EmployeeController : ApiController
    {
        [HttpPost]
        [ResponseType(typeof(Contracts.BaseResponse))]
        [Route("api/Employee/Employee/")]
        public HttpResponseMessage Employee([FromBody]Contracts.EmployeeRequest request)
        {
            Contracts.BaseResponse response = new Contracts.BaseResponse();
            try
            {
                switch (request.RequestType)
                {
                    case RequestType.Add:

                        if (EmployeeManager.IsValidAddRequest(request))
                        {
                            var result = EmployeeManager.GetEmployeeByNumber(request.EmployeeNo);

                            if(result == null)
                            {
                                int id = EmployeeManager.AddEmployee(request);

                                if (id > 0)
                                {
                                    response.IsSuccess = true;
                                }
                            }
                            else
                            {
                                response.IsSuccess = false;
                                response.Message = "Employee Number Is Used.";
                            }
                           
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Invalid Request.";
                        }

                        break;
                    case RequestType.Update:
                        var employee = EmployeeManager.GetEmployeeByNumber(request.EmployeeNo);

                        if (employee != null)
                        {
                            EmployeeManager.UpdateEmployee(request);
                            response.IsSuccess = true;
                        }
                        else
                        {
                            response.IsSuccess = false;
                            response.Message = "Employee Not Found.";
                        }
                        break;
                    case RequestType.Delete:
                        EmployeeManager.DeleteEmployee(request.EmployeeNo);
                        response.IsSuccess = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message.ToString();
            }
            finally
            {
                if (response.IsSuccess)
                {
                    response.Message = "Done Successfully";
                }
            }

            return Request.CreateResponse<object>(HttpStatusCode.OK, response);

        }
    }
}
