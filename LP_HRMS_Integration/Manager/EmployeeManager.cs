﻿using LP_HRMS_Integration.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace LP_HRMS_Integration.Manager
{
    public class EmployeeManager
    {
        public static bool IsValidAddRequest(Contracts.EmployeeRequest request)
        {
            bool IsValid = true;

            if (request == null ||
                string.IsNullOrEmpty(request.EmployeeNo) ||
                string.IsNullOrEmpty(request.EmployeeNo) ||
                string.IsNullOrEmpty(request.EmployeeEmail) ||
                string.IsNullOrEmpty(request.EmployeeName) ||
                string.IsNullOrEmpty(request.DepartmentName) ||
                string.IsNullOrEmpty(request.WorkLocationName) ||
                string.IsNullOrEmpty(request.EmploymentCategory) ||
                string.IsNullOrEmpty(request.PositionName) ||
                string.IsNullOrEmpty(request.BusinessUnitName) ||
                request.DateOfBirth == null ||
                request.DateOfBirth == DateTime.MinValue ||
                request.DateOfBirth == DateTime.MaxValue ||
                request.ServiceDate == null ||
                request.ServiceDate == DateTime.MinValue ||
                request.ServiceDate == DateTime.MaxValue)
            {
                IsValid = false;
            }


            return IsValid;
        }

        public static int AddEmployee(Contracts.EmployeeRequest employee)
        {
            int newId = 0;

            ObjectParameter outputValue = new ObjectParameter("ID", typeof(int));
            using (AttendenceEntities context = new AttendenceEntities())
            {
                context.AddEmployee(employee.EmployeeNo,
                    employee.DateOfBirth,
                    employee.EmployeeName,
                    employee.EmployeeEmail,
                    employee.AppointmentStatus,
                    employee.EmploymentCategory,
                    employee.ServiceDate,
                    employee.PositionName,
                    employee.BusinessUnitName,
                    employee.WorkLocationName,
                    employee.DepartmentName,
                    employee.DivisionName,
                    employee.Band,
                    employee.Grade,
                    employee.PositionClassification,
                    employee.ManagerEmployeeNumber,
                    outputValue);
            }

            newId = Convert.ToInt32(outputValue.Value);

            return newId;
        }

        public static void UpdateEmployee(Contracts.EmployeeRequest employee)
        {
          
          
            using (AttendenceEntities context = new AttendenceEntities())
            {
                context.UpdateEmployee(employee.EmployeeNo,
                    employee.DateOfBirth,
                    employee.EmployeeName,
                    employee.EmployeeEmail,
                    employee.AppointmentStatus,
                    employee.EmploymentCategory,
                    employee.ServiceDate,
                    employee.PositionName,
                    employee.BusinessUnitName,
                    employee.WorkLocationName,
                    employee.DepartmentName,
                    employee.DivisionName,
                    employee.Band,
                    employee.Grade,
                    employee.PositionClassification,
                    employee.ManagerEmployeeNumber);
            }
     
        }

        public static bool DeleteEmployee(string employeeNumber)
        {
            bool IsSuccess = false;
            using (AttendenceEntities context = new AttendenceEntities())
            {
                context.DeleteEmployee(employeeNumber);
                IsSuccess = true;
            }
            return IsSuccess;
        }

        public static GetEmployeeInfoByEmployeeNo_Result GetEmployeeByNumber(string employeeNumber)
        {
            GetEmployeeInfoByEmployeeNo_Result result = null;
            using (AttendenceEntities context = new AttendenceEntities())
            {
                result = context.GetEmployeeInfoByEmployeeNo(employeeNumber).FirstOrDefault();
            }

            return result;
        }
    }
}