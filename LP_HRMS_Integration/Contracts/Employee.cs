﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LP_HRMS_Integration.Contracts
{
    public class BaseResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }

    public class EmployeeRequest
    {
        public RequestType RequestType { get; set; }

        public string EmployeeNo { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeEmail { get; set; }

        public string AppointmentStatus { get; set; }

        public string EmploymentCategory { get; set; }

        public DateTime? ServiceDate { get; set; }

        public string PositionName { get; set; }

        public string BusinessUnitName { get; set; }

        public string WorkLocationName { get; set; }

        public string DepartmentName { get; set; }

        public string DivisionName { get; set; }

        public string Band { get; set; }

        public string Grade { get; set; }

        public string PositionClassification { get; set; }

        public string ManagerEmployeeNumber { get; set; }

    }

    public enum RequestType
    {
        Add = 1,
        Update = 2,
        Delete = 3
    }
}