﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.Enums;

namespace LP.OMS.Channels.RestWCFServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRestDealerHiringServices" in both code and config file together.
    [ServiceContract]
    public interface IRestDealerHiringServices
    {

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "Login")]
        LP.OMS.Channels.Contracts.DealerHiringLoginResponse Login(LP.OMS.Channels.Contracts.DHLoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "AddDealerCreationRequest")]
        DealerCreationResponse AddDealerCreationRequest(DealerCreationContractRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetLookups")]
        LookupsList GetLookups(LookupsRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetMyRequests")]
        MyRequestsResponse GetMyRequests(MyRequestsRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetCreationRequestDetails")]
        CreationDetailsResponse GetCreationRequestDetails(CreationDetailsRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetUpdatingRequestDetails")]
        UpdatingDetailsResponse GetUpdatingRequestDetails(UpdatingDetailsRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DeActivateRequest")]
        DeActivateRequestResponse DeActivateRequest(DeActivateRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "ReActivateRequest")]
        ReActivateResponse ReActivateRequest(ReActivateRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SearchDealerRequest")]
        SearchDealerResponse SearchDealerRequest(SearchDealerRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "ResetPassword")]
        ResetPasswordResponse ResetPassword(DHResetPasswordRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DiscardRequest")]
        DiscardResponse DiscardRequest(DiscardRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "UpdateSaudiID")]
        DHBaseResponse UpdateSaudiID(UpdateSaudiIDRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "UpdateOTPMSISDN")]
        DHBaseResponse UpdateOTPMSISDN(UpdateOTPMSISDNRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "GetDescendantActivations")]
        DescendantActivationsResponse GetDescendantActivations(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "GetDescendantCommissions")]
        DescendantCommissionsResponse GetDescendantCommissions(LoginRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "GetActivationSummaryReport")]
        ActivationSummaryReportResponse GetActivationSummaryReport(ActivationSummaryReportRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "GetRechargeSummaryReport")]
        RechargeSummaryReportResponse GetRechargeSummaryReport(RechargeSummaryReportRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "GetCommissionsDetailsReports")]
        CommissionsDetailsReportResponse GetCommissionsDetailsReports(CommissionsDetailsReportRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "AddLocation")]
        AddLocationResponse AddLocation(AddLocationRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "SearchDealerRequestNew")]
        SearchDealerResponse SearchDealerRequestNew(SearchDealerRequest request);

    }
}
