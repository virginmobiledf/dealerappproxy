﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using LP.Core.Utilities.Exceptions;
using System.Configuration;
using System.Drawing;
using System.IO;
using LP.Core.Utilities.ServiceModel;
using LP.Core.Utilities.Logging;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Contracts;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;
using LP.OMS.Channels.RestWCFServices.WCFOnlineChannelServices;
using LP.OMS.Channels.RestWCFServices;
using System.Collections;
using LP.OMS.Channels.Contracts.DealerSImAssociation;

namespace LP.OMS.Channels.WCFServices
{
    [GeneralInspector]
    public class OnlineChannelServices : IOnlineChannelServices
    {
        public CallResponse SendRequest(SubmitRequest oSendRequestObj)
        {
            CallResponse oCallResponse = new CallResponse();

            using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
            {
                oCallResponse = client.SendRequest(oSendRequestObj);
            }

            return oCallResponse;
        }

        public CallResponse SubmitVirginActivation(VirginActivationRequest oVirginActivationRequest)
        {
            CallResponse oCallResponse = new CallResponse();

            using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
            {
                oCallResponse = client.SubmitVirginActivation(oVirginActivationRequest);
            }

            return oCallResponse;
        }
        public CustomerPlanDetailsResponse GetCustomerDetails(GetCustomerDetailsRequest orequest)
        {
            CustomerPlanDetailsResponse response = new CustomerPlanDetailsResponse() { IsPassed = true, ResponseMessage = "Success" };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetCustomerDetails(orequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSuctomerDetails", ex, false);
                response.IsPassed = false;
                Assembly a = Assembly.GetExecutingAssembly();

                ResourceManager rm = new ResourceManager("LP.OMS.Channels.RestWCFServices.Resources.OMSResources", a);

                response.ResponseMessage = rm.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);

                return response;
            }

            return response;
        }
        public LoginResponse Login(LoginRequest request)
        {
            LoginResponse response = new LoginResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.Login(request);
                }
                if (request.language == 2)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Login", ex, false);
                response.IsPassed = false;
                Assembly a = Assembly.GetExecutingAssembly();

                ResourceManager rm = new ResourceManager("LP.OMS.Channels.RestWCFServices.Resources.OMSResources", a);

                response.ResponseMessage = rm.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                //if (request.language == 2)
                //{

                //    Assembly a = Assembly.GetExecutingAssembly();
                //    ResourceManager rm = new ResourceManager("LP.OMS.Channels.Engine.Resources", a);
                //    response.ResponseMessage = rm.GetString("MSG_General_Execption");//"هناك خطأ اثناء تنفيذ العملية";
                //}
                //else
                //    response.ResponseMessage = "Error While executing activities";
                return response;
            }

            return response;
        }

        public LP.OMS.Channels.Contracts.Lookups GetLookups(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest)
        {
            Lookups lookups = new Lookups();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    lookups = client.GetLookups(oLoginRequest);


                    //Transform AppMessages Resx To Dictionary<string, string>
                    ResourceSet resourceSet = Resources.AppMessages.ResourceManager.GetResourceSet(GetAppCultureInfo(oLoginRequest.language), true, true);

                    

                    lookups.AppMessages = resourceSet.Cast<DictionaryEntry>().ToDictionary(r => r.Key.ToString(), r => r.Value.ToString());


                    ErrorHandling.HandleException("GetLookupsEx",
                        new Exception(Newtonsoft.Json.JsonConvert.SerializeObject(resourceSet).ToString() + "  " +
                        Newtonsoft.Json.JsonConvert.SerializeObject(lookups.AppMessages).ToString(),new Exception(""))
                        , false);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetLookups", ex, false);
                return new Lookups();
            }

            return lookups;
        }

        private CultureInfo GetAppCultureInfo(int appLang)
        {
            CultureInfo cultureInfo;
            if (appLang == 2)
            {
                cultureInfo = CultureInfo.GetCultureInfo("ar-JO");
            }
            else
            {
                cultureInfo = CultureInfo.CurrentCulture;
            }
            return cultureInfo;
        }

        public DealerActivitiesResponseColelction GetDealerActivitiesList(AndroidReportRequest oAndroidReportRequest)
        {
            DealerActivitiesResponseColelction oCallResponse = new DealerActivitiesResponseColelction();
            oCallResponse.DealerActivitiesResponseList = new List<DealerActivitiesResponse>();

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.GetDealerActivitiesList(oAndroidReportRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateSubscriptionCount", ex, false);
                oCallResponse = new DealerActivitiesResponseColelction();
                oCallResponse.IsPassed = false;

                if (oAndroidReportRequest.oLoginRequest.language == 2)
                    oCallResponse.ResponseMessage = "هناك خطأ اثناء تنفيذ العملية";
                else
                    oCallResponse.ResponseMessage = "Error While executing activities";

                return oCallResponse;
            }

            return oCallResponse;
        }

        public ResubmissionCallResponse GetFailedRequests(FaildActivationRequest oFaildActivationRequest)
        {

            ResubmissionCallResponse oCallResponse = new ResubmissionCallResponse();
            oCallResponse.CallResponseObj = new CallResponse();
            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.GetFailedRequests(oFaildActivationRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendRequest", ex, false);

                oCallResponse.CallResponseObj.IsPassed = false;
                oCallResponse.CallResponseObj.ResponseMessage = "Error While executing activities";
                return oCallResponse;
            }

            return oCallResponse;
        }

        public CallResponse ChangePassword(HashingRequest oHashingRequest)
        {
            CallResponse oCallResponse = null;

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.ChangePassword(oHashingRequest);
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendRequest", ex, false);
                oCallResponse = new CallResponse();
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = "Error While executing activities";
                return oCallResponse;
            }

            return oCallResponse;
        }

        public CallResponse ResubmitRequest(ResubmitRequestObj request)
        {
            CallResponse oCallResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.ResubmitRequest(request);
                }
            }
            catch (Exception ex)
            {


                ErrorHandling.HandleException("ResubmitRequest", ex, false);

                oCallResponse = new CallResponse();

                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = "Error While executing activities";
                return oCallResponse;
            }

            return oCallResponse;
        }

        public StatusCheckResponse StatusCheck(StatusCheckRequest objRequest)
        {
            StatusCheckResponse objResponse = new StatusCheckResponse();

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objResponse = client.StatusCheck(objRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDetailsFromDocument", ex, false);

                objResponse.IsPassed = false;
                objResponse.ResponseMessage = "Error While executing activities";

            }
            return objResponse;

        }

        public EloadResponse Eload(EloadRequest objRequest)
        {
            EloadResponse objResponse = new EloadResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objResponse = client.Eload(objRequest);
                }
            }
            catch (Exception ex)
            {
                objResponse.IsPassed = false;
                objResponse.ResponseMessage = "General Error.";
            }
            return objResponse;
        }

        public LP.OMS.Channels.Contracts.ActivationReportCounters GetActivationReportCounters(LoginRequest objLogin)
        {
            LP.OMS.Channels.Contracts.ActivationReportCounters objActivationReportCounters = null;

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objActivationReportCounters = client.GetActivationReportCounters(objLogin);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationReportCounters", ex, false);
                objActivationReportCounters = new Contracts.ActivationReportCounters();
                objActivationReportCounters.IsPassed = false;
                objActivationReportCounters.ResponseMessage = "Error While executing activities";
                return objActivationReportCounters;
            }
            return objActivationReportCounters;
        }

        public List<LP.OMS.Channels.Contracts.ActivationResponse> GetDealerActivations(Contracts.CommissionDataByDealerRequest objActivationReportRequest)
        {
            List<LP.OMS.Channels.Contracts.ActivationResponse> lstActivations = null;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    lstActivations = client.GetDealerActivations(objActivationReportRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDealerActivations", ex, false);
            }
            return lstActivations;
        }

        public List<Contracts.CommissionReportResponse> GetCommissionReport(Contracts.CommissionDataByDealerRequest objCommissionDataByDealerRequest)
        {
            List<Contracts.CommissionReportResponse> objResponse = null;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objResponse = client.GetCommissionReport(objCommissionDataByDealerRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCommissionReport", ex, false);
            }
            return objResponse;
        }

        public LP.OMS.Channels.Contracts.NotifyApplicationLogResponse NotifyLocalApplicationLog(NotifyApplicationLogRequest request)
        {
            Contracts.NotifyApplicationLogResponse objResponse = null;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objResponse = client.NotifyLocalApplicationLog(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("NotifyLocalApplicationLog", ex, false);
            }
            return objResponse;
        }

        public PhysicalFormCheckResponse PhysicalFormCollectionCheck(PhysicalFormCheckRequest request)
        {
            Contracts.PhysicalFormCheckResponse objResponse = null;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objResponse = client.PhysicalFormCollectionCheck(request);
                    // Reference to the caller of the service to identify the request.
                    objResponse.Reference = request.Reference;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("PhysicalFormCollectionCheck", ex, false);
            }
            return objResponse;
        }

        public CallResponse PhysicalFormsCollectionSubmit(PhysicalFormCollectionRequest request)
        {
            CallResponse oCallResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.PhysicalFormsCollectionSubmit(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("PhysicalFormsCollectionSubmit", ex, false);
            }

            return oCallResponse;
        }

        public VirginVanityActivationCheckResponse VerifyVirginVanityNumber(VirginVanityActivationCheckRequest objVirginVanityActivationCheckRequest)
        {
            Contracts.VirginVanityActivationCheckResponse objVirginVanityActivationCheckResponse = null;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objVirginVanityActivationCheckResponse = client.VerifyVirginVanityNumber(objVirginVanityActivationCheckRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyVirginVanityNumber", ex, false);
            }
            return objVirginVanityActivationCheckResponse;
        }

        public CallResponse ActivateVirginVanityNumber(VirginVanityActivationActivationRequest objVirginVanityActivationActivationRequest)
        {
            CallResponse oCallResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.ActivateVirginVanityNumber(objVirginVanityActivationActivationRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ActivateVirginVanityNumber", ex, false);
            }

            return oCallResponse;
        }

        public DealerBalanceResponse GetDealerBalance(LoginRequest objLoginRequest)
        {
            DealerBalanceResponse oDealerBalanceResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oDealerBalanceResponse = client.GetDealerBalance(objLoginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetDealerBalance", ex, false);
                oDealerBalanceResponse.IsPassed = false;
                oDealerBalanceResponse.ResponseMessage = "Error in get dealer balance";
            }

            return oDealerBalanceResponse;
        }

        public CallResponse CheckCustomerId(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            CallResponse oCallResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.CheckCustomerId(objCheckCustomerIdRquest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckCustomerId", ex, false);
            }

            return oCallResponse;
        }

        public CheckCustomerSubscriptionsResponse CheckCustomerSubscriptions(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            CheckCustomerSubscriptionsResponse objCheckCustomerSubscriptionsResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objCheckCustomerSubscriptionsResponse = client.CheckCustomerSubscriptions(objCheckCustomerIdRquest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckCustomerSubscriptionsResponse", ex, false);
            }

            return objCheckCustomerSubscriptionsResponse;
        }

        public VerifyCustomerResponse VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest)
        {
            VerifyCustomerResponse objVerifyCustomerResponse = null;

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objVerifyCustomerResponse = client.VerifyCustomerSubscriptions(objVerifyCustomerRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyCustomerSubscriptions", ex, false);
            }

            return objVerifyCustomerResponse;
        }

        public CheckVirginCustomerIdResponse CheckVirginCustomerId(CheckVirginCustomerIdRequest objCheckVirginCustomerIdRquest)
        {
            CheckVirginCustomerIdResponse objCheckVirginCustomerIdResponse = null;

            try
            {

                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objCheckVirginCustomerIdResponse = client.CheckVirginCustomerId(objCheckVirginCustomerIdRquest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckVirginCustomerId", ex, false);
            }

            return objCheckVirginCustomerIdResponse;
        }

        public CallResponse LogFingerprintNFIQTrials(LogFingerprintNFIQTrialsRequest logNFIQTrialsRequest)
        {
            var result = new CallResponse();
            try
            {
                using (var client = new OnlineChannelServicesClient())
                {
                    result = client.LogFingerprintNFIQTrials(logNFIQTrialsRequest);
                }
            }
            catch (Exception ex)
            {
                result.IsPassed = false;
                ErrorHandling.HandleException("LogFingerprintNFIQTrials", ex, false);
            }

            return result;
        }

        public CallResponse VerifyOwnershipChange(OwnerChangeRequest request)
        {
            var response = new CallResponse();
            try
            {
                using (var client = new OnlineChannelServicesClient())
                {
                    response = client.VerifyOwnershipChange(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyOwnershipChange", ex, false);
            }

            return response;
        }

        public CallResponse SubmitOwnershipChange(OwnerChangeRequest request)
        {
            var response = new CallResponse();
            try
            {
                using (var client = new OnlineChannelServicesClient())
                {
                    response = client.SubmitOwnershipChange(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitOwnershipChange", ex, false);
            }

            return response;
        }

        //Anas Alzube
        public CallResponse ReplaceSIM(SIMReplacementRequest Request)
        {
            var response = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ReplaceSIM(Request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ReplaceSIM", ex, false);
                response.IsPassed = false;
            }
            return response;
        }

        //Anas Alzube
        public CallResponse ValidateReplaceSIM(SIMReplacementRequest Request)
        {
            var response = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ValidateReplaceSIM(Request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateReplaceSIM", ex, false);
                response.IsPassed = false;
            }
            return response;
        }

        //Anas Alzube
        public CallResponse AddStockRequest(StockItemRequest sRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.AddStockRequest(sRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AddStockRequest", ex, false);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public void LogError(DSTErrorLog dstErrorLog)
        {
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    client.LogError(dstErrorLog);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("DST LogError", ex, true);
            }
        }

        //Anas Alzube
        public BundleResponse GetBundle(BundlesRequest request)
        {
            BundleResponse oBundleResponse = new BundleResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oBundleResponse = client.GetBundle(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetBundle", ex, false);
                oBundleResponse.IsPassed = false;
                oBundleResponse.ResponseMessage = ex.Message;
            }
            return oBundleResponse;
        }
        //Anas Alzube
        public CallResponse SubmitBundle(SubmitBundleRequest SubmitBundleRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.SubmitBundle(SubmitBundleRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitBundle", ex, false);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public int SelfcareResubmitRequest(SelfCareResubmitObj selfCareResubmitObj)
        {
            int ResponseCode = (int)ResubmissionResponseCodes.UNKNOWN_ERROR;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    ResponseCode = client.SelfcareResubmitRequest(selfCareResubmitObj.MSISDN, selfCareResubmitObj.IDNumber, selfCareResubmitObj.Brand, selfCareResubmitObj.DocumentType, selfCareResubmitObj.AdditionalInfo, selfCareResubmitObj.LstResubmissionDocuments);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SelfcareResubmitRequest", ex, false);
            }
            return ResponseCode;
        }

        public AvailableProductsResponse GetAvailableProducts(ProductSellingRequest request)
        {
            AvailableProductsResponse oAvailableResponse = new AvailableProductsResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oAvailableResponse = client.GetAvailableProducts(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableProducts", ex, false);
            }
            return oAvailableResponse;
        }

        public ProductSellingResponse SubmitProductSelling(ProductSellingRequest request)
        {
            ProductSellingResponse ProductSellingResponse = new ProductSellingResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    ProductSellingResponse = client.SubmitProductSelling(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitProductSelling", ex, false);
            }
            return ProductSellingResponse;
        }

        public CallResponse CheckProductSellingEligibility(CheckProductSellingEligibilityRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.CheckProductSellingEligibility(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckProductSellingEligibility", ex, false);
            }
            return oCallResponse;
        }

        public CallResponse VerfiyLoginCode(VerfiyLoginCode request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.VerfiyLoginCode(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerfiyLoginCode", ex, false);
            }

            return oCallResponse;
        }

        public CallResponse ResendLoginCode(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.ResendLoginCode(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ResendLoginCode", ex, false);
            }

            return oCallResponse;
        }

        public CallResponse Logout(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.Logout(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Logout", ex, false);
            }

            return oCallResponse;
        }

        public SematiLoginCallResponse LoginToSemati(LoginRequest request)
        {
            SematiLoginCallResponse oSematiLoginCallResponse = new SematiLoginCallResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    oSematiLoginCallResponse = client.LoginToSemati(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("LoginToSemati", ex, false);
            }

            return oSematiLoginCallResponse;

        }

        public CallResponse CheckSematiAuthentication(CheckSematiAuthenticationObject checkSematiAuthenticationObject)
        {
            CallResponse oCallResponse = new CallResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    oCallResponse = client.CheckSematiAuthentication(checkSematiAuthenticationObject);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckSematiAuthentication", ex, false);
            }

            return oCallResponse;
        }

        public OneSignalResponse GetOneSignalData(LoginRequest loginRequest)
        {
            OneSignalResponse oneSignalResponse = new OneSignalResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oneSignalResponse = client.GetOneSignalData(loginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetOneSignalData", ex, false);
            }

            return oneSignalResponse;
        }

        public ExtendSessionRequest ExtendSession(LoginRequest loginRequest)
        {
            ExtendSessionRequest extendSessionRequest = new ExtendSessionRequest();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    var request = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest;
                    string sessionToken = request.Headers["SessionToken"];
                    extendSessionRequest = client.ExtendSession(loginRequest, sessionToken);
                }
            }
            catch (Exception ex)
            {
                extendSessionRequest.AMErrorCode = 11;//SessionInvalid
                ErrorHandling.HandleException("ExtendSession", ex, false);
            }

            return extendSessionRequest;
        }

        public NextSessionCheckTimeResponse GetNextSessionCheckTime(LoginRequest loginRequest)
        {
            NextSessionCheckTimeResponse nextSessionCheckTimeResponse = new NextSessionCheckTimeResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    var request = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest;
                    string sessionToken = request.Headers["SessionToken"];
                    nextSessionCheckTimeResponse = client.GetNextSessionCheckTime(loginRequest, sessionToken);
                    ResourceSet resourceSet = Resources.AppMessages.ResourceManager.GetResourceSet(GetAppCultureInfo(loginRequest.language), true, true);
                    nextSessionCheckTimeResponse.Message = resourceSet.GetString("alert_Session_will_expire");
                    return nextSessionCheckTimeResponse;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetNextSessionCheckTime", ex, false);
            }

            return nextSessionCheckTimeResponse;
        }

        public GetAllowedIDTypesByIMSIResponse GetAllowedIDTypesByIMSI(GetAllowedIDTypesByIMSIObj getAllowedIDTypesByIMSIObj)
        {
            GetAllowedIDTypesByIMSIResponse response = new GetAllowedIDTypesByIMSIResponse();
            response.idTypes = new List<AllowedIDType>();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    response = client.GetAllowedIDTypesByIMSI(getAllowedIDTypesByIMSIObj);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAllowedIDTypesByIMSI", ex, false);
            }

            return response;
        }

        public GetSubscriptionTypesResponse GetSubscriptionTypes(GetSubscriptionTypeObj getSubscriptionObj)
        {
            GetSubscriptionTypesResponse response = new GetSubscriptionTypesResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    response = client.GetSubscriptionTypes(getSubscriptionObj);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSubscriptionTypes", ex, false);
            }

            return response;
        }


        public List<SliderImageResponse> GetSliderImages(SliderImageRequest sliderImageRequest)
        {
            List<SliderImageResponse> sliderImageResponse = new List<SliderImageResponse>();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    sliderImageResponse = client.GetSliderImages(sliderImageRequest.LoginRequest, sliderImageRequest.Brand);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSliderImages", ex, false);
            }

            return sliderImageResponse;
        }

        public int GetTotalActivation(LoginRequest loginRequest)
        {
            int totalActivation = 0;

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    totalActivation = client.GetTotalActivation(loginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTotalActivation", ex, false);
            }

            return totalActivation;
        }

        public double GetTotalCommission(LoginRequest loginRequest)
        {
            double totalCommission = 0;

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    totalCommission = client.GetTotalCommission(loginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTotalCommission", ex, false);
            }

            return totalCommission;
        }

        public AvailableDealerBalanceResponse GetAvailableDealerBalance(LoginRequest loginRequest)
        {
            AvailableDealerBalanceResponse availableDealerBalanceResponse = new AvailableDealerBalanceResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {

                    availableDealerBalanceResponse = client.GetAvailableDealerBalance(loginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableDealerBalance", ex, false);
            }

            return availableDealerBalanceResponse;
        }

        public AvailableProductsResponse GetAvailableProductsBySubscriptionType(ProductSellingBySubscriptionTypeRequest request)
        {
            AvailableProductsResponse availableProductsResponse = new AvailableProductsResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    availableProductsResponse = client.GetAvailableProductsBySubscriptionType(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableProductsBySubscriptionType", ex, false);
            }

            return availableProductsResponse;
        }

        public GetStocksResponse GetStocks(GetStockRequest getStockRequest)
        {
            GetStocksResponse response = new GetStocksResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetStocks(getStockRequest.LoginRequest, getStockRequest.Brand);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetStocks", ex, false);
            }
            return response;
        }

        public SendRequestStockResponse SendRequestStock(SendRequestStockRequest stockRequest)
        {
            SendRequestStockResponse response = new SendRequestStockResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.SendRequestStock(stockRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendRequestStock", ex, false);
            }
            return response;
        }

        public List<AvailableRatePlan> GetAvailableVanityRatePlans(VanityRatePlansRequest vanityRatePlansRequest)
        {
            List<AvailableRatePlan> response = new List<AvailableRatePlan>();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetAvailableVanityRatePlans(vanityRatePlansRequest.LoginRequest, vanityRatePlansRequest.IdType);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableVanityRatePlans", ex, false);
            }
            return response;
        }

        public double GetRemainingSessionInSeconds(LoginRequest loginRequest)
        {
            double remaining = 0;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    remaining = client.GetRemainingSessionInSeconds(loginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetRemainingSessionInSeconds", ex, false);
            }
            return remaining;
        }

        public List<PossibleFailureReason> GetPossibleFailureReasons(PossibleFailureReasonRequest possibleFailureReasonRequest)
        {
            List<PossibleFailureReason> failureReasons = new List<PossibleFailureReason>();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    failureReasons = client.GetPossibleFailureReasons(possibleFailureReasonRequest.LoginRequest, possibleFailureReasonRequest.Brand);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetPossibleFailureReasons", ex, false);
            }
            return failureReasons;
        }

        public bool SubmitDealerFailureReasonAndNote(DealerFailureReasonNote failureReason)
        {
            bool response = false;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.SubmitDealerFailureReasonAndNote(failureReason);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitDealerFailureReasonAndNote", ex, false);
            }
            return response;
        }

        #region New Activation & Commission Reports

        public ActivationGradeDetailsReportResponse GetActivationGradeDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationGradeDetailsReportResponse response = new ActivationGradeDetailsReportResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetActivationGradeDetailsReport(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationGradeDetailsReport", ex, false);
            }

            return response;
        }

        public ActivationRechargeDetailsReportResponse GetActivationRechargeDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationRechargeDetailsReportResponse response = new ActivationRechargeDetailsReportResponse()
            {
                RechargeSummary = new List<RechargeSummary>()
            };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetActivationRechargeDetailsReport(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationRechargeDetailsReport", ex, false);
            }

            return response;
        }

        public ActivationFPEDetailsReportResponse GetActivationFPEDetailsReport(ActivationCommissionReportRequest request)
        {
            ActivationFPEDetailsReportResponse response = new ActivationFPEDetailsReportResponse()
            {
                FPESummary = new List<FPESummary>()
            };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetActivationFPEDetailsReport(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationRechargeDetailsReport", ex, false);
            }

            return response;
        }

        public CommissionPaymentDetailsReportResponse GetCommissionPaymentDetails(ActivationCommissionReportRequest request)
        {
            CommissionPaymentDetailsReportResponse response = new CommissionPaymentDetailsReportResponse()
            {
                CommissionPaymentSummary = new List<CommissionPaymentSummary>()
            };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetCommissionPaymentDetails(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCommissionPaymentDetails", ex, false);
            }

            return response;
        }

        public ActivationReportSummary GetActivationReportSummary(ActivationCommissionReportRequest request)
        {
            ActivationReportSummary response = new ActivationReportSummary()
            {
                GradeChartSummary = new ChartSummary() { ChartItems = new List<ChartItem>() },
                RechargeChartSummary = new ChartSummary() { ChartItems = new List<ChartItem>() },
                FPEChartSummary = new ChartSummary() { ChartItems = new List<ChartItem>() }
            };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetActivationReportSummary(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetActivationReportSummary", ex, false);
            }

            return response;
        }

        public CommissionReportSummary GetCommissionReportSummary(ActivationCommissionReportRequest request)
        {
            CommissionReportSummary response = new CommissionReportSummary()
            {
                CommissionChartSummary = new ChartSummary(),
                PaidChartSummary = new ChartSummary(),
                PendingChartSummary = new ChartSummary()
            };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetCommissionReportSummary(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCommissionReportSummary", ex, false);
            }

            return response;
        }

        public CancelMNPPortInResponse CancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            CancelMNPPortInResponse response = new CancelMNPPortInResponse()
            { };

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.CancelMNPPortInRequest(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelMNPPortInRequest", ex, false);
            }

            return response;
        }

        public List<OperatorDetails> GetTelecomOperators(LoginRequest request)
        {
            List<OperatorDetails> response = new List<OperatorDetails>();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetTelecomOperators(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTelecomOperators", ex, false);
            }

            return response;
        }

        public CancelMNPPortInResponse ValidateCancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            CancelMNPPortInResponse response = new CancelMNPPortInResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ValidateCancelMNPPortInRequest(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateCancelMNPPortInRequest", ex, false);
            }

            return response;
        }

        public List<decimal> GetTopupAmounts(LoginRequest request)
        {
            List<decimal> response = new List<decimal>();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetTopupAmounts(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetTopupAmounts", ex, false);
            }

            return response;
        }

        public ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request)
        {
            ValidateAndRegisterFamilyResponse response = new ValidateAndRegisterFamilyResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ValidateAndRegisterFamily(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateAndRegisterFamily", ex, false);
            }

            return response;
        }

        public CancelFamilyRegistrationResponse CancelFamilyRegistration(CancelFamilyRegistrationRequest request)
        {
            CancelFamilyRegistrationResponse response = new CancelFamilyRegistrationResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.CancelFamilyRegistration(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelFamilyRegistration", ex, false);
            }

            return response;
        }

        #endregion

        public BaseResponse AssociateScannedSim(DealerSimAssociationRequest request)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.AssociateScannedSim(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AssociateScannedSim", ex, false);
            }

            return response;
        }

        public PagedResult<ShopAssociatedSIMs> GetAssociatedShops(ShopAssociatedSIMsRequest request)
        {
            PagedResult<ShopAssociatedSIMs> response = new PagedResult<ShopAssociatedSIMs>();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetAssociatedShops(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAssociatedShops", ex, false);
            }

            return response;
        }

        public RefillDealerEWalletResponse RefillDealerEWallet(RefillDealerEWalletRequest request)
        {
            RefillDealerEWalletResponse response = new RefillDealerEWalletResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.RefillDealerEWallet(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("RefillDealerEWallet", ex, false);
            }

            return response;
        }

        public List<NumberInfo> GetAllNumbersRealtedByNumberId(string idNumber)
        {
            List<NumberInfo> numberInfoList = new List<NumberInfo>();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    numberInfoList = client.GetAllNumbersRealtedByNumberId(idNumber);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAllNumbersRealtedByNumberId", ex, false);
            }

            return numberInfoList;
        }

        public OTPResponse SendOTP(OTPRequest OTPRequest)
        {
            OTPResponse response = new OTPResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.SendOTP(OTPRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendOTP", ex, false);
            }

            return response;
        }

        public OTPResponse ValidateOTP(ValidateOTPRequest validateOTPRequest)
        {
            OTPResponse response = new OTPResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ValidateOTP(validateOTPRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateOTP", ex, false);
            }
            return response;
        }

        public SendEsimQRImageResponse SendEsimQRImage(SendEsimQRImageRequest request)
        {
            SendEsimQRImageResponse response = new SendEsimQRImageResponse();

            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.SendEsimQRImage(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SendEsimQRImage", ex, false);
            }

            return response;
        }

        ////Anas Alzube
        //public CallResponse PasswordResetGetVerificationCode(PasswordResetGetVerificationCodeRequest request)
        //{
        //    CallResponse oCallResponse = new CallResponse();
        //    try
        //    {
        //        using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
        //        {
        //            oCallResponse = client.PasswordResetGetVerificationCode(request);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("PasswordResetGetVerificationCode", ex, false);
        //        oCallResponse.IsPassed = false;
        //        oCallResponse.ResponseMessage = ex.Message;
        //    }
        //    return oCallResponse;
        //}

        //public CheckVerificationResetPasswordResponse CheckVerificationResetPassword(CheckVerificationResetPasswordRequest request)
        //{
        //    CheckVerificationResetPasswordResponse response = new CheckVerificationResetPasswordResponse();
        //    try
        //    {
        //        using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
        //        {
        //            response = client.CheckVerificationResetPassword(request);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("CheckVerificationResetPassword", ex, false);
        //        response. oCallResponse.IsPassed = false;
        //        response.oCallResponse.ResponseMessage = ex.Message;
        //    }
        //    return response;

        //}

        public TerminationResponse SIMTermination(TerminationRequest TerminationRequest)
        {
            TerminationResponse response = new TerminationResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.SIMTermination(TerminationRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SIMTermination", ex, false);
            }

            return response;
        }

        public CustomerActivePlanDetailsResponse GetCustomerSubscriptionInfo(CustomerPlanRequest CustomerPlanRequest)
        {
            CustomerActivePlanDetailsResponse CustomerActivePlanDetailsResponse = new CustomerActivePlanDetailsResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    CustomerActivePlanDetailsResponse = client.GetCustomerSubscriptionInfo(CustomerPlanRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetCustomerSubscriptionInfo", ex, false);
            }

            return CustomerActivePlanDetailsResponse;
        }

        public VerifyChangeSubscriptionTypeResponse VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest VerifyChangeSubscriptionTypeRequest)
        {
            VerifyChangeSubscriptionTypeResponse respone = new VerifyChangeSubscriptionTypeResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    respone = client.VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerfityChangeSubscriptionType", ex, false);
            }

            return respone;
        }

        public ChangeSubscriptionTypeResponse ChangeSubscriptionType(ChangeSubscriptionTypeRequest request)
        {
            ChangeSubscriptionTypeResponse respone = new ChangeSubscriptionTypeResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    respone = client.ChangeSubscriptionType(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ChangeSubscriptionType", ex, false);
            }

            return respone;
        }

        public ModernTradeResponse SearchDealerModernTrade(ModernTradeRequest request)
        {

            ModernTradeResponse respone = new ModernTradeResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    respone = client.SearchDealerModernTrade(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SearchDealerModernTrade", ex, false);
            }

            return respone;

        }

        public NearestLocationsResponse GetNearestLocations(LoginRequest LoginRequest)
        {
            NearestLocationsResponse response = new NearestLocationsResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetNearestLocations(LoginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetNearestLocations", ex, false);
            }

            return response;
        }

        public BaseResponse AttendanceTracking(AttendanceTrackingRequest request)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.AttendanceTracking(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AttendanceTracking", ex, false);
            }

            return response;
        }

        public GetLocationByIDExternalResponse GetLocationByIDExternal(LoginRequest request)
        {
            GetLocationByIDExternalResponse response = new GetLocationByIDExternalResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetLocationByIDExternal(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetLocationByIDExternal", ex, false);
            }

            return response;
        }

        public DealerAchievementResponse GetMTDealerAchievementReport(DealerAchievementRequest request)
        {
            DealerAchievementResponse response = new DealerAchievementResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetMTDealerAchievementReport(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetMTDealerAchievementReport", ex, false);
            }

            return response;
        }

        public MNPQuarantineStatusResponse IsNumberQuarantined(MNPQuarantineStatusRequest request)
        {
            MNPQuarantineStatusResponse response = new MNPQuarantineStatusResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.IsNumberQuarantined(request);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsNumberQuarantined", ex, false);
            }

            return response;
        }

        public VanityTypesResponse GetVanityTypes(LoginRequest LoginRequest)
        {
            VanityTypesResponse response = new VanityTypesResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetVanityTypes(LoginRequest);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetVanityTypes", ex, false);
            }

            return response;
        }

        public AvailableNumbersResponse GetAvailableNumbers(AvailableNumbersRequest request)
        {
            AvailableNumbersResponse response = new AvailableNumbersResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetAvailableNumbers(request);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetAvailableNumbers", ex, false);
            }

            return response;
        }

        public BookNumberResponse BookNumber(BookNumberRequest request)
        {
            BookNumberResponse response = new BookNumberResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.BookNumber(request);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("BookNumber", ex, false);
            }

            return response;
        }

        public ValidateIDBookingResponse ValidateIDBooking(ValidateIDBookingRequest request)
        {
            ValidateIDBookingResponse response = new ValidateIDBookingResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ValidateIDBooking(request);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateIDBooking", ex, false);
            }

            return response;
        }

        public CancelBookingResponse CancelBookingNumber(CancelBookingNumberRequest request)
        {
            CancelBookingResponse response = new CancelBookingResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.CancelBookingNumber(request);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CancelBooking", ex, false);
            }

            return response;
        }

        public CallResponse CheckSessionValidity(CheckSessionValidityRequest request)
        {
            CallResponse response = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.CheckSessionValidity(request.SessionToken, request.Lang,
                        request.ExtendSession);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckSessionValidity", ex, false);
            }

            return response;
        }

        public IsAttendanceVerificationRequiredResponse IsAttendanceVerificationRequired(LoginRequest LoginRequest)
        {
            IsAttendanceVerificationRequiredResponse response = new IsAttendanceVerificationRequiredResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.IsAttendanceVerificationRequired(LoginRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("IsAttendanceVerificationRequired", ex, false);
            }

            return response;
        }

        public GetSubscriptionTypeResponse GetSubscriptionType(GetSubscriptionTypeRequest request)
        {
            GetSubscriptionTypeResponse response = new GetSubscriptionTypeResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.GetSubscribtionType(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetSubscriptionType", ex, false);
            }

            return response;
        }
    }
}
