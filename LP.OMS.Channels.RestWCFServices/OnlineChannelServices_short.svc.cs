﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using LP.Core.Utilities.Exceptions;
using System.Configuration;
using System.Drawing;
using System.IO;
using LP.Core.Utilities.ServiceModel;
using LP.Core.Utilities.Logging;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Contracts;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;
using LP.OMS.Channels.RestWCFServices.WCFOnlineChannelServices;
using LP.OMS.Channels.RestWCFServices;
using LP.OMS.Channels.Contracts.DealerSImAssociation;

namespace LP.OMS.Channels.WCFServices
{
    public class OnlineChannelServices_short : IOnlineChannelServices
    {

        public CallResponse SendRequest(SubmitRequest oSendRequestObj)
        {
            return new CallResponse() { IsPassed = true, MSISDN = "968961xxxxx" }; ;
        }
        public CallResponse SubmitVirginActivation(VirginActivationRequest oVirginActivationRequest)
        {
            return new CallResponse() { IsPassed = true, MSISDN = "968961xxxxx" }; ;
        }
        public LoginResponse Login(LoginRequest request)
        {
            LoginResponse response = new LoginResponse();
            if (request.Username == "test" && request.Password == "test")
            {
                response.ApplicationApkUrl = "";
                response.ApplicationFormImageQuality = 100;
                response.ApplicationLogExpiryHours = 1;
                response.ConvertActivationDocsToGrayScale = false;
                response.CustomerID = 0;
                response.HasAnyFriendiOperation = false;
                response.HasAnyVirginOperation = true;
                response.hasMinimalAppVersion = true;
                response.iccidLength = 16;
                response.iccidStartWith = "";
                response.IdCopyImageQuality = 100;
                response.imsiLength = 16;
                response.imsiStartWith = "";
                response.IsPassed = true;
                response.IsRequesterFBO = false;
                response.KitIDLength = 10;
                response.KitIDStartWith = "0";
                response.LogApplicationCrashes = false;
                response.MandatoryField = (new string[] { "IdNo", "IDTypeId", "NationalityID", "IDExpiryDate", "VisaIssueDate", "IdCopyDocument", "VisaCopyDocument", "PassportCopyDocument", "ELoad_MSISDN", "ELoad_ID", "ELoad_Amount", "DealerCode", "VanityMSISDN", "VanityBookingCode", "Voucher" }).ToList();
                response.MaxPhysicalFormCollectionBatchSize = 20;
                response.msisdnLength = 10;
                response.msisdnStartWith = "";
                response.NewApplicationVersionExists = false;
                response.NotifyApplicationLogs = false;
                response.OpActivation = 1;
                response.OpActivationReport = -3;
                response.OpCommissionReport = -8;
                response.OpDealerActivation = -2;
                response.OpELoad = -1;
                response.OpFNDIResubmission = -6;
                response.OpOnlyResubmission = 300;
                response.OpPhysicalFormCollection = -9;
                response.OpVirginPostpaidActivation = -5;
                response.OpVirginPrepaidActivation = -4;
                response.OpVirginResubmission = -7;
                response.OpVirginVanityActivation = -10;
                response.ResponseMessage = "";
                response.UserName = "tempUser";
                response.UserPrivilageList = (new int[] { 1, -1, -3, -5, -4, -7, -6, -8, -10 }).ToList();
                response.vaildAge = 1000;
                response.WellcomeMessage = "";
                response.IsPassed = true;
            }
            else
            {

                response.IsPassed = false;
                response.ResponseMessage = "Please verify the entered username or password.";
            }
            return response;
        }

        public LP.OMS.Channels.Contracts.Lookups GetLookups(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest)
        {
            Lookups lookups = new Lookups();

            lookups.Countries = new List<Country>();
            lookups.Countries.Add(new Country() { Code = "001", Description = "Jordan", Id = 1 });
            lookups.Countries.Add(new Country() { Code = "002", Description = "Saudi Arabia", Id = 2 });
            lookups.Countries.Add(new Country() { Code = "003", Description = "UAE", Id = 3 });
            lookups.Countries.Add(new Country() { Code = "004", Description = "Oman", Id = 4 });
            lookups.Countries.Add(new Country() { Code = "005", Description = "Qatar", Id = 5 });
            lookups.Countries.Add(new Country() { Code = "006", Description = "Bahrain", Id = 6 });

            lookups.currentHijriDate = new CurrentHijriDate() { Day = "1", Month = "1", Year = "2015" };

            lookups.DealerActivationIDTypes = new List<IDType>();
            lookups.DealerActivationIDTypes.Add(new IDType() { Code = "S", Description = "OMANI CIVIL ID", Id = 1, ValidityYears = 20 });
            lookups.DealerActivationIDTypes.Add(new IDType() { Code = "I", Description = "OMANI PASSPORT", Id = 2, ValidityYears = 5 });

            lookups.Genders = new List<Gender>();
            lookups.Genders.Add(new Gender() { Code = "M", Id = 1, Name = "Male" });
            lookups.Genders.Add(new Gender() { Code = "F", Id = 2, Name = "Feale" });

            lookups.IDTypes = new List<IDType>();
            lookups.IDTypes.Add(new IDType() { Code = "S", Description = "OMANI CIVIL ID", Id = 1, ValidityYears = 20 });
            lookups.IDTypes.Add(new IDType() { Code = "S", Description = "OMANI PASSPORT", Id = 2, ValidityYears = 5 });
            lookups.IDTypes.Add(new IDType() { Code = "G", Description = "GCC ID", Id = 3, ValidityYears = -5 });
            lookups.IDTypes.Add(new IDType() { Code = "GP", Description = "GCC PASSPORT", Id = 4, ValidityYears = 0 });
            lookups.IDTypes.Add(new IDType() { Code = "V", Description = "OTHER NATIONALITY PASSPORT", Id = 5, ValidityYears = 0 });
            lookups.IDTypes.Add(new IDType() { Code = "V", Description = "DIPLOMATIC ID CARD", Id = 6, ValidityYears = 0 });
            lookups.IDTypes.Add(new IDType() { Code = "S", Description = "OMANI MINISTRY ID", Id = 7, ValidityYears = 0 });
            lookups.IDTypes.Add(new IDType() { Code = "I", Description = "OMANI RESIDENT CARD", Id = 8, ValidityYears = 0 });

            lookups.Titles = new List<Title>();
            lookups.Titles.Add(new Title() { Code = "M", Description = "Mr.", Id = 1 });
            lookups.Titles.Add(new Title() { Code = "M", Description = "Ms.", Id = 2 });

            lookups.WFActivityStatuses = new List<WFActivityStatus>();
            lookups.WFActivityStatuses.Add(new WFActivityStatus() { Code = "", Description = "Pending", Id = 1 });


            return lookups;
        }

        public DealerActivitiesResponseColelction GetDealerActivitiesList(AndroidReportRequest oAndroidReportRequest)
        {
            DealerActivitiesResponseColelction oCallResponse = new DealerActivitiesResponseColelction();
            oCallResponse.DealerActivitiesResponseList = new List<DealerActivitiesResponse>();
            Random objRand = new Random(1000000000);
            for (int i = 0; i < 100; i++)
            {
                oCallResponse.DealerActivitiesResponseList.Add(new DealerActivitiesResponse()
                {
                    ChannelName = "DST",
                    DocumentTypeDescription = "Activation",
                    DocumentTypeId = 1,
                    Imsi = "422000" + objRand.Next().ToString(),
                    IsResubmit = (objRand.Next() % 2 == 0),
                    MSISDN = "9665" + objRand.Next().ToString(),
                    RequestDate = DateTime.Now.AddDays(-1 * (objRand.Next() % 10)).AddMonths(-1 * (objRand.Next() % 10)).AddYears(-1 * (objRand.Next() % 10)).ToString(),
                    StatusForeignName = "Completed",
                    StatusName = "مكتمل"
                });

            }


            return oCallResponse;
        }

        public ResubmissionCallResponse GetFailedRequests(FaildActivationRequest oFaildActivationRequest)
        {
            return new ResubmissionCallResponse();
        }

        public CallResponse ChangePassword(HashingRequest oHashingRequest)
        {
            return new CallResponse() { IsPassed = true };
        }

        public CallResponse ResubmitRequest(ResubmitRequestObj request)
        {
            return new CallResponse() { IsPassed = true };
        }

        public StatusCheckResponse StatusCheck(StatusCheckRequest objRequest)
        {
            return new StatusCheckResponse() { IsPassed = true, IMSIStatus = true, KitStatus = true };

        }

        public EloadResponse Eload(EloadRequest objRequest)
        {
            return new EloadResponse() { IsPassed = true };
        }



        public LP.OMS.Channels.Contracts.ActivationReportCounters GetActivationReportCounters(LoginRequest objLogin)
        {
            LP.OMS.Channels.Contracts.ActivationReportCounters objActivationReportCounters = new ActivationReportCounters();

            objActivationReportCounters.IsPassed = true;
            objActivationReportCounters.LanchToDateActivations = 1500;
            objActivationReportCounters.MonthlyActivations = 600;
            objActivationReportCounters.WeeklyActivations = 150;
            objActivationReportCounters.DailyActivations = 35;

            return objActivationReportCounters;
        }

        public List<LP.OMS.Channels.Contracts.ActivationResponse> GetDealerActivations(Contracts.CommissionDataByDealerRequest objActivationReportRequest)
        {
            List<LP.OMS.Channels.Contracts.ActivationResponse> lstActivations = new List<ActivationResponse>();
            int endPeriod = 35;
            switch (objActivationReportRequest.reportViewMode)
            {

                case ReportViewMode.Weekly:
                    endPeriod = 150;
                    break;
                case ReportViewMode.Monthly:
                    endPeriod = 600;
                    break;
                case ReportViewMode.LanchToDate:
                    endPeriod = 1500;
                    break;
            }
            Random objRand = new Random(1000000000);
            for (int i = 0; i < endPeriod; i++)
            {
                lstActivations.Add(new ActivationResponse()
                {
                    ActivationDate = DateTime.Now.AddDays(-1 * (objRand.Next() % 10)).AddMonths(-1 * (objRand.Next() % 10)).AddYears(-1 * (objRand.Next() % 10)),
                    FirstPayableEventReceived = (objRand.Next() % 2 == 0),
                    Grade = (Grade)(objRand.Next() % 5),
                    IMSI = "422000" + objRand.Next().ToString(),
                    PhysicalFormDelivered = (objRand.Next() % 2 == 0)
                });

            }
            return lstActivations;
        }

        public List<Contracts.CommissionReportResponse> GetCommissionReport(Contracts.CommissionDataByDealerRequest objCommissionDataByDealerRequest)
        {
            List<Contracts.CommissionReportResponse> objResponse = new List<CommissionReportResponse>();
            Random objRand = new Random(1000000000);
            int end = 150;
            switch (objCommissionDataByDealerRequest.reportViewMode)
            {
                case ReportViewMode.Daily:
                    end = 1;
                    break;
                case ReportViewMode.Weekly:
                    end = 7;
                    break;
                case ReportViewMode.Monthly:
                    end = 30;
                    break;

            }
            for (int c = 1; c < 15; c++)
            {
                CommissionReportResponse obj = new CommissionReportResponse()
                {
                    CycleDate = DateTime.Now.AddDays(-1 * (objRand.Next() % 10)).AddMonths(-1 * (objRand.Next() % 10)).AddYears(-1 * (objRand.Next() % 10)),
                    CycleName = "Cycle Name " + c,
                    CycleSchemas = new List<Schema>()
                };
                for (int s = 1; s < 5; s++)
                {
                    Schema objShema = new Schema()
                    {
                        PaymentDate = DateTime.Now.AddDays(-1 * (objRand.Next() % 10)).AddMonths(-1 * (objRand.Next() % 10)).AddYears(-1 * (objRand.Next() % 10)),
                        PaymentStatus = (objRand.Next() % 2 == 0 ? "Paid" : "Not Paid"),
                        SchemaName = "Schema " + c + "_" + s,
                        CommissionElements = new List<CommissionSchemaElement>()
                    };
                    for (int e = 1; e < 20; e++)
                    {
                        CommissionSchemaElement objCommissionSchemaElement = new CommissionSchemaElement()
                        {
                            Category = "",
                            Date = DateTime.Now.AddDays(-1 * (objRand.Next() % 10)).AddMonths(-1 * (objRand.Next() % 10)).AddYears(-1 * (objRand.Next() % 10)),
                            Product = "Product_" + e,
                            Requester = "Requester_" + e,
                            UIdentifier = objRand.Next().ToString(),
                            CommissionDetails = new Dictionary<string, CommissionPayment>()
                        };
                        for (int cd = 1; cd < 10; cd++)
                        {
                            objCommissionSchemaElement.CommissionDetails.Add("Element_" + cd, new CommissionPayment() { CommissionPaidAmount = objRand.Next() % 8 });
                        }
                        objShema.CommissionElements.Add(objCommissionSchemaElement);
                    }

                    obj.CycleSchemas.Add(objShema);
                }
                objResponse.Add(obj);
            }
            return objResponse;
        }

        public LP.OMS.Channels.Contracts.NotifyApplicationLogResponse NotifyLocalApplicationLog(NotifyApplicationLogRequest request)
        {
            return new NotifyApplicationLogResponse() { IsPassed = true };
        }



        public PhysicalFormCheckResponse PhysicalFormCollectionCheck(PhysicalFormCheckRequest request)
        {
            return new PhysicalFormCheckResponse() { IsValid = true };
        }

        public CallResponse PhysicalFormsCollectionSubmit(PhysicalFormCollectionRequest request)
        {
            return new CallResponse() { IsPassed = true };
        }



        public VirginVanityActivationCheckResponse VerifyVirginVanityNumber(VirginVanityActivationCheckRequest objVirginVanityActivationCheckRequest)
        {
            Contracts.VirginVanityActivationCheckResponse objVirginVanityActivationCheckResponse = null;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    objVirginVanityActivationCheckResponse = client.VerifyVirginVanityNumber(objVirginVanityActivationCheckRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerifyVirginVanityNumber", ex, false);
            }
            return objVirginVanityActivationCheckResponse;
        }

        public CallResponse ActivateVirginVanityNumber(VirginVanityActivationActivationRequest objVirginVanityActivationActivationRequest)
        {
            return new CallResponse() { IsPassed = true };
        }


        public DealerBalanceResponse GetDealerBalance(LoginRequest objLoginRequest)
        {
            return new DealerBalanceResponse() { IsPassed = true, Balance = 100.0F };
        }

        public CallResponse VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest)
        {
            return new CallResponse() { IsPassed = true };
        }

        public CallResponse CheckCustomerId(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            throw new NotImplementedException();
        }

        public CheckCustomerSubscriptionsResponse CheckCustomerSubscriptions(CheckCustomerIdRquest objCheckCustomerIdRquest)
        {
            throw new NotImplementedException();
        }

        VerifyCustomerResponse IOnlineChannelServices.VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest)
        {
            throw new NotImplementedException();
        }
        public CheckVirginCustomerIdResponse CheckVirginCustomerId(CheckVirginCustomerIdRequest objCheckVirginCustomerIdRquest)
        {
            throw new NotImplementedException();
        }

        public CallResponse LogFingerprintNFIQTrials(LogFingerprintNFIQTrialsRequest logNFIQTrialsRequest)
        {
            throw new NotImplementedException();
        }

        public CallResponse VerifyOwnershipChange(OwnerChangeRequest request)
        {
            throw new NotImplementedException();
        }

        public CallResponse SubmitOwnershipChange(OwnerChangeRequest request)
        {
            throw new NotImplementedException();
        }

        public CallResponse TestVerifyOwnershipChange(OwnerChangeRequest request)
        {
            throw new NotImplementedException();
        }

        public CallResponse TestSubmitOwnershipChange(OwnerChangeRequest request)
        {
            throw new NotImplementedException();
        }

        public CallResponse ReplaceSIM(SIMReplacementRequest Request)
        {
            var response = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ReplaceSIM(Request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ReplaceSIM", ex, false);
                response.IsPassed = false;
            }
            return response;
        }

        public CallResponse ValidateReplaceSIM(SIMReplacementRequest Request)
        {
            var response = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    response = client.ValidateReplaceSIM(Request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ValidateReplaceSIM", ex, false);
                response.IsPassed = false;
            }
            return response;
        }

        public CallResponse AddStockRequest(StockItemRequest sRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.AddStockRequest(sRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("AddStockRequest", ex, false);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public void LogError(DSTErrorLog dstErrorLog)
        {
            throw new NotImplementedException();
        }

        public BundleResponse GetBundle(BundlesRequest request)
        {
            BundleResponse oBundleResponse = new BundleResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oBundleResponse = client.GetBundle(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetBundle", ex, false);
                oBundleResponse.IsPassed = false;
                oBundleResponse.ResponseMessage = ex.Message;
            }
            return oBundleResponse;
        }

        public CallResponse SubmitBundle(SubmitBundleRequest SubmitBundleRequest)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.SubmitBundle(SubmitBundleRequest);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SubmitBundle", ex, false);
                oCallResponse.IsPassed = false;
                oCallResponse.ResponseMessage = ex.Message;
            }
            return oCallResponse;
        }

        public int SelfcareResubmitRequest(SelfCareResubmitObj selfCareResubmitObj)
        {
            int ResponseCode = (int)ResubmissionResponseCodes.UNKNOWN_ERROR;
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    ResponseCode = client.SelfcareResubmitRequest(selfCareResubmitObj.MSISDN, selfCareResubmitObj.IDNumber, selfCareResubmitObj.Brand, selfCareResubmitObj.DocumentType, selfCareResubmitObj.AdditionalInfo, selfCareResubmitObj.LstResubmissionDocuments);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("SelfcareResubmitRequest", ex, false);
            }
            return ResponseCode;
        }

        public AvailableProductsResponse GetAvailableProducts(ProductSellingRequest request)
        {
            AvailableProductsResponse oAvailableResponse = new AvailableProductsResponse();

            using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
            {
                oAvailableResponse = client.GetAvailableProducts(request);
            }

            return oAvailableResponse;
        }

        public ProductSellingResponse SubmitProductSelling(ProductSellingRequest request)
        {
            ProductSellingResponse ProductSellingResponse = new ProductSellingResponse();

            using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
            {
                ProductSellingResponse = client.SubmitProductSelling(request);
            }

            return ProductSellingResponse;
        }

        public CallResponse CheckProductSellingEligibility(CheckProductSellingEligibilityRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.CheckProductSellingEligibility(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("CheckProductSellingEligibility", ex, false);
            }
            return oCallResponse;
        }

        public CallResponse VerfiyLoginCode(VerfiyLoginCode request)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.VerfiyLoginCode(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("VerfiyLoginCode", ex, false);
            }
            return oCallResponse;
        }

        public CallResponse ResendLoginCode(LoginRequest request)
        {
            CallResponse oCallResponse = new CallResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    oCallResponse = client.ResendLoginCode(request);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ResendLoginCode", ex, false);
            }
            return oCallResponse;
        }

        public NextSessionCheckTimeResponse GetNextSessionCheckTime(LoginRequest loginRequest)
        {
            var response = new NextSessionCheckTimeResponse();
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    var request = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest;
                    string sessionToken = request.Headers["SessionToken"];
                    response = client.GetNextSessionCheckTime(loginRequest,sessionToken);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("GetNextSessionCheckTime", ex, false);
            }

            return response;
        }

        public ExtendSessionRequest ExtendSession(LoginRequest loginRequest)
        {
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    var request = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest;
                    string sessionToken = request.Headers["SessionToken"];
                    return client.ExtendSession(loginRequest,sessionToken);
                }
            }
            catch (LPBizException lpBizException)
            {
                lpBizException.PuplishFault();
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("ExtendSession", ex,  true);
            }
            ExtendSessionRequest oExtendSessionRequest = new ExtendSessionRequest() ;
            return oExtendSessionRequest;
        }
        //public CallResponse PasswordResetGetVerificationCode(PasswordResetGetVerificationCodeRequest request)
        //{
        //    CallResponse oCallResponse = new CallResponse();
        //    try
        //    {
        //        using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
        //        {
        //            oCallResponse = client.PasswordResetGetVerificationCode(request);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("ResetPassword", ex, false);
        //        oCallResponse.IsPassed = false;
        //        oCallResponse.ResponseMessage = ex.Message;
        //    }
        //    return oCallResponse;
        //}

        //public CheckVerificationResetPasswordResponse CheckVerificationResetPassword(CheckVerificationResetPasswordRequest request)
        //{
        //    CheckVerificationResetPasswordResponse response = new CheckVerificationResetPasswordResponse();
        //    try
        //    {
        //        using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
        //        {
        //            response = client.CheckVerificationResetPassword(request);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.HandleException("CheckVerificationResetPassword", ex, false);
        //        response.oCallResponse.IsPassed = false;
        //        response.oCallResponse.ResponseMessage = ex.Message;
        //    }
        //    return response;

        //}


        public CallResponse Logout(LoginRequest request)
        {
            throw new NotImplementedException();
        }

        public SematiLoginCallResponse LoginToSemati(LoginRequest request)
        {
            throw new NotImplementedException();
        }

        public CallResponse CheckSematiAuthentication(CheckSematiAuthenticationObject checkSematiAuthenticationObject)
        {
            throw new NotImplementedException();
        }

        public OneSignalResponse GetOneSignalData(LoginRequest loginRequest)
        {
            throw new NotImplementedException();
        }

        public GetAllowedIDTypesByIMSIResponse GetAllowedIDTypesByIMSI(GetAllowedIDTypesByIMSIObj getAllowedIDTypesByIMSIObj)
        {
            throw new NotImplementedException();
        }

        public GetSubscriptionTypesResponse GetSubscriptionTypes(GetSubscriptionTypeObj getSubscriptionObj)
        {
            throw new NotImplementedException();
        }

        public List<SliderImageResponse> GetSliderImages(SliderImageRequest sliderImageRequest)
        {
            throw new NotImplementedException();
        }

        public int GetTotalActivation(LoginRequest loginRequest)
        {
            throw new NotImplementedException();
        }

        public double GetTotalCommission(LoginRequest loginRequest)
        {
            throw new NotImplementedException();
        }

        public AvailableDealerBalanceResponse GetAvailableDealerBalance(LoginRequest loginRequest)
        {
            throw new NotImplementedException();
        }

        public AvailableProductsResponse GetAvailableProductsBySubscriptionType(ProductSellingBySubscriptionTypeRequest request)
        {
            throw new NotImplementedException();
        }

        public GetStocksResponse GetStocks(GetStockRequest getStockRequest)
        {
            throw new NotImplementedException();
        }

        public SendRequestStockResponse SendRequestStock(SendRequestStockRequest stockRequest)
        {
            throw new NotImplementedException();
        }

        public List<AvailableRatePlan> GetAvailableVanityRatePlans(VanityRatePlansRequest vanityRatePlansRequest)
        {
            throw new NotImplementedException();
        }

        public double GetRemainingSessionInSeconds(LoginRequest loginRequest)
        {
            throw new NotImplementedException();
        }

        public List<PossibleFailureReason> GetPossibleFailureReasons(PossibleFailureReasonRequest possibleFailureReasonRequest)
        {
            throw new NotImplementedException();
        }

        public bool SubmitDealerFailureReasonAndNote(DealerFailureReasonNote failureReason)
        {
            throw new NotImplementedException();
        }

        public CustomerPlanDetailsResponse GetCustomerDetails(GetCustomerDetailsRequest orequest)
        {
            throw new NotImplementedException();
        }

        public ActivationGradeDetailsReportResponse GetActivationGradeDetailsReport(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public ActivationRechargeDetailsReportResponse GetActivationRechargeAndFPEDetailsReport(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public CommissionPaymentDetailsReportResponse GetCommissionPaymentDetails(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public ActivationRechargeDetailsReportResponse GetActivationRechargeDetailsReport(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public ActivationFPEDetailsReportResponse GetActivationFPEDetailsReport(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public ActivationReportSummary GetActivationReportSummary(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public CommissionReportSummary GetCommissionReportSummary(ActivationCommissionReportRequest request)
        {
            throw new NotImplementedException();
        }

        public CancelMNPPortInResponse CancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            throw new NotImplementedException();
        }

        public List<OperatorDetails> GetTelecomOperators(LoginRequest request)
        {
            throw new NotImplementedException();
        }

        public CancelMNPPortInResponse ValidateCancelMNPPortInRequest(CancelMNPPortInRequest request)
        {
            throw new NotImplementedException();
        }

        public List<decimal> GetTopupAmounts(LoginRequest request)
        {
            throw new NotImplementedException();
        }

        public ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request)
        {
            throw new NotImplementedException();
        }

        public CancelFamilyRegistrationResponse CancelFamilyRegistration(CancelFamilyRegistrationRequest request)
        {
            throw new NotImplementedException();
        }

        public BaseResponse AssociateScannedSim(DealerSimAssociationRequest request)
        {
            throw new NotImplementedException();
        }

        public PagedResult<ShopAssociatedSIMs> GetAssociatedShops(ShopAssociatedSIMsRequest request)
        {
            throw new NotImplementedException();
        }

        public RefillDealerEWalletResponse RefillDealerEWallet(RefillDealerEWalletRequest request)
        {
            throw new NotImplementedException();
        }

        public List<NumberInfo> GetAllNumbersRealtedByNumberId(string idNumber)
        {
            throw new NotImplementedException();
        }

        public OTPResponse SendOTP(OTPRequest OTPRequest)
        {
            throw new NotImplementedException();
        }

        public OTPResponse ValidateOTP(ValidateOTPRequest validateOTPRequest)
        {
            throw new NotImplementedException();
        }

        public SendEsimQRImageResponse SendEsimQRImage(SendEsimQRImageRequest request)
        {
            throw new NotImplementedException();
        }

        public TerminationResponse SIMTermination(TerminationRequest TerminationRequest)
        {
            throw new NotImplementedException();
        }

        public CustomerActivePlanDetailsResponse GetCustomerSubscriptionInfo(CustomerPlanRequest CustomerPlanRequest)
        {
            throw new NotImplementedException();
        }

        public VerifyChangeSubscriptionTypeResponse VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest VerifyChangeSubscriptionTypeRequest)
        {
            throw new NotImplementedException();
        }

        public ChangeSubscriptionTypeResponse ChangeSubscriptionType(ChangeSubscriptionTypeRequest request)
        {
            throw new NotImplementedException();
        }

        public ModernTradeResponse SearchDealerModernTrade(ModernTradeRequest request)
        {
            throw new NotImplementedException();
        }

        public NearestLocationsResponse GetNearestLocations(LoginRequest LoginRequest)
        {
            throw new NotImplementedException();
        }

        public BaseResponse AttendanceTracking(AttendanceTrackingRequest request)
        {
            throw new NotImplementedException();
        }
        public GetLocationByIDExternalResponse GetLocationByIDExternal(LoginRequest request)
        {
            throw new NotImplementedException();
        }

        public DealerAchievementResponse GetMTDealerAchievementReport(DealerAchievementRequest request)
        {
            throw new NotImplementedException();
        }
        public MNPQuarantineStatusResponse IsNumberQuarantined(MNPQuarantineStatusRequest request)
        {
            throw new NotImplementedException();
        }

        public VanityTypesResponse GetVanityTypes(LoginRequest LoginRequest)
        {
            throw new NotImplementedException();
        }

        public AvailableNumbersResponse GetAvailableNumbers(AvailableNumbersRequest request)
        {
            throw new NotImplementedException();
        }

        public BookNumberResponse BookNumber(BookNumberRequest request)
        {
            throw new NotImplementedException();
        }

        public ValidateIDBookingResponse ValidateIDBooking(ValidateIDBookingRequest request)
        {
            throw new NotImplementedException();
        }

        public CancelBookingResponse CancelBookingNumber(CancelBookingNumberRequest request)
        {
            throw new NotImplementedException();
        }

        public CallResponse CheckSessionValidity(CheckSessionValidityRequest request)
        {
            throw new NotImplementedException();
        }

        public IsAttendanceVerificationRequiredResponse IsAttendanceVerificationRequired(LoginRequest request)
        {
            throw new NotImplementedException();
        }

        public GetSubscriptionTypeResponse GetSubscriptionType(GetSubscriptionTypeRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
