﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.Enums;
using LP.OMS.Channels.RestWCFServices;
using LP.OMS.Channels.RestWCFServices.DealerHiringServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace LP.OMS.Channels.RestWCFServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestDealerHiringServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RestDealerHiringServices.svc or RestDealerHiringServices.svc.cs at the Solution Explorer and start debugging.
    [GeneralInspector]
    public class RestDealerHiringServices : IRestDealerHiringServices
    {

        public DealerHiringLoginResponse Login(DHLoginRequest request)
        {
            DealerHiringLoginResponse response = new DealerHiringLoginResponse();

            try
            {
                DealerHiringServicesClient client = new DealerHiringServicesClient();
                // using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                // {
                response = client.Login(request);

                bool success = false;

                try
                {
                    if (client.State != CommunicationState.Faulted)
                    {
                        client.Close();
                        success = true;
                    }
                }
                finally
                {
                    if (!success)
                    {
                        client.Abort();
                    }
                }


                //  }
                if (request.language == 2)
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ar-JO");
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.HandleException("Login", ex, false);
                response.Status = false;
                Assembly a = Assembly.GetExecutingAssembly();

                ResourceManager rm = new ResourceManager("LP.OMS.Channels.RestWCFServices.Resources.OMSResources", a);

                response.Message = rm.GetString("MSG_General_Execption", Thread.CurrentThread.CurrentCulture);
                //if (request.language == 2)
                //{

                //    Assembly a = Assembly.GetExecutingAssembly();
                //    ResourceManager rm = new ResourceManager("LP.OMS.Channels.Engine.Resources", a);
                //    response.ResponseMessage = rm.GetString("MSG_General_Execption");//"هناك خطأ اثناء تنفيذ العملية";
                //}
                //else
                //    response.ResponseMessage = "Error While executing activities";
                return response;
            }

            return response;
        }
        public DealerCreationResponse AddDealerCreationRequest(DealerCreationContractRequest request)
        {
            DealerCreationResponse response = new DealerCreationResponse();

            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.AddDealerCreationRequest(request);
            }

            return response;
        }

        public LookupsList GetLookups(LookupsRequest request)
        {
            LookupsList response = new LookupsList();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetLookups(request);
            }
            return response;
        }

        public MyRequestsResponse GetMyRequests(MyRequestsRequest request)
        {
            MyRequestsResponse response = new MyRequestsResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetMyRequests(request);
            }
            return response;
        }

        public CreationDetailsResponse GetCreationRequestDetails(CreationDetailsRequest request)
        {
            CreationDetailsResponse response = new CreationDetailsResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetCreationRequestDetails(request);
            }
            return response;
        }

        public UpdatingDetailsResponse GetUpdatingRequestDetails(UpdatingDetailsRequest request)
        {
            UpdatingDetailsResponse response = new UpdatingDetailsResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetUpdatingRequestDetails(request);
            }
            return response;
        }

        public DeActivateRequestResponse DeActivateRequest(DeActivateRequest request)
        {
            DeActivateRequestResponse response = new DeActivateRequestResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.DeActivateRequest(request);
            }
            return response;
        }

        public ReActivateResponse ReActivateRequest(ReActivateRequest request)
        {
            ReActivateResponse response = new ReActivateResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.ReActivateRequest(request);
            }
            return response;
        }

        public SearchDealerResponse SearchDealerRequest(SearchDealerRequest request)
        {
            SearchDealerResponse response = new SearchDealerResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.SearchDealerRequest(request);
            }
            return response;
        }

        public SearchDealerResponse SearchDealerRequestNew(SearchDealerRequest request)
        {
            SearchDealerResponse response = new SearchDealerResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.SearchDealerRequestNew(request);
            }
            return response;
        }

        public ResetPasswordResponse ResetPassword(DHResetPasswordRequest request)
        {
            ResetPasswordResponse response = new ResetPasswordResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.ResetPassword(request);
            }
            return response;
        }

        public DiscardResponse DiscardRequest(DiscardRequest request)
        {
            DiscardResponse response = new DiscardResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.DiscardRequest(request);
            }
            return response;
        }

        public DHBaseResponse UpdateSaudiID(UpdateSaudiIDRequest request)
        {
            DHBaseResponse response = new DHBaseResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.UpdateSaudiID(request);
            }

            return response;
        }

        public DHBaseResponse UpdateOTPMSISDN(UpdateOTPMSISDNRequest request)
        {
            DHBaseResponse response = new DHBaseResponse();
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.UpdateOTPMSISDN(request);
            }

            return response;
        }

        public DescendantActivationsResponse GetDescendantActivations(LoginRequest request)
        {
            DescendantActivationsResponse response = new DescendantActivationsResponse()
            {
                Result = new List<DescendantActivations>()
            };
            //response.Result.Add(new DescendantActivations() { DescendantCode ="123",TotalLastMonthActivations=120,TotalYesterdayActivations=23,TotalTodayActivations=12});
            //response.Result.Add(new DescendantActivations() { DescendantCode = "234", TotalLastMonthActivations = 153, TotalYesterdayActivations = 15, TotalTodayActivations = 5 });
            //response.Result.Add(new DescendantActivations() { DescendantCode = "456", TotalLastMonthActivations = 156, TotalYesterdayActivations = 19, TotalTodayActivations = 7 });
            //response.Status = true;
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetDescendantActivations(request);
            }

            return response;
        }

        public DescendantCommissionsResponse GetDescendantCommissions(LoginRequest request)
        {
            DescendantCommissionsResponse response = new DescendantCommissionsResponse()
            {
                Result = new List<DescendantCommissions>()
            };
            //response.Result.Add(new DescendantCommissions() { DescendantCode = "123", TotalLastMonthCommissions = 120, TotalYesterdayCommissions = 23, TotalTodayCommissions = 12 });
            //response.Result.Add(new DescendantCommissions() { DescendantCode = "234", TotalLastMonthCommissions = 153, TotalYesterdayCommissions = 15, TotalTodayCommissions = 5 });
            //response.Result.Add(new DescendantCommissions() { DescendantCode = "456", TotalLastMonthCommissions = 156, TotalYesterdayCommissions = 19, TotalTodayCommissions = 7 });
            //response.Status = true;
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetDescendantCommissions(request);
            }

            return response;
        }

        public ActivationSummaryReportResponse GetActivationSummaryReport(ActivationSummaryReportRequest request)
        {
            ActivationSummaryReportResponse response = new ActivationSummaryReportResponse()
            {
                Result = new ActivationSummaryReport()
            };
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetActivationSummaryReport(request.oLoginRequest, request.DealerCode, request.DateSearchCriteria);
            }

            return response;
        }

        public RechargeSummaryReportResponse GetRechargeSummaryReport(RechargeSummaryReportRequest request)
        {
            RechargeSummaryReportResponse response = new RechargeSummaryReportResponse()
            {
                Result = new RechargeSummaryReport()
            };
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetRechargeSummaryReport(request.oLoginRequest, request.DealerCode, request.DateSearchCriteria);
            }

            return response;
        }

        public CommissionsDetailsReportResponse GetCommissionsDetailsReports(CommissionsDetailsReportRequest request)
        {
            CommissionsDetailsReportResponse response = new CommissionsDetailsReportResponse()
            {
                Result = new CommissionsDetailsReport()
                {
                    CommissionsDetailsDataItems = new List<CommissionsDetailsDataItem>(),
                    Headers = new List<string>()
                }
            };
            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.GetCommissionsDetailsReports(request.oLoginRequest, request.DealerCode, request.GeneralSearchCriteria, request.DateSearchCriteria);
            }

            return response;
        }

        public AddLocationResponse AddLocation(AddLocationRequest request)
        {
            AddLocationResponse response = new AddLocationResponse();

            using (DealerHiringServicesClient client = new DealerHiringServicesClient())
            {
                response = client.AddLocation(request);
            }

            return response;
        }
    }
}
