using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Contracts.DealerSImAssociation;

namespace LP.OMS.Channels.WCFServices
{
    [ServiceContract]
    public interface IOnlineChannelServices
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "GetCustomerDetails")]
        CustomerPlanDetailsResponse GetCustomerDetails(GetCustomerDetailsRequest orequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SendRequest")]
        LP.OMS.Channels.Contracts.CallResponse SendRequest(LP.OMS.Channels.Contracts.SubmitRequest oSendRequestObj);
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SubmitVirginActivation")]
        LP.OMS.Channels.Contracts.CallResponse SubmitVirginActivation(LP.OMS.Channels.Contracts.VirginActivationRequest oVirginActivationRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetLookups")]
        LP.OMS.Channels.Contracts.Lookups GetLookups(LP.OMS.Channels.Contracts.LoginRequest oLoginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "Login")]
        LP.OMS.Channels.Contracts.LoginResponse Login(LP.OMS.Channels.Contracts.LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetDealerActivitiesList")]
        LP.OMS.Channels.Contracts.DealerActivitiesResponseColelction GetDealerActivitiesList(LP.OMS.Channels.Contracts.AndroidReportRequest oAndroidReportRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetFailedRequests")]
        LP.OMS.Channels.Contracts.ResubmissionCallResponse GetFailedRequests(LP.OMS.Channels.Contracts.FaildActivationRequest oFaildActivationRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "ChangePAssword")]
        LP.OMS.Channels.Contracts.CallResponse ChangePassword(LP.OMS.Channels.Contracts.HashingRequest oHashingRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "ResubmitRequest")]
        LP.OMS.Channels.Contracts.CallResponse ResubmitRequest(LP.OMS.Channels.Contracts.ResubmitRequestObj request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "StatusCheck")]
        LP.OMS.Channels.Contracts.StatusCheckResponse StatusCheck(LP.OMS.Channels.Contracts.StatusCheckRequest objRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "Eload")]
        LP.OMS.Channels.Contracts.EloadResponse Eload(LP.OMS.Channels.Contracts.EloadRequest objRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetActivationReportCounters")]
        LP.OMS.Channels.Contracts.ActivationReportCounters GetActivationReportCounters(LoginRequest objLogin);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetDealerActivations")]
        List<LP.OMS.Channels.Contracts.ActivationResponse> GetDealerActivations(LP.OMS.Channels.Contracts.CommissionDataByDealerRequest objActivationReportRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetCommissionReport")]
        List<LP.OMS.Channels.Contracts.CommissionReportResponse> GetCommissionReport(LP.OMS.Channels.Contracts.CommissionDataByDealerRequest objCommissionDataByDealerRequest);
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "NotifyLocalApplicationLog")]
        LP.OMS.Channels.Contracts.NotifyApplicationLogResponse NotifyLocalApplicationLog(NotifyApplicationLogRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PhysicalFormCollectionCheck")]
        LP.OMS.Channels.Contracts.PhysicalFormCheckResponse PhysicalFormCollectionCheck(LP.OMS.Channels.Contracts.PhysicalFormCheckRequest request);
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PhysicalFormsCollectionSubmit")]
        LP.OMS.Channels.Contracts.CallResponse PhysicalFormsCollectionSubmit(LP.OMS.Channels.Contracts.PhysicalFormCollectionRequest request);
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "VerifyVirginVanityNumber")]
        VirginVanityActivationCheckResponse VerifyVirginVanityNumber(VirginVanityActivationCheckRequest objVirginVanityActivationCheckRequest);
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ActivateVirginVanityNumber")]
        CallResponse ActivateVirginVanityNumber(VirginVanityActivationActivationRequest objVirginVanityActivationActivationRequest);

        [WebInvoke(Method = "POST",
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "GetDealerBalance")]
        DealerBalanceResponse GetDealerBalance(LoginRequest objLoginRequest);



        [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "CheckCustomerId")]
        CallResponse CheckCustomerId(CheckCustomerIdRquest objCheckCustomerIdRquest);

        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "CheckCustomerSubscriptions")]
        CheckCustomerSubscriptionsResponse CheckCustomerSubscriptions(CheckCustomerIdRquest objCheckCustomerIdRquest);
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "VerifyCustomerSubscriptions")]
        VerifyCustomerResponse VerifyCustomerSubscriptions(VerifyCustomerRequest objVerifyCustomerRequest);
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "CheckVirginCustomerId")]
        CheckVirginCustomerIdResponse CheckVirginCustomerId(CheckVirginCustomerIdRequest objCheckVirginCustomerIdRquest);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "LogFingerprintNFIQTrials")]
        CallResponse LogFingerprintNFIQTrials(LogFingerprintNFIQTrialsRequest logNFIQTrialsRequest);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "VerifyOwnershipChange")]
        CallResponse VerifyOwnershipChange(OwnerChangeRequest request);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SubmitOwnershipChange")]
        CallResponse SubmitOwnershipChange(OwnerChangeRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "AddStockRequest")]
        CallResponse AddStockRequest(LP.OMS.Channels.Contracts.StockItemRequest sRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "GetBundle")]
        BundleResponse GetBundle(BundlesRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "SubmitBundle")]
        CallResponse SubmitBundle(SubmitBundleRequest SubmitBundleRequest);

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "LogError")]
        void LogError(DSTErrorLog dstErrorLog);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "ReplaceSIM")]
        CallResponse ReplaceSIM(SIMReplacementRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "ValidateReplaceSIM")]
        CallResponse ValidateReplaceSIM(SIMReplacementRequest Request);

        [OperationContract]
        [WebInvoke(Method = "POST",
              RequestFormat = WebMessageFormat.Json,
              ResponseFormat = WebMessageFormat.Json,
              UriTemplate = "SelfcareResubmitRequest")]
        int SelfcareResubmitRequest(SelfCareResubmitObj selfCareResubmitObj);

        [OperationContract]
        [WebInvoke(Method = "POST",
             RequestFormat = WebMessageFormat.Json,
             ResponseFormat = WebMessageFormat.Json,
             UriTemplate = "GetAvailableProducts")]
        AvailableProductsResponse GetAvailableProducts(ProductSellingRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SubmitProductSelling")]
        ProductSellingResponse SubmitProductSelling(ProductSellingRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "CheckProductSellingEligibility")]
        CallResponse CheckProductSellingEligibility(CheckProductSellingEligibilityRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "VerfiyLoginCode")]
        CallResponse VerfiyLoginCode(VerfiyLoginCode request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "ResendLoginCode")]
        CallResponse ResendLoginCode(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "Logout")]
        CallResponse Logout(LoginRequest request);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //UriTemplate = "PasswordResetGetVerificationCode")]
        //CallResponse PasswordResetGetVerificationCode(PasswordResetGetVerificationCodeRequest request);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //UriTemplate = "CheckVerificationResetPassword")]
        //CheckVerificationResetPasswordResponse CheckVerificationResetPassword(CheckVerificationResetPasswordRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "LoginToSemati")]
        SematiLoginCallResponse LoginToSemati(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "CheckSematiAuthentication")]
        CallResponse CheckSematiAuthentication(CheckSematiAuthenticationObject checkSematiAuthenticationObject);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetOneSignalData")]
        OneSignalResponse GetOneSignalData(LoginRequest loginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "GetNextSessionCheckTime")]
        NextSessionCheckTimeResponse GetNextSessionCheckTime(LoginRequest loginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "ExtendSession")]
        ExtendSessionRequest ExtendSession(LoginRequest loginRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
              RequestFormat = WebMessageFormat.Json,
              ResponseFormat = WebMessageFormat.Json,
              UriTemplate = "GetAllowedIDTypesByIMSI")]
        GetAllowedIDTypesByIMSIResponse GetAllowedIDTypesByIMSI(GetAllowedIDTypesByIMSIObj getAllowedIDTypesByIMSIObj);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetSubscriptionTypes")]
        GetSubscriptionTypesResponse GetSubscriptionTypes(GetSubscriptionTypeObj getSubscriptionObj);

        [OperationContract]
        [WebInvoke(Method = "POST",
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetSliderImages")]
        List<SliderImageResponse> GetSliderImages(SliderImageRequest sliderImageRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "GetTotalActivation")]
        int GetTotalActivation(LoginRequest loginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "GetTotalCommission")]
        double GetTotalCommission(LoginRequest loginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "GetAvailableDealerBalance")]
        AvailableDealerBalanceResponse GetAvailableDealerBalance(LoginRequest loginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetAvailableProductsBySubscriptionType")]
        AvailableProductsResponse GetAvailableProductsBySubscriptionType(ProductSellingBySubscriptionTypeRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
     RequestFormat = WebMessageFormat.Json,
     ResponseFormat = WebMessageFormat.Json,
     UriTemplate = "GetStocks")]
        GetStocksResponse GetStocks(GetStockRequest getStockRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "SendRequestStock")]
        SendRequestStockResponse SendRequestStock(SendRequestStockRequest stockRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetAvailableVanityRatePlans")]
        List<AvailableRatePlan> GetAvailableVanityRatePlans(VanityRatePlansRequest vanityRatePlansRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetRemainingSessionInSeconds")]
        double GetRemainingSessionInSeconds(LoginRequest loginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetPossibleFailureReasons")]
        List<PossibleFailureReason> GetPossibleFailureReasons(PossibleFailureReasonRequest possibleFailureReasonRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "SubmitDealerFailureReasonAndNote")]
        bool SubmitDealerFailureReasonAndNote(DealerFailureReasonNote failureReason);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetActivationGradeDetailsReport")]
        ActivationGradeDetailsReportResponse GetActivationGradeDetailsReport(ActivationCommissionReportRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetActivationRechargeDetailsReport")]
        ActivationRechargeDetailsReportResponse GetActivationRechargeDetailsReport(ActivationCommissionReportRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetActivationFPEDetailsReport")]
        ActivationFPEDetailsReportResponse GetActivationFPEDetailsReport(ActivationCommissionReportRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetCommissionPaymentDetails")]
        CommissionPaymentDetailsReportResponse GetCommissionPaymentDetails(ActivationCommissionReportRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetActivationReportSummary")]
        ActivationReportSummary GetActivationReportSummary(ActivationCommissionReportRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetCommissionReportSummary")]
        CommissionReportSummary GetCommissionReportSummary(ActivationCommissionReportRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "CancelMNPPortInRequest")]
        CancelMNPPortInResponse CancelMNPPortInRequest(CancelMNPPortInRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "ValidateCancelMNPPortInRequest")]
        CancelMNPPortInResponse ValidateCancelMNPPortInRequest(CancelMNPPortInRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetTelecomOperators")]
        List<OperatorDetails> GetTelecomOperators(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetTopupAmounts")]
        List<decimal> GetTopupAmounts(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "ValidateAndRegisterFamily")]
        ValidateAndRegisterFamilyResponse ValidateAndRegisterFamily(ValidateAndRegisterFamilyRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "CancelFamilyRegistration")]
        CancelFamilyRegistrationResponse CancelFamilyRegistration(CancelFamilyRegistrationRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "AssociateScannedSim")]
        BaseResponse AssociateScannedSim(DealerSimAssociationRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "GetAssociatedShops")]
        PagedResult<ShopAssociatedSIMs> GetAssociatedShops(ShopAssociatedSIMsRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
   RequestFormat = WebMessageFormat.Json,
   ResponseFormat = WebMessageFormat.Json,
   UriTemplate = "RefillDealerEWallet")]
        RefillDealerEWalletResponse RefillDealerEWallet(RefillDealerEWalletRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetAllNumbersRealtedByNumberId")]
        List<NumberInfo> GetAllNumbersRealtedByNumberId(string idNumber);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "SendOTP")]
        OTPResponse SendOTP(OTPRequest OTPRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "ValidateOTP")]
        OTPResponse ValidateOTP(ValidateOTPRequest validateOTPRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "SendEsimQRImage")]
        SendEsimQRImageResponse SendEsimQRImage(SendEsimQRImageRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "SIMTermination")]
        TerminationResponse SIMTermination(TerminationRequest TerminationRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetCustomerSubscriptionInfo")]
        CustomerActivePlanDetailsResponse GetCustomerSubscriptionInfo(CustomerPlanRequest CustomerPlanRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "VerfityChangeSubscriptionType")]
        VerifyChangeSubscriptionTypeResponse VerfityChangeSubscriptionType(VerifyChangeSubscriptionTypeRequest VerifyChangeSubscriptionTypeRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "ChangeSubscriptionType")]
        ChangeSubscriptionTypeResponse ChangeSubscriptionType(ChangeSubscriptionTypeRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "SearchDealerModernTrade")]
        ModernTradeResponse SearchDealerModernTrade(ModernTradeRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetNearestLocations")]
        NearestLocationsResponse GetNearestLocations(LoginRequest LoginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "AttendanceTracking")]
        BaseResponse AttendanceTracking(AttendanceTrackingRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetLocationByIDExternal")]
        GetLocationByIDExternalResponse GetLocationByIDExternal(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetMTDealerAchievementReport")]
        DealerAchievementResponse GetMTDealerAchievementReport(DealerAchievementRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "IsNumberQuarantined")]
        MNPQuarantineStatusResponse IsNumberQuarantined(MNPQuarantineStatusRequest request);



        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetVanityTypes")]
        VanityTypesResponse GetVanityTypes(LoginRequest LoginRequest);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetAvailableNumbers")]
        AvailableNumbersResponse GetAvailableNumbers(AvailableNumbersRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "BookNumber")]
        BookNumberResponse BookNumber(BookNumberRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "ValidateIDBooking")]
        ValidateIDBookingResponse ValidateIDBooking(ValidateIDBookingRequest request);


        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "CancelBooking")]
        CancelBookingResponse CancelBookingNumber(CancelBookingNumberRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "CheckSessionValidity")]
        CallResponse CheckSessionValidity(CheckSessionValidityRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "IsAttendanceVerificationRequired")]
        IsAttendanceVerificationRequiredResponse IsAttendanceVerificationRequired(LoginRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST",
RequestFormat = WebMessageFormat.Json,
ResponseFormat = WebMessageFormat.Json,
UriTemplate = "GetSubscriptionType")]
        GetSubscriptionTypeResponse GetSubscriptionType(GetSubscriptionTypeRequest request);

    }
}
