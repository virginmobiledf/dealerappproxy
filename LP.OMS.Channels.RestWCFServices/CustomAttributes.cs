﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;
using Newtonsoft.Json;
using LP.OMS.Channels.RestWCFServices.WCFOnlineChannelServices;
using LP.OMS.Channels.Contracts;
using System.Threading;

namespace LP.OMS.Channels.RestWCFServices
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GeneralInspectorAttribute : Attribute, IParameterInspector, IServiceBehavior
    {
        private static readonly string securityHeaderName = ConfigurationManager.AppSettings["SecurityHeaderName"];
        private static readonly string appSecurityKey = ConfigurationManager.AppSettings["AppSecurityKey"];

        public object BeforeCall(string operationName, object[] inputs)
        {
            //Check Basic Security            
            var request = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest;
            var headerVal = request.Headers[securityHeaderName];
            if (!string.IsNullOrWhiteSpace(headerVal) && headerVal == appSecurityKey)
            {
                //Authenticated
                if (operationName != "Login" && operationName != "Logout" 
                    && operationName != "SelfcareResubmitRequest"
                    && operationName != "CheckSessionValidity")
                {
                    try
                    {
                        using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                        {
                            CallResponse oCallResponse = client.CheckSessionValidity(request.Headers["SessionToken"], int.Parse(request.Headers["Lang"]), (operationName != "GetNextSessionCheckTime" && operationName != "GetRemainingSessionInSeconds"));

                            System.ServiceModel.Web.WebOperationContext ctx = System.ServiceModel.Web.WebOperationContext.Current;

                            ctx.OutgoingResponse.Headers.Add("Message", oCallResponse.ResponseMessage);

                            if (!oCallResponse.IsPassed)
                            {
                                ctx.OutgoingResponse.Headers.Add("Code", "1"); // 1 here means false
                                throw new DSTChannelsAuthenticationException();
                            }
                            else
                                ctx.OutgoingResponse.Headers.Add("Code", "0"); // Success
                        }
                    }
                    catch(Exception ex)
                    {
                        var xx = 10;
                    } 
                }

      

                LogRequestForTesting(operationName, inputs);

                return null;
            }
            else
            {
                //Not Authenticated
                throw new DSTChannelsAuthenticationException();
            }
        }

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            //System.ServiceModel.Web.WebOperationContext ctx = System.ServiceModel.Web.WebOperationContext.Current;
            //ctx.OutgoingResponse.Headers.Add("Code", "0");
            //ctx.OutgoingResponse.Headers.Add("MessageEN", "Success");
            //ctx.OutgoingResponse.Headers.Add("MessageAR", "ناجح");
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                if (channelDispatcher == null)
                {
                    continue;
                }

                foreach (var endPoint in channelDispatcher.Endpoints)
                {
                    if (endPoint == null)
                    {
                        continue;
                    }

                    foreach (var opertaion in endPoint.DispatchRuntime.Operations)
                    {
                        opertaion.ParameterInspectors.Add(this);
                    }
                }
            }
        }

        private static void LogRequestForTesting(string operationName, object[] inputs)
        {
            try
            {
                bool enableRequestLogging = false;

                var configVal = ConfigurationManager.AppSettings["EnableRequestLogging"];

                if (configVal != null)
                {
                    bool.TryParse(configVal, out enableRequestLogging);
                    if (enableRequestLogging)
                    {
                        string requestData = operationName + Environment.NewLine;
                        foreach (var item in inputs)
                        {
                            requestData += JsonConvert.SerializeObject(item) + Environment.NewLine;
                        }

                        string logsFolder = System.Web.Hosting.HostingEnvironment.MapPath("~/RequestLogs");
                        if (!System.IO.Directory.Exists(logsFolder))
                        {
                            System.IO.Directory.CreateDirectory(logsFolder);
                        }

                        string filePath = System.IO.Path.Combine(logsFolder, string.Format("{0}_{1}.txt", operationName, DateTime.Now.ToString("yyyyMMddhhmmss")));
                        System.IO.File.WriteAllText(filePath, requestData);
                    }
                }
            }
            catch { }
        }
    }
}