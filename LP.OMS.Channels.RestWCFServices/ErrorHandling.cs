﻿using LP.OMS.Channels.RestWCFServices.WCFOnlineChannelServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LP.OMS.Channels.RestWCFServices
{
    public static class ErrorHandling
    {
        private static object lockObj = new object();

        public static void HandleException(string methodName, System.Exception ex, bool rethrow = true)
        {
            try
            {
                using (OnlineChannelServicesClient client = new OnlineChannelServicesClient())
                {
                    client.HandleException(methodName, ex.Message, ex.ToString());
                }
            }
            catch 
            {
                lock (lockObj)
                {
                    System.IO.File.WriteAllText(@"C:\LeadingPoint\LP.OMS.Channels.RestWCFServices\log.txt", ex.ToString());
                }
                //System.Diagnostics.EventLog.WriteEntry("DST-Rest", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
        }
    }
}