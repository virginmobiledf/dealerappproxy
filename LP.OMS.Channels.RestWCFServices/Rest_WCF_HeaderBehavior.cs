using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Configuration;
using System.Net;
using System.ServiceModel;

namespace LP.OMS.Channels.WCFServices
{
    public class Rest_WCF_HeaderBehavior : BehaviorExtensionElement, IEndpointBehavior
    {
        public override Type BehaviorType
        {
            get { return typeof(Rest_WCF_HeaderBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new Rest_WCF_HeaderBehavior();
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(
                new Rest_WCF_HeaderInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(
                new Rest_WCF_HeaderInspector());
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            return;
        }

        private class Rest_WCF_HeaderInspector : IClientMessageInspector, IDispatchMessageInspector
        {

            private const string HEADER_NS = "http://www.Leading-Point.com/Utilities/ServiceModel";
            private const string SESSION_HEADER_NAME = "SessionToken";
          
            public object BeforeSendRequest(ref Message request, IClientChannel channel)
            {
                var request1 = System.ServiceModel.Web.WebOperationContext.Current.IncomingRequest;

                MessageHeader<string> mhSessionID = new MessageHeader<string>
                {
                    Content = request1.Headers["SessionToken"],
                    Actor = string.Empty,
                    MustUnderstand = false,
                };
                request.Headers.Add(mhSessionID.GetUntypedHeader(SESSION_HEADER_NAME, HEADER_NS));

                return null;
            }

            //Nothing to do here, we don't receive inbound messages..
            public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
            {
                return null;
            }

            //Nothing to do here, we don't receive inbound messages..
            public void BeforeSendReply(ref Message reply, object correlationState)
            {
            }

            public void AfterReceiveReply(ref Message reply, object correlationState)
            {
                // We don't need anything back from Rest, it sends the Dealer ID, but we don't need it...
            }

        }
    }
}
