﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LP.OMS.Channels.RestWCFServices
{
    public class DSTChannelsAuthenticationException : Exception
    {
        public DSTChannelsAuthenticationException() : base("Security Violation!")
        {
            
        }
    }
}