﻿using LP.OMS.Channels.MT.WindowsService.Helpers;
using LP.OMS.Channels.MT.WindowsService.Timers;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LP.OMS.Channels.MT.WindowsService
{
    public abstract class BaseCronJob
    {
        public static volatile bool StopAllJobs;
        public static ConcurrentDictionary<IJobDetail, IJobDetail> CurrantJobs
            = new ConcurrentDictionary<IJobDetail, IJobDetail>();
        public static ISchedulerFactory schedFact = new StdSchedulerFactory();
        public static IScheduler sched;
        static protected ConfigurationSection.TimersElementCollection config { get; } = Configuration.Config.Timers;
        protected ConfigurationSection.TimersElementCollection.TimerElement TimerConfig;
        protected bool Pulse = true;
        protected BaseCronJob()
        {
            StopAllJobs = false;
        }

        public virtual bool LogTimersSchedule(string TimerName, DateTime start, DateTime? end)
        {
            return true;
        }

        public static async Task TimerSetup()
        {
            try
            {
                LogHelper.WriteEntry("Enter TimerSetup");
                sched = await schedFact.GetScheduler();
                await sched.Start();

                LogHelper.WriteEntry($"Processing {Configuration.Config.Timers.Count} Config(s)");
                foreach (ConfigurationSection.TimersElementCollection.TimerElement time in Configuration.Config.Timers)
                {
                    if (!string.IsNullOrWhiteSpace(time.Disabled) && time.Disabled == "true")
                        continue;

                    IJobDetail job = null;
                    LogHelper.WriteEntry($"Building Job for {time.Type} ....");
                    switch (time.Type)
                    {
                        case "AutoCheckout":
                            job = JobBuilder.Create<AutoCheckout>()
                                            .WithIdentity(time.Name, "Timer")
                                              .UsingJobData(new JobDataMap(new Dictionary<string,
                                              ConfigurationSection.TimersElementCollection.TimerElement>() {
                                             { "TimerName", time } }))
                                             .Build();
                            break;

                        case "VerficationNotification":
                            job = JobBuilder.Create<VerficationNotification>()
                                            .WithIdentity(time.Name, "Timer")
                                              .UsingJobData(new JobDataMap(new Dictionary<string,
                                              ConfigurationSection.TimersElementCollection.TimerElement>() {
                                             { "TimerName", time } }))
                                             .Build();
                            break;

                        case "AutoCheckout_MissedVerification_Report":
                            job = JobBuilder.Create<AutoCheckout_MissedVerification_Report>()
                                            .WithIdentity(time.Name, "Timer")
                                              .UsingJobData(new JobDataMap(new Dictionary<string,
                                              ConfigurationSection.TimersElementCollection.TimerElement>() {
                                             { "TimerName", time } }))
                                             .Build();
                            break;

                        case "AutomaticCheckOut":
                            job = JobBuilder.Create<AutomaticCheckOut>()
                                            .WithIdentity(time.Name, "Timer")
                                              .UsingJobData(new JobDataMap(new Dictionary<string,
                                              ConfigurationSection.TimersElementCollection.TimerElement>() {
                                             { "TimerName", time } }))
                                             .Build();
                            break;
                    }

                    ITrigger trigger = TriggerBuilder.Create()
                       .WithIdentity("Trigger" + time.Name, "TimerTrigger")
                       .WithCronSchedule(time.CronExp)
                              .Build();

                    await sched.ScheduleJob(job, trigger);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEntry($"Time setup exception:{ex.Message}, details:{ex.InnerException}", System.Diagnostics.EventLogEntryType.Error);
            }
        }

        public virtual void Start(ConfigurationSection.TimersElementCollection.TimerElement config)
        {
            try
            {
                LogHelper.WriteEntry($"calling ExecutionCore for {config.Type}");
                this.TimerConfig = config;
                ExecutionCore();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        protected abstract void ExecutionCore();
    }
}
