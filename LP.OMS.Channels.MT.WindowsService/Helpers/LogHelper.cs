﻿using LP.OMS.Channels.MT.WindowsService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.Channels.MT.WindowsService
{
    /// <summary>  
    /// Summary description for ExceptionLogging  
    /// </summary>  
    public static class LogHelper
    {
        private static string ErrorlineNo, Errormsg, extype, exurl, hostIp, ErrorLocation, HostAdd;

        private static bool EnableExtraLogging = ConfigurationManager.AppSettings["EnableExtraLogging"]!=null?bool.Parse( ConfigurationManager.AppSettings["EnableExtraLogging"]):false;

        public static void SendErrorToText(Exception ex)
        {
            var line = Environment.NewLine + Environment.NewLine;

            ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            Errormsg = ex.GetType().Name.ToString();
            extype = ex.GetType().ToString();
            ErrorLocation = ex.Message.ToString();

            try
            {
                string filepath = ConfigurationManager.AppSettings["LogErrorPath"]; //Text File Path

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + ".txt";   //Text File Name

                if (!File.Exists(filepath))
                {
                    File.Create(filepath).Dispose();
                }
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Error Line No :" + " " + ErrorlineNo + line + "Error Message:" + " " + Errormsg + line + "Exception Type:" + " " + extype + line + "Error Location :" + " " + ErrorLocation + line + " Error Page Url:" + " " + exurl + line + "User Host IP:" + " " + hostIp + line;
                    sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(line);
                    sw.WriteLine(error);
                    sw.WriteLine("--------------------------------*End*------------------------------------------");
                    sw.WriteLine(line);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private static string _EventApplicationSourceName = "LP.OMS.Channels.MT.WindowsService";
        public static void WriteEntry(string entry, EventLogEntryType entryType = EventLogEntryType.Information)
        {
            if (!EnableExtraLogging)
                return;
            EventLog appLog = new EventLog();
            appLog.Source = CreateEventSource(_EventApplicationSourceName);
            appLog.WriteEntry(entry, entryType);
        }
        private static string CreateEventSource(string currentAppName)
        {

            string eventSource = currentAppName;
            bool sourceExists;
            try
            {
                // searching the source throws a security exception ONLY if not exists!
                sourceExists = EventLog.SourceExists(eventSource);
                if (!sourceExists)
                {   // no exception until yet means the user as admin privilege
                    EventLog.CreateEventSource(eventSource, "Application");
                }
            }
            catch (SecurityException)
            {
                eventSource = "Application";
            }

            return eventSource;
        }

        public static void LogInfoError(string error)
        {
            var line = Environment.NewLine + Environment.NewLine;
            string filepath = ConfigurationManager.AppSettings["LogErrorPath"]; //Text File Path

            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }

            filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + ".txt";   //Text File Name

            if (!File.Exists(filepath))
            {
                File.Create(filepath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(filepath))
            {

                sw.WriteLine("-----------Error Details on " + " " + DateTime.Now.ToString() + "-----------------");
                sw.WriteLine("-------------------------------------------------------------------------------------");
                sw.WriteLine(line);
                sw.WriteLine(error);
                sw.WriteLine("--------------------------------*End*------------------------------------------");
                sw.WriteLine(line);
                sw.Flush();
                sw.Close();
            }
        }

        public static void LogError(Exception ex, int? attendanceID)
        {
            try
            {
                using(AttendenceEntities db = new AttendenceEntities())
                {
                    _ = db.ErrorLogs.Add(new ErrorLog()
                    {
                        AttendanceID = attendanceID,
                        Datetime = DateTime.Now,
                        Description = ex?.Message,
                        StackTrace = ex?.StackTrace
                    });

                    db.SaveChanges();
                }
            }
            catch (Exception)
            { }
        }
    }
}
