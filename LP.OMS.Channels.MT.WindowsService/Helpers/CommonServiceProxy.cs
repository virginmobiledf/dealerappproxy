﻿using LP.OMS.Channels.MT.WindowsService.CommonServices;

namespace LP.OMS.Channels.MT.WindowsService
{
    public class CommonServiceProxy
    {
        public static Requester GetRequesterByName(string requesterName)
        {
            Requester requester = null;
            using (CommonServicesClient client = new CommonServicesClient())
            {
                using (UserContextScope userScope = new UserContextScope(client.InnerChannel, ""))
                {
                    requester = client.GetRequesterByName(requesterName);
                }
            }

            return requester;
        }
    }
}
