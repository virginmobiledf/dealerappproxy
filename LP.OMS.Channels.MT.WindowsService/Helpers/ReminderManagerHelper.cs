﻿
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Engine.Proxies;
using LP.OMS.Channels.MT.ReminderManager.Notification;
using LP.OMS.Channels.MT.WindowsService.AdministrationServices;
using LP.OMS.Channels.MT.WindowsService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using OMSLanguages = LP.OMS.Channels.MT.WindowsService.AdministrationServices.OMSLanguages;
using Requester = LP.OMS.Channels.MT.WindowsService.AdministrationServices.Requester;

namespace LP.OMS.Channels.MT.WindowsService
{
    public class ReminderManagerHelper
    {
        public static void StartReminderProcess()
        {
            try
            {
                int attendanceID = -1;

                using (AttendenceEntities context = new AttendenceEntities())
                {
                    int RemainingTimeToSendSMS = Convert.ToInt32(ConfigurationManager.AppSettings["RemainingTimeToSendSMS"]);
                    TimeSpan time = new TimeSpan(0, RemainingTimeToSendSMS, 0);

                    var result = context.GetAttendancesForCheckOutSMS_V2(time).ToList();

                    if (result != null && result.Count() > 0)
                    {
                        foreach (var item in result)
                        {
                            try
                            {
                                attendanceID = item.ID;
                                var requester = CommonServiceProxy.GetRequesterByName(item.DealerCode);
                                if (requester != null)
                                {
                                    using (AdministrationServicesClient client = new AdministrationServicesClient("WSHttpBinding_IAdministrationServices"))
                                    {
                                        Requester dealer = client.GetRequester(requester.ID, OMSLanguages.Local);
                                        if (dealer != null)
                                        {
                                            // Send Notification
                                            if (!string.IsNullOrEmpty(dealer.ContactMSISDN))
                                            {
                                                string dealerPreferredLanguage = dealer.LanguagePreferenceIsEnglish ? "en" : "ar";
                                                NEPostEventData NEEventData = new NEPostEventData() { EventParam = new List<EventParam>() };
                                                EventParam eventParam1 = new EventParam() { Name = "strRequestDate", Value = DateTime.Now.ToString() };
                                                EventParam eventParam2 = new EventParam() { Name = "lang", Value = dealerPreferredLanguage };
                                                NEEventData.EventParam.Add(eventParam1);
                                                NEEventData.EventParam.Add(eventParam2);

                                                bool IsSuccess = NEProxy.CallNotification("DealerApp", "AttendanceReminderSMS", dealer.ContactMSISDN, NEEventData);

                                                if (IsSuccess)
                                                {
                                                    context.SetAsReminderedAttendance(attendanceID);
                                                    context.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogHelper.LogError(ex, attendanceID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        public static void GenerateReport()
        {
            try
            {
                List<string> recipients = ConfigurationManager.AppSettings["AttendanceSummaryEmailRecipients"]
                    .Split(',').ToList();
                Dictionary<string, string> locations = new Dictionary<string, string>();
                string location = string.Empty;
                StringBuilder html = new StringBuilder("<ul>");
                StringBuilder html2 = new StringBuilder("<ul>");

                using (AttendenceEntities context = new AttendenceEntities())
                {
                    var result = context.GetAutoCheckoutDealersByDate(DateTime.Now.AddDays(-1)).ToList();
                    List<string> result2 = context.GetMissedVerificationDealersByDate(DateTime.Now.AddDays(-1),
                        null).ToList();

                    if ((result != null && result.Count() > 0) || (result2 != null && result2.Any()))
                    {
                        foreach (var item in result)
                        {
                            var requester = CommonServiceProxy.GetRequesterByName(item.DealerCode);

                            string locationName = string.Empty;
                            if (!string.IsNullOrEmpty(item.LocationID))
                            {
                                locations.TryGetValue(item.LocationID, out locationName);
                                if (!string.IsNullOrEmpty(locationName))
                                    location = locationName;
                            }

                            if (string.IsNullOrEmpty(location))
                                location = ModernTradeController.GetLocationByID(item.LocationID,
                                              1, item.DealerCode)?.LocationName;

                            if (!string.IsNullOrEmpty(item.LocationID) && !locations.Any(e => e.Key == item.LocationID))
                                locations.Add(item.LocationID ?? "", location ?? "");

                            HRMS_Proxy.AddAttendanceEntry(new AddAttendanceRequest()
                            {
                                AttendanceDetails = new AttendanceDetails()
                                {
                                    EntryType = "2",
                                    Location = location,
                                    UserCode = requester.FaxNumber,
                                    EntryTime = DateTime.Now.Date.ToString("yyyy-MM-dd HH':'mm':'ss")
                                }
                            });

                            html.Append($"<li>{item.DealerCode}</li>");
                        }
                        foreach (var item in result2)
                        {
                            html2.Append($"<li>{item}</li>");
                        }
                        html.Append("</ul>");
                        html2.Append("</ul>");

                        if (html.ToString().Contains("<li>"))
                        {
                            foreach (var recipient in recipients)
                            {
                                NEProxy.CallNotification("DealerApp", "AttendanceMissedCheckout",
                                recipient,
                                new NEPostEventData()
                                {
                                    EventParam = new List<EventParam>()
                                    { new EventParam()
                                    {
                                        Name = "DealersMissedCheckout",
                                        Value = html.ToString()
                                    }
                                }
                                });
                            }
                        }

                        if (html2.ToString().Contains("<li>"))
                        {
                            foreach (var recipient in recipients)
                            {
                                NEProxy.CallNotification("DealerApp", "AttendanceMissedVerification",
                            recipient,
                            new NEPostEventData()
                            {
                                EventParam = new List<EventParam>() { new EventParam() {
                                Name = "DealersMissedVerification",
                                Value = html2.ToString()
                            } }
                            });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }
    }
}
