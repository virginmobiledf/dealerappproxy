﻿namespace LP.OMS.Channels.MT.WindowsService.Helpers
{
    public static class Constants
    {

        public enum NotificationStatus
        {
            Pending = 1,
            Sent = 2,
            FirstReminderSent = 3,
            SecondReminderSent = 4,
            Closed = 5
        }

        public enum VerficationStatus
        {
        Pending=1,
        UserCheckedOut=2,
        Verified=3,
        NotVerified=4
        }
    }
}
