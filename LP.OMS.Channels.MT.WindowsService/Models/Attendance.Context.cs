﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LP.OMS.Channels.MT.WindowsService.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Linq;
    
    public partial class AttendenceEntities : DbContext
    {
        public AttendenceEntities()
            : base("name=AttendenceEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Attendence> Attendences { get; set; }
        public virtual DbSet<AttendenceProcessType> AttendenceProcessTypes { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<NotificationLog> NotificationLogs { get; set; }
        public virtual DbSet<NotificationStatu> NotificationStatus { get; set; }
        public virtual DbSet<VerificationNotification> VerificationNotifications { get; set; }
    
        public virtual int AddEmployee(string employeeNo, Nullable<System.DateTime> dateOfBirth, string employeeName, string employeeEmail, string appointmentStatus, string employmentCategory, Nullable<System.DateTime> serviceDate, string positionName, string businessUnitName, string workLocationName, string departmentName, string divisionName, string band, string grade, string positionClassification, string managerEmployeeNumber, ObjectParameter iD)
        {
            var employeeNoParameter = employeeNo != null ?
                new ObjectParameter("EmployeeNo", employeeNo) :
                new ObjectParameter("EmployeeNo", typeof(string));
    
            var dateOfBirthParameter = dateOfBirth.HasValue ?
                new ObjectParameter("DateOfBirth", dateOfBirth) :
                new ObjectParameter("DateOfBirth", typeof(System.DateTime));
    
            var employeeNameParameter = employeeName != null ?
                new ObjectParameter("EmployeeName", employeeName) :
                new ObjectParameter("EmployeeName", typeof(string));
    
            var employeeEmailParameter = employeeEmail != null ?
                new ObjectParameter("EmployeeEmail", employeeEmail) :
                new ObjectParameter("EmployeeEmail", typeof(string));
    
            var appointmentStatusParameter = appointmentStatus != null ?
                new ObjectParameter("AppointmentStatus", appointmentStatus) :
                new ObjectParameter("AppointmentStatus", typeof(string));
    
            var employmentCategoryParameter = employmentCategory != null ?
                new ObjectParameter("EmploymentCategory", employmentCategory) :
                new ObjectParameter("EmploymentCategory", typeof(string));
    
            var serviceDateParameter = serviceDate.HasValue ?
                new ObjectParameter("ServiceDate", serviceDate) :
                new ObjectParameter("ServiceDate", typeof(System.DateTime));
    
            var positionNameParameter = positionName != null ?
                new ObjectParameter("PositionName", positionName) :
                new ObjectParameter("PositionName", typeof(string));
    
            var businessUnitNameParameter = businessUnitName != null ?
                new ObjectParameter("BusinessUnitName", businessUnitName) :
                new ObjectParameter("BusinessUnitName", typeof(string));
    
            var workLocationNameParameter = workLocationName != null ?
                new ObjectParameter("WorkLocationName", workLocationName) :
                new ObjectParameter("WorkLocationName", typeof(string));
    
            var departmentNameParameter = departmentName != null ?
                new ObjectParameter("DepartmentName", departmentName) :
                new ObjectParameter("DepartmentName", typeof(string));
    
            var divisionNameParameter = divisionName != null ?
                new ObjectParameter("DivisionName", divisionName) :
                new ObjectParameter("DivisionName", typeof(string));
    
            var bandParameter = band != null ?
                new ObjectParameter("Band", band) :
                new ObjectParameter("Band", typeof(string));
    
            var gradeParameter = grade != null ?
                new ObjectParameter("Grade", grade) :
                new ObjectParameter("Grade", typeof(string));
    
            var positionClassificationParameter = positionClassification != null ?
                new ObjectParameter("PositionClassification", positionClassification) :
                new ObjectParameter("PositionClassification", typeof(string));
    
            var managerEmployeeNumberParameter = managerEmployeeNumber != null ?
                new ObjectParameter("ManagerEmployeeNumber", managerEmployeeNumber) :
                new ObjectParameter("ManagerEmployeeNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddEmployee", employeeNoParameter, dateOfBirthParameter, employeeNameParameter, employeeEmailParameter, appointmentStatusParameter, employmentCategoryParameter, serviceDateParameter, positionNameParameter, businessUnitNameParameter, workLocationNameParameter, departmentNameParameter, divisionNameParameter, bandParameter, gradeParameter, positionClassificationParameter, managerEmployeeNumberParameter, iD);
        }
    
        public virtual int AddNotificationLog(Nullable<System.DateTime> creationDate, string request, string response, Nullable<bool> isSuccess, string apiName, string apiUri)
        {
            var creationDateParameter = creationDate.HasValue ?
                new ObjectParameter("CreationDate", creationDate) :
                new ObjectParameter("CreationDate", typeof(System.DateTime));
    
            var requestParameter = request != null ?
                new ObjectParameter("Request", request) :
                new ObjectParameter("Request", typeof(string));
    
            var responseParameter = response != null ?
                new ObjectParameter("Response", response) :
                new ObjectParameter("Response", typeof(string));
    
            var isSuccessParameter = isSuccess.HasValue ?
                new ObjectParameter("IsSuccess", isSuccess) :
                new ObjectParameter("IsSuccess", typeof(bool));
    
            var apiNameParameter = apiName != null ?
                new ObjectParameter("ApiName", apiName) :
                new ObjectParameter("ApiName", typeof(string));
    
            var apiUriParameter = apiUri != null ?
                new ObjectParameter("ApiUri", apiUri) :
                new ObjectParameter("ApiUri", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddNotificationLog", creationDateParameter, requestParameter, responseParameter, isSuccessParameter, apiNameParameter, apiUriParameter);
        }
    
        public virtual int AutomaticCheckOut_V2(int attendanceWorkingHours)
        {
            var apiUriParameter = new ObjectParameter("WorkingHours", attendanceWorkingHours);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AutomaticCheckOut_V2", apiUriParameter);
        }
    
        public virtual int DeleteEmployee(string employeeNo)
        {
            var employeeNoParameter = employeeNo != null ?
                new ObjectParameter("EmployeeNo", employeeNo) :
                new ObjectParameter("EmployeeNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteEmployee", employeeNoParameter);
        }
    
        public virtual ObjectResult<GetActiveAttendance_V2_Result> GetActiveAttendance_V2(string daelerCode)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetActiveAttendance_V2_Result>("GetActiveAttendance_V2", daelerCodeParameter);
        }
    
        public virtual ObjectResult<string> GetActiveLocationIDByDealerCode(string daelerCode)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetActiveLocationIDByDealerCode", daelerCodeParameter);
        }
    
        public virtual ObjectResult<GetAllActiveAttendances_Result> GetAllActiveAttendances()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllActiveAttendances_Result>("GetAllActiveAttendances");
        }
    
        public virtual ObjectResult<GetAttendancesForCheckOutSMS_Result> GetAttendancesForCheckOutSMS(Nullable<System.TimeSpan> remainingTimeForSendSMS)
        {
            var remainingTimeForSendSMSParameter = remainingTimeForSendSMS.HasValue ?
                new ObjectParameter("RemainingTimeForSendSMS", remainingTimeForSendSMS) :
                new ObjectParameter("RemainingTimeForSendSMS", typeof(System.TimeSpan));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAttendancesForCheckOutSMS_Result>("GetAttendancesForCheckOutSMS", remainingTimeForSendSMSParameter);
        }
    
        public virtual ObjectResult<GetEmployeeInfoByEmployeeNo_Result> GetEmployeeInfoByEmployeeNo(string employeeNo)
        {
            var employeeNoParameter = employeeNo != null ?
                new ObjectParameter("EmployeeNo", employeeNo) :
                new ObjectParameter("EmployeeNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetEmployeeInfoByEmployeeNo_Result>("GetEmployeeInfoByEmployeeNo", employeeNoParameter);
        }
    
        public virtual int SetAsReminderedAttendance(Nullable<int> iD)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SetAsReminderedAttendance", iDParameter);
        }
    
        public virtual int SP_GetRendomVerificationDates(Nullable<System.DateTime> checkinDate, Nullable<int> verificationsNo, Nullable<int> workingHours)
        {
            var checkinDateParameter = checkinDate.HasValue ?
                new ObjectParameter("CheckinDate", checkinDate) :
                new ObjectParameter("CheckinDate", typeof(System.DateTime));
    
            var verificationsNoParameter = verificationsNo.HasValue ?
                new ObjectParameter("VerificationsNo", verificationsNo) :
                new ObjectParameter("VerificationsNo", typeof(int));
    
            var workingHoursParameter = workingHours.HasValue ?
                new ObjectParameter("WorkingHours", workingHours) :
                new ObjectParameter("WorkingHours", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetRendomVerificationDates", checkinDateParameter, verificationsNoParameter, workingHoursParameter);
        }
    
        public virtual int UpdateActiveAttendanceSessionID(Nullable<int> iD, string sessionID)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var sessionIDParameter = sessionID != null ?
                new ObjectParameter("SessionID", sessionID) :
                new ObjectParameter("SessionID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateActiveAttendanceSessionID", iDParameter, sessionIDParameter);
        }
    
        public virtual int UpdateDealerActiveAttendanceForCheckOut(string daelerCode, string checkoutID)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            var checkoutIDParameter = checkoutID != null ?
                new ObjectParameter("CheckoutID", checkoutID) :
                new ObjectParameter("CheckoutID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateDealerActiveAttendanceForCheckOut", daelerCodeParameter, checkoutIDParameter);
        }
    
        public virtual int UpdateEmployee(string employeeNo, Nullable<System.DateTime> dateOfBirth, string employeeName, string employeeEmail, string appointmentStatus, string employmentCategory, Nullable<System.DateTime> serviceDate, string positionName, string businessUnitName, string workLocationName, string departmentName, string divisionName, string band, string grade, string positionClassification, string managerEmployeeNumber)
        {
            var employeeNoParameter = employeeNo != null ?
                new ObjectParameter("EmployeeNo", employeeNo) :
                new ObjectParameter("EmployeeNo", typeof(string));
    
            var dateOfBirthParameter = dateOfBirth.HasValue ?
                new ObjectParameter("DateOfBirth", dateOfBirth) :
                new ObjectParameter("DateOfBirth", typeof(System.DateTime));
    
            var employeeNameParameter = employeeName != null ?
                new ObjectParameter("EmployeeName", employeeName) :
                new ObjectParameter("EmployeeName", typeof(string));
    
            var employeeEmailParameter = employeeEmail != null ?
                new ObjectParameter("EmployeeEmail", employeeEmail) :
                new ObjectParameter("EmployeeEmail", typeof(string));
    
            var appointmentStatusParameter = appointmentStatus != null ?
                new ObjectParameter("AppointmentStatus", appointmentStatus) :
                new ObjectParameter("AppointmentStatus", typeof(string));
    
            var employmentCategoryParameter = employmentCategory != null ?
                new ObjectParameter("EmploymentCategory", employmentCategory) :
                new ObjectParameter("EmploymentCategory", typeof(string));
    
            var serviceDateParameter = serviceDate.HasValue ?
                new ObjectParameter("ServiceDate", serviceDate) :
                new ObjectParameter("ServiceDate", typeof(System.DateTime));
    
            var positionNameParameter = positionName != null ?
                new ObjectParameter("PositionName", positionName) :
                new ObjectParameter("PositionName", typeof(string));
    
            var businessUnitNameParameter = businessUnitName != null ?
                new ObjectParameter("BusinessUnitName", businessUnitName) :
                new ObjectParameter("BusinessUnitName", typeof(string));
    
            var workLocationNameParameter = workLocationName != null ?
                new ObjectParameter("WorkLocationName", workLocationName) :
                new ObjectParameter("WorkLocationName", typeof(string));
    
            var departmentNameParameter = departmentName != null ?
                new ObjectParameter("DepartmentName", departmentName) :
                new ObjectParameter("DepartmentName", typeof(string));
    
            var divisionNameParameter = divisionName != null ?
                new ObjectParameter("DivisionName", divisionName) :
                new ObjectParameter("DivisionName", typeof(string));
    
            var bandParameter = band != null ?
                new ObjectParameter("Band", band) :
                new ObjectParameter("Band", typeof(string));
    
            var gradeParameter = grade != null ?
                new ObjectParameter("Grade", grade) :
                new ObjectParameter("Grade", typeof(string));
    
            var positionClassificationParameter = positionClassification != null ?
                new ObjectParameter("PositionClassification", positionClassification) :
                new ObjectParameter("PositionClassification", typeof(string));
    
            var managerEmployeeNumberParameter = managerEmployeeNumber != null ?
                new ObjectParameter("ManagerEmployeeNumber", managerEmployeeNumber) :
                new ObjectParameter("ManagerEmployeeNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateEmployee", employeeNoParameter, dateOfBirthParameter, employeeNameParameter, employeeEmailParameter, appointmentStatusParameter, employmentCategoryParameter, serviceDateParameter, positionNameParameter, businessUnitNameParameter, workLocationNameParameter, departmentNameParameter, divisionNameParameter, bandParameter, gradeParameter, positionClassificationParameter, managerEmployeeNumberParameter);
        }
    
        public virtual int SP_GetRandomVerificationDates_No12PMlimit(Nullable<System.DateTime> checkinDate, Nullable<int> verificationsNo, Nullable<int> workingHours)
        {
            var checkinDateParameter = checkinDate.HasValue ?
                new ObjectParameter("CheckinDate", checkinDate) :
                new ObjectParameter("CheckinDate", typeof(System.DateTime));
    
            var verificationsNoParameter = verificationsNo.HasValue ?
                new ObjectParameter("VerificationsNo", verificationsNo) :
                new ObjectParameter("VerificationsNo", typeof(int));
    
            var workingHoursParameter = workingHours.HasValue ?
                new ObjectParameter("WorkingHours", workingHours) :
                new ObjectParameter("WorkingHours", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_GetRandomVerificationDates_No12PMlimit", checkinDateParameter, verificationsNoParameter, workingHoursParameter);
        }
    
        public virtual ObjectResult<GetActiveNotificationAttendance_V2_Result> GetActiveNotificationAttendance_V2(string daelerCode)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetActiveNotificationAttendance_V2_Result>("GetActiveNotificationAttendance_V2", daelerCodeParameter);
        }
    
        public virtual ObjectResult<GetNotificationsToBeSent_Result> GetNotificationsToBeSent()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetNotificationsToBeSent_Result>("GetNotificationsToBeSent");
        }
    
        public virtual int AddErrorLog(string description, string stackTrace, Nullable<System.DateTime> datetime, Nullable<int> attendanceID)
        {
            var descriptionParameter = description != null ?
                new ObjectParameter("Description", description) :
                new ObjectParameter("Description", typeof(string));
    
            var stackTraceParameter = stackTrace != null ?
                new ObjectParameter("StackTrace", stackTrace) :
                new ObjectParameter("StackTrace", typeof(string));
    
            var datetimeParameter = datetime.HasValue ?
                new ObjectParameter("Datetime", datetime) :
                new ObjectParameter("Datetime", typeof(System.DateTime));
    
            var attendanceIDParameter = attendanceID.HasValue ?
                new ObjectParameter("AttendanceID", attendanceID) :
                new ObjectParameter("AttendanceID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddErrorLog", descriptionParameter, stackTraceParameter, datetimeParameter, attendanceIDParameter);
        }
    
        public virtual ObjectResult<string> GetMissedVerificationDealersByDate(Nullable<System.DateTime> date, string daelerCode)
        {
            var dateParameter = date.HasValue ?
                new ObjectParameter("Date", date) :
                new ObjectParameter("Date", typeof(System.DateTime));
    
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetMissedVerificationDealersByDate", dateParameter, daelerCodeParameter);
        }
    
        public virtual ObjectResult<GetNotificationsToBeClosedByDueTime_Result> GetNotificationsToBeClosedByDueTime()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetNotificationsToBeClosedByDueTime_Result>("GetNotificationsToBeClosedByDueTime");
        }
    
        public virtual int UpdateNotificationStatus(Nullable<int> notificationID, Nullable<int> notificationStatus, Nullable<int> verificationStatus)
        {
            var notificationIDParameter = notificationID.HasValue ?
                new ObjectParameter("NotificationID", notificationID) :
                new ObjectParameter("NotificationID", typeof(int));
    
            var notificationStatusParameter = notificationStatus.HasValue ?
                new ObjectParameter("NotificationStatus", notificationStatus) :
                new ObjectParameter("NotificationStatus", typeof(int));
    
            var verificationStatusParameter = verificationStatus.HasValue ?
                new ObjectParameter("VerificationStatus", verificationStatus) :
                new ObjectParameter("VerificationStatus", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateNotificationStatus", notificationIDParameter, notificationStatusParameter, verificationStatusParameter);
        }
    
        public virtual ObjectResult<GetFirstRemindersToBeSent_Result> GetFirstRemindersToBeSent()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetFirstRemindersToBeSent_Result>("GetFirstRemindersToBeSent");
        }
    
        public virtual ObjectResult<GetSecondRemindersToBeSent_Result> GetSecondRemindersToBeSent()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetSecondRemindersToBeSent_Result>("GetSecondRemindersToBeSent");
        }
    
        public virtual ObjectResult<GetNotificationsToBeClosedByUserCheckOut_Result> GetNotificationsToBeClosedByUserCheckOut()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetNotificationsToBeClosedByUserCheckOut_Result>("GetNotificationsToBeClosedByUserCheckOut");
        }
    
        public virtual int Insert_Attendance(string daelerCode, string locationID, string location_Lat, string location_Lon, Nullable<int> processID, Nullable<System.DateTime> perfomedAt, Nullable<System.DateTime> workingHours_Strat, Nullable<System.DateTime> workingHours_End, string sessionID, string deviceID, Nullable<int> checkoutID, Nullable<bool> isAutomatic, Nullable<bool> isMTDealer, Nullable<bool> isCheckoutReminderSent, string channelID, ObjectParameter iD)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            var locationIDParameter = locationID != null ?
                new ObjectParameter("LocationID", locationID) :
                new ObjectParameter("LocationID", typeof(string));
    
            var location_LatParameter = location_Lat != null ?
                new ObjectParameter("Location_Lat", location_Lat) :
                new ObjectParameter("Location_Lat", typeof(string));
    
            var location_LonParameter = location_Lon != null ?
                new ObjectParameter("Location_Lon", location_Lon) :
                new ObjectParameter("Location_Lon", typeof(string));
    
            var processIDParameter = processID.HasValue ?
                new ObjectParameter("ProcessID", processID) :
                new ObjectParameter("ProcessID", typeof(int));
    
            var perfomedAtParameter = perfomedAt.HasValue ?
                new ObjectParameter("PerfomedAt", perfomedAt) :
                new ObjectParameter("PerfomedAt", typeof(System.DateTime));
    
            var workingHours_StratParameter = workingHours_Strat.HasValue ?
                new ObjectParameter("WorkingHours_Strat", workingHours_Strat) :
                new ObjectParameter("WorkingHours_Strat", typeof(System.DateTime));
    
            var workingHours_EndParameter = workingHours_End.HasValue ?
                new ObjectParameter("WorkingHours_End", workingHours_End) :
                new ObjectParameter("WorkingHours_End", typeof(System.DateTime));
    
            var sessionIDParameter = sessionID != null ?
                new ObjectParameter("SessionID", sessionID) :
                new ObjectParameter("SessionID", typeof(string));
    
            var deviceIDParameter = deviceID != null ?
                new ObjectParameter("DeviceID", deviceID) :
                new ObjectParameter("DeviceID", typeof(string));
    
            var checkoutIDParameter = checkoutID.HasValue ?
                new ObjectParameter("CheckoutID", checkoutID) :
                new ObjectParameter("CheckoutID", typeof(int));
    
            var isAutomaticParameter = isAutomatic.HasValue ?
                new ObjectParameter("IsAutomatic", isAutomatic) :
                new ObjectParameter("IsAutomatic", typeof(bool));
    
            var isMTDealerParameter = isMTDealer.HasValue ?
                new ObjectParameter("IsMTDealer", isMTDealer) :
                new ObjectParameter("IsMTDealer", typeof(bool));
    
            var isCheckoutReminderSentParameter = isCheckoutReminderSent.HasValue ?
                new ObjectParameter("IsCheckoutReminderSent", isCheckoutReminderSent) :
                new ObjectParameter("IsCheckoutReminderSent", typeof(bool));
    
            var channelIDParameter = channelID != null ?
                new ObjectParameter("ChannelID", channelID) :
                new ObjectParameter("ChannelID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_Attendance", daelerCodeParameter, locationIDParameter, location_LatParameter, location_LonParameter, processIDParameter, perfomedAtParameter, workingHours_StratParameter, workingHours_EndParameter, sessionIDParameter, deviceIDParameter, checkoutIDParameter, isAutomaticParameter, isMTDealerParameter, isCheckoutReminderSentParameter, channelIDParameter, iD);
        }
    
        public virtual ObjectResult<string> UpdateNotificationVerfication(string dealerCode, Nullable<int> attendanceID)
        {
            var dealerCodeParameter = dealerCode != null ?
                new ObjectParameter("DealerCode", dealerCode) :
                new ObjectParameter("DealerCode", typeof(string));
    
            var attendanceIDParameter = attendanceID.HasValue ?
                new ObjectParameter("AttendanceID", attendanceID) :
                new ObjectParameter("AttendanceID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("UpdateNotificationVerfication", dealerCodeParameter, attendanceIDParameter);
        }
    
        public virtual ObjectResult<GetAttendancesForCheckOutSMS_V2_Result> GetAttendancesForCheckOutSMS_V2(Nullable<System.TimeSpan> remainingTimeForSendSMS)
        {
            var remainingTimeForSendSMSParameter = remainingTimeForSendSMS.HasValue ?
                new ObjectParameter("RemainingTimeForSendSMS", remainingTimeForSendSMS) :
                new ObjectParameter("RemainingTimeForSendSMS", typeof(System.TimeSpan));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAttendancesForCheckOutSMS_V2_Result>("GetAttendancesForCheckOutSMS_V2", remainingTimeForSendSMSParameter);
        }
    
        public virtual ObjectResult<GetFirstReminderToBeIgnoredByUserCheckOut_Result> GetFirstReminderToBeIgnoredByUserCheckOut()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetFirstReminderToBeIgnoredByUserCheckOut_Result>("GetFirstReminderToBeIgnoredByUserCheckOut");
        }
    
        public virtual ObjectResult<GetNotificationsToBeIgnoredByUserCheckOut_Result> GetNotificationsToBeIgnoredByUserCheckOut()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetNotificationsToBeIgnoredByUserCheckOut_Result>("GetNotificationsToBeIgnoredByUserCheckOut");
        }
    
        public virtual int Insert_Attendance_V2(string daelerCode, string locationID, string location_Lat, string location_Lon, Nullable<int> processID, Nullable<System.DateTime> perfomedAt, Nullable<System.DateTime> workingHours_Strat, Nullable<System.DateTime> workingHours_End, string sessionID, string deviceID, Nullable<int> checkoutID, Nullable<bool> isAutomatic, Nullable<bool> isMTDealer, Nullable<bool> isCheckoutReminderSent, string channelID, Nullable<int> performedUsing, ObjectParameter iD)
        {
            var daelerCodeParameter = daelerCode != null ?
                new ObjectParameter("DaelerCode", daelerCode) :
                new ObjectParameter("DaelerCode", typeof(string));
    
            var locationIDParameter = locationID != null ?
                new ObjectParameter("LocationID", locationID) :
                new ObjectParameter("LocationID", typeof(string));
    
            var location_LatParameter = location_Lat != null ?
                new ObjectParameter("Location_Lat", location_Lat) :
                new ObjectParameter("Location_Lat", typeof(string));
    
            var location_LonParameter = location_Lon != null ?
                new ObjectParameter("Location_Lon", location_Lon) :
                new ObjectParameter("Location_Lon", typeof(string));
    
            var processIDParameter = processID.HasValue ?
                new ObjectParameter("ProcessID", processID) :
                new ObjectParameter("ProcessID", typeof(int));
    
            var perfomedAtParameter = perfomedAt.HasValue ?
                new ObjectParameter("PerfomedAt", perfomedAt) :
                new ObjectParameter("PerfomedAt", typeof(System.DateTime));
    
            var workingHours_StratParameter = workingHours_Strat.HasValue ?
                new ObjectParameter("WorkingHours_Strat", workingHours_Strat) :
                new ObjectParameter("WorkingHours_Strat", typeof(System.DateTime));
    
            var workingHours_EndParameter = workingHours_End.HasValue ?
                new ObjectParameter("WorkingHours_End", workingHours_End) :
                new ObjectParameter("WorkingHours_End", typeof(System.DateTime));
    
            var sessionIDParameter = sessionID != null ?
                new ObjectParameter("SessionID", sessionID) :
                new ObjectParameter("SessionID", typeof(string));
    
            var deviceIDParameter = deviceID != null ?
                new ObjectParameter("DeviceID", deviceID) :
                new ObjectParameter("DeviceID", typeof(string));
    
            var checkoutIDParameter = checkoutID.HasValue ?
                new ObjectParameter("CheckoutID", checkoutID) :
                new ObjectParameter("CheckoutID", typeof(int));
    
            var isAutomaticParameter = isAutomatic.HasValue ?
                new ObjectParameter("IsAutomatic", isAutomatic) :
                new ObjectParameter("IsAutomatic", typeof(bool));
    
            var isMTDealerParameter = isMTDealer.HasValue ?
                new ObjectParameter("IsMTDealer", isMTDealer) :
                new ObjectParameter("IsMTDealer", typeof(bool));
    
            var isCheckoutReminderSentParameter = isCheckoutReminderSent.HasValue ?
                new ObjectParameter("IsCheckoutReminderSent", isCheckoutReminderSent) :
                new ObjectParameter("IsCheckoutReminderSent", typeof(bool));
    
            var channelIDParameter = channelID != null ?
                new ObjectParameter("ChannelID", channelID) :
                new ObjectParameter("ChannelID", typeof(string));
    
            var performedUsingParameter = performedUsing.HasValue ?
                new ObjectParameter("PerformedUsing", performedUsing) :
                new ObjectParameter("PerformedUsing", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_Attendance_V2", daelerCodeParameter, locationIDParameter, location_LatParameter, location_LonParameter, processIDParameter, perfomedAtParameter, workingHours_StratParameter, workingHours_EndParameter, sessionIDParameter, deviceIDParameter, checkoutIDParameter, isAutomaticParameter, isMTDealerParameter, isCheckoutReminderSentParameter, channelIDParameter, performedUsingParameter, iD);
        }
    
        public virtual ObjectResult<GetAutoCheckoutDealersByDate_Result> GetAutoCheckoutDealersByDate(Nullable<System.DateTime> date)
        {
            var dateParameter = date.HasValue ?
                new ObjectParameter("Date", date) :
                new ObjectParameter("Date", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAutoCheckoutDealersByDate_Result>("GetAutoCheckoutDealersByDate", dateParameter);
        }
    }
}
