﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LP.OMS.Channels.MT.WindowsService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            LogHelper.WriteEntry($"MT_WS started at {DateTime.Now}");
            Thread oThread = new Thread(new ThreadStart(Start));
            oThread.Start();
        }

        protected override void OnStop()
        {
            LogHelper.WriteEntry($"MT_WS stopped at {DateTime.Now}");
        }

        public async void Start()
        {
            try
            {
                LogHelper.WriteEntry($"calling BaseCronJob.TimerSetup()");
                await BaseCronJob.TimerSetup();
                LogHelper.WriteEntry($"finish calling BaseCronJob.TimerSetup()");
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }
    }
}
