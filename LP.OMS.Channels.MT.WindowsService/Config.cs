﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace LP.OMS.Channels.MT.WindowsService
{
    public sealed class Configuration
    {
        private static ConfigurationSection _config;

        static Configuration()
        {
            try
            {
                string path = null;
                if ((ConfigurationManager.AppSettings["IsLocal"]).ToString().ToLower() == "true")
                {
                    path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    path = Path.GetFullPath(Path.Combine(path, @"..\..\"));
                }
                else
                {
                    path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                }

                var map = new ExeConfigurationFileMap { ExeConfigFilename = path + "\\" + ConfigurationManager.AppSettings["ConfigFilePath"].ToString() };
                var Configuration = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                _config = ((ConfigurationSection)Configuration.Sections["Configs"]);
            }
            catch (Exception ex)
            {
                string createText = ex.Message + Environment.NewLine;
                LogHelper.WriteEntry($"Configuration init exception:{ex.Message}, details:{ex.InnerException}", System.Diagnostics.EventLogEntryType.Error);
            }
        }

        private Configuration()
        { }

        public static ConfigurationSection Config
        {
            get
            {
                return _config;
            }
            set
            {
                _config = value;
            }
        }
    }

    public sealed partial class ConfigurationSection : System.Configuration.ConfigurationSection, IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            XPathNavigator nav = section.CreateNavigator();
            string typename = (string)nav.Evaluate("string(@type)");
            Type t = Type.GetType(typename);
            XmlSerializer ser = new XmlSerializer(t);
            return ser.Deserialize(new XmlNodeReader(section));
        }

        [System.Configuration.ConfigurationPropertyAttribute("Timers")]
        [System.Configuration.ConfigurationCollectionAttribute(typeof(TimersElementCollection.TimerElement), AddItemName = "Timer")]
        public TimersElementCollection Timers
        {
            get
            {
                return ((TimersElementCollection)(this["Timers"]));
            }
        }

        public sealed partial class TimersElementCollection : ConfigurationElementCollection
        {
            public TimerElement this[int i]
            {
                get
                {
                    return ((TimerElement)(this.BaseGet(i)));
                }
            }

            public new TimerElement this[string s]
            {
                get
                {
                    return ((TimerElement)(this.BaseGet(s)));
                }
            }

            protected override System.Configuration.ConfigurationElement CreateNewElement()
            {
                return new TimerElement();
            }

            protected override object GetElementKey(System.Configuration.ConfigurationElement element)
            {
                return ((TimerElement)(element)).Name;
            }

            public sealed class TimerElement : ConfigurationElement
            {
                [ConfigurationProperty("name", IsRequired = true)]
                public string Name
                {
                    get
                    {
                        return (string)base["name"];
                    }
                    set
                    {
                        base["name"] = value;
                    }
                }

                [ConfigurationProperty("cronExp", IsRequired = true)]
                public string CronExp
                {
                    get
                    {
                        return (string)base["cronExp"];
                    }
                    set
                    {
                        base["cronExp"] = value;
                    }
                }

                [ConfigurationProperty("disabled", IsRequired = false)]
                public string Disabled
                {
                    get
                    {
                        return (string)base["disabled"];
                    }
                    set
                    {
                        base["disabled"] = value;
                    }
                }

                [ConfigurationProperty("type", IsRequired = true)]
                public string Type
                {
                    get
                    {
                        return (string)base["type"];
                    }
                    set
                    {
                        base["type"] = value;
                    }
                }
            }
        }
    }
}
