﻿
using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Engine.Proxies;
using LP.OMS.Channels.MT.ReminderManager.Notification;
using LP.OMS.Channels.MT.WindowsService.AdministrationServices;
using LP.OMS.Channels.MT.WindowsService.Helpers;
using LP.OMS.Channels.MT.WindowsService.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OMSLanguages = LP.OMS.Channels.MT.WindowsService.AdministrationServices.OMSLanguages;
using Requester = LP.OMS.Channels.MT.WindowsService.AdministrationServices.Requester;

namespace LP.OMS.Channels.MT.WindowsService.Timers
{
    [DisallowConcurrentExecution]

    public class VerficationNotification : BaseCronJob, IJob
    {
        public static bool IsRunning { get; set; }
        protected override void ExecutionCore()
        {
            LogHelper.WriteEntry($"Enter VerficationNotification->ExecutionCore ");
            IsRunning = true;
            DateTime startTimer = DateTime.Now;
            try
            {
                Run();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
            finally
            {
                LogTimersSchedule(this.TimerConfig.Name, startTimer, DateTime.Now);
                IJobDetail job;
                bool Removed = false;
                try
                {
                    Removed = CurrantJobs
                      .TryRemove(CurrantJobs
                      .FirstOrDefault(z => z.Key.Key.Name
                      .Contains(this.TimerConfig.Name.ToString())).Key, out job);
                }
                catch (Exception ex)
                {
                    CurrantJobs.Clear();
                }
                if (!Removed)
                    CurrantJobs.Clear();

                IsRunning = false;
            }
        }

        public void Run()
        {
            try
            {
                using (AttendenceEntities context = new AttendenceEntities())
                {
                    ProcessNotificationsToBeIgnoredByUserCheckOut(context);
                    ProcessFirstReminderToBeIgnoredByUserCheckOut(context);
                    ProcessNotificationsToBeClosedByUserCheckOut(context);
                    ProcessNotificationsToBeClosedByDueTime(context);
                    ProcessNotificationsToBeSent(context);
                    ProcessFirstRemindersToBeSent(context);
                    ProcessSecondRemindersToBeSent(context);
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }
        private static void ProcessNotificationsToBeSent(AttendenceEntities context)
        {
            try
            {
                context.GetNotificationsToBeSent().ToList().ForEach(item =>
                {
                    if (CallNE(context, "VerficationNotification", item.CheckInId.GetValueOrDefault(), item.Id))
                    {
                        LogHelper.WriteEntry($"changing NotificationStatus for itemID:{item.Id} from pending to sent");
                        context.UpdateNotificationStatus(item.Id, (int)Helpers.Constants.NotificationStatus.Sent, item.VerificationStatus);
                    }
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void AddErrorLog(string description, string stackTrace, DateTime datetime, int attendanceID)
        {
            try
            {
                using (AttendenceEntities context = new AttendenceEntities())
                {
                    context.AddErrorLog(description, stackTrace, datetime, attendanceID);
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static bool CallNE(AttendenceEntities context, string responseStatusCode, int checkInId, int verficationNotificationID)
        {
            try
            {
                string dealerCode = GetAttendanceIdentifier(context, checkInId);
                LogHelper.WriteEntry($"dealerCode for checkInId:{checkInId} is {dealerCode}");
                var requester = CommonServiceProxy.GetRequesterByName(dealerCode);
                LogHelper.WriteEntry($"Getting requester from OMS requester != null:{requester != null}");
                Requester dealer;
                if (requester != null)
                {
                    using (AdministrationServicesClient client = new AdministrationServicesClient("WSHttpBinding_IAdministrationServices"))
                    {
                        dealer = client.GetRequester(requester.ID, OMSLanguages.Local);
                    }
                }
                else
                {
                    AddErrorLog("CallNotification", $"Requester not found,dealerCode:{dealerCode},NotificationID:{verficationNotificationID}", DateTime.Now, checkInId);
                    return false;
                }
                string dealerPreferredLanguage = dealer.LanguagePreferenceIsEnglish ? "en" : "ar";
                NEPostEventData NEEventData = new NEPostEventData() { EventParam = new List<EventParam>() };
                EventParam eventParam1 = new EventParam() { Name = "strRequestDate", Value = DateTime.Now.ToString() };
                EventParam eventParam2 = new EventParam() { Name = "lang", Value = dealerPreferredLanguage };
                NEEventData.EventParam.Add(eventParam1);
                NEEventData.EventParam.Add(eventParam2);

                bool IsSuccess = NEProxy.CallNotification("MT_Attendance", responseStatusCode, dealer.ContactMSISDN, NEEventData);
                LogHelper.WriteEntry($"CallNotification for responseStatusCode:{responseStatusCode},MSISDN:{dealer.ContactMSISDN} is {IsSuccess}");

                return IsSuccess;
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }

            return false;
        }

        private static string GetAttendanceIdentifier(AttendenceEntities context, int checkInId)
        {
            try
            {
                string returnValue = string.Empty;
                var attendance = context.Attendences.Where(x => x.ID == checkInId).FirstOrDefault();
                if (attendance != null && !string.IsNullOrEmpty(attendance.DealerCode))
                {
                    returnValue = attendance.DealerCode;
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }

            return null;
        }

        private static void ProcessFirstRemindersToBeSent(AttendenceEntities context)
        {
            try
            {
                context.GetFirstRemindersToBeSent().ToList().ForEach(item =>
                {
                    if (CallNE(context, "1stReminder", item.CheckInId.GetValueOrDefault(), item.Id))
                    {
                        context.UpdateNotificationStatus(item.Id, (int)Helpers.Constants.NotificationStatus.FirstReminderSent, item.VerificationStatus);
                    }
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void ProcessSecondRemindersToBeSent(AttendenceEntities context)
        {
            try
            {
                context.GetSecondRemindersToBeSent().ToList().ForEach(item =>
                {
                    if (CallNE(context, "2ndReminder", item.CheckInId.GetValueOrDefault(), item.Id))
                    {
                        context.UpdateNotificationStatus(item.Id, (int)Helpers.Constants.NotificationStatus.SecondReminderSent, item.VerificationStatus);
                    }
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void ProcessNotificationsToBeClosedByDueTime(AttendenceEntities context)
        {
            Dictionary<string, string> locations = new Dictionary<string, string>();
            string location = string.Empty;

            try
            {
                context.GetNotificationsToBeClosedByDueTime().ToList().ForEach(item =>
                {
                    string dealerCode = GetAttendanceIdentifier(context, item.CheckInId.Value);
                    AccessManagementController.EndLastActiveSessionBySessionKey(dealerCode);
                    var attendanceRecord = context.Attendences.FirstOrDefault(x => x.ID == item.CheckInId.Value);

                    if (attendanceRecord != null)
                    {
                        DateTime date = DateTime.Now;
                        ObjectParameter AttendanceId = new ObjectParameter("iD", typeof(int));
                        context.Insert_Attendance_V2(dealerCode, attendanceRecord.LocationID, attendanceRecord.Location_Lat,
                        attendanceRecord.Location_Lon, (int)AttendanceProcessType.CheckOut_MissedVerfication, date,
                       attendanceRecord.WorkingHours_Start, attendanceRecord.WorkingHours_End,
                        attendanceRecord.SessionID, attendanceRecord.DeviceID, null,
                        false, attendanceRecord.IsMTDealer, false, null /*DistributerId*/, attendanceRecord.PerformedUsing, AttendanceId);

                        var requester = CommonServiceProxy.GetRequesterByName(dealerCode);

                        string locationName = string.Empty;
                        if (!string.IsNullOrEmpty(attendanceRecord.LocationID))
                        {
                            locations.TryGetValue(attendanceRecord.LocationID, out locationName);
                            if (!string.IsNullOrEmpty(locationName))
                                location = locationName;
                        }

                        if (string.IsNullOrEmpty(location))
                            location = ModernTradeController.GetLocationByID(attendanceRecord.LocationID,
                                           1, attendanceRecord.DealerCode)?.LocationName;

                        if (!string.IsNullOrEmpty(attendanceRecord.LocationID) &&
                            !locations.Any(e => e.Key == attendanceRecord.LocationID))
                            locations.Add(attendanceRecord.LocationID ?? "", location ?? "");

                        HRMS_Proxy.AddAttendanceEntry(new AddAttendanceRequest()
                        {
                            AttendanceDetails = new AttendanceDetails()
                            {
                                EntryType = "2",
                                Location = location,
                                UserCode = requester.FaxNumber,
                                EntryTime = date.ToString("yyyy-MM-dd HH':'mm':'ss")
                            }
                        });

                        context.UpdateNotificationStatus(item.Id, (int)Constants.NotificationStatus.Closed, (int)Constants.VerficationStatus.NotVerified);
                        context.UpdateNotificationVerfication(dealerCode, (int)AttendanceId.Value);
                        context.UpdateDealerActiveAttendanceForCheckOut(dealerCode, AttendanceId.Value.ToString());
                    }
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void ProcessNotificationsToBeClosedByUserCheckOut(AttendenceEntities context)
        {
            try
            {
                context.GetNotificationsToBeClosedByUserCheckOut().ToList().ForEach(item =>
                {
                    context.UpdateNotificationStatus(item.Id, (int)Helpers.Constants.NotificationStatus.Closed, (int)Helpers.Constants.VerficationStatus.UserCheckedOut);
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void ProcessNotificationsToBeIgnoredByUserCheckOut(AttendenceEntities context)
        {
            try
            {
                context.GetNotificationsToBeIgnoredByUserCheckOut().ToList().ForEach(item =>
                {
                    context.UpdateNotificationStatus(item.Id,
                        (int)Constants.NotificationStatus.Sent, item.VerificationStatus);
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void ProcessFirstReminderToBeIgnoredByUserCheckOut(AttendenceEntities context)
        {
            try
            {
                context.GetFirstReminderToBeIgnoredByUserCheckOut().ToList().ForEach(item =>
                {
                    context.UpdateNotificationStatus(item.Id,
                        (int)Constants.NotificationStatus.FirstReminderSent, item.VerificationStatus);
                });
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            try
            {
                var time = (ConfigurationSection.TimersElementCollection.TimerElement)context.JobDetail.JobDataMap["TimerName"];
                if (CurrantJobs.Count(z => z.Key.Key.Name.Contains(time.Name)) >= 1 || StopAllJobs)
                    return;
                CurrantJobs.GetOrAdd(context.JobDetail, context.JobDetail);
                new System.Threading.Tasks.Task(() => { this.Start(time); }).Start();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }
    }
}
