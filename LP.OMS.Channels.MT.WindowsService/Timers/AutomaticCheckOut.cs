﻿using LP.OMS.Channels.Contracts;
using LP.OMS.Channels.Engine;
using LP.OMS.Channels.Engine.Proxies;
using LP.OMS.Channels.MT.ReminderManager.Notification;
using LP.OMS.Channels.MT.WindowsService.AdministrationServices;
using LP.OMS.Channels.MT.WindowsService.Helpers;
using LP.OMS.Channels.MT.WindowsService.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OMSLanguages = LP.OMS.Channels.MT.WindowsService.AdministrationServices.OMSLanguages;
using Requester = LP.OMS.Channels.MT.WindowsService.AdministrationServices.Requester;

namespace LP.OMS.Channels.MT.WindowsService.Timers
{
    [DisallowConcurrentExecution]

    public class AutomaticCheckOut : BaseCronJob, IJob
    {
        public static bool IsRunning { get; set; }
        protected override void ExecutionCore()
        {
            LogHelper.WriteEntry($"Enter VerficationNotification->ExecutionCore ");
            IsRunning = true;
            DateTime startTimer = DateTime.Now;
            try
            {
                Run();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
            finally
            {
                LogTimersSchedule(this.TimerConfig.Name, startTimer, DateTime.Now);
                IJobDetail job;
                bool Removed = false;
                try
                {
                    Removed = CurrantJobs
                      .TryRemove(CurrantJobs
                      .FirstOrDefault(z => z.Key.Key.Name
                      .Contains(this.TimerConfig.Name.ToString())).Key, out job);
                }
                catch (Exception ex)
                {
                    CurrantJobs.Clear();
                }
                if (!Removed)
                    CurrantJobs.Clear();

                IsRunning = false;
            }
        }

        public void Run()
        {
            try
            {
                var attendanceWorkingHours = Convert.ToInt32(ConfigurationManager.AppSettings["LP.OMS.Channels.Engine.Attendance.WorkingHours"]);

                using (AttendenceEntities db = new AttendenceEntities())
                {
                    db.AutomaticCheckOut_V2(attendanceWorkingHours);
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        private static void AddErrorLog(string description, string stackTrace, DateTime datetime, int attendanceID)
        {
            try
            {
                using (AttendenceEntities context = new AttendenceEntities())
                {
                    context.AddErrorLog(description, stackTrace, datetime, attendanceID);
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }

        public async System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            try
            {
                var time = (ConfigurationSection.TimersElementCollection.TimerElement)context.JobDetail.JobDataMap["TimerName"];
                if (CurrantJobs.Count(z => z.Key.Key.Name.Contains(time.Name)) >= 1 || StopAllJobs)
                    return;
                CurrantJobs.GetOrAdd(context.JobDetail, context.JobDetail);
                new System.Threading.Tasks.Task(() => { this.Start(time); }).Start();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
        }
    }
}
