﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.Channels.MT.WindowsService.Timers
{
    [DisallowConcurrentExecution]

    public class AutoCheckout : BaseCronJob, IJob
    {
        public static bool IsRunning { get; set; }
        protected override void ExecutionCore()
        {
            LogHelper.WriteEntry($"Enter AutoCheckout->ExecutionCore ");
            IsRunning = true;
            DateTime startTimer = DateTime.Now;
            try
            {
                LogHelper.WriteEntry("calling StartReminderProcess");
                ReminderManagerHelper.StartReminderProcess();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
            finally
            {
                LogTimersSchedule(this.TimerConfig.Name, startTimer, DateTime.Now);
                IJobDetail job;
                bool Removed = false;
                try
                {
                    Removed = CurrantJobs
                      .TryRemove(CurrantJobs
                      .FirstOrDefault(z => z.Key.Key.Name
                      .Contains(this.TimerConfig.Name.ToString())).Key, out job);
                }
                catch (Exception ex)
                {
                    CurrantJobs.Clear();
                }
                if (!Removed)
                    CurrantJobs.Clear();

                IsRunning = false;
            }
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var time = (ConfigurationSection.TimersElementCollection.TimerElement)context.JobDetail.JobDataMap["TimerName"];
            if (CurrantJobs.Count(z => z.Key.Key.Name.Contains(time.Name)) >= 1 || StopAllJobs)
                return;
            CurrantJobs.GetOrAdd(context.JobDetail, context.JobDetail);
            new Task(() => { this.Start(time); }).Start();
        }
    }
}
