﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.Channels.MT.WindowsService.Timers
{
    public class AutoCheckout_MissedVerification_Report : BaseCronJob, IJob
    {
        public static bool IsRunning { get; set; }
        public async Task Execute(IJobExecutionContext context)
        {
            var time = (ConfigurationSection.TimersElementCollection.TimerElement)context.JobDetail.JobDataMap["TimerName"];
            if (CurrantJobs.Count(z => z.Key.Key.Name.Contains(time.Name)) >= 1 || StopAllJobs)
                return;
            CurrantJobs.GetOrAdd(context.JobDetail, context.JobDetail);
            new Task(() => { this.Start(time); }).Start();
        }

        protected override void ExecutionCore()
        {
            LogHelper.WriteEntry($"Enter AutoCheckout_MissedVerification_Report->ExecutionCore ");
            IsRunning = true;
            DateTime startTimer = DateTime.Now;
            try
            {
                LogHelper.WriteEntry("calling GenerateReport");
                ReminderManagerHelper.GenerateReport();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex, null);
            }
            finally
            {
                LogTimersSchedule(this.TimerConfig.Name, startTimer, DateTime.Now);
                IJobDetail job;
                bool Removed = false;
                try
                {
                    Removed = CurrantJobs
                      .TryRemove(CurrantJobs
                      .FirstOrDefault(z => z.Key.Key.Name
                      .Contains(this.TimerConfig.Name.ToString())).Key, out job);
                }
                catch (Exception ex)
                {
                    CurrantJobs.Clear();
                }
                if (!Removed)
                    CurrantJobs.Clear();

                IsRunning = false;
            }
        }
    }
}
