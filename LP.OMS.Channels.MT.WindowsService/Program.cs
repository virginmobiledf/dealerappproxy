﻿using LP.OMS.Channels.MT.WindowsService.Timers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace LP.OMS.Channels.MT.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //AutomaticCheckOut_V2 verficationNotification = new AutomaticCheckOut_V2();
            //verficationNotification.Run();
            //Service1 service1 = new Service1();
            //service1.Start();
            //while (true) { }

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
